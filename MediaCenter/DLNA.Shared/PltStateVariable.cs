﻿using System;
using System.Xml;
using System.Collections.Generic;

namespace Platinum
{
	public class PltStateVariable
	{
		public PltService Service{ get; set; }

		public String Name{ get; set; }

		public String DataType{ get; set; }

		public String DefaultValue{ get; set; }

		public Boolean IsSendingEvents{ get; set; }

		public List<String> AllowedValues{ get; set; }

		public AllowedValueRange AllowedValueRange{ get; set; }

		protected bool m_IsSendingEventsIndirectly;
		protected TimeSpan m_Rate;
		protected DateTime m_LastEvent;

		protected String m_Value;

		protected Dictionary<String,String> m_ExtraAttributes;

		public PltStateVariable (PltService service)
		{
			Service = service; 
			AllowedValueRange = null;
			IsSendingEvents = false;
			m_IsSendingEventsIndirectly = true;
			m_ExtraAttributes = new Dictionary<String,String> ();
			AllowedValues = new List<String> ();
		}

		public void Dispose ()
		{
			AllowedValues.Clear ();
			if (AllowedValueRange != null)
				AllowedValueRange = null;
		
		}


		public Boolean GetSCPDXML (XmlElement node)
		{
			XmlElement variable = node.OwnerDocument.CreateElement (node.Prefix,"stateVariable",node.NamespaceURI);
			node.AppendChild (variable);
			variable.SetAttribute ("sendEvents", IsSendingEvents ? "yes" : "no");

			Plt.CheckSevere (PltUtilities.AddChildText (variable, "name", Name));

			Plt.CheckSevere (PltUtilities.AddChildText (variable, "dataType", DataType));
			if (DefaultValue.Length > 0) {
				Plt.CheckSevere (PltUtilities.AddChildText (variable, "defaultValue", DefaultValue));
			}

			if (AllowedValues.Count > 0) {
				XmlElement allowedValueList = node.OwnerDocument.CreateElement (node.Prefix,"allowedValueList",node.NamespaceURI);
				variable.AppendChild (allowedValueList);
				for (int l = 0; l < AllowedValues.Count; l++) {
					Plt.CheckSevere (PltUtilities.AddChildText (allowedValueList, "allowedValue", AllowedValues [l]));
				}
			} else if (AllowedValueRange != null) {
				XmlElement range = node.OwnerDocument.CreateElement (node.Prefix,"allowedValueRange",node.NamespaceURI);
				variable.AppendChild (range);
				Plt.CheckSevere (PltUtilities.AddChildText (range, "minimum", AllowedValueRange.MinValue.ToString ()));
				Plt.CheckSevere (PltUtilities.AddChildText (range, "maximum", AllowedValueRange.MaxValue.ToString ()));
				if (AllowedValueRange.Step != -1) {
					Plt.CheckSevere (PltUtilities.AddChildText (range, "step", AllowedValueRange.Step.ToString ()));
				}
			}

			return true;
		}

	
		public bool IsSendingEventsIndirecly {
			get {
				return (!IsSendingEvents &&	!Name.StartsWith ("A_ARG_TYPE_") &&	m_IsSendingEventsIndirectly);
			}				
		}

		public void DisableIndirectEventing ()
		{
			m_IsSendingEventsIndirectly = false;
		}

		public Boolean SetRate (TimeSpan rate)
		{
			if (!IsSendingEvents)
				return false;

			m_Rate = rate;
			return true;
		}

		public Boolean SetValue (String value)
		{			 
			if (value == null) {
				return false;
			}

			// update only if it's different
			if (m_Value != value) {
				Boolean res = ValidateValue (value);
				
				if (Plt.IsFailed (res)) {
					return res;
				}

				m_Value = value;
				Service.AddChanged (this); 
			}

			return true;
		}

		public Boolean ValidateValue (String value)
		{
			if (DataType.Equals ("string")) {
				// if we have a value allowed restriction, make sure the value is in our list
				if (AllowedValues.Count > 0) {
					// look for a comma separated list
					String _value = value;
					String[] values = _value.Split (',');
					for (int i = 0; i < values.Length; i++) {
						String val = values [i];				
						val.Trim ();
						if (!AllowedValues.Contains (val)) {
							Plt.LogWarn (String.Format ("Invalid value of %s for state variable %s", val, Name));
							throw new Exception ("NPT_ERROR_INVALID_PARAMETERS");
						}
					

					}
				}
			}

			// TODO: there are more to it than allowed values, we need to test for range, etc..
			return true; 
		}


		public Boolean SetExtraAttribute (String name, String value)
		{
			m_ExtraAttributes.Add (name, value);
			return true;
		}

		public String GetValue ()
		{ 
			return m_Value;    
		}

		 
		 

		public static PltStateVariable Find (List<PltStateVariable> vars, String name)
		{
			PltStateVariable stateVariable = null;
			Plt.ContainerFind (vars, new PltStateVariableNameFinder (name), out stateVariable);
			return stateVariable;
		}


		public Boolean IsReadyToPublish ()
		{
			DateTime now = DateTime.Now;
					
			if (m_Rate.TotalMilliseconds == 0 || m_LastEvent.AddSeconds (m_Rate.TotalSeconds).CompareTo (now) < 0) {
				m_LastEvent = now;
				return true;
			}

			return false;
		}

		public Boolean Serialize (XmlElement node)
		{
			foreach (KeyValuePair<String,String> dic in m_ExtraAttributes) {			
				node.SetAttribute (dic.Key, dic.Value);
			}
			 			 
			node.SetAttribute ("val", GetValue ());
			return true;
		
		}
	}

	public class AllowedValueRange
	{
		public int MinValue{ get; set; }

		public int MaxValue{ get; set; }

		public int Step{ get; set; }
		

	}

	public class PltStateVariableNameFinder :IFinder<PltStateVariable>
	{
		public String Name{ get; set; }

		public PltStateVariableNameFinder (String name)
		{
			Name = name;
		}

		public Boolean IsFound (PltStateVariable state_variable)
		{
			return state_variable.Name.Equals (Name);
		}
      
	}
 
}
   
 
   
   