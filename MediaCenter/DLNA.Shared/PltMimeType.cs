﻿using System;

namespace Platinum
{
	public class FileTypeMap
	{
		public FileTypeMap (String extension, String mimeType)
		{
			Extension = extension;
			MimeType = mimeType;
		}

		public String Extension{ get; set; }

		public String MimeType{ get; set; }
	}

	public class PltMimeType
	{

		public static FileTypeMap[] PltHttpFileRequestHandler_DefaultFileTypeMap = new FileTypeMap[] {
			new FileTypeMap ("l16", "audio/L16;rate=44100;channels=2"),
			new FileTypeMap ("l16m", "audio/L16;rate=44100;channels=1"),

			new FileTypeMap ("wav", "audio/wav"),
			new FileTypeMap ("wavm", "audio/wav"),
			new FileTypeMap ("alac", "audio/x-alac"),
			//{"wavm",  "audio/x-wav"},
			new FileTypeMap ("l16m32", "audio/L16;rate=32000;channels=1"),
			 
			new FileTypeMap ("xml", "text/xml; charset=\"utf-8\""),
			new FileTypeMap ("htm", "text/html"),
			new FileTypeMap ("html", "text/html"),
			new FileTypeMap ("c", "text/plain"),
			new FileTypeMap ("h", "text/plain"),
			new FileTypeMap ("txt", "text/plain"),
			new FileTypeMap ("css", "text/css"),
			new FileTypeMap ("manifest", "text/cache-manifest"),
			new FileTypeMap ("gif", "image/gif"),
			new FileTypeMap ("thm", "image/jpeg"),
			new FileTypeMap ("png", "image/png"),
			new FileTypeMap ("tif", "image/tiff"),
			new FileTypeMap ("tiff", "image/tiff"),
			new FileTypeMap ("jpg", "image/jpeg"),
			new FileTypeMap ("jpeg", "image/jpeg"),
			new FileTypeMap ("jpe", "image/jpeg"),
			new FileTypeMap ("jp2", "image/jp2"),
			new FileTypeMap ("png", "image/png"),
			new FileTypeMap ("bmp", "image/bmp"),
			new FileTypeMap ("aif", "audio/x-aiff"),
			new FileTypeMap ("aifc", "audio/x-aiff"),
			new FileTypeMap ("aiff", "audio/x-aiff"),
			new FileTypeMap ("mpa", "audio/mpeg"),
			new FileTypeMap ("mp2", "audio/mpeg"),
			new FileTypeMap ("mp3", "audio/mpeg"),
			new FileTypeMap ("m4a", "audio/mp4"),
			new FileTypeMap ("wma", "audio/x-ms-wma"),
			new FileTypeMap ("wav", "audio/x-wav"),
			new FileTypeMap ("mpeg", "video/mpeg"),
			new FileTypeMap ("mpg", "video/mpeg"),
			new FileTypeMap ("mp4", "video/mp4"),
			new FileTypeMap ("m4v", "video/mp4"),
			new FileTypeMap ("ts", "video/MP2T"), // RFC 3555
			new FileTypeMap ("mpegts", "video/MP2T"),
			new FileTypeMap ("mov", "video/quicktime"),
			new FileTypeMap ("qt", "video/quicktime"),
			new FileTypeMap ("wmv", "video/x-ms-wmv"),
			new FileTypeMap ("wtv", "video/x-ms-wmv"),
			new FileTypeMap ("asf", "video/x-ms-asf"),
			new FileTypeMap ("mkv", "video/x-matroska"),
			new FileTypeMap ("flv", "video/x-flv"),
			new FileTypeMap ("avi", "video/x-msvideo"),
			new FileTypeMap ("divx", "video/x-msvideo"),
			new FileTypeMap ("xvid", "video/x-msvideo"),
			new FileTypeMap ("doc", "application/msword"),
			new FileTypeMap ("js", "application/javascript"),
			new FileTypeMap ("m3u8", "application/x-mpegURL"),
			new FileTypeMap ("pdf", "application/pdf"),
			new FileTypeMap ("ps", "application/postscript"),
			new FileTypeMap ("eps", "application/postscript"),
			new FileTypeMap ("zip", "application/zip")
		
		};


		static FileTypeMap[] PltHttpFileRequestHandler_360FileTypeMap = new FileTypeMap[] {
			new FileTypeMap ("l16", "audio/L16"),
			new FileTypeMap ("l16m", "audio/L16"),
			new FileTypeMap ("l16m32", "audio/L16"),
			new FileTypeMap ("avi", "video/avi"),
			new FileTypeMap ("divx", "video/avi"),
			new FileTypeMap ("xvid", "video/avi"),
			new FileTypeMap ("mov", "video/quicktime")
		};

		static FileTypeMap[] PltHttpFileRequestHandler_PS3FileTypeMap = new FileTypeMap[] {
			new FileTypeMap ("avi", "video/x-msvideo"),
			new FileTypeMap ("divx", "video/x-msvideo"),
			new FileTypeMap ("xvid", "video/x-msvideo"),
			new FileTypeMap ("mov", "video/mp4")
		};
			
		static FileTypeMap[] PltHttpFileRequestHandler_SonosFileTypeMap = new FileTypeMap[] {
			new FileTypeMap ("wav", "audio/wav")
		};





		private PltMimeType ()
		{
		}

	 


		public static String GetMimeType (String filename, PltHttpRequestContext context = null)
		{
			return GetMimeType (filename, context!=null ? PltHttpHelper.GetDeviceSignature (context.GetRequest ()) : PltDeviceSignature.PLT_DEVICE_UNKNOWN);
		}

		public static String GetMimeType (String filename, PltDeviceSignature signature = PltDeviceSignature.PLT_DEVICE_UNKNOWN)
		{
			int last_dot = filename.LastIndexOf ('.');
			if (last_dot >= 0) { // passing just the extension is ok (ex .mp3)
				String extension = filename.Substring (last_dot + 1);
				return GetMimeTypeFromExtension (extension, signature);
			}

			return "application/octet-stream";
		}

		public static String GetMimeTypeFromExtension (String extension, PltHttpRequestContext context = null)
		{
			return GetMimeTypeFromExtension (extension, context!=null ? PltHttpHelper.GetDeviceSignature (context.GetRequest ()) : PltDeviceSignature.PLT_DEVICE_UNKNOWN);
		}

		public static String GetMimeTypeFromExtension (String extension, PltDeviceSignature signature = PltDeviceSignature.PLT_DEVICE_UNKNOWN)
		{
			if (signature != PltDeviceSignature.PLT_DEVICE_UNKNOWN) {
				// look for special case for 360
				if (signature == PltDeviceSignature.PLT_DEVICE_XBOX /*|| signature == PLT_DEVICE_WMP*/) {
					for (int i = 0; i < PltHttpFileRequestHandler_360FileTypeMap.Length; i++) {
						if (extension.Equals (PltHttpFileRequestHandler_360FileTypeMap [i].Extension)) {
							return PltHttpFileRequestHandler_360FileTypeMap [i].MimeType;
						}
					}

					// fallback to default if not found
				} else if (signature == PltDeviceSignature.PLT_DEVICE_PS3) {
					for (int i = 0; i < PltHttpFileRequestHandler_PS3FileTypeMap.Length; i++) {
						if (extension.Equals (PltHttpFileRequestHandler_PS3FileTypeMap [i].Extension)) {
							return PltHttpFileRequestHandler_PS3FileTypeMap [i].MimeType;
						}
					}

					// fallback to default if not found
				} else if (signature == PltDeviceSignature.PLT_DEVICE_SONOS) {
					for (int i = 0; i < PltHttpFileRequestHandler_SonosFileTypeMap.Length; i++) {
						if (extension.Equals (PltHttpFileRequestHandler_SonosFileTypeMap [i].Extension)) {
							return PltHttpFileRequestHandler_SonosFileTypeMap [i].MimeType;
						}
					}

					// fallback to default if not found
				}
			}

			// dlna custom ones
			for (int i = 0; i < PltHttpFileRequestHandler_DefaultFileTypeMap.Length; i++) {
				if (extension.Equals (PltHttpFileRequestHandler_DefaultFileTypeMap [i].Extension)) {
					return PltHttpFileRequestHandler_DefaultFileTypeMap [i].MimeType;
				}
			}
				
			return  "application/octet-stream";
		}

	}
	 
 
	   
	 

}

