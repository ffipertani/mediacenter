﻿using System;
using System.Xml;
using System.Collections.Generic;

namespace Platinum
{
	public class PltDidl
	{


		/*----------------------------------------------------------------------
|   constants
+---------------------------------------------------------------------*/
		public static uint PLT_FILTER_MASK_ALL = 0xFFFFFFFF;

		public static uint PLT_FILTER_MASK_CREATOR = 0x00000001;
		public static uint PLT_FILTER_MASK_ARTIST = 0x00000002;
		public static uint PLT_FILTER_MASK_ALBUM = 0x00000004;
		public static uint PLT_FILTER_MASK_GENRE = 0x00000008;
		public static uint PLT_FILTER_MASK_ALBUMARTURI = 0x00000010;
		public static uint PLT_FILTER_MASK_DESCRIPTION = 0x00000020;
		public static uint PLT_FILTER_MASK_SEARCHABLE = 0x00000040;
		public static uint PLT_FILTER_MASK_CHILDCOUNT = 0x00000080;
		public static uint PLT_FILTER_MASK_ORIGINALTRACK = 0x00000100;
		public static uint PLT_FILTER_MASK_ACTOR = 0x00000200;
		public static uint PLT_FILTER_MASK_AUTHOR = 0x00000400;
		public static uint PLT_FILTER_MASK_DATE = 0x00000800;
		public static uint PLT_FILTER_MASK_PROGRAMTITLE = 0x00001000;
		public static uint PLT_FILTER_MASK_SERIESTITLE = 0x00002000;
		public static uint PLT_FILTER_MASK_EPISODE = 0x00004000;
		public static uint PLT_FILTER_MASK_TITLE = 0x00008000;

		public static uint PLT_FILTER_MASK_RES = 0x00010000;
		public static uint PLT_FILTER_MASK_RES_DURATION = 0x00020000;
		public static uint PLT_FILTER_MASK_RES_SIZE = 0x00040000;
		public static uint PLT_FILTER_MASK_RES_PROTECTION = 0x00080000;
		public static uint PLT_FILTER_MASK_RES_RESOLUTION = 0x00100000;
		public static uint PLT_FILTER_MASK_RES_BITRATE = 0x00200000;
		public static uint PLT_FILTER_MASK_RES_BITSPERSAMPLE = 0x00400000;
		public static uint PLT_FILTER_MASK_RES_NRAUDIOCHANNELS	= 0x00800000;
		public static uint PLT_FILTER_MASK_RES_SAMPLEFREQUENCY	= 0x01000000;

		public static uint PLT_FILTER_MASK_LONGDESCRIPTION = 0x02000000;
		public static uint PLT_FILTER_MASK_ICON = 0x04000000;

		public static uint PLT_FILTER_MASK_TOC = 0x02000000;
		public static uint PLT_FILTER_MASK_SEARCHCLASS = 0x04000000;
		public static uint PLT_FILTER_MASK_REFID = 0x08000000;

		public static String PLT_FILTER_FIELD_TITLE = "dc:title";
		public static String PLT_FILTER_FIELD_CREATOR = "dc:creator";
		public static String PLT_FILTER_FIELD_DATE = "dc:date";
		public static String PLT_FILTER_FIELD_ARTIST = "upnp:artist";
		public static String PLT_FILTER_FIELD_ACTOR = "upnp:actor";
		public static String PLT_FILTER_FIELD_AUTHOR = "upnp:author";
		public static String PLT_FILTER_FIELD_ALBUM = "upnp:album";
		public static String PLT_FILTER_FIELD_GENRE = "upnp:genre";
		public static String PLT_FILTER_FIELD_ALBUMARTURI = "upnp:albumArtURI";
		public static String PLT_FILTER_FIELD_ALBUMARTURI_DLNAPROFILEID = "upnp:albumArtURI@dlna:profileID";
		public static String PLT_FILTER_FIELD_DESCRIPTION = "dc:description";
		public static String PLT_FILTER_FIELD_LONGDESCRIPTION = "upnp:longDescription";
		public static String PLT_FILTER_FIELD_ICON = "upnp:icon";
		public static String PLT_FILTER_FIELD_ORIGINALTRACK = "upnp:originalTrackNumber";
		public static String PLT_FILTER_FIELD_PROGRAMTITLE = "upnp:programTitle";
		public static String PLT_FILTER_FIELD_SERIESTITLE = "upnp:seriesTitle";
		public static String PLT_FILTER_FIELD_EPISODE = "upnp:episodeNumber";
		public static String PLT_FILTER_FIELD_SEARCHCLASS =	"upnp:searchClass";
		public static String PLT_FILTER_FIELD_SEARCHABLE = "@searchable";
		public static String PLT_FILTER_FIELD_CHILDCOUNT = "@childcount";
		public static String PLT_FILTER_FIELD_CONTAINER_CHILDCOUNT = "container@childCount";
		public static String PLT_FILTER_FIELD_CONTAINER_SEARCHABLE = "container@searchable";
		public static String PLT_FILTER_FIELD_REFID = "@refID";

		public static String PLT_FILTER_FIELD_RES = "res";
		public static String PLT_FILTER_FIELD_RES_DURATION = "res@duration";
		public static String PLT_FILTER_FIELD_RES_DURATION_SHORT = "@duration";
		public static String PLT_FILTER_FIELD_RES_SIZE = "res@size";
		public static String PLT_FILTER_FIELD_RES_PROTECTION = "res@protection";
		public static String PLT_FILTER_FIELD_RES_RESOLUTION = "res@resolution";
		public static String PLT_FILTER_FIELD_RES_BITRATE = "res@bitrate";
		public static String PLT_FILTER_FIELD_RES_BITSPERSAMPLE = "res@bitsPerSample";
		public static String PLT_FILTER_FIELD_RES_NRAUDIOCHANNELS = "res@nrAudioChannels";
		public static String PLT_FILTER_FIELD_RES_SAMPLEFREQUENCY = "res@sampleFrequency";


		public static String didl_header = "<DIDL-Lite xmlns=\"urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:upnp=\"urn:schemas-upnp-org:metadata-1-0/upnp/\" xmlns:dlna=\"urn:schemas-dlna-org:metadata-1-0/\">";
		public static String didl_footer = "</DIDL-Lite>";
		public static String didl_namespace_dc = "http://purl.org/dc/elements/1.1/";
		public static String didl_namespace_upnp = "urn:schemas-upnp-org:metadata-1-0/upnp/";
		public static String didl_namespace_dlna = "urn:schemas-dlna-org:metadata-1-0/";



		public static Boolean IsMask (uint mask, uint value)
		{
			return (mask & value) != 0;
		}

		public static Boolean  ToDidl (PltMediaObject  aobject, String filter, ref String  didl)
		{
			uint mask = ConvertFilterToMask (filter);

			 
			return aobject.ToDidl (mask, ref didl);
		}

		public static Boolean  FromDidl (String xml, List<PltMediaObject> objects)
		{
			String str;
			PltMediaObject aobject = null;
			XmlElement node = null;
			XmlElement didl = null;
			 
			XmlDocument doc = new XmlDocument ();
			doc.LoadXml (xml);
			Plt.LogFine ("Parsing Didl...");
			node = doc.DocumentElement;

			if (node == null) {
				Plt.LogFatal ("Invalid node type");
				return false;
			}

			didl = node;

			if (didl.Name.EqualsIgnoreCase ("DIDL-Lite")) {
				Plt.LogFatal ("Invalid node tag");
				return false;
			}

			// create entry list
			objects = new List<PltMediaObject> ();

			// for each child, find out if it's a container or not
			// and then invoke the FromDidl on it
			foreach (XmlElement child in didl.ChildNodes) {
				if (child.Name.EqualsIgnoreCase ("Container")) {
					aobject = new PltMediaContainer ();
				} else if (child.Name.EqualsIgnoreCase ("item")) {
					aobject = new PltMediaItem ();
				} else {
					Plt.LogWarn ("Invalid node tag");
					continue;
				}

				if (Plt.IsFailed (aobject.FromDidl (child))) {
					Plt.LogWarn (String.Format ("Invalid didl for object: %s", PltXmlHelper.Serialize (child, false)));
					continue;
				}

				objects.Add (aobject);
				aobject = null; // reset to make sure it doesn't get deleted twice in case of error
			}

			 

			 
			return true;			 
		}

		public static void        AppendXmlEscape (ref String parout, String parin)
		{
			if (parin == null)
				return;

			parin = parin.Replace ("<", "&lt;");
			parin = parin.Replace (">", "&gt;");
			parin = parin.Replace ("&", "&amp;");
			parin = parin.Replace ("\"", "&quot;");
			parin = parin.Replace ("'", "&apos;");	

			parout += parin;
			 
		}

		public static void        AppendXmlUnEscape (ref String parout, String parin)
		{
			parin = parin.Replace ("&lt;", "<");
			parin = parin.Replace ("&gt;", ">");
			parin = parin.Replace ("&amp;", "&");
			parin = parin.Replace ("&quot;", "\"");
			parin = parin.Replace ("&apos;", "'");		

			parout += parin;
		}

		public static Boolean  ParseTimeStamp (String timestamp, out int seconds)
		{
			// assume a timestamp in the format HH:MM:SS.FFF
			int separator;
			String str = timestamp;
			int value;

			// reset output params first
			seconds = 0;

			// remove milliseconds first if any
			if ((separator = str.LastIndexOf ('.')) != -1) {
				str = str.Substring (0, separator);
			}

			// look for next separator
			if ((separator = str.LastIndexOf (':')) == -1)
				return false;

			// extract seconds
			Plt.CheckWarining (Int32.TryParse (str.Substring (separator + 1), out value));
			seconds = value;
			str = str.Substring (0, separator);

			// look for next separator
			if ((separator = str.LastIndexOf (':')) == -1)
				return false;

			// extract minutes
			Plt.CheckWarining (Int32.TryParse (str.Substring (separator + 1), out value));
			seconds += 60 * value;
			str = str.Substring (0, separator);

			// extract hours
			Plt.CheckWarining (Int32.TryParse (str, out value));
			seconds += 3600 * value;

			return true;
		}

		public static String  FormatTimeStamp (int seconds)
		{
			String result = "";
			int hours = seconds / 3600;
			if (hours == 0) {
				result += "0:";
			} else {
				result += hours + ":";
			}

			int minutes = (seconds / 60) % 60;
			if (minutes == 0) {
				result += "00:";
			} else {
				if (minutes < 10) {
					result += '0';
				}
				result += minutes + ":";
			}

			int secs = seconds % 60;
			if (secs == 0) {
				result += "00";
			} else {
				if (secs < 10) {
					result += '0';
				}
				result += secs;
			}

			result += ".000"; // needed for XBOX360 otherwise it won't play the track
			return result;
		}

		public static Boolean  ParseTimeStamp (String parin, out TimeSpan timespan)
		{
			int seconds;
			Boolean res = ParseTimeStamp (parin, out seconds);
			timespan = TimeSpan.FromSeconds (seconds);
			return res;
		}

		public static uint  ConvertFilterToMask (String filter)
		{
			// easy out
			if (filter.Length == 0)
				return PLT_FILTER_MASK_ALL;

			// a filter string is a comma delimited set of fields identifying
			// a given DIDL property (or set of properties).  
			// These fields are or start with: upnp:, @, res@, res, dc:, container@
			int comma = 0;
			uint mask = 0;
			String s = filter;

			while (comma >= 0) {
				comma = s.IndexOf (",");
				String currentString = "";
				if (comma < 0) {
					currentString = s;
				} else {
					currentString = s.Substring (0, comma);
					s = s.Substring (comma + 1);
				}
				currentString = currentString.Trim ();

			 

				if (currentString.Equals ("*")) {
					// return now, there's no point in parsing the rest
					return PLT_FILTER_MASK_ALL;
				}

				// title is required, so we return a non empty mask
				mask |= PLT_FILTER_MASK_TITLE;

				if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_TITLE)) {
					mask |= PLT_FILTER_MASK_TITLE;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_REFID)) {
					mask |= PLT_FILTER_MASK_REFID;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_CREATOR)) {
					mask |= PLT_FILTER_MASK_CREATOR;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_ARTIST)) {
					mask |= PLT_FILTER_MASK_ARTIST;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_ACTOR)) {
					mask |= PLT_FILTER_MASK_ACTOR;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_AUTHOR)) {
					mask |= PLT_FILTER_MASK_AUTHOR;       
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_DATE)) {
					mask |= PLT_FILTER_MASK_DATE;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_ALBUM)) {
					mask |= PLT_FILTER_MASK_ALBUM;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_GENRE)) {
					mask |= PLT_FILTER_MASK_GENRE;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_ALBUMARTURI) ||
				           currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_ALBUMARTURI_DLNAPROFILEID)) {
					mask |= PLT_FILTER_MASK_ALBUMARTURI;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_DESCRIPTION)) {
					mask |= PLT_FILTER_MASK_DESCRIPTION;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_ORIGINALTRACK)) {
					mask |= PLT_FILTER_MASK_ORIGINALTRACK;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_SEARCHABLE)) {
					mask |= PLT_FILTER_MASK_SEARCHABLE;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_SEARCHCLASS)) {
					mask |= PLT_FILTER_MASK_SEARCHCLASS;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_CONTAINER_SEARCHABLE)) {
					mask |= PLT_FILTER_MASK_SEARCHABLE;       
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_CHILDCOUNT)) {
					mask |= PLT_FILTER_MASK_CHILDCOUNT;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_CONTAINER_CHILDCOUNT)) {
					mask |= PLT_FILTER_MASK_CHILDCOUNT;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_PROGRAMTITLE)) {
					mask |= PLT_FILTER_MASK_PROGRAMTITLE;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_SERIESTITLE)) {
					mask |= PLT_FILTER_MASK_SERIESTITLE;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_EPISODE)) {
					mask |= PLT_FILTER_MASK_EPISODE;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_RES)) {
					mask |= PLT_FILTER_MASK_RES;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_RES_DURATION) ||
				           currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_RES_DURATION_SHORT)) {
					mask |= PLT_FILTER_MASK_RES | PLT_FILTER_MASK_RES_DURATION;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_RES_SIZE)) {
					mask |= PLT_FILTER_MASK_RES | PLT_FILTER_MASK_RES_SIZE;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_RES_PROTECTION)) {
					mask |= PLT_FILTER_MASK_RES | PLT_FILTER_MASK_RES_PROTECTION;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_RES_RESOLUTION)) {
					mask |= PLT_FILTER_MASK_RES | PLT_FILTER_MASK_RES_RESOLUTION;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_RES_BITRATE)) {
					mask |= PLT_FILTER_MASK_RES | PLT_FILTER_MASK_RES_BITRATE;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_RES_BITSPERSAMPLE)) {
					mask |= PLT_FILTER_MASK_RES | PLT_FILTER_MASK_RES_BITSPERSAMPLE;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_RES_NRAUDIOCHANNELS)) {
					mask |= PLT_FILTER_MASK_RES | PLT_FILTER_MASK_RES_NRAUDIOCHANNELS;
				} else if (currentString.EqualsIgnoreCase (PLT_FILTER_FIELD_RES_SAMPLEFREQUENCY)) {
					mask |= PLT_FILTER_MASK_RES | PLT_FILTER_MASK_RES_SAMPLEFREQUENCY;
				}

				if (comma < 0) {
					return mask;
				}

				 
			}

			return mask;
		}


			 
	}
} 