﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Net;

namespace Platinum
{
	public class PltTaskManager
	{
		List<PltThreadTask> m_Tasks;
		Object m_TasksLock = new Object ();
		Object m_CallbackLock = new Object ();
		Queue<int> m_Queue;
		int m_MaxTasks;
		int m_RunningTasks;
		bool m_Stopping;

		public PltTaskManager (int max_items = 0)
		{
			m_Tasks = new List<PltThreadTask> ();

			m_Queue = null;
			m_MaxTasks = max_items;
			m_RunningTasks = 0;
			m_Stopping = false;
		}

		public void Dispose ()
		{
			Abort ();
		}
			
		public Boolean Abort ()
		{
			int num_running_tasks;

			do {
				lock (m_TasksLock) {					 
					m_Stopping = true;

					// unblock the queue if any by deleting it
					if (m_Queue != null) {
						m_Queue.Clear ();
						m_Queue = null;
					}
				}

				// abort all running tasks
				lock (m_TasksLock) {			
					foreach (PltThreadTask task in m_Tasks) {
						if (!task.IsAborting (0)) {
							task.Stop (false);
						}
					}					 
					num_running_tasks = m_Tasks.Count;
				}

				if (num_running_tasks == 0)
					break; 

				Thread.Sleep (TimeSpan.FromMilliseconds (50));
			} while (true);

			return true;
		}

		public Boolean Reset ()
		{
			lock (m_TasksLock) {
				m_Stopping = false;
			}
			return true;
		}

		public int GetMaxTasks ()
		{
			return m_MaxTasks; 
		}

		// called by PLT_ThreadTask
		public Boolean AddTask (PltThreadTask task)
		{

			Boolean result = true;
			int val = 0;

			// verify we're not stopping or maxed out number of running tasks
			 
			lock (m_TasksLock) {

				// returning an error if we're stopping
				if (m_Stopping) {						 
					if (task.AutoDestroy)
						task.Dispose ();
					throw new Exception ("NPT_CHECK_WARNING(NPT_ERROR_INTERRUPTED");
				}

				if (m_MaxTasks!=0) {
					val = val!=0 ? val : default(int);

					if (m_Queue==null) {
						m_Queue = new Queue<int> (m_MaxTasks);
					}


					// try to add to queue but don't block forever if queue is full
					try {
						m_Queue.Enqueue (val);
							 
					} catch {
						Console.WriteLine ("Error enqueue");
						result = false;
					}
				}
				// start task now
				if (Plt.IsFailed (result = task.StartThread ())) {					 
					// Remove task from queue and delete task if autodestroy is set
					RemoveTask (task);

					return result;
				}

				Console.WriteLine ("[TaskManager {0} {1}/{2} running tasks", this, ++m_RunningTasks, m_MaxTasks);

				// keep track of running task
				m_Tasks.Add (task);
			}
			 

			return result;

		}

		public Boolean RemoveTask (PltThreadTask task)
		{
			Boolean result = true;

			lock (m_TasksLock) {
			
				if (m_Queue!=null) {
					try {
						m_Queue.Dequeue ();
					} catch {
						Console.Error.WriteLine ("Error dequeuing");
					}
					 
				}

				Console.WriteLine ("[TaskManager {0} {1}/{2} running tasks", this, --m_RunningTasks, m_MaxTasks);
				m_Tasks.Remove (task);
			}

			// cleanup task only if auto-destroy flag was set
			// otherwise it's the owner's responsability to
			// clean it up
			if (task.AutoDestroy)
				task.Dispose ();

			return true;
		}

		public virtual Boolean StartTask (PltThreadTask task)
		{
			return StartTask (task,new TimeSpan(0));			 
		}

		public virtual Boolean StartTask (PltThreadTask task, TimeSpan delay, bool auto_destroy = true)
		{
			Plt.CheckPointerSevere (task);
			return task.Start (this, delay, auto_destroy);
		}
	}
}

