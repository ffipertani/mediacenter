﻿using System;
using System.Collections.Generic;

namespace Platinum
{
	public class PltProtocolInfo
	{


		public static String PLT_FIELD_ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		public static String PLT_FIELD_NUM =  "0123456789";

		public static long PLT_SP_FLAG_MASK = 0x80000000;
		public static long PLT_LOP_NPT_MASK = 0x40000000;
		public static long PLT_LOP_BYTES_MASK =            0x20000000;
		public static long PLT_PLAYCONTAINER_PARAM_MASK  =  0x10000000;
		public static long PLT_S0_INCREASING_MASK =         0x08000000;
		public static long PLT_SN_INCREASING_MASK=          0x04000000;
		public static long PLT_RTSP_PAUSE_MASK=             0x02000000;
		public static long PLT_TM_S_MASK =                  0x01000000;
		public static long PLT_TM_I_MASK   =                0x00800000;
		public static long PLT_TM_B_MASK  =                 0x00400000;
		public static long PLT_HTTP_STALLING_MASK =         0x00200000;
		public static long PLT_DLNA_V1_5_FLAG_MASK =        0x00100000;
		public static long PLT_LP_FLAG_MASK    =            0x00010000;
		public static long PLT_CLEARTEXTBYTESEEK_FULL_MASK =0x00008000;
		public static long PLT_LOP_CLEARTEXTBYTES_MASK=     0x00004000;

		static char[] PLT_DLNAPNCharsToValidate        =  (PLT_FIELD_ALPHA+PLT_FIELD_NUM+"_").ToCharArray();
		static char[] PLT_DLNAPSCharsToValidate         = (PLT_FIELD_NUM+"-,/").ToCharArray();
		static char[] PLT_DLNAFlagCharsToValidate       = "01".ToCharArray();
		static char[] PLT_DLNAHexCharsToValidate        = (PLT_FIELD_NUM+"ABCDEFabcdef").ToCharArray();
		static char[] PLT_DLNAOTherNameCharsToValidate  = (PLT_FIELD_ALPHA+ PLT_FIELD_NUM).ToCharArray();
		static char[] PLT_DLNAOTherValueCharsToValidate = (PLT_FIELD_ALPHA+PLT_FIELD_NUM+"_-+,").ToCharArray();


		static FileTypeMap[] PLT_HttpFileRequestHandler_DefaultDlnaMap = new FileTypeMap[]{
			new FileTypeMap("image/gif",      "DLNA.ORG_PN=GIF_LRG"),
			new FileTypeMap("image/jpeg",     "DLNA.ORG_PN=JPEG_LRG"),
			new FileTypeMap("image/jp2",      "DLNA.ORG_PN=JPEG_LRG"),
			new FileTypeMap("image/png",      "DLNA.ORG_PN=PNG_LRG"),
			new FileTypeMap("image/bmp",      "DLNA.ORG_PN=BMP_LRG"),
			new FileTypeMap("image/tiff",     "DLNA.ORG_PN=TIFF_LRG"),
			new FileTypeMap("audio/L16;rate=44100;channels=2", "DLNA.ORG_PN=LPCM;DLNA.ORG_OP=01;DLNA.ORG_CI=1;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("audio/L16;rate=44100;channels=1", "DLNA.ORG_PN=LPCM;DLNA.ORG_OP=01;DLNA.ORG_CI=1;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("audio/L16;rate=32000;channels=1", "DLNA.ORG_PN=LPCM;DLNA.ORG_OP=01;DLNA.ORG_CI=1;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("audio/mpeg",     "DLNA.ORG_PN=MP3;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("audio/mp4",      "DLNA.ORG_PN=AAC_ISO;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("audio/x-ms-wma", "DLNA.ORG_PN=WMABASE;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("audio/wav",      "DLNA.ORG_OP=01;DLNA.ORG_CI=1;DLNA.ORG_FLAGS=01500000000000000000000000000000"), // UVerse
			new FileTypeMap("audio/x-wav",    "DLNA.ORG_OP=01;DLNA.ORG_CI=1;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("video/avi",      "DLNA.ORG_PN=AVI;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("video/mp4",      "DLNA.ORG_PN=MPEG4_P2_SP_AAC;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("video/mpeg",     "DLNA.ORG_PN=MPEG_PS_PAL;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("video/quicktime","DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("video/x-ms-wmv", "DLNA.ORG_PN=WMVHIGH_BASE;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("video/x-msvideo","DLNA.ORG_PN=AVI;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("video/x-ms-asf", "DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("video/x-matroska","DLNA.ORG_OP=01;DLNA.ORG_CI=0"),
			new FileTypeMap("video/x-flv",    "DLNA.ORG_OP=01;DLNA.ORG_CI=0"),
		};

		static FileTypeMap[] PLT_HttpFileRequestHandler_360DlnaMap = new FileTypeMap[]{
			new FileTypeMap("video/quicktime","DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("video/mp4",      "DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("audio/wav",      "DLNA.ORG_OP=01;DLNA.ORG_CI=1;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("audio/mp4",      "DLNA.ORG_PN=AAC_ISO;DLNA.ORG_OP=01;DLNA.ORG_FLAGS=01500000000000000000000000000000"),
			new FileTypeMap("audio/mpeg",     "DLNA.ORG_PN=MP3;DLNA.ORG_OP=01"),
			new FileTypeMap("audio/L16",      "DLNA.ORG_PN=LPCM;DLNA.ORG_OP=01;DLNA.ORG_CI=1"),
			new FileTypeMap("audio/x-ms-wma", "DLNA.ORG_PN=WMABASE;DLNA.ORG_OP=01;DLNA.ORG_CI=0")
		};

		static FileTypeMap[] 	PLT_HttpFileRequestHandler_SonosDlnaMap = new FileTypeMap[]{
			new FileTypeMap("audio/wav",      "*")
		};

		static FileTypeMap[] 	PLT_HttpFileRequestHandler_PS3DlnaMap =new FileTypeMap[] {
			new FileTypeMap("image/jpg",  "DLNA.ORG_OP=01"),
			new FileTypeMap("image/png",  "DLNA.ORG_OP=01")
		};





		private String           m_Protocol;
		private String           m_Mask;
		private String           m_ContentType;
		private String           m_Extra;

		private String           m_DLNA_PN;    // DLNA.ORG_PN Parameter (pn-param)
		private String           m_DLNA_OP;    // Operations Parameter (op-param)
		private String           m_DLNA_PS;    // Server-Side PlaySpeeds Parameter (ps-param)
		private String           m_DLNA_CI;    // Conversion Indicator Flag (ci-param)
		private String           m_DLNA_FLAGS; // Flags Parameter (flags-param)
		private String           m_DLNA_MAXSP; // Maximum RTSP Speed Header value (maxsp-param)
		private List<FieldEntry> m_DLNA_OTHER; // Vendor-defined 4th field Parameters (other-param)

		private bool                 m_Valid;

		public PltProtocolInfo ()
		{
			m_Valid = false;
		}

		public PltProtocolInfo (String protocol_info)
		{
			SetProtocolInfo(protocol_info);
		}

		public PltProtocolInfo (String protocol, String mask, String content_type, String extra)
		{
			m_Protocol = protocol;
			m_Mask = mask;
			m_ContentType = content_type;
			m_Extra = extra;
			m_Valid = false;

			ValidateExtra();

		}

	 
		// class methods
		public static String  GetMimeTypeFromProtocolInfo(String protocol_info)
		{
			/*
			NPT_String info = protocol_info;
			NPT_List<NPT_String> fragments = info.Split(":");
			if (fragments.GetItemCount() != 4) return "";
			return *fragments.GetItem(2);
			*/

			PltProtocolInfo info = new PltProtocolInfo(protocol_info);
			return info.m_ContentType;
		}

		public static String GetDlnaExtension(String mime_type, PltHttpRequestContext context = null)
		{
			return GetDlnaExtension(mime_type, context!=null?PltHttpHelper.GetDeviceSignature(context.GetRequest()):PltDeviceSignature.PLT_DEVICE_UNKNOWN);
		}



		public static String GetDlnaExtension(String mime_type, PltDeviceSignature signature = PltDeviceSignature.PLT_DEVICE_UNKNOWN)
		{
			String _mime_type = mime_type;

			if (signature != PltDeviceSignature.PLT_DEVICE_UNKNOWN) {
				// look for special case for 360
				if (signature == PltDeviceSignature.PLT_DEVICE_XBOX || signature == PltDeviceSignature.PLT_DEVICE_WMP) {
					for (int i = 0; i < PLT_HttpFileRequestHandler_360DlnaMap.Length; i++) {
						if (_mime_type.Equals (PLT_HttpFileRequestHandler_360DlnaMap [i].MimeType)) {
							return PLT_HttpFileRequestHandler_360DlnaMap [i].Extension;
						}
					}
				} else if (signature == PltDeviceSignature.PLT_DEVICE_SONOS) {
					for (int i = 0; i < PLT_HttpFileRequestHandler_SonosDlnaMap.Length; i++) {
						if (_mime_type.Equals (PLT_HttpFileRequestHandler_SonosDlnaMap [i].MimeType)) {
							return PLT_HttpFileRequestHandler_SonosDlnaMap [i].Extension;
						}
					}
				} else if (signature == PltDeviceSignature.PLT_DEVICE_PS3) {
					for (int i = 0; i < PLT_HttpFileRequestHandler_PS3DlnaMap.Length; i++) {
						if (_mime_type.Equals (PLT_HttpFileRequestHandler_PS3DlnaMap [i].MimeType)) {
							return PLT_HttpFileRequestHandler_PS3DlnaMap [i].Extension;
						}
					}

					return "DLNA.ORG_OP=01"; // Should we try default dlna instead?
				}
			}

			for (int i = 0; i < PLT_HttpFileRequestHandler_DefaultDlnaMap.Length; i++) {
				if (_mime_type.Equals (PLT_HttpFileRequestHandler_DefaultDlnaMap [i].MimeType)) {
					return PLT_HttpFileRequestHandler_DefaultDlnaMap [i].Extension;
				}
			}

			return "*";
		}

		public static PltProtocolInfo GetProtocolInfo(String filename, 	bool with_dlna_extension = true, PltHttpRequestContext context = null)
		{
			return GetProtocolInfoFromMimeType(PltMimeType.GetMimeType(filename, context), 	with_dlna_extension, context);
		}

		public static PltProtocolInfo GetProtocolInfo(String filename, bool with_dlna_extension = true, PltDeviceSignature signature = PltDeviceSignature.PLT_DEVICE_UNKNOWN)
		{
			return GetProtocolInfoFromMimeType(PltMimeType.GetMimeType(filename, signature), with_dlna_extension, signature);
		}

		public static PltProtocolInfo GetProtocolInfoFromMimeType(String mime_type, bool with_dlna_extension = true, PltHttpRequestContext context = null)
		{
			return GetProtocolInfoFromMimeType(mime_type, with_dlna_extension, 	context!=null?PltHttpHelper.GetDeviceSignature(context.GetRequest()):PltDeviceSignature.PLT_DEVICE_UNKNOWN);
		}

		public static PltProtocolInfo GetProtocolInfoFromMimeType(String mime_type, bool with_dlna_extension = true, PltDeviceSignature signature = PltDeviceSignature.PLT_DEVICE_UNKNOWN)
		{
			return new PltProtocolInfo ("http-get:*:" + mime_type + ":" + (with_dlna_extension ? GetDlnaExtension (mime_type, signature) : "*"));
		}


		 

		public String GetProtocol()
		{
			return m_Protocol;  
		}

		public String  GetMask()
		{
			return m_Mask; 
		}

		public String GetContentType()
		{
			return m_ContentType;  
		}

		public String GetExtra() { 
			return m_Extra; 
		}

		public String GetDLNA_PN()
		{ 
			return m_DLNA_PN; 
		}

		public bool IsValid() { return m_Valid; }

		public override String ToString()
		{
			String output = m_Protocol + ":";
			output += m_Mask + ":";
			output += m_ContentType + ":";
			// if it wasn't valid or extra is not DLNA, just use it as is
			if (!m_Valid || m_Extra == "*") {
				output += m_Extra;
			} else {
				bool add_semicolon = false;
				if (!String.IsNullOrEmpty(m_DLNA_PN)) {
					output += "DLNA.ORG_PN=" + m_DLNA_PN;
					add_semicolon = true;
				}
				if (!String.IsNullOrEmpty(m_DLNA_OP)) {
					if (add_semicolon) output += ";";
					output += "DLNA.ORG_OP=" + m_DLNA_OP;
					add_semicolon = true;
				}
				if (!String.IsNullOrEmpty(m_DLNA_PS)) {
					if (add_semicolon) output += ";";
					output += "DLNA.ORG_PS=" + m_DLNA_PS;
					add_semicolon = true;
				}
				if (!String.IsNullOrEmpty(m_DLNA_CI)) {
					if (add_semicolon) output += ";";
					output += "DLNA.ORG_CI=" + m_DLNA_CI;
					add_semicolon = true;
				}
				if (!String.IsNullOrEmpty(m_DLNA_FLAGS)) {
					if (add_semicolon) output += ";";
					output += "DLNA.ORG_FLAGS=" + m_DLNA_FLAGS;
					add_semicolon = true;
				}
				if (!String.IsNullOrEmpty(m_DLNA_MAXSP)) {
					if (add_semicolon) output += ";";
					output += "DLNA.ORG_MAXSP=" + m_DLNA_MAXSP;
					add_semicolon = true;
				}
				if (m_DLNA_OTHER.Count>0) {
					foreach(FieldEntry iter in m_DLNA_OTHER){
						if (add_semicolon) output += ";";
						output += iter.m_Key + "=" + iter.m_Value;
						add_semicolon = true;
					}
					 
				}
			}

			return output;
		}

		public bool Match(PltProtocolInfo other)
		{
			// we need the first 3 params
			if (m_Protocol.Equals("*") &&
				!other.GetProtocol().Equals('*') &&
				!m_Protocol.Equals(other.GetProtocol())) return false;

			if (m_Mask.Equals("*") &&
				!other.GetMask().Equals("*") &&
				!m_Mask.Equals(other.GetMask())) return false;

			if (m_ContentType.Equals("*") &&
				!other.GetContentType().Equals("*") &&
				!m_ContentType.Equals(other.GetContentType())) return false;

			// match DLNAPn of 4th item if not '*'
			if (m_Extra.Equals("*") ||
				other.GetExtra().Equals("*") ||
				(!String.IsNullOrEmpty(m_DLNA_PN) && m_DLNA_PN.Equals(other.GetDLNA_PN()))) return true;

			return false;
		}



		private Boolean SetProtocolInfo(String protocol_info)
		{
			if (String.IsNullOrEmpty(protocol_info) || protocol_info[0] == '\0') 
				throw new Exception("NPT_ERROR_INVALID_PARAMETERS");

			String[] parts = protocol_info.Split(':');
			if (parts.Length != 4) 
				throw new Exception(" NPT_ERROR_INVALID_SYNTAX") ;


			m_Protocol    = parts[0];
			m_Mask        = parts[1];
			m_ContentType = parts[2];
			m_Extra       = parts[3];

			return ValidateExtra();
		}


		private Boolean ValidateField(String val, char[]  valid_chars, int num_chars = 0)// 0 means variable number of chars
		{
			if (valid_chars.Length==0 || String.IsNullOrEmpty(val) || val[0] == '\0') 
				throw new Exception("NPT_ERROR_INVALID_PARAMETERS");

			// shortcut
			if (num_chars>0 && val.Length != num_chars)
				throw new Exception("NPT_ERROR_INVALID_SYNTAX");
			char[] buf = val.ToCharArray ();
			foreach(char c in buf){
				if (c == '\0') return true;
				Boolean found = false;

				foreach(char p in valid_chars){
					if(c.Equals(p)){
						found = true;
						break;
					}
				}
				if (!found)
					return false;

			}

			return true;
		}

		private Boolean ParseExtra(List<FieldEntry> entries)
		{
			if (m_Extra == "*") return true;

			// remove extra characters which could cause parsing errors
			m_Extra.Trim(new char[]{';'});

			String[] fields = m_Extra.Split(';');
			foreach (String field in fields) {
				String[] entry = field.Split('=');
				if (entry.Length != 2) throw new Exception("NPT_ERROR_INVALID_SYNTAX");
				entries.Add(new FieldEntry(entry[0], entry[1]));				 
			}

			return true;
		}

		private Boolean ValidateExtra()
		{
			if (m_Extra != "*") {
				m_Valid = false;

				List<FieldEntry> entries = new List<FieldEntry>();
				Plt.CheckSevere(ParseExtra(entries));
				 
				// parse other optional fields
				PltProtocolInfoParserState state = PltProtocolInfoParserState.PLT_PROTINFO_PARSER_STATE_START;
				foreach (FieldEntry entry in entries) {
					if (entry.m_Key.Equals("DLNA.ORG_PN") ) {
						// pn-param only allowed as first param
						if (state > PltProtocolInfoParserState.PLT_PROTINFO_PARSER_STATE_START) {
							Plt.LogWarn(String.Format("Failure to parse Protocol Info Extras:%s", m_Extra));
							return false;
						}

						if (Plt.IsFailed (ValidateField (entry.m_Value, PLT_DLNAPNCharsToValidate))) {
							Plt.LogWarn(String.Format("Failure to parse Protocol Info Extras:%s", m_Extra));
							return false;
						}

						m_DLNA_PN = entry.m_Value;
						state = PltProtocolInfoParserState.PLT_PROTINFO_PARSER_STATE_PN;
						continue;
					} else if (entry.m_Key.Equals("DLNA.ORG_OP") ) {
						// op-param only allowed after pn-param
						if (state > PltProtocolInfoParserState.PLT_PROTINFO_PARSER_STATE_PN) {
							Plt.LogWarn(String.Format("Failure to parse Protocol Info Extras:%s", m_Extra));
							return false;
						}

						// validate value
						if (Plt.IsFailed (ValidateField (entry.m_Value, PLT_DLNAFlagCharsToValidate, 2))) {
							Plt.LogWarn(String.Format("Failure to parse Protocol Info Extras:%s", m_Extra));
							return false;
						}

						m_DLNA_OP = entry.m_Value;
						state = PltProtocolInfoParserState.PLT_PROTINFO_PARSER_STATE_OP;
						continue;
					} else if (entry.m_Key.Equals("DLNA.ORG_PS")) {
						// ps-param only allowed after op-param
						if (state > PltProtocolInfoParserState.PLT_PROTINFO_PARSER_STATE_OP) {
							Plt.LogWarn(String.Format("Failure to parse Protocol Info Extras:%s", m_Extra));
							return false;
						}

						// validate value
						if (Plt.IsFailed (ValidateField (entry.m_Value, PLT_DLNAPSCharsToValidate))) {
							Plt.LogWarn(String.Format("Failure to parse Protocol Info Extras:%s", m_Extra));
							return false;
						}

						m_DLNA_PS = entry.m_Value;
						state = PltProtocolInfoParserState.PLT_PROTINFO_PARSER_STATE_PS;
						continue;
					} else if (entry.m_Key.Equals("DLNA.ORG_CI")) {
						// ci-param only allowed after ps-param
						if (state > PltProtocolInfoParserState.PLT_PROTINFO_PARSER_STATE_PS) {
							Plt.LogWarn(String.Format("Failure to parse Protocol Info Extras:%s", m_Extra));
							return false;
						}

						// validate value
						if (Plt.IsFailed (ValidateField (entry.m_Value, PLT_DLNAFlagCharsToValidate, 1))) {
							Plt.LogWarn(String.Format("Failure to parse Protocol Info Extras:%s", m_Extra));
							return false;
						}

						m_DLNA_CI = entry.m_Value;
						state = PltProtocolInfoParserState.PLT_PROTINFO_PARSER_STATE_CI;
						continue;
					} else if (entry.m_Key.Equals("DLNA.ORG_FLAGS") ){
						// flags-param only allowed after ci-param
						if (state > PltProtocolInfoParserState.PLT_PROTINFO_PARSER_STATE_CI) {
							Plt.LogWarn(String.Format("Failure to parse Protocol Info Extras:%s", m_Extra));
							return false;
						}

						// validate value
						if (Plt.IsFailed (ValidateField (entry.m_Value, PLT_DLNAHexCharsToValidate,	32))) {
							Plt.LogWarn(String.Format("Failure to parse Protocol Info Extras:%s", m_Extra));
							return false;
						}

						m_DLNA_FLAGS = entry.m_Value;
						state = PltProtocolInfoParserState.PLT_PROTINFO_PARSER_STATE_FLAGS;
						continue;
					} else if (entry.m_Key.Equals("DLNA.ORG_MAXSP")) {
						// maxsp-param only allowed after flags-param
						if (state > PltProtocolInfoParserState.PLT_PROTINFO_PARSER_STATE_FLAGS) { 
							Plt.LogWarn(String.Format("Failure to parse Protocol Info Extras:%s", m_Extra));
							return false;
						}

						// validate value
						if(Plt.IsFailed(ValidateField(entry.m_Value, (PLT_FIELD_NUM+".").ToCharArray()))){
							Plt.LogWarn(String.Format("Failure to parse Protocol Info Extras:%s", m_Extra));
							return false;
						}

						m_DLNA_MAXSP = entry.m_Value;
						state = PltProtocolInfoParserState.PLT_PROTINFO_PARSER_STATE_MAXSP;
						continue;
					} else {
						// don't switch state for unknown value so we don't break parsing next ones
						// Sony TVs for example have DLNA.ORG_PN=xx;SONY.COM_PN=xx;DLNA.ORG_FLAGS=xxx
						//state = PLT_PROTINFO_PARSER_STATE_OTHER;

						// validate key first which should IANA_*<"a"-"z","A"-"Z","0"-"9">
						int index = entry.m_Key.IndexOf("_");
						if (index == -1) {
							Plt.LogWarn(String.Format("Failure to parse Protocol Info Extras:%s", m_Extra));
							return false;
						}

						// validate key
						if (Plt.IsFailed(ValidateField(	entry.m_Key.Substring(index),PLT_DLNAOTherNameCharsToValidate))) {
							Plt.LogWarn(String.Format("Invalid protocolinfo 4th field other param: %s=%s",entry.m_Key, entry.m_Value));
							continue;
						}

						// validate value
						if (Plt.IsFailed(ValidateField(entry.m_Value, PLT_DLNAOTherValueCharsToValidate))) {

							Plt.LogWarn(String.Format("Invalid protocolinfo 4th field other param: %s=%s",entry.m_Key, entry.m_Value));
							continue;
						}

						m_DLNA_OTHER.Add(entry);
						continue;
					}
				}

				 
			}

			m_Valid = true;
			return true;
		}
	}


	class FieldEntry {
		public String m_Key;
		public String m_Value;

		public FieldEntry(String key, String value) 
		{
			m_Key = key;
			m_Value = value;
		}
	}


	class PLT_HttpFileRequestHandler_DefaultDlnaExtMapEntry
	{
		public String mime_type;
		public String dlna_ext;
	}

	enum PltProtocolInfoParserState{
		PLT_PROTINFO_PARSER_STATE_START,
		PLT_PROTINFO_PARSER_STATE_PN,
		PLT_PROTINFO_PARSER_STATE_OP,
		PLT_PROTINFO_PARSER_STATE_PS,
		PLT_PROTINFO_PARSER_STATE_CI,
		PLT_PROTINFO_PARSER_STATE_FLAGS,
		PLT_PROTINFO_PARSER_STATE_MAXSP,
		PLT_PROTINFO_PARSER_STATE_OTHER
	} 
}
  
 
   