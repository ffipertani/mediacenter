﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Net;

namespace Platinum
{
	public class PltEventNotification
	{
		public DateTime ReceptionTime{ get;set;}
		public HttpUri RequestUrl{ get; set;}
		public String SID{ get; set;}
		public int EventKey{get;set;}
		public String XmlBody{get;set;}

		protected PltEventNotification()
		{
			EventKey = 0;
		}



		public static PltEventNotification Parse(HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
			PltHttp.Log ("",request);

			PltEventNotification notification = new PltEventNotification ();
			notification.RequestUrl = request.Url;

			String sid = PltUPnPMessageHelper.GetSID (request);
			String nt = PltUPnPMessageHelper.GetNT (request);
			String nts = PltUPnPMessageHelper.GetNTS (request);

			if (String.IsNullOrEmpty(sid)) {
				badRequest (response);
				return null;
			}
			notification.SID = sid;

			if (String.IsNullOrEmpty(nt) || String.IsNullOrEmpty(nts)) {
				response.SetStatus (400, "Bad request");
				badRequest (response);
				return null;
			}

			if (!nt.Equals ("upnp:event") || !nts.Equals ("upnp:propchange")) {
				badRequest (response);
				return null;
			}

			// if the sequence number is less than our current one, we got it out of order
			// so we disregard it
			int eventKey;
			PltUPnPMessageHelper.GetSeq (request, out eventKey );
			notification.EventKey = eventKey;
			// parse body
			String xmlBody;
			if (Plt.IsFailed (PltHttpHelper.GetBody (request, out xmlBody))) {
				badRequest (response);
				return null;
			}
			notification.XmlBody = xmlBody;
			return notification;
		}

		private static void badRequest(HttpResponse response)
		{
			Console.Error.WriteLine ("CtrlPoint received bad event notify request\r\n");
			if (response.GetStatusCode () == 200) {
				response.SetStatus (412, "Precondition Failed");
			}
		 
		}
	}




 

	public class PltEventSubscriber
	{
		protected PltTaskManager		    m_TaskManager;
		protected PltService              m_Service;
		protected int               m_EventKey;
		protected PltHttpClientSocketTask  m_SubscriberTask;
		protected String                m_SID;
		protected IPAddress         m_LocalIf;
		protected List<String>			    m_CallbackURLs;
		protected DateTime             m_ExpirationTime;

		public PltEventSubscriber(PltTaskManager task_manager, PltService service, String sid, int timeout_secs = -1)
		{
			m_TaskManager = task_manager;
			m_Service = service;
			m_EventKey = 0;
			m_SubscriberTask = null;
			m_SID = sid;

			Plt.LogFine(String.Format("Creating new subscriber ({0})", m_SID));
			SetTimeout(timeout_secs);

		}
		public void Dispose()
		{
			Plt.LogFine(String.Format("Deleting subscriber ({0})", m_SID));
			if (m_SubscriberTask!=null) {
				m_SubscriberTask.Kill();
				m_SubscriberTask = null;
			}
		}

		public PltService GetService()
		{
			return m_Service;
		}

		public int GetEventKey()
		{
			return m_EventKey;
		}

		public Boolean SetEventKey(int value)
		{
			m_EventKey = value;
			return true;
		}

		public IPAddress GetLocalIf()
		{
			return m_LocalIf;
		}

		public Boolean SetLocalIf(IPAddress value)
		{
			m_LocalIf = value;
			return true;
		}

		public DateTime GetExpirationTime()
		{
			return m_ExpirationTime;
		}

		public Boolean SetTimeout(int seconds = -1)
		{
			Plt.LogFine(String.Format("subscriber ({0}) expiring in {1} seconds", m_SID, seconds));

			// -1 means infinite but we default to 300 secs
			if (seconds == -1) seconds = 300;
			m_ExpirationTime = DateTime.Now;
			m_ExpirationTime = m_ExpirationTime.Add(TimeSpan.FromSeconds (seconds));

			return true;
		}

		public String GetSID() { 
			return m_SID;
		}

		public Boolean FindCallbackURL(String callback_url)
		{
			String res;
			return Plt.ContainerFind(m_CallbackURLs, new StringFinder(callback_url), out res);
		}

		public Boolean AddCallbackURL(String callback_url)
		{
			Plt.CheckPointerSevere(callback_url);

			Plt.LogFine(String.Format("Adding callback \"{0}\" to subscriber {1}", callback_url, m_SID));
			 m_CallbackURLs.Add(callback_url);
			return true;
		}

		public Boolean Notify(List<PltStateVariable> vars)
		{
			// verify we have eventable variables
			bool foundVars = false;
			XmlDocument doc = new XmlDocument ();
			XmlElement propertyset = doc.CreateElement("e","propertyset","urn:schemas-upnp-org:event-1-0");
	 


			foreach(PltStateVariable avar in vars){
				if (avar.IsSendingEvents) {
					XmlElement property = doc.CreateElement("e", "property","urn:schemas-upnp-org:event-1-0");
					propertyset.AppendChild(property);
					Plt.CheckSevere(PltXmlHelper.AddChildText(property, avar.Name, avar.GetValue()));
					foundVars = true;
				}
			}
			// no eventable state variables found!
			if (foundVars == false) {
				return false;
			}

			// format the body with the xml
			String xml;
			if (Plt.IsFailed(PltXmlHelper.Serialize(propertyset, out xml))) {
				Plt.Severe();
			}
			propertyset = null;

			// parse the callback url
			HttpUri url = new HttpUri(m_CallbackURLs[0]);
			if (!url.IsValid()) {
				Plt.Severe();
			}
			// format request
			HttpRequest request = new HttpRequest(url, "NOTIFY",HttpMessage.NPT_HTTP_PROTOCOL_1_1);
			HttpEntity entity;
			PltHttpHelper.SetBody(request, xml, out entity);

			// add the extra headers
			entity.SetContentType("text/xml; charset=\"utf-8\"");
			PltUPnPMessageHelper.SetNT(request, "upnp:event");
			PltUPnPMessageHelper.SetNTS(request, "upnp:propchange");
			PltUPnPMessageHelper.SetSID(request, m_SID);
			PltUPnPMessageHelper.SetSeq(request, m_EventKey);

			// wrap around sequence to 1
			if (++m_EventKey == 0) m_EventKey = 1;

			// start the task now if not started already
			if (m_SubscriberTask==null) {
				// TODO: the subscriber task should inform subscriber if
				// a notification failed to be received so it can be removed
				// from the list of subscribers inside the device host
				PltHttpClientSocketTask task = new PltHttpClientSocketTask(request, true);

				// short connection time out in case subscriber is not alive

				//HttpClient::Config config;
				//config.m_ConnectionTimeout = 2000;
				//task.SetHttpClientConfig(config);

				// add initial delay to make sure ctrlpoint receives response to subscription
				// before our first NOTIFY. Also make sure task is not auto-destroy
				// since we want to destroy it manually when the subscriber goes away.
				TimeSpan delay = TimeSpan.FromMilliseconds(50);
				Plt.CheckSevere(m_TaskManager.StartTask(task, delay , false));

				// Task successfully started, keep around for future notifications
				m_SubscriberTask = task;

			} else {
				m_SubscriberTask.AddRequest(request);
			}

			return true;
		}
	}

	public class PltEventSubscriberFinderBySID:IFinder<PltEventSubscriber>
	{
		String m_SID;
		public PltEventSubscriberFinderBySID(String sid)
		{
			m_SID = sid;
		}

		public Boolean IsFound(PltEventSubscriber sub)
		{
			return m_SID.Equals(sub.GetSID());
		}
	}


	public class PltEventSubscriberFinderByCallbackURL:IFinder<PltEventSubscriber>
	{
		String m_CallbackURL;

		public PltEventSubscriberFinderByCallbackURL(String callback_url) {
			m_CallbackURL = callback_url;
		}

		public Boolean IsFound(PltEventSubscriber sub)
		{
			return Plt.IsSucceded(sub.FindCallbackURL(m_CallbackURL));
		}
	}

	public class PltEventSubscriberFinderByService:IFinder<PltEventSubscriber>
	{
		PltService m_Service;

		public PltEventSubscriberFinderByService(PltService service){
			m_Service = service;
		}

		public Boolean IsFound(PltEventSubscriber sub)
		{
			return (m_Service == sub.GetService());
		}
	}

}