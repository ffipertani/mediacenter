﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Net;

namespace Platinum
{
	/*
	The PLT_DeviceData class holds information about a device being advertised or
	found by a control point. It maintains a list of services and 
	embedded devices if any.
		*/
	public class PltDeviceData
	{

		public PltDeviceData () : this (new HttpUri ("http://localhost",80, "/description.xml"), "", PltConstants.GetInstance ().DefaultDeviceLease, "", "")
		{
		}

		public PltDeviceData (
			HttpUri	description_url, 
			String      uuid,
			TimeSpan leaseTime,
			String      deviceType,
			String      friendlyName)
		{
			Manufacturer = "Plutinosoft LLC";
			ManufacturerURL = "http://www.plutinosoft.com";
			URLDescription = description_url;
			DeviceType = deviceType;
			FriendlyName = friendlyName;

			BootId = 0;
			NextBootId = 0;
			 
			if (String.IsNullOrEmpty (uuid)) {
				PltUPnPMessageHelper.GenerateGUID (out uuid);
			}
			UUID = uuid;
			SetLeaseTime (leaseTime);
			SetURLBase (URLDescription);
			UpdateConfigId ();
		}


		public Boolean FindEmbeddedDevice (String uuid, out PltDeviceData device)
		{
			Boolean res = Plt.ContainerFind (EmbeddedDevices, new UUIDDeviceFinder (uuid), out	device);
			if (res)
				return res;

			for (int i = 0; i < EmbeddedDevices.Count; i++) {
				res = EmbeddedDevices [i].FindEmbeddedDevice (uuid, out	device);
				if (res)
					return res;
			}

			return false;
		}

		Boolean FindEmbeddedDeviceByType (String type, out PltDeviceData device)
		{
			Boolean res = Plt.ContainerFind (EmbeddedDevices, new TypeDeviceFinder (type), out	device);
			if (res)
				return res;

			for (int i = 0; i < EmbeddedDevices.Count; i++) {
				res = EmbeddedDevices [i].FindEmbeddedDeviceByType (type, out device);
				if (res)
					return res;
			}

			return false;
		}

		public Boolean FindServiceById (String id, out PltService service)
		{
			return Plt.ContainerFind (Services, new PltServiceIDFinder (id), out service);
		}

		public Boolean FindServiceByType (String type, out PltService service)
		{
			return Plt.ContainerFind (Services, new PltServiceTypeFinder (type), out service);
		}

		public Boolean FindServiceByName (String name, out PltService service)
		{
			return Plt.ContainerFind (Services, new PltServiceNameFinder (name), out service);
		}

		public Boolean FindServiceBySCPDURL (String url, out PltService service, bool recursive = false)
		{
			Boolean res = Plt.ContainerFind (Services, new PltServiceSCPDURLFinder (url), out service);
			if (Plt.IsSucceded (res))
				return res;

			if (recursive) {
				for (int i = 0; i < EmbeddedDevices.Count; i++) {
					res = EmbeddedDevices [i].FindServiceBySCPDURL (url, out service,	recursive);
					if (Plt.IsSucceded (res))
						return res;
				}
			}

			return false;
		}

		public Boolean FindServiceByControlURL (String url, out PltService service, bool recursive = false)
		{
			Boolean res = Plt.ContainerFind (Services, new PltServiceControlURLFinder (url), out service);
			if (Plt.IsSucceded (res))
				return res;

			if (recursive) {
				for (int i = 0; i < EmbeddedDevices.Count; i++) {
					res = EmbeddedDevices [i].FindServiceByControlURL (url, out service,	recursive);
					if (Plt.IsSucceded (res))
						return res;
				}
			}

			return false;
		}

		public Boolean FindServiceByEventSubURL (String url, out PltService service, bool recursive = false)
		{
			Boolean res = Plt.ContainerFind (Services, new PltServiceEventSubURLFinder (url), out service);
			if (Plt.IsSucceded (res))
				return res;

			if (recursive) {
				for (int i = 0; i < EmbeddedDevices.Count; i++) {
					res = EmbeddedDevices [i].FindServiceByEventSubURL (url, out	service, recursive);
					if (Plt.IsSucceded (res))
						return res;
				}
			}

			return false;
		}




		/* called by PLT_Device subclasses */
		public void AddEmbeddedDevice (PltDeviceData device)
		{
			UpdateConfigId ();

			device.ParentUUID = UUID;
			EmbeddedDevices.Add (device);
		}

		public void RemoveEmbeddedDevice (PltDeviceData device)
		{
			for (int i = 0; i < EmbeddedDevices.Count; i++) {
				if (EmbeddedDevices [i] == device) {
					UpdateConfigId ();
					EmbeddedDevices.RemoveAt (i);
					return;
				}
			}

			throw new Exception ("NPT_ERROR_NO_SUCH_ITEM");
		}

		public Boolean  AddService (PltService service)
		{
			if (String.IsNullOrEmpty(service.GetServiceType () ) ||
				String.IsNullOrEmpty(service.GetServiceID ()) ||
				String.IsNullOrEmpty(service.GetSCPDURL ()) ||
				String.IsNullOrEmpty(service.GetControlURL ()) ||
				String.IsNullOrEmpty(service.GetEventSubURL ()) ) {
				throw new Exception ("NPT_ERROR_INVALID_PARAMETERS");
			}
			UpdateConfigId ();
			Services.Add (service);
			return true;
		}

		public Boolean RemoveService (PltService service)
		{
			for (int i = 0; i < Services.Count; i++) {
				if (Services [i] == service) {
					UpdateConfigId ();
					Services.RemoveAt (i);
					return true;
				}
			}

			throw new Exception ("NPT_ERROR_NO_SUCH_ITEM");
		}

		/* BOOTID UPnP 1/1 */
		public void SetBootId (int bootId)
		{
			this.BootId = bootId;
		}

		public void SetNextBootId (int nextBootId)
		{
			this.NextBootId = nextBootId;
		}

		public int GenerateNextBootId ()
		{
			DateTime now = DateTime.Now;
			int value = (int)now.TimeOfDay.TotalSeconds;
			if (value == this.BootId)
				++value;
			return value;
		}


		protected virtual void Cleanup ()
		{
			 
			Services.Apply (new DisposerApplier<PltService> ());
			Services.Clear ();
			EmbeddedDevices.Clear ();
			Icons.Clear ();
			 
		}

		protected virtual Boolean OnAddExtraInfo (XmlElement device_node)
		{
			return true;
		}




		public Boolean Root {
			get {
				return String.IsNullOrEmpty (ParentUUID);
			}
		}

		public int BootId{ get; set; }

		public int NextBootId{ get; set; }

		public int ConfigId{ get; set; }

		public String Manufacturer{ get; set; }

		public String ManufacturerURL{ get; set; }

		public String ModelDescription{ get; set; }

		public String ModelName{ get; set; }

		public String ModelNumber{ get; set; }

		public String ModelURL{ get; set; }

		public String SerialNumber{ get; set; }

		public String UPC{ get; set; }

		public String PresentationURL{ get; set; }

		public String DlnaDoc{ get; set; }

		public String DlnaCap{ get; set; }

		public String AggregationFlags{ get; set; }

		public String UUID{ get; protected set; }

		public String ParentUUID{ get; protected set; }

		public String DeviceType{ get; protected set; }

		public HttpUri URLDescription{ get; protected set; }

		public HttpUri URLBase{ get; protected set; }

		public String FriendlyName{ get; protected set; }

		public TimeSpan LeaseTime{ get; protected set; }

		public DateTime LeaseTimeLastUpdate{ get; protected set; }

		public List<PltService> Services{ get; protected set; } = new List<PltService>();

		public List<PltDeviceData> EmbeddedDevices{ get; protected set; } = new List<PltDeviceData>();

		public List<PltDeviceIcon> Icons{ get; protected set; } = new List<PltDeviceIcon>();

		public IPAddress LocalIfaceIp{ get; protected set; }

		public String	Representation{ get; protected set; }



		public virtual Boolean  GetDescription (out String  desc)
		{

			Boolean res = true;
			XmlElement spec = null;
			XmlDocument doc = new XmlDocument ();
			desc = null;
			XmlElement root = doc.CreateElement ("root","urn:schemas-upnp-org:device-1-0");
			doc.AppendChild (root);			 
			root.SetAttribute ("xmlns:dlna", "urn:schemas-dlna-org:device-1-0");
			root.SetAttribute ("configId", ConfigId.ToString());

			// add spec version
			spec = doc.CreateElement  ("","specVersion","urn:schemas-upnp-org:device-1-0");
			root.AppendChild (spec);
			if (Plt.IsFailed (res = PltXmlHelper.AddChildText (spec, "major", "1"))) {
				return false;
			}
			if (Plt.IsFailed (res = PltXmlHelper.AddChildText (spec, "minor", "1"))) {
				return false;
			}

			// get device xml
			XmlElement device;
			if (Plt.IsFailed (res = GetDescription (root,out device))) {
				return false;
			}

			// serialize node
			res = PltXmlHelper.Serialize (root, out desc, true, 2);

			return res;
		}

		public virtual String  GetDescriptionUrl (String ip_address = null)
		{			 
			UriBuilder builder = new UriBuilder (URLDescription.Uri);
				 
			// replace host with ip address specified
			if (!String.IsNullOrEmpty (ip_address)) {
				builder.Host = ip_address;
			}

			return builder.Uri.ToString();
		}

		public virtual HttpUri GetURLBase ()
		{
			return URLBase;
		}

		public virtual HttpUri NormalizeURL (String url)
		{

			if (url.StartsWith ("http://"))
				return new HttpUri (url);
		
			UriBuilder builder = new UriBuilder (URLBase.Uri);
			HttpUri norm_url = URLBase;
			if (url.StartsWith ("/")) {
				builder.Path = url;
			} else {
				builder.Path = "/" + url;
			}

			return new HttpUri(builder.Uri.ToString());
		}

		public virtual Boolean  GetDescription (XmlElement root){
			XmlElement device_out;
			return GetDescription (root, out device_out);
		}

		public virtual Boolean  GetDescription (XmlElement root, out XmlElement device_out)
		{
			XmlElement device = root.OwnerDocument.CreateElement ("","device","urn:schemas-upnp-org:device-1-0");

			device_out = device;

			root.AppendChild (device);

			// device properties
			Plt.CheckSevere (PltXmlHelper.AddChildText (device, "deviceType", DeviceType));
			Plt.CheckSevere (PltXmlHelper.AddChildText (device, "friendlyName", FriendlyName));
			Plt.CheckSevere (PltXmlHelper.AddChildText (device, "manufacturer", Manufacturer));
			Plt.CheckSevere (PltXmlHelper.AddChildText (device, "manufacturerURL", ManufacturerURL));
			Plt.CheckSevere (PltXmlHelper.AddChildText (device, "modelDescription", ModelDescription));
			Plt.CheckSevere (PltXmlHelper.AddChildText (device, "modelName", ModelName));
			if (!String.IsNullOrEmpty (ModelNumber))
				Plt.CheckSevere (PltXmlHelper.AddChildText (device, "modelNumber", ModelNumber));
			if (!String.IsNullOrEmpty (SerialNumber))
				Plt.CheckSevere (PltXmlHelper.AddChildText (device, "serialNumber", SerialNumber));
			Plt.CheckSevere (PltXmlHelper.AddChildText (device, "modelURL", ModelURL)); // moved after modelNumber to go around a bug in UCTT 
			Plt.CheckSevere (PltXmlHelper.AddChildText (device, "UDN", "uuid:" + UUID));

			if (!String.IsNullOrEmpty (PresentationURL)) {
				Plt.CheckSevere (PltXmlHelper.AddChildText (device, "presentationURL", PresentationURL));
			}

			// Extra info not in UPnP specs
			Plt.CheckWarining (OnAddExtraInfo (device));

			// DLNA support
			if (!String.IsNullOrEmpty (DlnaDoc)) {
				XmlElement dlnadoc = root.OwnerDocument.CreateElement ("dlna","X_DLNADOC","urn:schemas-dlna-org:device-1-0");
				dlnadoc.Prefix = "dlna";
				 
				dlnadoc.InnerText += DlnaDoc;
				device.AppendChild (dlnadoc);
			}
			if (!String.IsNullOrEmpty (DlnaCap)) {
				XmlElement dlnacap = root.OwnerDocument.CreateElement ("dlna","X_DLNACAP","urn:schemas-dlna-org:device-1-0");
				dlnacap.Prefix = "dlna";

				dlnacap.InnerText += DlnaCap;
				device.AppendChild (dlnacap);
			}

			// icons
			if (Icons.Count>0) {
				XmlElement icons = root.OwnerDocument.CreateElement ("iconList");
				device.AppendChild (icons);
				for (int i = 0; i < Icons.Count; i++) {
					XmlElement icon = root.OwnerDocument.CreateElement ("icon");
					icons.AppendChild (icon);
					Plt.CheckSevere (PltXmlHelper.AddChildText (icon, "mimetype", Icons [i].MimeType));
					Plt.CheckSevere (PltXmlHelper.AddChildText (icon, "width", Icons [i].Width.ToString ()));
					Plt.CheckSevere (PltXmlHelper.AddChildText (icon, "height", Icons [i].Height.ToString()));
					Plt.CheckSevere (PltXmlHelper.AddChildText (icon, "depth", Icons [i].Depth.ToString()));
					Plt.CheckSevere (PltXmlHelper.AddChildText (icon, "url", Icons [i].UrlPath.ToString()));
				}
			}

			// services
			XmlElement services =root.OwnerDocument.CreateElement("","serviceList","urn:schemas-upnp-org:device-1-0");
			device.AppendChild (services);
			Plt.CheckSevere (Services.ApplyUntil (new PltServiceGetDescriptionIterator (services), ApplyUntil.ResultSuccess));

			// PS3 support
			if (!String.IsNullOrEmpty (AggregationFlags)) {
				XmlElement aggr = root.OwnerDocument.CreateElement ("av","aggregationFlags","urn:schemas-sonycom:av");				 
				aggr.InnerText += AggregationFlags;
				device.AppendChild (aggr);
			}

			// embedded devices
			if (EmbeddedDevices.Count > 0) {
				XmlElement deviceList = root.OwnerDocument.CreateElement ("deviceList");
				device.AppendChild (deviceList);

				Plt.CheckSevere (EmbeddedDevices.ApplyUntil (new PltDeviceDataGetDescriptionIterator (deviceList), ApplyUntil.ResultSuccess));
			}

			return true;
		}

		public virtual String  GetIconUrl (String mimetype = null, int maxsize = 0, int maxdepth = 0)
		{
		 
			PltDeviceIcon icon=new PltDeviceIcon();

			for (int i = 0; i < Icons.Count; i++) {
				if ((!String.IsNullOrEmpty (mimetype) && !Icons [i].MimeType.Equals (mimetype)) ||
				    (maxsize>0 && Icons [i].Width > maxsize) ||
				    (maxsize>0 && Icons [i].Height > maxsize) ||
				    (maxdepth>0 && Icons [i].Depth > maxdepth))
					continue;

				// pick the biggest and better resolution we can
				if (icon.Width >= Icons [i].Width ||
				    icon.Height >= Icons [i].Height ||
				    icon.Depth >= Icons [i].Depth ||
				    String.IsNullOrEmpty (Icons [i].UrlPath))
					continue;

				icon = Icons [i];
			}

			if (icon==null || icon.UrlPath.Equals (""))
				return "";

			return NormalizeURL (icon.UrlPath).ToString ();
		}



		/* called by PLT_CtrlPoint when an existing device location is updated */
		private Boolean    SetDescriptionUrl (HttpUri url)
		{
			SetURLBase (url);
			URLDescription = url;
			return true;
		}

		public Boolean SetLeaseTime (TimeSpan lease_time)
		{
			return SetLeaseTime (lease_time, DateTime.Now);
		}

		protected Boolean SetLeaseTime (TimeSpan lease_time, DateTime lease_time_last_update)
		{
			// Enforce 10 seconds min lease time
			LeaseTime = (lease_time.TotalSeconds >= 10) ? lease_time : PltConstants.GetInstance ().DefaultDeviceLease;

			// get current time as last update time if none passed
			 
			LeaseTimeLastUpdate = lease_time_last_update;
			return true;
		}


		private Boolean SetURLBase (HttpUri url)
		{
			if (URLBase == null || URLBase.Uri == null) {
				URLBase = url;
				return true;
			}
			// only http scheme supported
			UriBuilder builder = new UriBuilder (URLBase.Uri);
			builder.Scheme = url.Uri.Scheme;

			// update port if any
			if (url.Uri.Port != Plt.INVALID_PORT)
				builder.Port = url.Uri.Port;

			// update host if any
			if (url.Uri.Host.Length != 0)
				builder.Host = url.Uri.Host;

			// update path
			String path = url.Uri.PathAndQuery;

			// remove trailing file according to RFC 2396
			if (!path.EndsWith ("/")) {
				int index = path.LastIndexOf ('/');
				if (index < 0)
					return false;
				path = path.Substring (0, index + 1);
			}
			builder.Path = path;
			URLBase = new HttpUri(builder.Uri.ToString());
			return true;
			   
		}

		protected DateTime GetLeaseTimeLastUpdate ()
		{
			return LeaseTimeLastUpdate;
		}



		private void UpdateConfigId ()
		{
			Random random = new Random ();

			int nextConfigId = random.Next () & 0xFFFFFF;
			if (ConfigId == nextConfigId) {
				// prevent value to underflow
				if (nextConfigId > 0) {
					nextConfigId -= 1;
				} else {
					nextConfigId += 1;
				}
			}

			ConfigId = nextConfigId;
		}



		public override String ToString ()
		{
			StringWriter writer = new StringWriter ();
			writer.WriteLine ("Device GUID: " + UUID);
			writer.WriteLine ("Device Type: " + DeviceType);
			writer.WriteLine ("Device Base Url: " + GetURLBase ().ToString ());
			writer.WriteLine ("Device Friendly Name: " + FriendlyName);

			return writer.ToString ();
		}

		/* class methods */
		public static Boolean SetDescription (out PltDeviceData      root_device,
		                                       TimeSpan              leasetime,
		                                       HttpUri                   description_url,
		                                       String                   description, 
		                                       HttpRequestContext context)
		{
			XmlDocument tree = new XmlDocument ();
			Boolean res = true;
			XmlElement root = null;
			String URLBase;
			String configId;


			 
			root_device = new PltDeviceData (description_url, "", leasetime, "", "");
			 
			tree.LoadXml (description);

			root = tree.DocumentElement;
			if (root == null ||
			    !root.Name.Equals ("root") ||
			    String.IsNullOrEmpty (root.NamespaceURI) ||
			    !root.NamespaceURI.Equals ("urn:schemas-upnp-org:device-1-0")) {
				Plt.LogFine (String.Format ("root namespace is invalid: %s", (root != null && !String.IsNullOrEmpty (root.NamespaceURI)) ? root.NamespaceURI : "null"));
				return res;
			}

			// look for optional URLBase element
			if (Plt.IsSucceded (PltXmlHelper.GetChildText (root, "URLBase", out URLBase))) {
				HttpUri url = new HttpUri (URLBase);
				// Some devices like Connect360 try to be funny - not so
				if (url.Uri.Host.ToLower ().Equals ("localhost") ||
				    url.Uri.Host.ToLower ().Equals ("127.0.0.1")) {
					UriBuilder builder = new UriBuilder (url.Uri);
					builder.Host = context.RemoteAddress.Address.ToString ();

				}
				root_device.SetURLBase (url);
			} else {
				// No URLBase, derive from description url
				root_device.SetURLBase (description_url);
			}

			// at least one root device child element is required
			XmlElement device = PltXmlHelper.GetChild (root, "device");
			 
			if (root_device==null) {
				return res;
			}

			res = SetDescriptionDevice (root_device, device, context);

			// reset configId if and set it back from root attribute
			root_device.ConfigId = 0;
			 
			if (Plt.IsSucceded (PltXmlHelper.GetAttribute (root, "configId", out configId))) {				 
				int value;
				if (Plt.IsSucceded (int.TryParse (configId, out value))) {
					root_device.ConfigId = value;
				}
			}

			return res;
		}


		private static Boolean SetDescriptionDevice (PltDeviceData device, XmlElement  device_node, HttpRequestContext context)
		{
			 
			device.LocalIfaceIp = context.LocalAddress.Address;
			String deviceType, uuid;
			Plt.CheckSevere (PltXmlHelper.GetChildText (device_node, "deviceType", out deviceType));
			Plt.CheckSevere (PltXmlHelper.GetChildText (device_node, "UDN", out uuid));
			device.DeviceType = deviceType;
			device.UUID = uuid;

			// remove uuid: prefix
			if (device.UUID.StartsWith ("uuid:")) {
				device.UUID = device.UUID.Substring (5);
			}
			String friendlyName, manufacturer, manufacturerURL, modelDescription, modelName, modelURL, modelNumber, serialNumber, presentationURL;
			// optional attributes
			PltXmlHelper.GetChildText (device_node, "friendlyName", out friendlyName);
			PltXmlHelper.GetChildText (device_node, "manufacturer", out manufacturer);
			PltXmlHelper.GetChildText (device_node, "manufacturerURL",out manufacturerURL);
			PltXmlHelper.GetChildText (device_node, "modelDescription", out modelDescription);
			PltXmlHelper.GetChildText (device_node, "modelName", out modelName);
			PltXmlHelper.GetChildText (device_node, "modelURL",out modelURL);
			PltXmlHelper.GetChildText (device_node, "modelNumber", out modelNumber);
			PltXmlHelper.GetChildText (device_node, "serialNumber", out serialNumber);
			PltXmlHelper.GetChildText (device_node, "presentationURL", out presentationURL);

			device.FriendlyName = friendlyName;
			device.Manufacturer = manufacturer;
			device.ManufacturerURL = manufacturerURL;
			device.ModelDescription = modelDescription;
			device.ModelName = modelName;
			device.ModelURL = modelURL;
			device.ModelNumber = modelNumber;
			device.SerialNumber = serialNumber;
			device.PresentationURL = presentationURL;

			// enumerate icons
			XmlElement iconList = PltXmlHelper.GetChild (device_node, "iconList");
			if (iconList != null) {
				List<XmlElement> icons = new List<XmlElement> ();
				PltXmlHelper.GetChildren (iconList, out icons, "icon");

				for (int k = 0; k < icons.Count; k++) {
					PltDeviceIcon icon;
					String width, height, depth, mime, path;
					int nwidth, nheight, ndepth;
					PltXmlHelper.GetChildText (icons [k], "mimetype", out mime);
					PltXmlHelper.GetChildText (icons [k], "url", out path);

					if (Plt.IsSucceded (PltXmlHelper.GetChildText (icons [k], "width", out width)))
						Int32.TryParse (width, out nwidth);
					if (Plt.IsSucceded (PltXmlHelper.GetChildText (icons [k], "height", out height)))
						Int32.TryParse (height, out nheight);
					if (Plt.IsSucceded (PltXmlHelper.GetChildText (icons [k], "depth", out depth)))
						Int32.TryParse (depth, out ndepth);
					icon = new PltDeviceIcon (mime, nwidth, nheight, ndepth, path);
					device.Icons.Add (icon);
				}
			}

			// enumerate device services
			XmlElement serviceList = PltXmlHelper.GetChild (device_node, "serviceList");
			if (serviceList != null) {
				List<XmlElement> services = new List<XmlElement> ();
				PltXmlHelper.GetChildren (serviceList, out services, "service");
				for (int k = 0; k < services.Count; k++) {
					String type, id, url;
					PltXmlHelper.GetChildText (services [k], "serviceType", out type);
					PltXmlHelper.GetChildText (services [k], "serviceId", out id);    
					PltService service = new PltService (device, type, id, null);

					PltXmlHelper.GetChildText (services [k], "SCPDURL", out url);
					service.SetSCPDURL (url);

					PltXmlHelper.GetChildText (services [k], "controlURL", out url);
					service.SetControlURL (url);

					PltXmlHelper.GetChildText (services [k], "eventSubURL", out url);
					service.SetEventSubURL (url);

					device.AddService (service);
				}
			}

			// enumerate embedded devices
			XmlElement deviceList = PltXmlHelper.GetChild (device_node, "deviceList");
			if (deviceList != null) {
				List<XmlElement> devices = new List<XmlElement> ();
				PltXmlHelper.GetChildren (deviceList, out devices, "device");
				for (int k = 0; k < devices.Count; k++) {    
					// create an embedded device with same url base and leasetime as parent
					PltDeviceData embedded_device = new PltDeviceData (device.URLDescription, "", device.LeaseTime,"","");
					Plt.CheckSevere (PltDeviceData.SetDescriptionDevice (embedded_device, devices [k], context));
					device.AddEmbeddedDevice (embedded_device);
				}
			}

			// TODO: Parse extra DLNA stuff

			return true;
		}

	}


	 

	class TypeDeviceFinder:IFinder<PltDeviceData>
	{
		public string Type{ get; set; }

		public TypeDeviceFinder (String type)
		{
			Type = type;
		}

		public Boolean IsFound (PltDeviceData dev1)
		{
			if (dev1.DeviceType.Equals (Type)) {
				return true;
			}
			return false;
		}
	}

	public class UUIDDeviceFinder:IFinder<PltDeviceData>
	{
		public string UUID{ get; set; }

		public UUIDDeviceFinder (String uuid)
		{
			UUID = uuid;
		}

		public Boolean IsFound (PltDeviceData dev1)
		{
			if (dev1.UUID.Equals (UUID)) {
				return true;
			}
			return false;
		}
	}


	public class PltDeviceIcon
	{
		public String MimeType{ get; set; }

		public int Width{ get; set; }

		public int	Height{ get; set; }

		public int Depth{ get; set; }

		public String UrlPath{ get; set; }

		public	PltDeviceIcon (String mimeType = "", int   width = 0, int   height = 0,	int   depth = 0, String urlPath = "")
		{
			MimeType = mimeType;
			Width = width;
			Height = height;
			Depth = depth;
			UrlPath = urlPath;
		}
	}

	public class PltDeviceDataFinderByType:IFinder<PltDeviceData>
	{
		private String m_Type;

		public PltDeviceDataFinderByType (String type)
		{
			m_Type = type;
		}

		public Boolean IsFound (PltDeviceData data)
		{
			return data.GetType ().Equals (m_Type);
		}
	}


	public class PltServiceGetDescriptionIterator:IApplier<PltService>
	{
		XmlElement m_Parent;

		public PltServiceGetDescriptionIterator(XmlElement parent)
		{
			m_Parent = parent ;
		}

		public Boolean Apply(PltService data) {
			XmlElement service;
			return data.GetDescription(m_Parent,out service);
		}

		 

	}

	public class PltDeviceDataGetDescriptionIterator:IApplier<PltDeviceData>
	{
		XmlElement m_Parent;

		public PltDeviceDataGetDescriptionIterator(XmlElement parent)
		{
			m_Parent = parent ;
		}

		public Boolean Apply(PltDeviceData data) {			 
			return data.GetDescription(m_Parent);
		}



	};
}

