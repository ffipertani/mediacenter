﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.IO;
using System.Xml;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Platinum
{
	public class PltCtrlPoint:HttpRequestHandler,PltSsdpPacketListener,PltSsdpSearchResponseListener
	{
		List<String> m_UUIDsToIgnore = new List<String>();
		List<PltCtrlPointListener> m_ListenerList = new List<PltCtrlPointListener> ();
		PltHttpServer m_EventHttpServer;
		PltTaskManager m_TaskManager;
		Object m_Lock;
		private Thread lockThread;
		public List<PltDeviceData> m_RootDevices = new List<PltDeviceData>();
		internal List<PltEventSubscriber> m_Subscribers = new List<PltEventSubscriber>();
		String m_SearchCriteria;
		bool m_Started;
		List<PltEventNotification> m_PendingNotifications = new List<PltEventNotification>();
		List<String> m_PendingInspections = new List<String>();

		public PltCtrlPoint (String searchCriteria = "upnp:rootdevice")  // pass null to prevent repeated automatic search
		{
			m_EventHttpServer = null;
			m_TaskManager = null;
			m_SearchCriteria = searchCriteria;
			m_Started = false;
			m_Lock = new Object ();
		}

		public virtual Boolean GetPort (out int port)
		{
			if (!m_Started)
				throw new Exception ("NPT_ERROR_INVALID_STATE");

			port = m_EventHttpServer.GetPort ();
			return true;
		}

		public virtual Boolean AddListener (PltCtrlPointListener listener)
		{
			lock (m_Lock) {
				if (!m_ListenerList.Contains (listener)) {
					m_ListenerList.Add (listener);
				}
				return true;
			}
		}

		public virtual Boolean RemoveListener (PltCtrlPointListener listener)
		{
			lock (m_Lock) {
				m_ListenerList.Remove (listener);
				return true;
			}
		}


		public virtual void IgnoreUUID (String uuid)
		{
			if (!m_UUIDsToIgnore.Contains (uuid)) {
				m_UUIDsToIgnore.Add (uuid);
			}
		}

		public virtual Boolean Search ()
		{
			return Search (new HttpUri ("239.255.255.250", 1900, "*"), "upnp:rootdevice", 5, TimeSpan.FromSeconds (50), // pass NPT_TimeInterval(0.) for one time only
				new TimeSpan (0));
		}

		public virtual Boolean Search (HttpUri url, String  target)
		{
			return Search (url, target, 5, TimeSpan.FromSeconds (50), new TimeSpan (0));
		}

		public virtual Boolean Search (HttpUri url, String  target, int mx, TimeSpan frequency, TimeSpan initial_delay)
		{
			// pass NPT_TimeInterval(0.) for one time only

			if (!m_Started)
				Console.WriteLine ("NPT_ERROR_INVALID_STATE");

			List<NetworkInterface> if_list;
			 
			//	List<NetworkInterfaceAddress>::Iterator net_if_addr;

			Plt.CheckSevere (PltUPnPMessageHelper.GetNetworkInterfaces (out if_list, true));

			foreach (NetworkInterface net_if in if_list) {
				// make sure the interface is at least broadcast or multicast

				if (!net_if.SupportsMulticast) {
					continue;
				}       
				List<IPAddress> addresses = new List<IPAddress> ();
				PltUPnPMessageHelper.GetIPAddresses (net_if, out addresses);
				foreach (IPAddress ad in addresses) {
					PltSsdpSearchTask task = CreateSearchTask (url, target, mx, frequency, ad);
					m_TaskManager.StartTask (task, initial_delay);
				}
			 
			}

			if_list.Apply (new DisposerApplier<NetworkInterface> ());
			return true;
		}

		public virtual Boolean Discover ()
		{
			return Discover (new HttpUri ("239.255.255.250", 1900, "*"), "ssdp:all", 5, TimeSpan.FromSeconds (5), new TimeSpan (0));
		}

		public virtual Boolean Discover (HttpUri url, String target, int mx, TimeSpan frequency, // pass NPT_TimeInterval(0.) for one time only
		                                 TimeSpan initial_delay)
		{
			if (!m_Started)
				Console.WriteLine ("NPT_ERROR_INVALID_STATE");

			// make sure mx is at least 1
			if (mx < 1)
				mx = 1;

			// create socket
			UdpClient socket = new UdpClient ();

			// create request
			HttpRequest request = new HttpRequest (url, "M-SEARCH", HttpMessage.NPT_HTTP_PROTOCOL_1_1);
			PltUPnPMessageHelper.SetMX (request, mx);
			PltUPnPMessageHelper.SetST (request, target);
			PltUPnPMessageHelper.SetMAN (request, "\"ssdp:discover\"");
			request.Headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_USER_AGENT, PltConstants.GetInstance ().DefaultUserAgent);

			// force HOST to be the regular multicast address:port
			// Some servers do care (like WMC) otherwise they won't respond to us
			request.Headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_HOST, "239.255.255.250:1900");

			// create task
			PltThreadTask task = new PltSsdpSearchTask (socket.Client, this, request, (frequency.TotalMilliseconds > 0 && frequency.TotalMilliseconds < 5000) ? new TimeSpan (0, 0, 5) : frequency);  /* repeat no less than every 5 secs */
			return m_TaskManager.StartTask (task, initial_delay);
		}

		public virtual Boolean InspectDevice (HttpUri location, String uuid)
		{
			return InspectDevice (location, uuid, PltConstants.GetInstance ().DefaultDeviceLease);
		}

		public virtual Boolean InspectDevice (HttpUri location, String uuid, TimeSpan leasetime)
		{
			lock (m_Lock) {
				lockThread = Thread.CurrentThread;
				// check if already inspecting device
				String pending_uuid;
				if (Plt.IsSucceded (Plt.ContainerFind<String> (m_PendingInspections, new StringFinder (uuid), out pending_uuid))) {
					return true;
				}

				Plt.LogFine (String.Format ("Inspecting device \"{0}\" detected @ {1}", uuid, location.ToString ()));

				if (!location.IsValid ()) {
					Plt.LogFine (String.Format ("Invalid device description url: {1}", location.ToString ()));
					return false;
				}

				// remember that we're now inspecting the device
				m_PendingInspections.Add (uuid);

				// Start a task to retrieve the description
				PltCtrlPointGetDescriptionTask task = new PltCtrlPointGetDescriptionTask (location, this, leasetime, uuid);

				// Add a delay to make sure that we received late NOTIFY bye-bye
				TimeSpan delay = new TimeSpan (0, 0, 5);
				m_TaskManager.StartTask (task, delay);

				return true;
			}
		}

		// actions
		public virtual Boolean FindActionDesc (PltDeviceData device, String service_type, String action_name, out PltActionDesc action_desc)
		{
			action_desc = null;
			if (device == null)
				throw new Exception ("NPT_ERROR_INVALID_PARAMETERS");

			// look for the service
			PltService service;
			if (Plt.IsFailed (device.FindServiceByType (service_type, out service))) {
				Plt.LogFine (String.Format ("Service %s not found", service_type));
				return false;
			}

			action_desc = service.FindActionDesc (action_name);
			if (action_desc == null) {
				Plt.LogFine (String.Format ("Action %s not found in service", action_name));
				return false;
			}

			return true;
		}

		public virtual Boolean CreateAction (PltDeviceData device, String service_type, String action_name, out PltAction action)
		{
			if (device == null)
				throw new Exception ("NPT_ERROR_INVALID_PARAMETERS");

			lock (m_Lock) {
				lockThread = Thread.CurrentThread;
				PltActionDesc action_desc;
				Plt.CheckSevere (FindActionDesc (device, service_type, action_name, out action_desc));

				PltDeviceData root_device;
				Plt.CheckSevere (FindDevice (device.UUID, out root_device, true));

				action = new PltAction (action_desc, root_device);
				return true;
			}
		}

		public virtual Boolean InvokeAction (PltAction action, byte[] userdata = null)
		{
			if (!m_Started)
				throw new Exception ("NPT_CHECK_WARNING(NPT_ERROR_INVALID_STATE)");

			PltService service = action.GetActionDesc ().GetService ();

			// create the request
			HttpUri url = new HttpUri (service.GetControlURL (true));
			HttpRequest request = new HttpRequest (url, "POST", HttpMessage.NPT_HTTP_PROTOCOL_1_1);

			// create a memory stream for our request body
			MemoryStream stream = new MemoryStream ();
			action.FormatSoapRequest (stream);


			// set the request body
			HttpEntity entity = null;
			PltHttpHelper.SetBody (request, stream, out entity);

			entity.SetContentType ("text/xml; charset=\"utf-8\"");
			String service_type = service.GetServiceType ();
			String action_name = action.GetActionDesc ().GetName ();
			request.Headers.SetHeader ("SOAPAction", "\"" + service_type + "#" + action_name + "\"");

			// create a task to post the request
			PltCtrlPointInvokeActionTask task = new PltCtrlPointInvokeActionTask (request, this, action, userdata);

			// queue the request
			m_TaskManager.StartTask (task);

			return true;
		}

		// events
		public virtual Boolean Subscribe (PltService service, bool cancel = false, byte[] userdata = null)
		{
			lock (m_Lock) {
				lockThread = Thread.CurrentThread;
				if (!m_Started)
					throw new Exception ("NPT_CHECK_WARNING(NPT_ERROR_INVALID_STATE)");

				HttpRequest request = null;

				// make sure service is subscribable
				if (!service.IsSubscribable ())
					return false;

				// event url
				HttpUri url = new HttpUri (service.GetEventSubURL (true));

				// look for the corresponding root device & sub
				PltDeviceData root_device;
				PltEventSubscriber sub;
				Plt.CheckWarining (FindDevice (service.GetDevice ().UUID, out	root_device, true));

				// look for the subscriber with that service to decide if it's a renewal or not
				Plt.ContainerFind (m_Subscribers, new PltEventSubscriberFinderByService (service), out sub);

				if (cancel == false) {
					// renewal?
					if (sub != null) {
						PltThreadTask task = RenewSubscriber (sub);
						return m_TaskManager.StartTask (task);
					}

					Plt.LogFine (String.Format ("Subscribing to service \"{0}\" of device \"{1}\"", service.GetServiceID (), service.GetDevice ().FriendlyName));

					// prepare the callback url
					String uuid = service.GetDevice ().UUID;
					String service_id = service.GetServiceID ();
					String callback_uri = "/" + uuid + "/" + service_id;

					// create the request
					request = new HttpRequest (url, "SUBSCRIBE", HttpMessage.NPT_HTTP_PROTOCOL_1_1);
					// specify callback url using ip of interface used when 
					// retrieving device description
					HttpUri callbackUrl = new HttpUri (service.GetDevice ().LocalIfaceIp.ToString (), m_EventHttpServer.GetPort (), callback_uri);

					// set the required headers for a new subscription
					PltUPnPMessageHelper.SetNT (request, "upnp:event");
					PltUPnPMessageHelper.SetCallbacks (request, "<" + callbackUrl.ToString () + ">");
					PltUPnPMessageHelper.SetTimeOut (request, (int)PltConstants.GetInstance ().DefaultSubscribeLease.TotalSeconds);
				} else {
					Plt.LogFine (String.Format ("Unsubscribing subscriber \"%s\" for service \"{0}\" of device \"{1}\"", (sub != null ? sub.GetSID () : "unknown"), service.GetServiceID (), service.GetDevice ().FriendlyName));        

					// cancellation
					if (sub == null)
						return false;

					// create the request
					request = new HttpRequest (url, "UNSUBSCRIBE", HttpMessage.NPT_HTTP_PROTOCOL_1_1);
					PltUPnPMessageHelper.SetSID (request, sub.GetSID ());

					// remove from list now
					m_Subscribers.RemoveAll (asub => asub == sub);//.Remove(sub);
				}

				// verify we have request to send just in case
				Plt.CheckPointerSevere (request);

				// Prepare the request
				// create a task to post the request
				PltThreadTask atask = new PltCtrlPointSubscribeEventTask (request, this, root_device, service, userdata);
				m_TaskManager.StartTask (atask);

				return true;
			}
		}

		// NPT_HttpRequestHandler methods
		public override Boolean SetupResponse (HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
			if (request.Method.Equals ("NOTIFY")) {
				return ProcessHttpNotify (request, context, response);
			}

			Plt.LogWarn ("CtrlPoint received bad http request\r\n");
			response.SetStatus (412, "Precondition Failed");
			return true;
		}

		// PltSsdpSearchResponseListener methods
		public virtual Boolean ProcessSsdpSearchResponse (Boolean res, HttpRequestContext context, HttpResponse response)
		{
			Plt.CheckSevere (res);
			Plt.CheckPointerSevere (response);

			String ip_address = context.RemoteAddress.Address.ToString ();
			String protocol = response.Protocol;

			String prefix = String.Format ("PltCtrlPoint::ProcessSsdpSearchResponse from {0}:{1}", context.RemoteAddress.Address.ToString (), context.RemoteAddress.Port);
			PltHttp.Log (prefix, response);

			// any 2xx responses are ok
			if (response.GetStatusCode () / 100 == 2) {
				String st = response.Headers.GetHeaderValue ("st");
				String usn = response.Headers.GetHeaderValue ("usn");
				String ext = response.Headers.GetHeaderValue ("ext");
				Plt.CheckPointerSevere (st);
				Plt.CheckPointerSevere (usn);
				Plt.CheckPointerSevere (ext);

				String uuid;

				// if we get an advertisement other than uuid
				// verify it's formatted properly
				if (!String.Equals(usn,st)) {
					String[] components = usn.Split (new String[]{"::"}, StringSplitOptions.None);
					if (components.Length != 2)
						return false;

					if (!st.Equals (components [1]))
						return false;

					uuid = components [0].Substring (5);
				} else {
					uuid = usn.Substring (5);
				}

				if (m_UUIDsToIgnore.IndexOf (uuid) > 0) {
					Plt.LogFine (String.Format ("CtrlPoint received a search response from ourselves ({0})\n", uuid));
					return true;
				}

				return ProcessSsdpMessage (response, context, uuid);    
			}

			return false;
		}

		// PltSsdpPacketListener method
		public virtual Boolean OnSsdpPacket (HttpRequest request, HttpRequestContext context)
		{
			return ProcessSsdpNotify (request, context);
		}




		protected virtual Boolean DecomposeLastChangeVar (List<PltStateVariable> vars)
		{
			PltStateVariable lastChangeVar = null;
			if (Plt.IsSucceded (Plt.ContainerFind (vars, new PltStateVariableNameFinder ("LastChange"), out lastChangeVar))) {
				vars.Remove (lastChangeVar);
				PltService var_service = lastChangeVar.Service;
				String text = lastChangeVar.GetValue ();

				 
				XmlDocument xml = new XmlDocument ();
				try {
					xml.LoadXml (text);
				} catch (Exception e) {
					throw new Exception ("NPT_ERROR_INVALID_FORMAT" + e);
				}
				XmlElement node = xml.DocumentElement;

				 
				if (node.Name.Equals ("Event")) {
					// look for the instance with attribute id = 0
					XmlElement instance = null;
					for (int i = 0; i < node.ChildNodes.Count; i++) {
						XmlElement child = (XmlElement)node.ChildNodes [i];

						if (child.Name.Equals ("InstanceID")) {
							// extract the "val" attribute value
							String value = child.GetAttribute ("val");

							if (value != null && value.Equals ("0")) {
								instance = child;
								break;
							}
						}
					}

					// did we find an instance with id = 0 ?
					if (instance != null) {
						// all the children of the Instance node are state variables
						for (int j = 0; j < instance.ChildNodes.Count; j++) {
							XmlElement var_node = (XmlElement)instance.ChildNodes [j];
							 
							// look for the state variable in this service
							String value = var_node.GetAttribute ("val");
							PltStateVariable svar = var_service.FindStateVariable (var_node.Name);
							if (value != null && svar != null) {
								// get the value and set the state variable
								// if it succeeded, add it to the list of vars we'll event
								if (Plt.IsSucceded (svar.SetValue (value))) {
									vars.Add (svar);
									Plt.LogFine (String.Format ("LastChange var change for ({0}): {1}", svar.Name, svar.GetValue ()));
								}
							}
						}
					}
				}
				 
			}

			return true;
		}

		// methods
		public virtual Boolean Start (PltSsdpListenTask task)
		{
			if (m_Started)
				throw new Exception ("NPT_CHECK_WARNING(NPT_ERROR_INVALID_STATE)");

			m_TaskManager = new PltTaskManager ();

			m_EventHttpServer = new PltHttpServer ();
			m_EventHttpServer.AddRequestHandler (this, "/", true, true);
			m_EventHttpServer.Start ();

			// house keeping task
			m_TaskManager.StartTask (new PltCtrlPointHouseKeepingTask (this));

			// add ourselves as an listener to SSDP multicast advertisements
			task.AddListener (this);

			//    
			// use next line instead for DLNA testing, faster frequency for M-SEARCH
			//return m_SearchCriteria.GetLength()?Search(NPT_HttpUrl("239.255.255.250", 1900, "*"), m_SearchCriteria, 1, 5000):NPT_SUCCESS;
			// 

			m_Started = true;

			return m_SearchCriteria.Length > 0 ? Search (new HttpUri ("239.255.255.250", 1900, "*"), m_SearchCriteria) : true;
		}

		public virtual Boolean Stop (PltSsdpListenTask task)
		{
			if (m_Started)
				throw new Exception ("NPT_CHECK_WARNING(NPT_ERROR_INVALID_STATE)");

			m_Started = false;

			task.RemoveListener (this);

			m_EventHttpServer.Stop ();
			m_TaskManager.Abort ();

			// force remove all devices

			foreach (PltDeviceData data in m_RootDevices) {
				NotifyDeviceRemoved (data);
			}

			// we can safely clear everything without a lock
			// as there are no more tasks pending
			m_RootDevices.Clear ();
			m_Subscribers.Clear ();

			m_EventHttpServer = null;
			m_TaskManager = null;

			return true;
		}

		// SSDP & HTTP Notifications handling
		protected virtual Boolean ProcessSsdpNotify (HttpRequest request, HttpRequestContext context)
		{
			// get the address of who sent us some data back
			String ip_address = context.RemoteAddress.Address.ToString ();
			String method = request.Method;
			String uri = request.Url.Uri.AbsolutePath;
			String protocol = request.Protocol;

			if (method.Equals ("NOTIFY")) {
				String nts = PltUPnPMessageHelper.GetNTS (request);
				String nt = PltUPnPMessageHelper.GetNT (request);
				String usn = PltUPnPMessageHelper.GetUSN (request);

				String prefix = String.Format ("PltCtrlPoint::ProcessSsdpNotify from {0}:{1} ({2})",	
					                context.RemoteAddress.Address.ToString (), context.RemoteAddress.Port, !String.IsNullOrEmpty (usn) ? usn : "unknown");

				PltHttp.Log (prefix, request);

				if (!uri.Equals ("*") || !protocol.Equals ("HTTP/1.1"))
					return false;

				Plt.CheckPointerSevere (nts);
				Plt.CheckPointerSevere (nt);
				Plt.CheckPointerSevere (usn);

				String uuid;

				// if we get an advertisement other than uuid
				// verify it's formatted properly
				if (usn != nt) {
					String[] components = usn.Split ("::".ToCharArray ());
					if (components.Length != 2)
						return false;

					if (nt.Equals (components [1]))
						return false;

					uuid = components [0].Substring (5);
				} else {
					uuid = usn.Substring (5);
				}

				if (m_UUIDsToIgnore.IndexOf (uuid) > 0) {
					Plt.LogFine (String.Format ("Received a NOTIFY request from ourselves (%s)\n", uuid));
					return true;
				}

				// if it's a byebye, remove the device and return right away
				if (nts.Equals ("ssdp:byebye")) {
					Plt.LogFine (String.Format ("Received a byebye NOTIFY request from %s\n", uuid));

					lock (m_Lock) {
						lockThread = Thread.CurrentThread;
						// look for root device
						PltDeviceData root_device;
						FindDevice (uuid, out root_device, true);

						if (root_device != null)
							RemoveDevice (root_device);
						return true;
					}
				}

				return ProcessSsdpMessage (request, context, uuid);
			}

			return false;
		}

		protected virtual Boolean ProcessSsdpMessage (HttpMessage message, HttpRequestContext context, String uuid)
		{
			lock (m_Lock) {
				lockThread = Thread.CurrentThread;
				// check if we should ignore our own UUID
				if (m_UUIDsToIgnore.Contains (uuid))
					return true;

				String url = PltUPnPMessageHelper.GetLocation (message);
				Plt.CheckPointerSevere (url);

				// Fix for Connect360 which uses localhost in device description url
				HttpUri location = new HttpUri (url);
				if (location.Uri.Host.ToLower ().Equals ("localhost") ||
				    location.Uri.Host.ToLower ().Equals ("127.0.0.1")) {
					UriBuilder builder = new UriBuilder (location.Uri);
					builder.Host = context.RemoteAddress.Address.ToString ();
					location = new HttpUri (builder.Uri.ToString ());
				}

				// be nice and assume a default lease time if not found even though it's required
				TimeSpan leasetime;
				if (Plt.IsFailed (PltUPnPMessageHelper.GetLeaseTime (message, out leasetime))) {
					leasetime = PltConstants.GetInstance ().DefaultSubscribeLease;
				}

				// check if device (or embedded device) is already known
				PltDeviceData data;
				if (Plt.IsSucceded (FindDevice (uuid, out data))) {  

					//        // in case we missed the byebye and the device description has changed (ip or port)
					//        // reset base and assumes device is the same (same number of services and embedded devices)
					//        // FIXME: The right way is to remove the device and rescan it though but how do we know it changed?
					//        PltDeviceReadyIterator device_tester;
					//        if (NPT_SUCCEEDED(device_tester(data)) && data->GetDescriptionUrl().Compare(location.ToString(), true)) {
					//            NPT_LOG_INFO_2("Old device \"%s\" detected @ new location %s", 
					//                (const char*)data->GetFriendlyName(), 
					//                (const char*)location.ToString());
					//            data->SetURLBase(location);
					//        }

					// renew expiration time
					data.SetLeaseTime (leasetime);
					Plt.LogFine (String.Format ("Device \"{0}\" expiration time renewed..", data.FriendlyName));

					return true;
				}

				// start inspection
				return InspectDevice (location, uuid, leasetime);
			}
		}

		public virtual Boolean ProcessGetDescriptionResponse (Boolean                    res,
		                                                      HttpRequest					  request, 
		                                                      HttpRequestContext			  context,
		                                                      HttpResponse	              response,
		                                                      TimeSpan		              leasetime,
		                                                      String	                      uuid)
		{
			String desc = "";
			lock (m_Lock) {
				lockThread = Thread.CurrentThread;
				try {
					PltCtrlPointGetSCPDsTask task = null;
					 
					PltDeviceData root_device = null;
					PltDeviceData device = null;

					// Add a delay, some devices need it (aka Rhapsody)
					TimeSpan delay = new TimeSpan (0, 0, 0, 0, 100);

					String prefix = String.Format ("PltCtrlPoint::ProcessGetDescriptionResponse @ {0} (result = {1}, status = {2})", request.Url.ToString (), res, response != null ? response.GetStatusCode () : 0);

					// Remove pending inspection
					m_PendingInspections.Remove (uuid);

					// verify response was ok
					 
					Plt.CheckPointerSevere (response);

					// log response
					PltHttp.Log (prefix, response);

					// get response body
					res = PltHttpHelper.GetBody (response, out desc);
					Plt.CheckSevere (res);

					// create new root device
					Plt.CheckSevere (PltDeviceData.SetDescription (out root_device, leasetime, request.Url, desc, context));

					// make sure root device was not previously queried
					if (Plt.IsFailed (FindDevice (root_device.UUID, out device))) {
						m_RootDevices.Add (root_device);
						Plt.LogFine (String.Format ("Device \"{0}\" is now known as \"{1}\" ({2})", root_device.UUID, root_device.FriendlyName, root_device.GetDescriptionUrl (null)));

						// create one single task to fetch all scpds one after the other
						task = new PltCtrlPointGetSCPDsTask (this, root_device);
						if (!FetchDeviceSCPDs (task, root_device, 0)) {
							return false;
						}

						// if device has embedded devices, we want to delay fetching scpds
						// just in case there's a chance all the initial NOTIFY bye-bye have
						// not all been received yet which would cause to remove the devices
						// as we're adding them
						if (root_device.EmbeddedDevices.Count > 0) {
							delay = TimeSpan.FromSeconds (1);
						}

			
						if (!m_TaskManager.StartTask (task, delay)) {
							return false;
						}
					}

					return true;

				} catch (Exception e) {
					Plt.LogWarn (String.Format ("Bad Description response @ {0}: {1}", request.Url.ToString (), desc));
					return false;
				}
			}
		}



		public virtual Boolean ProcessGetSCPDResponse (Boolean	res,
		                                               HttpRequest	       			request,
		                                               HttpRequestContext 			context,
		                                               HttpResponse	            response,
		                                               PltDeviceData	      device)
		{
			 

			lock (m_Lock) {
				lockThread = Thread.CurrentThread;
				PltDeviceReadyIterator device_tester = new PltDeviceReadyIterator ();
				String scpd = "";
				PltDeviceData root_device = null;
				PltService service;

				String prefix = String.Format ("PltCtrlPoint::ProcessGetSCPDResponse for a service of device \"{0}\" @ {1} (result = {2}, status = {3})", 
					                device.FriendlyName, 
					                request.Url.ToString (),
					                res,
					                response != null ? response.GetStatusCode () : 0);

				try {
				
					Plt.CheckPointerSevere (response);

					PltHttp.Log (prefix, response);

					// make sure root device hasn't disappeared
					Plt.CheckSevere (FindDevice (device.UUID, out root_device, true));

					res = device.FindServiceBySCPDURL (request.Url.Uri.ToString (), out service);
					Plt.CheckSevere (res);

					// get response body
					res = PltHttpHelper.GetBody (response, out scpd);
					Plt.CheckSevere (res);

					// DIAL support
					if (root_device.GetType ().Equals ("urn:dial-multiscreen-org:device:dial:1")) {
						AddDevice (root_device);
						return true;
					}

					// set the service scpd
					res = service.SetSCPDXML (scpd);
					Plt.CheckSevere (res);

					// if root device is ready, notify listeners about it and embedded devices
					 
					if (Plt.IsSucceded (device_tester.Apply (root_device))) {
						AddDevice (root_device);
					}

					return true;


			
				} catch (Exception e) {
					Plt.LogWarn (String.Format ("Bad SCPD response for device \"{0}\":{1}", device.FriendlyName, scpd));
					if (root_device != null)
						RemoveDevice (root_device);
					return res;
				}
			}
		}


		public virtual Boolean ProcessActionResponse (Boolean res,
		                                              HttpRequest        request,
		                                              HttpRequestContext context,
		                                              HttpResponse             response,
		                                              PltAction          action,
		                                              byte[]                         userdata)
		{
			String service_type;
			String str;
			XmlElement xml = null;
			String name;
			String soap_action_name;
			XmlElement soap_action_response;
			XmlElement soap_body;
			XmlElement fault;
			String attr = null;
			PltActionDesc action_desc = action.GetActionDesc ();
			try {
				// reset the error code and desc
				action.SetError (0, "");

				// check context validity
				if (Plt.IsFailed (res) || response == null) {
					PltService service = action_desc.GetService ();
					Plt.LogWarn (String.Format ("Failed to reach %s for %s.%s (%d)", request.Url.ToString (), service.GetDevice ().UUID, service.GetServiceName (), res));
					return false;
				}

				PltHttp.Log ("PltCtrlPoint::ProcessActionResponse:", response);

				Plt.LogFine ("Reading/Parsing Action Response Body...");
				Plt.CheckSevere (PltHttpHelper.ParseBody (response, out xml));

				Plt.LogFine ("Analyzing Action Response Body...");

				// read envelope
				if (!xml.LocalName.Equals ("Envelope"))
					Plt.Severe ();
			
			

				// check namespace
				if (xml.NamespaceURI == null || !xml.NamespaceURI.Equals ("http://schemas.xmlsoap.org/soap/envelope/"))
					Plt.Severe ();
				 

				// check encoding
				attr = xml.GetAttribute ("encodingStyle", "http://schemas.xmlsoap.org/soap/envelope/");
				if (attr == null || !attr.Equals ("http://schemas.xmlsoap.org/soap/encoding/"))
					Plt.Severe ();
			 

				// read action
				soap_body = PltXmlHelper.GetChild (xml, "Body");
				Plt.CheckPointerSevere (soap_body);
			 

				// check if an error occurred
				fault = PltXmlHelper.GetChild (soap_body, "Fault");
				if (fault != null) {
					// we have an error
					ParseFault (action, fault);
					Plt.Severe ();
				}

				Plt.Assert (PltXmlHelper.GetChild (soap_body, out soap_action_response));					

				// verify action name is identical to SOAPACTION header
				Plt.Assert (soap_action_response.LocalName.Equals (action_desc.GetName () + "Response"));

				// verify namespace
				Plt.Assert (!String.IsNullOrEmpty (soap_action_response.NamespaceURI) && soap_action_response.NamespaceURI.Equals (action_desc.GetService ().GetServiceType ()));
		
				// read all the arguments if any
				foreach (XmlNode child in soap_action_response.ChildNodes) {
					action.SetArgumentValue (child.Name, !String.IsNullOrEmpty (child.InnerText) ? child.InnerText : "");
					Plt.CheckSevere (res);
				}
			 

				// create a buffer for our response body and call the service
				res = action.VerifyArguments (false);
				Plt.CheckSevere (res);
				 
			} catch (Exception e) {
				res = false;				 
			}
			lock (m_Lock) {
				lockThread = Thread.CurrentThread;
				m_ListenerList.Apply (new PltCtrlPointListenerOnActionResponseIterator (res, action, userdata));			 
			}
			return res;

		}


		public virtual Boolean ProcessSubscribeResponse (Boolean                    res,
		                                                 HttpRequest        request, 
		                                                 HttpRequestContext context, 
		                                                 HttpResponse             response,
		                                                 PltService                  service,
		                                                 byte[]                         userdata)
		{
	
			lock (m_Lock) {
				lockThread = Thread.CurrentThread;
				String sid = null;
				Int32 seconds;
				PltEventSubscriber sub;
				bool subscription = (request.Method.ToUpper () == "SUBSCRIBE");

				try {

					String prefix = String.Format ("PltCtrlPoint::ProcessSubscribeResponse ubscribe for service \"{0}\" (result = {1}, status code = {2})", 
						                subscription ? "S" : "Uns",
						                service.GetServiceID (),
						                res,
						                response != null ? response.GetStatusCode () : 0);

					PltHttp.Log (prefix, response);

					// if there's a failure or it's a response to a cancellation
					// we get out (any 2xx status code ok)
					if (Plt.IsFailed (res) || response == null || response.GetStatusCode () / 100 != 2) {
						return false;
					}

					if (subscription) {
						sid = PltUPnPMessageHelper.GetSID (response);
						if (sid == null || Plt.IsFailed (PltUPnPMessageHelper.GetTimeOut (response, out seconds))) {
							throw new Exception ("NPT_ERROR_INVALID_SYNTAX");
						}

						// Look for subscriber
						Plt.ContainerFind (m_Subscribers, new PltEventSubscriberFinderBySID (sid), out sub);

						Plt.LogFine (String.Format ("{0} subscriber \"{1}\" for service \"{2}\" of device \"{3}\" (timeout = {4})", sub == null ? "Updating timeout for" : "Creating new", sid, service.GetServiceID (), service.GetDevice ().FriendlyName, seconds));

						// create new subscriber if sid never seen before
						// or update subscriber expiration otherwise
						if (sub == null) {
							sub = new PltEventSubscriber (m_TaskManager, service, sid, seconds);
							m_Subscribers.Add (sub);
						} else {
							sub.SetTimeout (seconds);
						}

						// Process any pending notifcations for that subscriber we got a bit too early
						ProcessPendingEventNotifications ();

						return true;
					}
					 
			
				} catch (Exception e) {
					Plt.LogWarn (String.Format ("%subscription failed of sub \"%s\" for service \"%s\" of device \"%s\"", subscription ? "S" : "Uns", (!String.IsNullOrEmpty (sid) ? sid : "Unknown"), service.GetServiceID (), service.GetDevice ().FriendlyName));
					res = false;
				}
				// in case it was a renewal look for the subscriber with that service and remove it from the list
				if (Plt.IsSucceded (Plt.ContainerFind (m_Subscribers, new PltEventSubscriberFinderByService (service), out sub))) {
					m_Subscribers.Remove (sub);
				}
				return res;
			}
		}

		protected virtual Boolean ProcessHttpNotify (HttpRequest request, HttpRequestContext context, HttpResponse response)
		{			 
			lock (m_Lock) {
				lockThread = Thread.CurrentThread;
				List<PltStateVariable> vars;
				PltService service = null;
				PltEventSubscriber sub;
				Boolean result;

				PltHttp.Log ("PltCtrlPoint::ProcessHttpNotify:", request);
				try {
					// Create notification from request
					PltEventNotification notification = PltEventNotification.Parse (request, context, response);
					Plt.CheckPointerSevere (notification);

					// Give a last change to process pending notifications before throwing them out
					// by AddPendingNotification
					ProcessPendingEventNotifications ();

					// look for the subscriber with that sid
					if (Plt.IsFailed (Plt.ContainerFind (m_Subscribers, new PltEventSubscriberFinderBySID (notification.SID), out sub))) {
						Plt.LogWarn (String.Format ("Subscriber %s not found, delaying notification process.\n", notification.SID));
						AddPendingEventNotification (notification);
						return true;
					}

					// Process notification for subscriber
					service = sub.GetService ();
					result = ProcessEventNotification (sub, notification, out vars);


					Plt.CheckSevere (result);

					// Notify listeners
					if (vars.Count > 0) {
						m_ListenerList.Apply (new PltCtrlPointListenerOnEventNotifyIterator (service, vars));
					}
					 

		 
			
				} catch (Exception e) {
					Plt.LogWarn ("CtrlPoint received bad event notify request\r\n");
					if (response.GetStatusCode () == 200) {
						response.SetStatus (412, "Precondition Failed");
					}

				}
				return true;
			}
		}

		// Device management
		protected virtual Boolean AddDevice (PltDeviceData data)
		{
			lock (m_Lock) {
				return NotifyDeviceReady (data);
			}
		}

		protected virtual Boolean RemoveDevice (PltDeviceData data)
		{
			lock (m_Lock) {

				NotifyDeviceRemoved (data);
				CleanupDevice (data);

				return true;
			}
		}



		private PltThreadTask RenewSubscriber (PltEventSubscriber subscriber)
		{
			lock (m_Lock) {

				PltDeviceData root_device;
				if (Plt.IsFailed (FindDevice (subscriber.GetService ().GetDevice ().UUID, out root_device, true))) {
					return null;
				}

				Plt.LogFine (String.Format ("Renewing subscriber \"{0}\" for service \"{1}\" of device \"{2}\"", 
					subscriber.GetSID (),
					subscriber.GetService ().GetServiceID (),
					subscriber.GetService ().GetDevice ().FriendlyName));

				// create the request
				HttpRequest request = new HttpRequest (subscriber.GetService ().GetEventSubURL (true), "SUBSCRIBE", HttpMessage.NPT_HTTP_PROTOCOL_1_1);

				PltUPnPMessageHelper.SetSID (request, subscriber.GetSID ());
				PltUPnPMessageHelper.SetTimeOut (request, (int)PltConstants.GetInstance ().DefaultSubscribeLease.TotalSeconds);

				// Prepare the request
				// create a task to post the request
				return new PltCtrlPointSubscribeEventTask (request,	this, root_device,	subscriber.GetService ());
			}
		}

		private Boolean AddPendingEventNotification (PltEventNotification notification)
		{
			while (m_PendingNotifications.Count > 20) {
				PltEventNotification garbage = m_PendingNotifications [0];
				m_PendingNotifications.Remove (garbage);			
			}

			m_PendingNotifications.Add (notification);
			return true;
		}


		private Boolean ProcessPendingEventNotifications ()
		{
			int count = m_PendingNotifications.Count;
			while (count-- > 0) {
				List<PltStateVariable> vars = new List<PltStateVariable> ();
				PltService service = null;
				PltEventNotification notification;

				if (m_PendingNotifications.Count > 0) {
					notification = m_PendingNotifications [0];
					m_PendingNotifications.RemoveAt (0);
					PltEventSubscriber sub;

					// look for the subscriber with that sid
					if (Plt.IsFailed (Plt.ContainerFind (m_Subscribers, new  PltEventSubscriberFinderBySID (notification.SID), out sub))) {
						m_PendingNotifications.Add (notification);
						continue;
					}

					// keep track of service for listeners later
					service = sub.GetService ();

					// Reprocess notification
					Plt.LogWarn (String.Format ("Reprocessing delayed notification for subscriber", notification.SID));
					Boolean result = ProcessEventNotification (sub, notification, out vars);
					 

					if (Plt.IsFailed (result))
						continue;
				}

				// notify listeners
				if (service != null && vars.Count > 0) {
					m_ListenerList.Apply (new PltCtrlPointListenerOnEventNotifyIterator (service, vars));
				}
			}

			return true;
		}

		private Boolean ProcessEventNotification (PltEventSubscriber subscriber, PltEventNotification notification, out List<PltStateVariable> vars)
		{
			XmlElement xml = null;
			PltService service = subscriber.GetService ();
			PltDeviceData device = service.GetDevice ();
			vars = new List<PltStateVariable> ();
			String uuid = device.UUID;
			String service_id = service.GetServiceID ();
			try {
				// callback uri for this sub
				String callback_uri = "/" + uuid + "/" + service_id;

				Plt.Assert (notification.RequestUrl.Uri.AbsolutePath.Equals (callback_uri));


				// if the sequence number is less than our current one, we got it out of order
				// so we disregard it
				Plt.Assert (subscriber.GetEventKey () == 0 || notification.EventKey > subscriber.GetEventKey ());
			
				// parse body
				Plt.CheckSevere (PltXmlHelper.Parse (notification.XmlBody, out xml));

				// check envelope
				Plt.Assert (xml.Name.Equals ("propertyset"));
			

				// check property set
				// keep a vector of the state variables that changed
				XmlElement property;
				PltStateVariable avar;

				foreach (XmlElement child in xml.ChildNodes) {
					if (!child.Name.Equals ("property"))
						continue;

					Plt.CheckSevere (PltXmlHelper.GetChild (child, out property));

					avar = service.FindStateVariable (property.Name);
					if (avar == null)
						continue;

					Plt.CheckSevere (avar.SetValue (property.InnerText != null ? property.InnerText : ""));

					vars.Add (avar);
				}


			 
				// update sequence
				subscriber.SetEventKey (notification.EventKey);

				// Look if a state variable LastChange was received and decompose it into
				// independent state variable updates
				DecomposeLastChangeVar (vars);
			
				return true;
			} catch {
			
				Plt.LogWarn ("CtrlPoint failed to process event notification");
			 
				return true;
			}
		}

		public Boolean DoHouseKeeping ()
		{
			List<PltDeviceData> devices_to_remove = new List<PltDeviceData> ();

			// remove expired devices
			 
			lock (m_Lock) {

				PltDeviceData head = null;
				PltDeviceData device = null;
				while (m_RootDevices.Count > 0) {
					device = m_RootDevices [0];
					m_RootDevices.RemoveAt (0);
					DateTime last_update = device.LeaseTimeLastUpdate;
					TimeSpan lease_time = device.LeaseTime;

					// check if device lease time has expired or if failed to renew subscribers 
					// TODO: UDA 1.1 says that root device and all embedded devices must have expired
					// before we can assume they're all no longer unavailable (we may have missed the root device renew)
					DateTime now = DateTime.Now;
					if (now.CompareTo (last_update.AddSeconds (lease_time.TotalSeconds * 2)) > 0) {
						devices_to_remove.Add (device);
					} else {
						// add the device back to our list since it is still alive
						m_RootDevices.Add (device);

						// keep track of first device added back to list
						// to know we checked all devices in initial list
						if (head == null)
							head = device;
					}

					// have we exhausted initial list?
					if (head != null && head == m_RootDevices [0])
						break;
				}

			}
		 

			// remove old devices
			 
			lock (m_Lock) {
				foreach (PltDeviceData device in devices_to_remove) {
					RemoveDevice (device);
				}
			}
			

			// renew subscribers of subscribed device services
			List<PltThreadTask> tasks = new List<PltThreadTask> ();
			 
			lock (m_Lock) {
				foreach (PltEventSubscriber sub in m_Subscribers) {
					DateTime now = DateTime.Now;

					// time to renew if within 90 secs of expiration
					if (now.CompareTo (sub.GetExpirationTime ().Subtract (TimeSpan.FromSeconds(90))) > 0) {
						PltThreadTask task = RenewSubscriber (sub);
						if (task != null)
							tasks.Add (task);
					}
				}
				 
			}
			 

			// Queue up all tasks now outside of lock, in case they
			// block because the task manager has maxed out number of running tasks
			// and to avoid a deadlock with tasks trying to acquire the lock in the response
			foreach (PltThreadTask task in tasks) {
				m_TaskManager.StartTask (task);
			}

			return true;
		}


		private Boolean FetchDeviceSCPDs (PltCtrlPointGetSCPDsTask task,
		                                  PltDeviceData   device, 
		                                  int               level)
		{
			if (level == 5 && device.EmbeddedDevices.Count > 0) {
				Plt.LogFatal ("Too many embedded devices depth! ");
				return false;
			}

			++level;

			// fetch embedded devices services scpds first
			for (int i = 0; i < device.EmbeddedDevices.Count; i++) {
				Plt.CheckSevere (FetchDeviceSCPDs (task, device.EmbeddedDevices [i], level));
			}

			// Get SCPD of device services now and bail right away if one fails
			return device.Services.ApplyUntil (new PltAddGetSCPDRequestIterator (task, device),	ApplyUntil.ResultSuccess);
		}

		// Device management
		private Boolean FindDevice (String uuid, out PltDeviceData device, bool return_root = false)
		{
			device = null;
			foreach (PltDeviceData iter in m_RootDevices) {
				if (iter.UUID.Equals (uuid)) {
					device = iter;
					return true;
				} else if (Plt.IsSucceded (iter.FindEmbeddedDevice (uuid, out device))) {
					// we found the uuid as an embedded device of this root
					// return root if told, otherwise return found embedded device
					if (return_root)
						device = iter;
					return true;
				}
			}
			return false;//NPT_ERROR_NO_SUCH_ITEM;
		}

		private Boolean NotifyDeviceReady (PltDeviceData data)
		{
			m_ListenerList.Apply (new PltCtrlPointListenerOnDeviceAddedIterator (data));

			/* recursively add embedded devices */
			List<PltDeviceData> embedded_devices = data.EmbeddedDevices;
			for (int i = 0; i < embedded_devices.Count; i++) {
				NotifyDeviceReady (embedded_devices [i]);
			}

			return true;
		}

		private Boolean NotifyDeviceRemoved (PltDeviceData data)
		{
			m_ListenerList.Apply (new PltCtrlPointListenerOnDeviceRemovedIterator (data));

			/* recursively add embedded devices */
			List<PltDeviceData> embedded_devices = data.EmbeddedDevices;
			for (int i = 0; i < embedded_devices.Count; i++) {
				NotifyDeviceRemoved (embedded_devices [i]);
			}

			return true;
		}

		private Boolean CleanupDevice (PltDeviceData data)
		{
			if (data == null)
				throw new Exception ("NPT_ERROR_INVALID_PARAMETERS");

			Plt.LogFine (String.Format ("Removing %s from device list\n", data.UUID));

			// Note: This must take the lock prior to being called
			// we can't take the lock here because this function
			// will be recursively called if device contains embedded devices

			/* recursively remove embedded devices */
			List<PltDeviceData> embedded_devices = data.EmbeddedDevices;
			for (int i = 0; i < embedded_devices.Count; i++) {
				CleanupDevice (embedded_devices [i]);
			}

			/* remove from list */
			m_RootDevices.Remove (data);

			/* unsubscribe from services */
			data.Services.Apply (new PltEventSubscriberRemoverIterator (this));

			return true;
		}

		private Boolean ParseFault (PltAction action, XmlElement fault)
		{
			XmlElement detail = PltXmlHelper.GetChild (fault, "detail");

			if (detail == null)
				return false;

			XmlElement upnp_error, error_code, error_desc;
			upnp_error = PltXmlHelper.GetChild (detail, "upnp_error");

			// WMP12 Hack
			if (upnp_error == null) {
				upnp_error = PltXmlHelper.GetChild (detail, "UPnPError", PltUPnPMessageHelper.NPT_XML_ANY_NAMESPACE);
				if (upnp_error == null)
					return false;
			}

			error_code = PltXmlHelper.GetChild (upnp_error, "errorCode", PltUPnPMessageHelper.NPT_XML_ANY_NAMESPACE);
			error_desc = PltXmlHelper.GetChild (upnp_error, "errorDescription", PltUPnPMessageHelper.NPT_XML_ANY_NAMESPACE);
			Int32 code = 501;    
			String desc = "";
			if (error_code != null && !String.IsNullOrEmpty (error_code.InnerText)) {
				String value = error_code.InnerText;
				Int32.TryParse (value, out code);
			}
			if (error_desc != null && !String.IsNullOrEmpty (error_desc.InnerText)) {
				desc = error_desc.InnerText;
			}

			action.SetError (code, desc);
			return true;
		}

		PltSsdpSearchTask CreateSearchTask (HttpUri url, String  target, int mx, TimeSpan frequency, IPAddress address)
		{
			if (mx < 1)
				mx = 1;
			//Socket s = new Socket (AddressFamily.InterNetwork,SocketType.Dgram,ProtocolType.Udp); 
			 
			// create socket
			//NPT_Reference<NPT_UdpMulticastSocket> socket(new NPT_UdpMulticastSocket());

		
			UdpClient socket = null;
			// bind to something > 1024 and different than 1900
			Random random = new Random ();
			int retries = 20;
			do {    
				int arandom = random.Next ();
				int port = (1024 + (arandom % 15000));
				if (port == 1900)
					continue;

				try {
					socket = new UdpClient (new IPEndPoint (address, port));
					socket.Ttl = (short)PltConstants.GetInstance ().SearchMulticastTimeToLive;
					break;
				} catch (Exception e) {
					Console.WriteLine (e);
				}

			} while (--retries > 0);

			if (retries == 0) {
				Plt.LogFatal ("Couldn't bind socket for Search Task");
				return null;
			}

			// create request
			HttpRequest request = new HttpRequest (url, "M-SEARCH", HttpMessage.NPT_HTTP_PROTOCOL_1_1);
			PltUPnPMessageHelper.SetMX (request, mx);
			PltUPnPMessageHelper.SetST (request, target);
			PltUPnPMessageHelper.SetMAN (request, "\"ssdp:discover\"");
			request.Headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_USER_AGENT, PltConstants.GetInstance ().DefaultUserAgent);

			// create task
			PltSsdpSearchTask task = new PltSsdpSearchTask (socket.Client,	this, request,	(frequency.TotalMilliseconds > 0 && frequency.TotalMilliseconds < 5000) ? TimeSpan.FromSeconds (5) : frequency);  /* repeat no less than every 5 secs */
			//socket.Detach ();

			return task;
		}

	}

	    
	 



	 



	public interface PltCtrlPointListener
	{
		Boolean OnDeviceAdded (PltDeviceData device);

		Boolean OnDeviceRemoved (PltDeviceData device);

		Boolean OnActionResponse (Boolean res, PltAction action, byte[] userdata);

		Boolean OnEventNotify (PltService service, List<PltStateVariable> vars);
	}





	/*----------------------------------------------------------------------
|   PltCtrlPointListenerOnDeviceAddedIterator class
+---------------------------------------------------------------------*/
	class PltCtrlPointListenerOnDeviceAddedIterator:IApplier<PltCtrlPointListener>
	{
		PltDeviceData m_Device;

		public PltCtrlPointListenerOnDeviceAddedIterator (PltDeviceData device)
		{
			m_Device = device;
		}

		public Boolean Apply (PltCtrlPointListener listener)
		{
			return listener.OnDeviceAdded (m_Device);
		}


	}

	/*----------------------------------------------------------------------
|   PltCtrlPointListenerOnDeviceRemovedIterator class
+---------------------------------------------------------------------*/
	class PltCtrlPointListenerOnDeviceRemovedIterator:IApplier<PltCtrlPointListener>
	{
		PltDeviceData m_Device;

		public PltCtrlPointListenerOnDeviceRemovedIterator (PltDeviceData device)
		{
			m_Device = device;
		}

		public Boolean Apply (PltCtrlPointListener listener)
		{
			return listener.OnDeviceRemoved (m_Device);
		}

	 
	
	};

	/*----------------------------------------------------------------------
|   PltCtrlPointListenerOnActionResponseIterator class
+---------------------------------------------------------------------*/
	class PltCtrlPointListenerOnActionResponseIterator:IApplier<PltCtrlPointListener>
	{
		Boolean m_Res;
		PltAction m_Action;
		byte[] m_Userdata;


		public PltCtrlPointListenerOnActionResponseIterator (Boolean res, PltAction action, byte[] userdata)
		{
			m_Res = res;
			m_Action = action;
			m_Userdata = userdata;
		}

		public Boolean Apply (PltCtrlPointListener listener)
		{
			return listener.OnActionResponse (m_Res, m_Action, m_Userdata);
		}

	}

	class PltCtrlPointListenerOnEventNotifyIterator:IApplier<PltCtrlPointListener>
	{
		PltService m_Service;
		List<PltStateVariable> m_Vars;

		public PltCtrlPointListenerOnEventNotifyIterator (PltService service, List<PltStateVariable> vars)
		{
			m_Service = service;
			m_Vars = vars; 
		}

		public Boolean Apply (PltCtrlPointListener listener)
		{
			return listener.OnEventNotify (m_Service, m_Vars);
		}
		 

	}

	/*----------------------------------------------------------------------
|   PltAddGetSCPDRequestIterator class
+---------------------------------------------------------------------*/
	public class PltAddGetSCPDRequestIterator:IApplier<PltService>
	{
		PltCtrlPointGetSCPDsTask m_Task;
		PltDeviceData m_Device;

	 
		public PltAddGetSCPDRequestIterator (PltCtrlPointGetSCPDsTask task, PltDeviceData   device)
		{
			m_Task = task;
			m_Device = device;
		}

		public Boolean Apply (PltService service)
		{
			// look for the host and port of the device
			String scpd_url = service.GetSCPDURL (true);

			Plt.LogFine (String.Format ("Queueing SCPD request for service \"{0}\" of device \"{1}\" @ {2}", service.GetServiceID (), service.GetDevice ().FriendlyName, scpd_url));

			// verify url before queuing just in case
			HttpUri url = new HttpUri (scpd_url);
			if (!url.IsValid ()) {
				Plt.LogFatal (String.Format ("Invalid SCPD url \"{0}\" for service \"{1}\" of device \"{2}\"!", scpd_url, service.GetServiceID (), service.GetDevice ().FriendlyName));
				return false;
			}

			// Create request and attach service to it
			PltCtrlPointGetSCPDRequest request = new PltCtrlPointGetSCPDRequest (m_Device, scpd_url, "GET", HttpMessage.NPT_HTTP_PROTOCOL_1_1);
			return m_Task.AddSCPDRequest (request);
		}
		 

	};

	/*----------------------------------------------------------------------
|   PltEventSubscriberRemoverIterator class
+---------------------------------------------------------------------*/
	// Note: The PltCtrlPoint::m_Lock must be acquired prior to using any
	// function such as Apply on this iterator
	class PltEventSubscriberRemoverIterator:IApplier<PltService>
	{
		PltCtrlPoint m_CtrlPoint;

		public PltEventSubscriberRemoverIterator (PltCtrlPoint ctrl_point)
		{
			m_CtrlPoint = ctrl_point;
		}

		public Boolean Apply (PltService service)
		{
			PltEventSubscriber sub;
			if (Plt.IsSucceded (Plt.ContainerFind (m_CtrlPoint.m_Subscribers, new PltEventSubscriberFinderByService (service), out sub))) {
				Plt.LogFine (String.Format ("Removed subscriber \"%s\"", sub.GetSID ()));
				m_CtrlPoint.m_Subscribers.Remove (sub);
			}

			return true;
		}
	}

	/*----------------------------------------------------------------------
|   PltServiceReadyIterator class
+---------------------------------------------------------------------*/
	class PltServiceReadyIterator:IApplier<PltService>
	{
		public	PltServiceReadyIterator ()
		{
		}

		public Boolean Apply (PltService service)
		{
			return service.IsValid () ? true : false;
		}
	}

	/*----------------------------------------------------------------------
|   PltDeviceReadyIterator class
+---------------------------------------------------------------------*/
	class PltDeviceReadyIterator:IApplier<PltDeviceData>
	{
		public	PltDeviceReadyIterator ()
		{
		}

		public Boolean Apply (PltDeviceData device)
		{
			Boolean res = device.Services.ApplyUntil (new PltServiceReadyIterator (), ApplyUntil.ResultSuccess);
			if (Plt.IsFailed (res))
				return res;

			res = device.EmbeddedDevices.ApplyUntil (new PltDeviceReadyIterator (), ApplyUntil.ResultSuccess);
			if (Plt.IsFailed (res))
				return res;

			// a device must have at least one service or embedded device 
			// otherwise it's not ready
			if (device.Services.Count == 0 && device.EmbeddedDevices.Count == 0) {
				return false;
			}

			return true;
		}
	}










}

