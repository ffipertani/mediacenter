﻿using System;

namespace Platinum
{
	public class PltConstants
	{

		private static PltConstants instance = new PltConstants();

		public TimeSpan DefaultDeviceLease{ get; set;}
		public TimeSpan DefaultSubscribeLease{get;set;}
		public String DefaultUserAgent{get;set;}
		public int SearchMulticastTimeToLive{get;set;}
		public int AnnounceMulticastTimeToLive{get;set;}

		public PltConstants ()
		{
			DefaultUserAgent = PltHttp.PLT_HTTP_DEFAULT_USER_AGENT;
			DefaultDeviceLease = new TimeSpan(0,0,1800);
			DefaultSubscribeLease = new TimeSpan(0,0,1800);
			AnnounceMulticastTimeToLive = 2;
			SearchMulticastTimeToLive = 2;
		}
			
		public static PltConstants GetInstance(){
			return instance;
		}
			
	}
}

