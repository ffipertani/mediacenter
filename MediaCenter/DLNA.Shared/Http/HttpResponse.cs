﻿using System;
using System.IO;

namespace Platinum
{
	public class HttpResponse:HttpMessage
	{
		uint m_StatusCode;
		String m_ReasonPhrase;

		public HttpResponse ()
		{
		}
		public HttpResponse (uint status_code, String reason_phrase) : this (status_code, reason_phrase,HttpMessage.NPT_HTTP_PROTOCOL_1_0)
		{
		}

		public HttpResponse (uint status_code, String reason_phrase, String protocol) : base (protocol)
		{
			m_StatusCode = status_code;
			m_ReasonPhrase = reason_phrase;
		}

		public static Boolean Parse (Stream stream, out HttpResponse response)
		{
			response = null;
			try {
				// default return value

				// read the response line
				String line = ReadLine (stream);
				if (line == null || line.Length == 0) {
					return false;
				}
			 

				Plt.LogFine (String.Format ("http response: {0}", line));

				// check the response line
				// we are lenient here, as we allow the response to deviate slightly from
				// strict HTTP (for example, ICY servers response with a method equal to
				// ICY insead of HTTP/1.X
				int first_space = line.IndexOf (' ');
				if (first_space < 1)
					throw new Exception ("NPT_ERROR_HTTP_INVALID_RESPONSE_LINE");
				int second_space = line.Substring (first_space + 1).IndexOf (' ');
				if (second_space < 0) {
					// some servers omit (incorrectly) the space and Reason-Code 
					// but we don't fail them just for that. Just check that the
					// status code looks ok
					if (line.Length != 12) {
						throw new Exception ("NPT_ERROR_HTTP_INVALID_RESPONSE_LINE");
					}
				} else if (second_space  != 3) {
					// the status code is not of length 3
					throw new Exception ("NPT_ERROR_HTTP_INVALID_RESPONSE_LINE");
				}

				// parse the response line
				String protocol = line.Substring (0, first_space);
				String status_code = line.Substring (first_space + 1, 3);
				String reason_phrase = line.Substring ((first_space+1) + (second_space+1) );

				// create a response object

				UInt32 status_code_int = 0;
				UInt32.TryParse (status_code, out status_code_int);
				response = new HttpResponse (status_code_int, reason_phrase, protocol);

				// parse headers
				Boolean result = response.ParseHeaders (stream);

				if (Plt.IsFailed (result)) {			 
					response = null;
				}

				return result;
			} catch (Exception e) {
				Console.Error.WriteLine (e);
				return false;
			}
		}


	 
		public Boolean	SetStatus (uint status_code, String reason_phrase, String protocol = null)
		{
			m_StatusCode = status_code;
			m_ReasonPhrase = reason_phrase;
			if (protocol!=null)
				Protocol = protocol;
			return true;
		}

		public Boolean SetProtocol (String protocol)
		{
			Protocol = protocol;
			return true;
		}

		public uint GetStatusCode ()
		{
			return m_StatusCode;   
		}

		public String  GetReasonPhrase ()
		{
			return m_ReasonPhrase; 
		}

		public virtual Boolean Emit (Stream stream)
		{
			MemoryStream ms = new MemoryStream ();
			StreamWriter writer = new StreamWriter (ms);
			writer.Write (Protocol);
			writer.Write (" ");
			writer.Write (m_StatusCode);
			writer.Write (" ");
			writer.Write (m_ReasonPhrase);
			writer.Write (HttpMessage.NPT_HTTP_LINE_TERMINATOR);
			writer.Flush ();
			// emit headers
			Headers.Emit (ms);

			// finish with an empty line
			writer.Write (HttpMessage.NPT_HTTP_LINE_TERMINATOR);
			writer.Flush ();

			ms.Position = 0;
		//	ms.CopyTo (stream);
			byte[] buf = ms.GetBuffer ();
			stream.Write (buf, 0, (int)ms.Length);


			return true;
		}

	}
}

