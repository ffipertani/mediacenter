﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Platinum
{
	public class HttpMessage
	{
		public static String NPT_HTTP_LINE_TERMINATOR = "\r\n";

		public static String NPT_HTTP_PROTOCOL_1_0 = "HTTP/1.0";
		public static String NPT_HTTP_PROTOCOL_1_1 = "HTTP/1.1";
		public static String NPT_HTTP_METHOD_GET = "GET";
		public static String NPT_HTTP_METHOD_HEAD = "HEAD";
		public static String NPT_HTTP_METHOD_POST = "POST";
		public static String NPT_HTTP_METHOD_PUT = "PUT";
		public static String NPT_HTTP_METHOD_OPTIONS = "OPTIONS";
		public static String NPT_HTTP_METHOD_DELETE = "DELETE";
		public static String NPT_HTTP_METHOD_TRACE = "TRACE";


		public static String NPT_HTTP_HEADER_HOST = "Host";
		public static String NPT_HTTP_HEADER_CONNECTION = "Connection";
		public static String NPT_HTTP_HEADER_USER_AGENT = "User-Agent";
		public static String NPT_HTTP_HEADER_SERVER = "Server";
		public static String NPT_HTTP_HEADER_CONTENT_LENGTH = "Content-Length";
		public static String NPT_HTTP_HEADER_CONTENT_TYPE = "Content-Type";
		public static String NPT_HTTP_HEADER_CONTENT_ENCODING = "Content-Encoding";
		public static String NPT_HTTP_HEADER_TRANSFER_ENCODING = "Transfer-Encoding";
		public static String NPT_HTTP_HEADER_LOCATION = "Location";
		public static String NPT_HTTP_HEADER_RANGE = "Range";
		public static String NPT_HTTP_HEADER_CONTENT_RANGE = "Content-Range";
		public static String NPT_HTTP_HEADER_COOKIE = "Cookie";
		public static String NPT_HTTP_HEADER_ACCEPT_RANGES = "Accept-Ranges";
		public static String NPT_HTTP_HEADER_AUTHORIZATION = "Authorization";


		public static int NPT_URL_INVALID_PORT = 0;
		public static int NPT_URL_DEFAULT_HTTP_PORT = 80;
		public static int NPT_URL_DEFAULT_HTTPS_PORT = 443;

		public static String NPT_HTTP_TRANSFER_ENCODING_CHUNKED = "chunked";


		private HttpHeaders headers = new HttpHeaders ();

		public HttpMessage () : this (NPT_HTTP_PROTOCOL_1_0)
		{
		}


		public HttpMessage (String protocol)
		{
			Protocol = protocol;
		}

		public HttpEntity Entity{ get; set; }

		public String Protocol{ get; set; }

		public HttpHeaders  Headers {
			get {
				return headers;
			}
		}


		protected static String ReadLine(Stream stream){
			byte[] buffer = new byte[9000];
			String line=null;
			int index = 0;
			while (true) {
				int res = stream.Read (buffer, index, 1);

				if (index>0 && ((char)buffer [index - 1]) == '\r' && (((char)buffer [index]) == '\n')) {
					line = System.Text.Encoding.UTF8.GetString (buffer, 0, index);
					line = line.Replace("\r","");
					break;
				}
				index++;
				if (res == 0) {
					break;
				}
			}
			return line;
		}


		protected virtual Boolean ParseHeaders (Stream stream)
		{

			return Headers.Parse (stream);
		}
	}
}

