﻿using System;
using System.IO;

namespace Platinum
{
	public class HttpChunkedStreamWriter:Stream
	{
		private Stream baseStream;

		public HttpChunkedStreamWriter (Stream stream)
		{
			baseStream = stream;


		}

		public override bool CanRead {
			get {
				return true;
			}
		}

		public override bool CanWrite {
			get {
				return true;
			}
		}

		public override bool CanSeek {
			get {
				return baseStream.CanSeek;
			}
		}

		public override long Seek (long offset, SeekOrigin origin)
		{
			return baseStream.Seek (offset, origin);
		}

		public override void Flush ()
		{
			baseStream.Flush ();
		}

		public override long Position {
			get {
				return baseStream.Position;
			}
			set {
				baseStream.Position = value;
			}
		}

		public override void SetLength (long value)
		{
			baseStream.SetLength (value);
		}

		public override long Length {
			get {
				return baseStream.Length;
			}
		}

		public override int Read (byte[] buffer, int offset, int count)
		{
			throw new NotImplementedException ();
		}

		public override void Write (byte[] buffer, int offset, int count)
		{
			int bytes_written	= 0;
			// default values
			if (bytes_written > 0)
				bytes_written = 0;

			// shortcut
			if (count == 0)
				return;

			// write the chunk header
			char[] size = new char[16];
			size [15] = '\n';
			size [14] = '\r';
			char c = size [14];
			int char_count = 2;
			int value = count;
			int index = 14;
			do {
				int digit = (int)(value % 16);
				if (digit < 10) {
					size [--index] = (char)(((int)'0') + digit);		//Questl'ho fatto io
				} else {
					size [--index] = (char)(((int)'A') + digit - 10);	//Questl'ho fatto io CONTROLLARE!!!
				}
				char_count++;
				value /= 16;
			} while(value > 0);
			StreamWriter writer = new StreamWriter (baseStream);
			writer.Write (size, 0, char_count);			
			writer.Flush ();
			baseStream.Write (buffer, 0, count);		
			baseStream.Flush ();
			writer.Write (HttpMessage.NPT_HTTP_LINE_TERMINATOR);
			writer.Flush ();
		}


	}
}

