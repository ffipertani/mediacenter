﻿using System;
using System.Collections.Generic;

namespace Platinum
{
	public class MediaController:PltCtrlPointListener
	{
		private PltCtrlPoint m_CtrlPoint;
		private PltMediaControllerDelegate m_Delegate;
		private List<PltDeviceData> m_MediaRenderers = new List<PltDeviceData> ();

		public MediaController (PltCtrlPoint ctrl_point, PltMediaControllerDelegate adelegate = null)
		{
			m_CtrlPoint = ctrl_point;
			m_Delegate = adelegate;
			m_CtrlPoint.AddListener (this);
		}

		public MediaController ()
		{
		}

		public void Dispose ()
		{
			m_CtrlPoint.RemoveListener (this);
		}

		// public methods
		public virtual void SetDelegate (PltMediaControllerDelegate adelegate)
		{
			m_Delegate = adelegate;
		}

	 

		public virtual Boolean OnDeviceAdded (PltDeviceData device)
		{
			// verify the device implements the function we need
			PltService serviceAVT = null;
			PltService serviceCMR;
			PltService serviceRC;
			String type;

			if (!device.DeviceType.StartsWith ("urn:schemas-upnp-org:device:MediaRenderer"))
				return false;

			// optional service
			type = "urn:schemas-upnp-org:service:AVTransport:*";
			if (Plt.IsSucceded (device.FindServiceByType (type, out serviceAVT))) {
				// in case it's a newer upnp implementation, force to 1
				Plt.LogFine (String.Format ("Service {0} found", type));
				serviceAVT.ForceVersion (1);
			}

			// required services
			type = "urn:schemas-upnp-org:service:ConnectionManager:*";
			if (Plt.IsFailed (device.FindServiceByType (type, out serviceCMR))) {
				Plt.LogFine (String.Format ("Service {0} not found", type));
				return false;
			} else {
				// in case it's a newer upnp implementation, force to 1
				serviceCMR.ForceVersion (1);
			}

			type = "urn:schemas-upnp-org:service:RenderingControl:*";
			if (Plt.IsFailed (device.FindServiceByType (type, out serviceRC))) {
				Plt.LogFine (String.Format ("Service {0} not found", type));
				return false;
			} else {
				// in case it's a newer upnp implementation, force to 1
				serviceRC.ForceVersion (1);
			}

			 
			lock (m_MediaRenderers) {

				PltDeviceData data;
				String uuid = device.UUID;

				// is it a new device?
				if (Plt.IsSucceded (Plt.ContainerFind (m_MediaRenderers, new  UUIDDeviceFinder (uuid), out data))) {
					Plt.LogWarn (String.Format ("Device ({0}) is already in our list!", uuid));
					return false;
				}

				Plt.LogFine (String.Format ("Device Found: {0}", device));

				m_MediaRenderers.Add (device);
			}

			if (m_Delegate != null && m_Delegate.OnMRAdded (device)) {
				// subscribe to services eventing only if delegate wants it
				if (serviceAVT != null)
					m_CtrlPoint.Subscribe (serviceAVT);

				// subscribe to required services
				m_CtrlPoint.Subscribe (serviceCMR);
				m_CtrlPoint.Subscribe (serviceRC);
			}

			return true;
		}

		public virtual Boolean OnDeviceRemoved (PltDeviceData device)
		{
			if (!device.DeviceType.StartsWith ("urn:schemas-upnp-org:device:MediaRenderer"))
				return false;


			lock (m_MediaRenderers) {

				// only release if we have kept it around
				PltDeviceData data;
				String uuid = device.UUID;

				// Have we seen that device?
				if (Plt.IsFailed (Plt.ContainerFind (m_MediaRenderers, new  UUIDDeviceFinder (uuid), out data))) {
					Plt.LogWarn (String.Format ("Device ({0}) not found in our list!", uuid));
					return false;
				}

				Plt.LogFine (String.Format ("Device Removed: {0}", device));

				m_MediaRenderers.Remove (device);
			}

			if (m_Delegate != null) {
				m_Delegate.OnMRRemoved (device);
			}

			return true;
		}

		public virtual Boolean OnActionResponse (Boolean res, PltAction action, byte[]userdata)
		{
			if (m_Delegate == null)
				return true;

			PltDeviceData device;
			String uuid = action.GetActionDesc ().GetService ().GetDevice ().UUID;

			/* extract action name */
			String actionName = action.GetActionDesc ().GetName ();

			/* AVTransport response ? */
			if (actionName.EqualsIgnoreCase ("GetCurrentTransportActions")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				return OnGetCurrentTransportActionsResponse (res, device, action, userdata);
			} else if (actionName.EqualsIgnoreCase ("GetDeviceCapabilities")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				return OnGetDeviceCapabilitiesResponse (res, device, action, userdata);
			} else if (actionName.EqualsIgnoreCase ("GetMediaInfo")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				return OnGetMediaInfoResponse (res, device, action, userdata);
			} else if (actionName.EqualsIgnoreCase ("GetPositionInfo")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				return OnGetPositionInfoResponse (res, device, action, userdata);
			} else if (actionName.EqualsIgnoreCase ("GetTransportInfo")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				return OnGetTransportInfoResponse (res, device, action, userdata);
			} else if (actionName.EqualsIgnoreCase ("GetTransportSettings")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				return OnGetTransportSettingsResponse (res, device, action, userdata);
			} else if (actionName.EqualsIgnoreCase ("Next")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				m_Delegate.OnNextResult (res, device, userdata);
			} else if (actionName.EqualsIgnoreCase ("Pause")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				m_Delegate.OnPauseResult (res, device, userdata);
			} else if (actionName.EqualsIgnoreCase ("Play")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				m_Delegate.OnPlayResult (res, device, userdata);
			} else if (actionName.EqualsIgnoreCase ("Previous")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				m_Delegate.OnPreviousResult (res, device, userdata);
			} else if (actionName.EqualsIgnoreCase ("Seek")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				m_Delegate.OnSeekResult (res, device, userdata);
			} else if (actionName.EqualsIgnoreCase ("SetAVTransportURI")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				m_Delegate.OnSetAVTransportURIResult (res, device, userdata);
			} else if (actionName.EqualsIgnoreCase ("SetPlayMode")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				m_Delegate.OnSetPlayModeResult (res, device, userdata);
			} else if (actionName.EqualsIgnoreCase ("Stop")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				m_Delegate.OnStopResult (res, device, userdata);
			} else if (actionName.EqualsIgnoreCase ("GetCurrentConnectionIDs")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				return OnGetCurrentConnectionIDsResponse (res, device, action, userdata);
			} else if (actionName.EqualsIgnoreCase ("GetCurrentConnectionInfo")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				return OnGetCurrentConnectionInfoResponse (res, device, action, userdata);
			} else if (actionName.EqualsIgnoreCase ("GetProtocolInfo")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				return OnGetProtocolInfoResponse (res, device, action, userdata);
			} else if (actionName.EqualsIgnoreCase ("SetMute")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				m_Delegate.OnSetMuteResult (res, device, userdata);
			} else if (actionName.EqualsIgnoreCase ("GetMute")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				return OnGetMuteResponse (res, device, action, userdata);
			} else if (actionName.EqualsIgnoreCase ("SetVolume")) { 
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				m_Delegate.OnSetVolumeResult (res, device, userdata);
			} else if (actionName.EqualsIgnoreCase ("GetVolume")) {
				if (Plt.IsFailed (FindRenderer (uuid, out device)))
					res = false;
				return OnGetVolumeResponse (res, device, action, userdata);
			}

			return true;
		}

		public virtual Boolean OnEventNotify (PltService service, List<PltStateVariable> vars)
		{
			if (!service.GetDevice ().DeviceType.StartsWith ("urn:schemas-upnp-org:device:MediaRenderer"))
				return false;

			if (m_Delegate == null)
				return true;

			/* make sure device associated to service is still around */
			PltDeviceData data;
			Plt.CheckWarining (FindRenderer (service.GetDevice ().UUID, out data));

			m_Delegate.OnMRStateVariablesChanged (service, vars);
			return true;
		}

		// AVTransport
		public Boolean GetCurrentTransportActions (PltDeviceData device, int instance_id, byte[] userdata)
		{
			 
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "GetCurrentTransportActions", out action));
			return InvokeActionWithInstance (action, instance_id, userdata);
			 
		}

		public Boolean GetDeviceCapabilities (PltDeviceData device, int instance_id, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "GetDeviceCapabilities", out action));
			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean GetMediaInfo (PltDeviceData device, int instance_id, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "GetMediaInfo", out	action));
			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean GetPositionInfo (PltDeviceData device, int instance_id, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "GetPositionInfo", out action));
			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean GetTransportInfo (PltDeviceData device, int instance_id, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "GetTransportInfo", out	action));
			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean GetTransportSettings (PltDeviceData device, int instance_id, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "GetTransportSettings", out	action));
			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean Next (PltDeviceData device, int instance_id, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "Next", out action));
			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean Pause (PltDeviceData device, int instance_id, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "Pause", out action));
			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean Play (PltDeviceData  device, int instance_id, String speed, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "Play", out action));

			// Set the speed
			if (Plt.IsFailed (action.SetArgumentValue ("Speed", speed))) {
				return false;// NPT_ERROR_INVALID_PARAMETERS;
			}

			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean Previous (PltDeviceData device, int instance_id, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "Previous", out	action));
			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean Seek (PltDeviceData  device, int instance_id, String unit, String target, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "Seek", out	action));

			// Set the unit
			if (Plt.IsFailed (action.SetArgumentValue ("Unit", unit))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			// Set the target
			if (Plt.IsFailed (action.SetArgumentValue ("Target", target))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public bool       CanSetNextAVTransportURI (PltDeviceData device)
		{
			if (device == null)
				return false;

			PltActionDesc action_desc;
			Boolean result = m_CtrlPoint.FindActionDesc (device,	"urn:schemas-upnp-org:service:AVTransport:1", "SetNextAVTransportURI", out action_desc);
			return result;
		}

		public Boolean SetAVTransportURI (PltDeviceData device, int instance_id, String uri, String metadata, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "SetAVTransportURI", out action));

			// set the uri
			if (Plt.IsFailed (action.SetArgumentValue ("CurrentURI", uri))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			// set the uri metadata
			if (Plt.IsFailed (action.SetArgumentValue ("CurrentURIMetaData", metadata))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean SetNextAVTransportURI (PltDeviceData device, int instance_id, String next_uri, String next_metadata, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "SetNextAVTransportURI", out action));

			// set the uri
			if (Plt.IsFailed (action.SetArgumentValue ("NextURI", next_uri))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			// set the uri metadata
			if (Plt.IsFailed (action.SetArgumentValue ("NextURIMetaData", next_metadata))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean SetPlayMode (PltDeviceData  device, int instance_id, String new_play_mode, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "SetPlayMode", out action));

			// set the New PlayMode
			if (Plt.IsFailed (action.SetArgumentValue ("NewPlayMode", new_play_mode))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean Stop (PltDeviceData device, int instance_id, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:AVTransport:1", "Stop", out action));
			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		// ConnectionManager
		public Boolean GetCurrentConnectionIDs (PltDeviceData device, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:ConnectionManager:1", "GetCurrentConnectionIDs", out action));

			// set the arguments on the action, this will check the argument values
			if (Plt.IsFailed (m_CtrlPoint.InvokeAction (action, userdata))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			return true;
		}

		public Boolean GetCurrentConnectionInfo (PltDeviceData device, int connection_id, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:ConnectionManager:1", "GetCurrentConnectionInfo", out action));

			// set the New PlayMode
			if (Plt.IsFailed (action.SetArgumentValue ("ConnectionID", connection_id.ToString ()))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			// set the arguments on the action, this will check the argument values
			if (Plt.IsFailed (m_CtrlPoint.InvokeAction (action, userdata))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			return true;
		}

		public Boolean GetProtocolInfo (PltDeviceData device, byte[] userdata)
		{

			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:ConnectionManager:1", "GetProtocolInfo", out action));

			// set the arguments on the action, this will check the argument values
			if (Plt.IsFailed (m_CtrlPoint.InvokeAction (action, userdata))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			return true;
		}

		// RenderingControl
		public Boolean SetMute (PltDeviceData device, int instance_id, String channel, bool mute, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:RenderingControl:1", "SetMute", out	action));

			// set the channel
			if (Plt.IsFailed (action.SetArgumentValue ("Channel", channel))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			// set the channel
			if (Plt.IsFailed (action.SetArgumentValue ("DesiredMute", mute ? "1" : "0"))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean GetMute (PltDeviceData device, int instance_id, String channel, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:RenderingControl:1", "GetMute", out action));

			// set the channel
			if (Plt.IsFailed (action.SetArgumentValue ("Channel", channel))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean SetVolume (PltDeviceData device, int instance_id, String channel, int volume, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:RenderingControl:1", "SetVolume", out action));

			// set the channel
			if (Plt.IsFailed (action.SetArgumentValue ("Channel", channel))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			if (Plt.IsFailed (action.SetArgumentValue ("DesiredVolume", volume.ToString ()))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean GetVolume (PltDeviceData device, int instance_id, String channel, byte[] userdata)
		{
			PltAction action;
			Plt.CheckSevere (m_CtrlPoint.CreateAction (device, "urn:schemas-upnp-org:service:RenderingControl:1", "GetVolume", out action));

			// set the channel
			if (Plt.IsFailed (action.SetArgumentValue ("Channel", channel))) {
				return false;//NPT_ERROR_INVALID_PARAMETERS;
			}

			return InvokeActionWithInstance (action, instance_id, userdata);
		}

		public Boolean FindRenderer (String uuid, out PltDeviceData device)
		{
			lock (m_MediaRenderers) {

				if (Plt.IsFailed (Plt.ContainerFind (m_MediaRenderers, new UUIDDeviceFinder (uuid), out device))) {
					Plt.LogFine (String.Format ("Device ({0}) not found in our list of renderers", uuid));
					return false;
				}

				return true;
			}
		}


		// VariableStates
		public virtual Boolean GetProtocolInfoSink (String device_uuid, out String[] sinks)
		{
			PltDeviceData renderer;
			Plt.CheckWarining (FindRenderer (device_uuid, out renderer));

			// look for ConnectionManager service
			PltService serviceCMR;
			Plt.CheckSevere (renderer.FindServiceByType ("urn:schemas-upnp-org:service:ConnectionManager:*", out serviceCMR));

			String value;
			Plt.CheckSevere (serviceCMR.GetStateVariableValue ("SinkProtocolInfo", out value));

			sinks = value.Split (',');
			return true;
		}

		public virtual Boolean GetTransportState (String  device_uuid, out String state)
		{
			PltDeviceData renderer;
			Plt.CheckWarining (FindRenderer (device_uuid, out renderer));

			// look for AVTransport service
			PltService serviceAVT;
			Plt.CheckSevere (renderer.FindServiceByType ("urn:schemas-upnp-org:service:AVTransport:*", out serviceAVT));

			Plt.CheckSevere (serviceAVT.GetStateVariableValue ("TransportState", out state));

			return true;
		}

		public virtual Boolean GetVolumeState (String  device_uuid, out int volume)
		{
			PltDeviceData renderer;
			Plt.CheckWarining (FindRenderer (device_uuid, out renderer));

			// look for RenderingControl service
			PltService serviceRC;
			Plt.CheckSevere (renderer.FindServiceByType ("urn:schemas-upnp-org:service:RenderingControl:*", out serviceRC));

			String value;
			Plt.CheckSevere (serviceRC.GetStateVariableValue ("Volume", out value));

			return Int32.TryParse (value, out volume);
		}

		// methods
	 
		public virtual Boolean FindMatchingProtocolInfo (String[] sinks, String protocol_info)
		{
			PltProtocolInfo protocol = new PltProtocolInfo (protocol_info);
			foreach (String str in sinks) {
				PltProtocolInfo sink = new PltProtocolInfo (str);
				if (sink.Match (protocol)) {
					return true;
				}
			}
			return false;// NPT_ERROR_NO_SUCH_ITEM;
		}

		public virtual Boolean FindBestResource (PltDeviceData device, PltMediaObject item, int resource_index)
		{
			if (item.m_Resources.Count <= 0)
				return false;//  NPT_ERROR_INVALID_PARAMETERS;

			String[] sinks;
			Plt.CheckSevere (GetProtocolInfoSink (device.UUID, out sinks));

			// look for best resource
			for (int i = 0; i < item.m_Resources.Count; i++) {
				if (Plt.IsSucceded (FindMatchingProtocolInfo (sinks, item.m_Resources [i].ProtocolInfo.ToString ()))) {
					resource_index = i;
					return true;
				}
			}

			return false;//NPT_ERROR_NO_SUCH_ITEM;
		}

		private Boolean InvokeActionWithInstance (PltAction action, int instance_id, byte[]userdata = null)
		{
			// Set the object id
			Plt.CheckSevere (action.SetArgumentValue ("InstanceID", instance_id.ToString ()));

			// set the arguments on the action, this will check the argument values
			return m_CtrlPoint.InvokeAction (action, userdata);
		}

		protected Boolean OnGetCurrentTransportActionsResponse (Boolean res, PltDeviceData device, PltAction action, byte[]userdata)
		{
			String actions;
			List<String> values = new List<String> ();

			if (Plt.IsFailed (res) || action.GetErrorCode () != 0) {
				m_Delegate.OnGetCurrentTransportActionsResult (false, device, null, userdata);
				return false;
			}

			if (Plt.IsFailed (action.GetArgumentValue ("Actions", out actions))) {
				m_Delegate.OnGetCurrentTransportActionsResult (false, device, null, userdata);
				return false;
			}

			// parse the list of actions and return a list to listener
			ParseCSV (actions, values);

			m_Delegate.OnGetCurrentTransportActionsResult (true, device, values, userdata);
			return true;				 
		}

		Boolean OnGetDeviceCapabilitiesResponse (Boolean res, PltDeviceData device, PltAction action, byte[]userdata)
		{
			String value;
			PltDeviceCapabilities capabilities = new PltDeviceCapabilities ();

			if (Plt.IsFailed (res) || action.GetErrorCode () != 0) {
				m_Delegate.OnGetDeviceCapabilitiesResult (false, device, null, userdata);
				return false;
			}

			if (Plt.IsFailed (action.GetArgumentValue ("PlayMedia", out value))) {
				m_Delegate.OnGetDeviceCapabilitiesResult (false, device, null, userdata);
				return false;
			}
			// parse the list of medias and return a list to listener
			ParseCSV (value, capabilities.PlayMedia);

			if (Plt.IsFailed (action.GetArgumentValue ("RecMedia", out value))) {
				m_Delegate.OnGetDeviceCapabilitiesResult (false, device, null, userdata);
				return false;
			}
			// parse the list of rec and return a list to listener
			ParseCSV (value, capabilities.RecMedia);

			if (Plt.IsFailed (action.GetArgumentValue ("RecQualityModes", out value))) {
				m_Delegate.OnGetDeviceCapabilitiesResult (false, device, null, userdata);
				return false;
			}
			// parse the list of modes and return a list to listener
			ParseCSV (value, capabilities.RecQualityModes);

			m_Delegate.OnGetDeviceCapabilitiesResult (true, device, capabilities, userdata);
			return true;

					 

		}

		Boolean OnGetMediaInfoResponse (Boolean res, PltDeviceData device, PltAction action, byte[]userdata)
		{
			String value;
			int intValue;
			TimeSpan timeSpanValue;

			PltMediaInfo info = new PltMediaInfo ();
			try {
				if (Plt.IsFailed (res) || action.GetErrorCode () != 0) {
					Plt.Severe ();
				}
			 
				if (Plt.IsFailed (action.GetArgumentValue ("NrTracks", out intValue))) {
					Plt.Severe ();
				} else {
					info.NumTracks = intValue;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("MediaDuration", out value))) {
					Plt.Severe ();
				}
				if (Plt.IsFailed (PltDidl.ParseTimeStamp (value, out timeSpanValue))) {
					Plt.Severe ();
				} else {
					info.MediaDuration = timeSpanValue;
				}

				if (Plt.IsFailed (action.GetArgumentValue ("CurrentURI", out value))) {
					Plt.Severe ();
				} else {
					info.CurUri = value;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("CurrentURIMetaData", out value))) {
					Plt.Severe ();
				} else {
					info.CurMetadata = value;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("NextURI", out value))) {
					Plt.Severe ();
				} else {
					info.NextUri = value;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("NextURIMetaData", out  value))) {
					Plt.Severe ();
				} else {
					info.NextMetadata = value;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("PlayMedium", out value))) {
					Plt.Severe ();
				} else {
					info.PlayMedium = value;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("RecordMedium", out value))) {
					Plt.Severe ();
				} else {
					info.RecMedium = value;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("WriteStatus", out value))) {
					Plt.Severe ();
				} else {
					info.WriteStatus = value;
				}
			} catch {
				m_Delegate.OnGetMediaInfoResult (false, device, null, userdata);
				return false;
			}
			m_Delegate.OnGetMediaInfoResult (true, device, info, userdata);
			return true;
		}

		Boolean OnGetPositionInfoResponse (Boolean res, PltDeviceData device, PltAction action, byte[]userdata)
		{
			String value;
			int intValue;
			TimeSpan tsValue;

			PltPositionInfo info = new PltPositionInfo ();
			try {
				if (Plt.IsFailed (res) || action.GetErrorCode () != 0) {
					Plt.Severe ();
				}

				if (Plt.IsFailed (action.GetArgumentValue ("Track", out intValue))) {
					Plt.Severe ();
				} else {
					info.Track = intValue;
				}

				if (Plt.IsFailed (action.GetArgumentValue ("TrackDuration", out value))) {
					Plt.Severe ();
				}
				if (Plt.IsFailed (PltDidl.ParseTimeStamp (value, out tsValue))) {
					// some renderers return garbage sometimes
					info.TrackDuration = TimeSpan.FromSeconds (0);
				} else {
					info.TrackDuration = tsValue;
				}

				if (Plt.IsFailed (action.GetArgumentValue ("TrackMetaData", out value))) {
					Plt.Severe ();
				} else {
					info.TrackMetadata = value;
				}

				if (Plt.IsFailed (action.GetArgumentValue ("TrackURI", out value))) {
					Plt.Severe ();
				} else {
					info.TrackUri = value;
				}

				if (Plt.IsFailed (action.GetArgumentValue ("RelTime", out value))) {
					Plt.Severe ();
				}

				// NOT_IMPLEMENTED is a valid value according to spec
				if (value != "NOT_IMPLEMENTED" && Plt.IsFailed (PltDidl.ParseTimeStamp (value, out tsValue))) {
					// some dogy renderers return garbage sometimes
					info.RelTime = TimeSpan.FromSeconds (-1);
				} else {
					info.RelTime = tsValue;
				}

				if (Plt.IsFailed (action.GetArgumentValue ("AbsTime", out value))) {
					Plt.Severe ();
				}

				// NOT_IMPLEMENTED is a valid value according to spec
				if (value != "NOT_IMPLEMENTED" && Plt.IsFailed (PltDidl.ParseTimeStamp (value, out tsValue))) {
					// some dogy renderers return garbage sometimes
					info.AbsTime = TimeSpan.FromSeconds (-1);
				} else {
					info.AbsTime = tsValue;
				}

				if (Plt.IsFailed (action.GetArgumentValue ("RelCount", out intValue))) {
					Plt.Severe ();
				} else {
					info.RelCount = intValue;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("AbsCount", out intValue))) {
					Plt.Severe ();
				} else {
					info.AbsCount = intValue;
				}
			} catch {
				m_Delegate.OnGetPositionInfoResult (false, device, null, userdata);
				return false;
			}
			m_Delegate.OnGetPositionInfoResult (true, device, info, userdata);
			return true;

				 

		}

		Boolean OnGetTransportInfoResponse (Boolean res, PltDeviceData device, PltAction action, byte[]userdata)
		{
			String value;

			PltTransportInfo info = new PltTransportInfo ();
			try {
				if (Plt.IsFailed (res) || action.GetErrorCode () != 0) {
					Plt.Severe ();
				}

				if (Plt.IsFailed (action.GetArgumentValue ("CurrentTransportState", out value))) {
					Plt.Severe ();
				} else {
					info.CurTransportState = value;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("CurrentTransportStatus", out value))) {
					Plt.Severe ();
				} else {
					info.CurTransportStatus = value;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("CurrentSpeed", out value))) {
					Plt.Severe ();
				} else {
					info.CurSpeed = value;
				}
			} catch {
				m_Delegate.OnGetTransportInfoResult (false, device, null, userdata);
				return false;
			}
			m_Delegate.OnGetTransportInfoResult (true, device, info, userdata);
			return true;
					 
		}

		Boolean OnGetTransportSettingsResponse (Boolean res, PltDeviceData device, PltAction action, byte[]userdata)
		{
			String value;

			PltTransportSettings settings = new PltTransportSettings ();
			try {
				if (Plt.IsFailed (res) || action.GetErrorCode () != 0) {
					Plt.Severe ();
				}

				if (Plt.IsFailed (action.GetArgumentValue ("PlayMode", out value))) {
					Plt.Severe ();
				} else {
					settings.PlayMode = value;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("RecQualityMode", out value))) {
					Plt.Severe ();
				} else {
					settings.RecQualityMode = value;
				}
			} catch {
				m_Delegate.OnGetTransportSettingsResult (false, device, null, userdata);
				return false;
			}
			m_Delegate.OnGetTransportSettingsResult (true, device, settings, userdata);
			return true;

			 

		}

		Boolean OnGetCurrentConnectionIDsResponse (Boolean res, PltDeviceData device, PltAction action, byte[]userdata)
		{
			String value;
			List<String> IDs = new List<String> ();

			try {
				if (Plt.IsFailed (res) || action.GetErrorCode () != 0) {
					Plt.Severe ();
				}

				if (Plt.IsFailed (action.GetArgumentValue ("ConnectionIDs", out value))) {
					Plt.Severe ();
				}
			} catch {
				m_Delegate.OnGetCurrentConnectionIDsResult (false, device, null, userdata);
				return false;
			}
			// parse the list of medias and return a list to listener
			ParseCSV (value, IDs);

			m_Delegate.OnGetCurrentConnectionIDsResult (true, device, IDs, userdata);
			return true;

					 

		}

		Boolean OnGetCurrentConnectionInfoResponse (Boolean res, PltDeviceData device, PltAction action, byte[]userdata)
		{
			String value;
			int intValue;
			PltConnectionInfo info = new PltConnectionInfo ();
			try {
				if (Plt.IsFailed (res) || action.GetErrorCode () != 0) {
					Plt.Severe ();
				}

				if (Plt.IsFailed (action.GetArgumentValue ("RcsID", out intValue))) {
					Plt.Severe ();
				} else {
					info.RcsId = intValue;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("AVTransportID", out intValue))) {
					Plt.Severe ();
				} else {
					info.AvtransportId = intValue;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("ProtocolInfo", out value))) {
					Plt.Severe ();
				} else {
					info.ProtocolInfo = value;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("PeerConnectionManager", out value))) {
					Plt.Severe ();
				} else {
					info.PeerConnectionMgr = value;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("PeerConnectionID", out intValue))) {
					Plt.Severe ();
				} else {
					info.PeerConnectionId = intValue;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("Direction", out value))) {
					Plt.Severe ();
				} else {
					info.Direction = value;
				}
				if (Plt.IsFailed (action.GetArgumentValue ("Status", out value))) {
					Plt.Severe ();
				} else {
					info.Status = value;
				}
			} catch {
				m_Delegate.OnGetCurrentConnectionInfoResult (false, device, null, userdata);
				return false;
			}
			m_Delegate.OnGetCurrentConnectionInfoResult (true, device, info, userdata);
			return true;
					 

		}

		Boolean OnGetProtocolInfoResponse (Boolean res, PltDeviceData device, PltAction action, byte[]userdata)
		{
			String source_info, sink_info;
			List<String> sources = new List<String> ();
			List<String> sinks = new List<String> ();

			try {
				if (Plt.IsFailed (res) || action.GetErrorCode () != 0) {
					Plt.Severe ();
				}

				if (Plt.IsFailed (action.GetArgumentValue ("Source", out source_info))) {
					Plt.Severe ();
				}
				ParseCSV (source_info, sources);

				if (Plt.IsFailed (action.GetArgumentValue ("Sink", out sink_info))) {
					Plt.Severe ();
				}
				ParseCSV (sink_info, sinks);

				m_Delegate.OnGetProtocolInfoResult (true, device, sources, sinks, userdata);
				return true;
			} catch {
					
				m_Delegate.OnGetProtocolInfoResult (false, device, null, null, userdata);
				return false;
			}
		}

		Boolean OnGetMuteResponse (Boolean res, PltDeviceData device, PltAction action, byte[]userdata)
		{
			String channel, mute;
			try {
				if (Plt.IsFailed (res) || action.GetErrorCode () != 0) {
					Plt.Severe ();
				}

				if (Plt.IsFailed (action.GetArgumentValue ("Channel", out channel))) {
					Plt.Severe ();
				}

				if (Plt.IsFailed (action.GetArgumentValue ("CurrentMute", out mute))) {
					Plt.Severe ();
				}

				m_Delegate.OnGetMuteResult (true, device, channel, PltService.IsTrue (mute) ? true : false, userdata);
				return true;
			} catch {
				//bad_action:
				m_Delegate.OnGetMuteResult (false, device, "", false, userdata);
				return false;
			}
		}

		Boolean OnGetVolumeResponse (Boolean res, PltDeviceData device, PltAction action, byte[]userdata)
		{
			String channel;
			String current_volume;
			int volume;
			try {
				if (Plt.IsFailed (res) || action.GetErrorCode () != 0) {
					Plt.Severe ();
				}

				if (Plt.IsFailed (action.GetArgumentValue ("Channel", out channel))) {
					Plt.Severe ();
				}

				if (Plt.IsFailed (action.GetArgumentValue ("CurrentVolume", out current_volume))) {
					Plt.Severe ();
				}

				if (Plt.IsFailed (Int32.TryParse (current_volume, out volume))) {
					Plt.Severe ();
				}

				m_Delegate.OnGetVolumeResult (true, device, channel, volume, userdata);
				return true;
			} catch {
				//bad_action:
				m_Delegate.OnGetVolumeResult (false, device, "", 0, userdata);
				return false;
			}
		}

		 

		private Boolean FindRendered (String uuid, PltDeviceData device)
		{
			lock (m_MediaRenderers) {

				if (Plt.IsFailed (Plt.ContainerFind (m_MediaRenderers, new UUIDDeviceFinder (uuid), out device))) {
					Plt.LogFine (String.Format ("Device ({0}) not found in our list of renderers", uuid));
					return false;
				}

				return true;
			}
		}

		public static void ParseCSV (String csv, List<String> values)
		{
			String start = csv;
		

			// look for the , character
			for (int i = 0; i < csv.Length; i++) {
				char c = csv.ToCharArray () [i];
				if (c == ',') {

				}
			}

			int indexComma = start.IndexOf (",");
			while (indexComma > 0) {
				String val = start.Substring (0, indexComma);
				values.Add (val.Trim ());
				start = start.Substring (indexComma);
				indexComma = start.IndexOf (",");
			}
			 
			String last = start.Trim ();
			if (last.Length > 0) {
				values.Add (last);
			}
		}

		 

	}

	public class PltDeviceCapabilities
	{
		public List<String> PlayMedia{ get; set; } = new List<String>();

		public List<String> RecMedia{ get; set; } = new List<String>();

		public List<String> RecQualityModes{ get; set; } = new List<String>();
	}


	public class PltMediaInfo
	{
		public int NumTracks{ get; set; }

		public TimeSpan MediaDuration{ get; set; }

		public String CurUri{ get; set; }

		public String CurMetadata{ get; set; }

		public String NextUri{ get; set; }

		public String NextMetadata{ get; set; }

		public String PlayMedium{ get; set; }

		public String RecMedium{ get; set; }

		public String WriteStatus{ get; set; }
	}

	public class PltPositionInfo
	{
		public int Track{ get; set; }

		public TimeSpan TrackDuration{ get; set; }

		public String TrackMetadata{ get; set; }

		public String TrackUri{ get; set; }

		public TimeSpan RelTime{ get; set; }

		public TimeSpan AbsTime{ get; set; }

		public int RelCount{ get; set; }

		public int AbsCount{ get; set; }
	}

	public class PltTransportInfo
	{
		public String CurTransportState{ get; set; }

		public String CurTransportStatus{ get; set; }

		public String CurSpeed{ get; set; }
	}

	public class PltTransportSettings
	{
		public String PlayMode{ get; set; }

		public String RecQualityMode{ get; set; }
	}

	public class PltConnectionInfo
	{
		public int RcsId{ get; set; }

		public int AvtransportId{ get; set; }

		public String ProtocolInfo{ get; set; }

		public String PeerConnectionMgr{ get; set; }

		public int PeerConnectionId{ get; set; }

		public String Direction{ get; set; }

		public String Status{ get; set; }
	}


	public class PltMediaControllerDelegate
	{
		 
		 

		public virtual bool OnMRAdded (PltDeviceData  device)
		{
			return true;
		}

		public virtual void OnMRRemoved (PltDeviceData  device)
		{
		}

		public virtual void OnMRStateVariablesChanged (PltService service, List<PltStateVariable>  vars)
		{
		}

		// AVTransport
		public virtual void OnGetCurrentTransportActionsResult (Boolean res, PltDeviceData  device, List<String> actions, byte[] userdata)
		{
		}

		public virtual void OnGetDeviceCapabilitiesResult (Boolean res, PltDeviceData  device, PltDeviceCapabilities capabilities, byte[] userdata)
		{
		}

		public virtual void OnGetMediaInfoResult (Boolean res, PltDeviceData  device, PltMediaInfo info, byte[] userdata)
		{
		}

		public virtual void OnGetPositionInfoResult (Boolean res, PltDeviceData  device, PltPositionInfo info, byte[] userdata)
		{
		}

		public virtual void OnGetTransportInfoResult (Boolean res, PltDeviceData  device, PltTransportInfo info, byte[] userdata)
		{
		}

		public virtual void OnGetTransportSettingsResult (Boolean res, PltDeviceData  device, PltTransportSettings settings, byte[] userdata)
		{
		}

		public virtual void OnNextResult (Boolean res, PltDeviceData  device, byte[] userdata)
		{
		}

		public virtual void OnPauseResult (Boolean res, PltDeviceData  device, byte[] userdata)
		{
		}

		public virtual void OnPlayResult (Boolean res, PltDeviceData  device, byte[] userdata)
		{
		}

		public virtual void OnPreviousResult (Boolean res, PltDeviceData  device, byte[] userdata)
		{
		}

		public virtual void OnSeekResult (Boolean res, PltDeviceData  device, byte[] userdata)
		{
		}

		public virtual void OnSetAVTransportURIResult (Boolean  res, PltDeviceData  device,	byte[] userdata)
		{
		}

		public virtual void OnSetPlayModeResult (Boolean res, PltDeviceData  device,	byte[] userdata)
		{
		}

		public virtual void OnStopResult (Boolean res, PltDeviceData  device, byte[] userdata)
		{
		}

		// ConnectionManager
		public virtual void OnGetCurrentConnectionIDsResult (Boolean res, PltDeviceData  device, List<String> ids, byte[] userdata)
		{
		}

		public virtual void OnGetCurrentConnectionInfoResult (Boolean res, PltDeviceData  device, PltConnectionInfo info, byte[] userdata)
		{
		}

		public virtual void OnGetProtocolInfoResult (Boolean res, PltDeviceData  device, List<String> sources, List<String> sinks, byte[] userdata)
		{
		}

		// RenderingControl
		public virtual void OnSetMuteResult (Boolean res, PltDeviceData  device, byte[] userdata)
		{
		}

		public virtual void OnGetMuteResult (Boolean res, PltDeviceData  device, String channel, bool mute,	byte[] userdata)
		{
		}

		public virtual void OnSetVolumeResult (Boolean res,	PltDeviceData  device, byte[] userdata)
		{
		}

		public virtual void OnGetVolumeResult (Boolean res,	PltDeviceData  device, String channel, int volume, byte[] userdata)
		{
		}
	}


}
		   
