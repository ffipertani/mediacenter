﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Globalization;

namespace Platinum
{
	 
	public class PltObjectClass
	{
		public String Type{ get; set; }
		public String FriendlyName{ get; set; }
	}

	public class PltSearchClass
	{
		public String Type{ get; set;}
		public String FriendlyName{get;set;}
		public bool IncludeDerived{ get; set; }
	}




	public class PltPersonRole
	{
		public String Name{ get; set; }
		public String Role{ get; set; }
	}

	public class PltPersonRoles  : List<PltPersonRole>
	{
		 
		public Boolean Add (String name, String role = "")
		{
			PltPersonRole person = new PltPersonRole();
			person.Name = name;
			person.Role = role;

			base.Add(person);
			return true;
		}

		public Boolean ToDidl (ref String didl, String tag)
		{
			String tmp="";
			foreach(PltPersonRole p in this){
				if (String.IsNullOrEmpty(p.Name) && Count>1 && !String.IsNullOrEmpty(tmp)) continue;

				tmp += "<upnp:" + tag;
				if (!String.IsNullOrEmpty(p.Role)) {
					tmp += " role=\"";
					PltDidl.AppendXmlEscape(ref tmp, p.Role);
					tmp += "\"";
				}
				tmp += ">";
				PltDidl.AppendXmlEscape(ref tmp, p.Name);
				tmp += "</upnp:" + tag + ">";
			}
			 

			didl += tmp;
			return true;
		}

		public Boolean FromDidl (List<XmlElement> nodes)
		{
			for (int i=0; i<nodes.Count; i++) {
				PltPersonRole person = new PltPersonRole();
				String name = nodes[i].InnerText;
				String role = nodes[i].GetAttribute("role");
				// DLNA 7.3.17
				if (!String.IsNullOrEmpty(name)) person.Name = name.Substring(0, 1024);
				if (!String.IsNullOrEmpty(role)) person.Role = role.Substring(0, 1024);
				base.Add(person);
			}
			return true;
		}
	}

	public class PltConstraint
	{
		public String AllowedUse{ get; set; }
		// (CSV)
		public String ValidityStart{ get; set; }
		public String ValidityEnd{ get; set; }
		public String RemainingTime{ get; set; }
		public String UsageInfo{ get; set; }
		public String RightsInfoUri{ get; set; }
		public String ContentInfoUri{ get; set; }
	}


	public class  PltPeopleInfo
	{
		public PltPersonRoles Artists{ get; set; }
		public PltPersonRoles Actors{ get; set; }
		public PltPersonRoles Authors{ get; set; }
		public String Producer{ get; set; }
		//TODO: can be multiple
		public String Director{ get; set; }
		//TODO: can be multiple
		public String Publisher{ get; set; }
		//TODO: can be multiple
		public String Contributor{ get; set; }
		// should match m_Creator (dc:creator) //TODO: can be multiple
	}


	public class PltAffiliationInfo
	{
		public List<String> Genres{ get; set; } = new List<String>();
		public String Album{ get; set; }
		//TODO: can be multiple
		public String Playlist{ get; set; }
		// dc:title of the playlist item the content belongs too //TODO: can be multiple
	}


	public class PltDescription
	{
		public String Description{ get; set; }
		public String LongDescription{ get; set; }
		public String IconUri{ get; set; }
		public String Region{ get; set; }
		public String Rating{ get; set; }
		public String Rights{ get; set; }
		//TODO: can be multiple
		public String Date{ get; set; }
		public String Language{ get; set; }
	}

	public class PltAlbumArtInfo
	{
		public String Uri{ get; set; }
		public String DlnaProfile{ get; set; }
	}


	public class PltExtraInfo
	{
		public List<PltAlbumArtInfo> AlbumArts{ get; set; }
		public String ArtistDiscographyUri{ get; set; }
		public String LyricsUri{ get; set; }
		public List<String> Relations{ get; set; }
		// dc:relation
	}




	public class PltMiscInfo
	{
		public int DvdRegionCode{ get; set; }
		public int OriginalTrackNumber{ get; set; }
		public String Toc{ get; set; }
		public String UserAnnotation{ get; set; }
		//TODO: can be multiple
	}



	public class PltStorageInfo
	{
		public long Total{ get; set; }
		public long Used{ get; set; }
		public long Free{ get; set; }
		public long MaxPartition{ get; set; }
		public long Medium{ get; set; }
	}



	public class PltRecordedInfo
	{
		public String ProgramTitle{ get; set; }
		public String SeriesTitle{ get; set; }
		public int EpisodeNumber{ get; set; }
	}


	/*----------------------------------------------------------------------
|   PltMediaItemResource
+---------------------------------------------------------------------*/
	public class PltMediaItemResource
	{


		public String Uri{ get; set; }
		public PltProtocolInfo ProtocolInfo{ get; set; }
		public int Duration{ get; set; }
		/* seconds */
		public long Size{ get; set; }
		public String Protection{ get; set; }
		public int Bitrate{ get; set; }
		/* bytes/seconds */
		public int BitsPerSample{ get; set; }
		public int SampleFrequency{ get; set; }
		public int NbAudioChannels{ get; set; }
		public String Resolution{ get; set; }
		public int ColorDepth{ get; set; }


		public PltMediaItemResource ()
		{
			Uri             = "";
			ProtocolInfo    = new PltProtocolInfo();
			Duration        = -1;
			Size            = -1;
			Protection      = "";
			Bitrate         = -1;
			BitsPerSample   = -1;
			SampleFrequency = -1;
			NbAudioChannels = -1;
			Resolution      = "";
			ColorDepth      = -1;
		}


	}










	/*----------------------------------------------------------------------
|   PltMediaObject
+---------------------------------------------------------------------*/
	/**
	The PltMediaObject class is any data entity that can be returned by a
	ContentDirectory Service from a browsing or searching action. This is the
	base class from which PltMediaItem and PltMediaContainer derive.
	*/
	public class PltMediaObject
	{
		 

		protected PltMediaObject ()
		{
			m_Restricted = true;
		}



		public bool IsContainer ()
		{
			return m_ObjectClass.Type.StartsWith ("object.container");
		}

		public static String GetUPnPClass (String filename, PltHttpRequestContext context = null)
		{
			String ret = null;
			String mime_type = PltMimeType.GetMimeType(filename, context);

			if (mime_type.StartsWith("audio")) {
				ret = "object.item.audioItem.musicTrack";
			} else if (mime_type.StartsWith("video")) {
				ret = "object.item.videoItem"; //Note: 360 wants "object.item.videoItem" and not "object.item.videoItem.Movie"
			} else if (mime_type.StartsWith("image")) {
				ret = "object.item.imageItem.photo";
			} else {
				ret = "object.item";
			}

			return ret;
		}

		public virtual Boolean Reset ()
		{
			m_ObjectClass.Type = "";
			m_ObjectClass.FriendlyName = "";
			m_ObjectID   = "";
			m_ParentID   = "";

			m_Title      = "";
			m_Creator    = "";
			m_Date		 = "";
			m_Restricted = true;

			m_People.Actors.Clear();
			m_People.Artists.Clear();    
			m_People.Authors.Clear();

			m_Affiliation.Album     = "";
			m_Affiliation.Genres.Clear();
			m_Affiliation.Playlist  = "";

			m_Description.Description	       = "";
			m_Description.LongDescription     = "";
			m_Description.IconUri             = "";
			m_ExtraInfo.AlbumArts.Clear();
			m_ExtraInfo.ArtistDiscographyUri = "";

			m_MiscInfo.OriginalTrackNumber = 0;
			m_MiscInfo.DvdRegionCode		 = 0;
			m_MiscInfo.Toc					 = "";
			m_MiscInfo.UserAnnotation		 = "";

			m_Recorded.ProgramTitle  = "";
			m_Recorded.SeriesTitle   = "";
			m_Recorded.EpisodeNumber = 0;

			m_Resources.Clear();

			m_Didl = "";

			return true;
		}

		public virtual Boolean ToDidl (String filter, ref String didl)
		{
			return ToDidl(PltDidl.ConvertFilterToMask(filter), ref didl);
		}

		public virtual Boolean ToDidl (uint mask, ref String didl)
		{
			 
			// title is required
			didl += "<dc:title>";
			PltDidl.AppendXmlEscape(ref didl, m_Title);
			didl += "</dc:title>";

			// creator
			if ( PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_CREATOR)) {
				didl += "<dc:creator>";
				if (String.IsNullOrEmpty(m_Creator)) m_Creator = "Unknown";
				PltDidl.AppendXmlEscape(ref didl, m_Creator);
				didl += "</dc:creator>";
			}

			// date
			if (PltDidl.IsMask(mask,PltDidl.PLT_FILTER_MASK_DATE) && !String.IsNullOrEmpty(m_Date)) {
				didl += "<dc:date>";
				PltDidl.AppendXmlEscape(ref didl, m_Date);
				didl += "</dc:date>";
			} 

			// artist
			if (PltDidl.IsMask(mask,PltDidl.PLT_FILTER_MASK_ARTIST)) {
				// force an empty artist just in case (not DLNA Compliant though)
				//if (m_People.artists.GetItemCount() == 0) m_People.artists.Add("");
				m_People.Artists.ToDidl(ref didl, "artist");
			}

			// actor
			if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_ACTOR)) {
				m_People.Actors.ToDidl(ref didl, "actor");
			}

			// actor
			if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_AUTHOR)) {
				m_People.Authors.ToDidl(ref didl, "author");
			}

			// album
			if (PltDidl.IsMask(mask,PltDidl.PLT_FILTER_MASK_ALBUM) && !String.IsNullOrEmpty(m_Affiliation.Album)) {
				didl += "<upnp:album>";
				PltDidl.AppendXmlEscape(ref didl, m_Affiliation.Album);
				didl += "</upnp:album>";
			}

			// genre
				if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_GENRE)) {
				// Add unknown genre
				if (m_Affiliation.Genres.Count == 0) 
					m_Affiliation.Genres.Add("Unknown");
				foreach(String gen in m_Affiliation.Genres){
					didl += "<upnp:genre>";
					PltDidl.AppendXmlEscape(ref didl, gen);
					didl += "</upnp:genre>";      
				}
				 
			}

			// album art URI
				if (PltDidl.IsMask(mask,PltDidl.PLT_FILTER_MASK_ALBUMARTURI) && m_ExtraInfo.AlbumArts.Count>0) {
				foreach(PltAlbumArtInfo iter in m_ExtraInfo.AlbumArts){
					didl += "<upnp:albumArtURI";
					if (!String.IsNullOrEmpty((iter.DlnaProfile))) {
						didl += " dlna:profileID=\"";
						PltDidl.AppendXmlEscape(ref didl, iter.DlnaProfile);
						didl += "\"";
					}
					didl += ">";
					PltDidl.AppendXmlEscape(ref didl, iter.Uri);
					didl += "</upnp:albumArtURI>";
				}				 
			}

			// description
				if (PltDidl.IsMask(mask,PltDidl.PLT_FILTER_MASK_DESCRIPTION) && !String.IsNullOrEmpty(m_Description.Description)) {
				didl += "<dc:description>";
				PltDidl.AppendXmlEscape(ref didl, m_Description.Description);
				didl += "</dc:description>";
			}

			// long description
				if (PltDidl.IsMask(mask,PltDidl.PLT_FILTER_MASK_LONGDESCRIPTION) && !String.IsNullOrEmpty(m_Description.LongDescription)) {
				didl += "<upnp:longDescription>";
				PltDidl.AppendXmlEscape(ref didl, m_Description.LongDescription);
				didl += "</upnp:longDescription>";
			}

			// icon
				if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_ICON) && !String.IsNullOrEmpty(m_Description.IconUri)) {
				didl += "<upnp:icon>";
				PltDidl.AppendXmlEscape(ref didl, m_Description.IconUri);
				didl += "</upnp:icon>";
			}

			// original track number
			if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_ORIGINALTRACK) && m_MiscInfo.OriginalTrackNumber > 0) {
				didl += "<upnp:originalTrackNumber>";
				didl += m_MiscInfo.OriginalTrackNumber.ToString();
				didl += "</upnp:originalTrackNumber>";
			}

			// program title
				if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_PROGRAMTITLE) && !String.IsNullOrEmpty(m_Recorded.ProgramTitle)) {
				didl += "<upnp:programTitle>";
				PltDidl.AppendXmlEscape(ref didl, m_Recorded.ProgramTitle);
				didl += "</upnp:programTitle>";
			}

			// series title
				if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_SERIESTITLE) && !String.IsNullOrEmpty(m_Recorded.SeriesTitle)) {
				didl += "<upnp:seriesTitle>";
				PltDidl.AppendXmlEscape(ref didl, m_Recorded.SeriesTitle);
				didl += "</upnp:seriesTitle>";
			}

			// episode number
				if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_EPISODE) && m_Recorded.EpisodeNumber > 0) {
				didl += "<upnp:episodeNumber>";
				didl += m_Recorded.EpisodeNumber.ToString();
				didl += "</upnp:episodeNumber>";
			}

				if (PltDidl.IsMask(mask,PltDidl.PLT_FILTER_MASK_TOC) && !String.IsNullOrEmpty(m_MiscInfo.Toc)) {
				didl += "<upnp:toc>";
				PltDidl.AppendXmlEscape(ref didl, m_MiscInfo.Toc);
				didl += "</upnp:toc>";
			}

			// resource
			if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_RES)) {
				for (int i=0; i<m_Resources.Count; i++) {
					didl += "<res";

						if (PltDidl.IsMask(mask ,PltDidl.PLT_FILTER_MASK_RES_DURATION) && m_Resources[i].Duration != -1) {
						didl += " duration=\"";
						didl += PltDidl.FormatTimeStamp(m_Resources[i].Duration);
						didl += "\"";
					}

						if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_RES_SIZE) && m_Resources[i].Size != -1) {
						didl += " size=\"";
						didl += m_Resources[i].Size;
						didl += "\"";
					}

						if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_RES_PROTECTION) && !String.IsNullOrEmpty(m_Resources[i].Protection)) {
						didl += " protection=\"";
						PltDidl.AppendXmlEscape(ref didl, m_Resources[i].Protection);
						didl += "\"";
					}

						if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_RES_RESOLUTION) && !String.IsNullOrEmpty(m_Resources[i].Resolution)) {
						didl += " resolution=\"";
						PltDidl.AppendXmlEscape(ref didl, m_Resources[i].Resolution);
						didl += "\"";
					}

						if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_RES_BITRATE) && m_Resources[i].Bitrate != -1) {                    
						didl += " bitrate=\"";
						didl += m_Resources[i].Bitrate.ToString();
						didl += "\"";
					}

						if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_RES_BITSPERSAMPLE) && m_Resources[i].BitsPerSample != -1) {                    
						didl += " bitsPerSample=\"";
						didl += m_Resources[i].BitsPerSample.ToString();
						didl += "\"";
					}

						if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_RES_SAMPLEFREQUENCY) && m_Resources[i].SampleFrequency != -1) {                    
						didl += " sampleFrequency=\"";
						didl += m_Resources[i].SampleFrequency.ToString();
						didl += "\"";
					}

						if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_RES_NRAUDIOCHANNELS) && m_Resources[i].NbAudioChannels != -1) {                    
						didl += " nrAudioChannels=\"";
						didl += m_Resources[i].NbAudioChannels.ToString();
						didl += "\"";
					}

					didl += " protocolInfo=\"";
					PltDidl.AppendXmlEscape(ref didl, m_Resources[i].ProtocolInfo.ToString());
					didl += "\">";
					PltDidl.AppendXmlEscape(ref didl, m_Resources[i].Uri);
					didl += "</res>";
				}
			}

			// class is required
			didl += "<upnp:class";
			if (!String.IsNullOrEmpty(m_ObjectClass.FriendlyName)) {
				didl += " name=\"" + m_ObjectClass.FriendlyName+"\"";
			}
			didl += ">";
			PltDidl.AppendXmlEscape(ref didl, m_ObjectClass.Type);
			didl += "</upnp:class>";

			return true;
		}

		public virtual Boolean FromDidl (XmlElement entry)
		{
			String str, xml;
			List<XmlElement> children;
			Boolean res;

			// check if item is restricted (is default true?)
			if (Plt.IsSucceded (PltXmlHelper.GetAttribute (entry, "restricted", out str, "", 5))) {
				m_Restricted = PltService.IsTrue (str);
			}

			// read non-required elements
			PltXmlHelper.GetChildText (entry, "creator", out m_Creator, PltDidl.didl_namespace_dc, 256);
			PltXmlHelper.GetChildText (entry, "date", out m_Date, PltDidl.didl_namespace_dc, 256);

			// parse date and make sure it's valid
			String parsed_date;
			DateTime date;

			if (Plt.IsFailed (DateTime.TryParseExact (m_Date, DateTimeFormatInfo.CurrentInfo.RFC1123Pattern, null, DateTimeStyles.None, out date))) {
				if (Plt.IsSucceded (DateTime.TryParseExact (m_Date, "dddd, dd-MMM-yy HH:mm:ss z", null, DateTimeStyles.None, out date))) {
					if (Plt.IsFailed (DateTime.TryParse (m_Date, out date))) {
						return false;
					}
				}


			}
				 
			m_Date = date.ToString();

			res = PltXmlHelper.GetAttribute (entry, "id", out m_ObjectID);
			Plt.CheckSevere (res);

			res = PltXmlHelper.GetAttribute (entry, "parentID", out m_ParentID);
			Plt.CheckSevere (res);

			PltXmlHelper.GetAttribute (entry, "refID", out m_ReferenceID);

			res = PltXmlHelper.GetChildText (entry, "title",out  m_Title, PltDidl.didl_namespace_dc);
			Plt.CheckSevere (res);

			res = PltXmlHelper.GetChildText (entry, "class",out str, PltDidl.didl_namespace_upnp);
			m_ObjectClass.Type = str;
			Plt.CheckSevere (res);

			// DLNA 7.3.17.3 max bytes for dc:title and upnp:class is 256 bytes
			m_Title = m_Title.Substring (0, 256);    
			m_ObjectClass.Type = m_ObjectClass.Type.Substring (0, 256);

			PltXmlHelper.GetChildren (entry, out children, "artist", PltDidl.didl_namespace_upnp);
			m_People.Artists.FromDidl (children);

			PltXmlHelper.GetChildren (entry, out children, "author", PltDidl.didl_namespace_upnp);
			m_People.Authors.FromDidl (children);

			PltXmlHelper.GetChildren (entry, out children, "actor", PltDidl.didl_namespace_upnp);
			m_People.Actors.FromDidl (children);

			PltXmlHelper.GetChildText (entry, "album",out str, PltDidl.didl_namespace_upnp, 256);
			m_Affiliation.Album = str;
			PltXmlHelper.GetChildText (entry, "programTitle", out str, PltDidl.didl_namespace_upnp);
			m_Recorded.ProgramTitle = str;
			PltXmlHelper.GetChildText (entry, "seriesTitle", out str, PltDidl.didl_namespace_upnp);
			m_Recorded.SeriesTitle = str;
			PltXmlHelper.GetChildText (entry, "episodeNumber", out str, PltDidl.didl_namespace_upnp);
			int value;
			if (Plt.IsFailed (Int32.TryParse (str, out value)))
				value = 0;
			m_Recorded.EpisodeNumber = value;

			children.Clear ();
			PltXmlHelper.GetChildren (entry, out children, "genre", PltDidl.didl_namespace_upnp);
			for (int i = 0; i < children.Count; i++) {
				if (!String.IsNullOrEmpty (children [i].InnerText)) {
					m_Affiliation.Genres.Add (children [i].InnerText.Substring (0, 256));
				}
			}

			PltXmlHelper.GetChildText (entry, "description",out str, PltDidl.didl_namespace_dc);
			m_Description.Description = str;
			PltXmlHelper.GetChildText (entry, "longDescription", out str, PltDidl.didl_namespace_upnp);
			m_Description.LongDescription = str;
			PltXmlHelper.GetChildText (entry, "icon", out str, PltDidl.didl_namespace_upnp);
			m_Description.IconUri = str;
			PltXmlHelper.GetChildText (entry, "toc", out str, PltDidl.didl_namespace_upnp);
			m_MiscInfo.Toc = str;

			// album arts
			children.Clear ();
			PltXmlHelper.GetChildren (entry, out children, "albumArtURI", PltDidl.didl_namespace_upnp);
			for (int i = 0; i < children.Count; i++) {
				if (!String.IsNullOrEmpty (children [i].InnerText)) {
					PltAlbumArtInfo info = new PltAlbumArtInfo();
					info.Uri = children [i].InnerText.Substring (0, 1024);
					PltXmlHelper.GetAttribute (children [i], "profileID",out str, PltDidl.didl_namespace_dlna);
					info.DlnaProfile = str;
					m_ExtraInfo.AlbumArts.Add (info);
				}
			}

			PltXmlHelper.GetChildText (entry, "originalTrackNumber", out str, PltDidl.didl_namespace_upnp);
			if (Plt.IsFailed (Int32.TryParse (str, out value)))
				value = 0;
			m_MiscInfo.OriginalTrackNumber = value;

			children.Clear ();
			PltXmlHelper.GetChildren (entry, out children, "res");
			for (int i = 0; i < children.Count; i++) {
				PltMediaItemResource resource = new PltMediaItemResource();

				// extract url
				if (String.IsNullOrEmpty (children [i].InnerText)) {
					Plt.LogWarn (String.Format ("No resource text found in: {0}", PltXmlHelper.Serialize (children [i])));
				} else {
					resource.Uri = children [i].InnerText.Substring (0, 1024);

					// basic uri validation, ignoring scheme (could be rtsp)
					HttpUri url = new HttpUri (resource.Uri, true);
					if (!url.IsValid ()) {
						Plt.LogWarn (String.Format ("Invalid resource uri: {0}", resource.Uri));
						continue;
					}
				}

				// extract protocol info
				String protocol_info;
				res = PltXmlHelper.GetAttribute (children [i], "protocolInfo", out protocol_info, "", 256);
				if (Plt.IsFailed (res)) {
					Plt.LogWarn (String.Format ("No protocol info found in: %s", PltXmlHelper.Serialize (children [i])));
				} else {
					resource.ProtocolInfo = new PltProtocolInfo (protocol_info);
					if (!resource.ProtocolInfo.IsValid ()) {
						Plt.LogWarn (String.Format ("Invalid resource protocol info: {0}", protocol_info));
					}
				}

				// extract known attributes
				String aval;
				PltXmlHelper.GetAttribute (children [i], "protection", out aval, "", 256);
				resource.Protection = aval;
				PltXmlHelper.GetAttribute (children [i], "resolution", out aval, "", 256);
				resource.Resolution = aval;

				if (Plt.IsSucceded (PltXmlHelper.GetAttribute (children [i], "size", out str, "", 256))) {
					long size;
					if (Plt.IsFailed (long.TryParse (str, out size)))
						resource.Size = -1;
					else
						resource.Size = size;
				}

				if (Plt.IsSucceded (PltXmlHelper.GetAttribute (children [i], "duration", out str, "", 256))) {
					int tsres;
					if (Plt.IsFailed (PltDidl.ParseTimeStamp (str, out tsres))) {
						// if error while converting, ignore and set to -1 to indicate we don't know the duration
						resource.Duration = -1;
						PltXmlHelper.RemoveAttribute (children [i], "duration");
					} else {
						resource.Duration = tsres;
						// DLNA: reformat duration in case it was not compliant
						str = PltDidl.FormatTimeStamp (resource.Duration);
						PltXmlHelper.SetAttribute (children [i], "duration", str); 
					}
				}    
				m_Resources.Add (resource);
			}

			// re serialize the entry didl as a we might need to pass it to a renderer
			// we may have modified the tree to "fix" issues, so as not to break a renderer
			// (don't write xml prefix as this didl could be part of a larger document)
			//res = PltXmlHelper::Serialize(*entry, xml, false);
			m_Didl = "";
			res = ToDidl (PltDidl.PLT_FILTER_MASK_ALL, ref m_Didl);
			Plt.CheckSevere (res);

			m_Didl = PltDidl.didl_header + m_Didl + PltDidl.didl_footer;    
			return true;
		}

		 
		/* common properties */
		public PltObjectClass m_ObjectClass;
		public String m_ObjectID;
		public String m_ParentID;
		public String m_ReferenceID;

		/* metadata */
		String m_Title;
		String m_Creator;
		String m_Date;
		PltPeopleInfo m_People;
		PltAffiliationInfo m_Affiliation;
		PltDescription m_Description;
		PltRecordedInfo m_Recorded;

		/* properties */
		public bool m_Restricted;

		/* extras */
		public PltExtraInfo m_ExtraInfo;

		/* miscellaneous info */
		public PltMiscInfo m_MiscInfo;

		/* resources related */
		public List<PltMediaItemResource> m_Resources;

		/* original DIDL for Control Points to pass to a renderer when invoking SetAVTransportURI */
		public String m_Didl;
	}


	/*----------------------------------------------------------------------
|   PltMediaItem
+---------------------------------------------------------------------*/
	/**
	The PltMediaItem class represents a first-level class derived directly from
	PltMediaObject. It most often represents a single piece of AV data. 
	*/
	public class PltMediaItem : PltMediaObject
	{
		 
		 
		public PltMediaItem ()
		{
			Reset();
		}
		 

		// PltMediaObject methods
		public override Boolean ToDidl (String filter,ref  String didl)
		{
			return base.ToDidl(filter, ref didl);
		}

		public Boolean ToDidl (uint mask, String didl)
		{
			didl += "<item id=\"";

			PltDidl.AppendXmlEscape(ref didl, m_ObjectID);
			didl += "\" parentID=\"";
			PltDidl.AppendXmlEscape(ref didl, m_ParentID);

			if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_REFID) && !String.IsNullOrEmpty(m_ReferenceID)) {
				didl += "\" refID=\"";
				PltDidl.AppendXmlEscape(ref didl, m_ReferenceID);
			}

			didl += "\" restricted=\"";
			didl += m_Restricted?"1\"":"0\"";

			didl += ">";

			Plt.CheckSevere(base.ToDidl(mask, ref didl));

			/* close tag */
			didl += "</item>";

			return true;
		}

		public override  Boolean FromDidl (XmlElement entry)
		{
			/* reset first */
			Reset();

			if (!entry.Name.Equals("item")) {
				Plt.CheckSevere (false);//NPT_ERROR_INTERNAL);
			}

			Boolean result = base.FromDidl(entry);

			// make sure we have at least one valid resource
			if (m_Resources.Count == 0) {
				Plt.CheckSevere (false);//NPT_ERROR_INVALID_PARAMETERS);
			}

			return result;
		}
	}




	/*----------------------------------------------------------------------
|   PltMediaContainer
+---------------------------------------------------------------------*/
	/**
	The PltMediaContainer class represents a first-level class derived directly
	from PltMediaObject. A PltMediaContainer represents a collection of 
	PltMediaObject instances.
	*/
	public class PltMediaContainer : PltMediaObject
	{


		public List<PltSearchClass> m_SearchClasses;

		/* properties */
		public bool m_Searchable;

		/* container info related */
		public int m_ChildrenCount;
		public int m_ContainerUpdateID;

		public PltMediaContainer ()
		{
			Reset();
		}


		// PltMediaObject methods
		public override Boolean Reset ()
		{
			m_SearchClasses.Clear();
			m_Searchable        = false;
			m_ChildrenCount     = -1;
			m_ContainerUpdateID = 0;

			return base.Reset();
		}

		public override Boolean ToDidl (String filter, ref String didl)
		{
			return base.ToDidl(filter,ref  didl);
		}

		public override  Boolean ToDidl (uint mask, ref String didl)
		{
			// container id property
			didl += "<container id=\"";
			PltDidl.AppendXmlEscape(ref didl, m_ObjectID);

			// parent id property
			didl += "\" parentID=\"";
			PltDidl.AppendXmlEscape(ref didl, m_ParentID);

			// ref id
			if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_REFID) && !String.IsNullOrEmpty(m_ReferenceID)) {
				didl += "\" refID=\"";
				PltDidl.AppendXmlEscape(ref didl, m_ReferenceID);
			}

			// restricted property
			didl += "\" restricted=\"";
			didl += m_Restricted?"1\"":"0\"";

			// searchable property
			if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_SEARCHABLE) ) {
				didl += " searchable=\"";
				didl += m_Searchable?"1\"":"0\"";
			}

			// childcount property
			if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_CHILDCOUNT) && m_ChildrenCount != -1) {
				didl += " childCount=\"";
				didl += m_ChildrenCount;
				didl += "\"";
			}

			didl += ">";

			if (PltDidl.IsMask(mask, PltDidl.PLT_FILTER_MASK_SEARCHCLASS) && m_SearchClasses.Count>0) {
									foreach (PltSearchClass search_class in m_SearchClasses) {
										didl += "<upnp:searchClass includeDerived=\"";
										didl += search_class.IncludeDerived?"1\"":"0\"";

										// frienly name is any
										if (!String.IsNullOrEmpty(search_class.FriendlyName)) {
											didl += " name=\"" + search_class.FriendlyName + "\"";
										}
										didl += ">";
										didl += search_class.Type;
										didl += "</upnp:searchClass>";
				}
			 
			}

			Plt.CheckSevere(base.ToDidl(mask, ref didl));

			/* close tag */
			didl += "</container>";
			return true;
		}

		public override Boolean FromDidl (XmlElement entry)
		{
			String str;

			/* reset first */
			Reset();

			// check entry type
			if (entry.Name.EqualsIgnoreCase("Container")) 
				return false;// NPT_ERROR_INTERNAL;

			// check if item is searchable (is default true?)
			if (Plt.IsSucceded(PltXmlHelper.GetAttribute(entry, "searchable", out str, "", 5))) {
				m_Searchable = PltService.IsTrue(str);
			}

			// look for childCount
			if (Plt.IsSucceded(PltXmlHelper.GetAttribute(entry, "childCount", out str, "", 256))) {
				int count;
				Plt.CheckSevere(Int32.TryParse(str,out count));
				m_ChildrenCount = count;
			}

			// upnp:searchClass child elements
			List<XmlElement> children;
			PltXmlHelper.GetChildren(entry, out children, "upnp:searchClass");

			for (int i=0; i<children.Count; i++) {
				PltSearchClass search_class = new PltSearchClass();

				// extract url
				if (children [i].Name == null) {
					Plt.LogWarn (String.Format ("No searchClass text found in: {0}", PltXmlHelper.Serialize (children [i])));
					continue;
				}

				// DLNA 7.3.17.4
				search_class.Type = children[i].InnerText.Substring(0, 256);

				// extract optional attribute name				 
				PltXmlHelper.GetAttribute(children[i], "name", out str);
				search_class.FriendlyName = str;

				// includeDerived property
				if (Plt.IsFailed(PltXmlHelper.GetAttribute(children[i], "includeDerived", out str))) {
					Plt.LogWarn(String.Format("No required attribute searchClass@includeDerived found in: {0}",PltXmlHelper.Serialize(children[i])));
					continue;
				}

				search_class.IncludeDerived = PltService.IsTrue(str);
				m_SearchClasses.Add(search_class);
			}

			return base.FromDidl(entry);
		}


	}

	 
}
 
      