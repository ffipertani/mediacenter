﻿using System;
using System.Collections.Generic;

namespace Platinum
{

	/*----------------------------------------------------------------------
	|   PLT_MediaServer
	+---------------------------------------------------------------------*/
	/**
	The PLT_MediaServer class implements the base class for a UPnP AV 
		Media Server device.
		*/
	public class MediaServer: PltDeviceHost
	{
		private PltMediaServerDelegate m_Delegate;
			


		static String[] BrowseFlagsStr = new String[]{ "BrowseMetadata",	"BrowseDirectChildren"	};
			 
			

		// class methods
		public static Boolean ParseBrowseFlag (String str, out BrowseFlags flag)
		{
			flag = BrowseFlags.BROWSEDIRECTCHILDREN;
			if (str.EqualsIgnoreCase (BrowseFlagsStr [0])) {
				flag = BrowseFlags.BROWSEMETADATA;
				return true;
			}
			if (str.EqualsIgnoreCase (BrowseFlagsStr [1])) {
				flag = BrowseFlags.BROWSEDIRECTCHILDREN;
				return true;
			}
			return false;
		}

		public static Boolean ParseSort (String sort, out String[] list)
		{
			list = null;

			// easy out
			if (sort.Length == 0 || sort.Equals ("*"))
				return true;

			list = sort.Split (',');
			foreach (String property in list) {
				String[] parsedProperty = property.Split (':');
				if (parsedProperty.Length != 2 || (!property.StartsWith ("-") && !property.StartsWith ("+"))) {
					Plt.LogWarn (String.Format ("Invalid SortCriteria property {0}", property));
					return false;
				}
			}
			 
			return true;
		}

		// constructor
		public MediaServer (String  friendly_name, bool show_ip = false, String  uuid = null, int   port = 0, bool port_rebind = false)
			: base ("/DeviceDescription.xml", uuid, "urn:schemas-upnp-org:device:MediaServer:1", friendly_name, show_ip, port, port_rebind)
		{
			m_Delegate = null;
			ModelDescription = "Plutinosoft AV Media Server Device";
			ModelName = "AV Media Server Device";
			ModelURL = "http://www.plutinosoft.com/platinum";
			DlnaDoc = "DMS-1.50";

		}

		// methods
		public virtual void SetDelegate (PltMediaServerDelegate adelegate)
		{
			m_Delegate = adelegate; 
		}

		PltMediaServerDelegate GetDelegate ()
		{
			return m_Delegate; 
		}

		public virtual void UpdateSystemUpdateID (int update)
		{
		}

		public virtual void UpdateContainerUpdateID (String id, int update)
		{
		}

			 
	 

		// PLT_DeviceHost methods
		protected override Boolean SetupServices ()
		{
			PltService service;

			{
				service = new PltService (this, "urn:schemas-upnp-org:service:ContentDirectory:1", "urn:upnp-org:serviceId:ContentDirectory", "ContentDirectory");
				Plt.CheckSevere (service.SetSCPDXML ((String)System.Text.Encoding.UTF8.GetString (ContentDirectorySearchSCPD.MS_ContentDirectorywSearchSCPD)));
				Plt.CheckSevere (AddService (service));

				service.SetStateVariable ("ContainerUpdateIDs", "");
				service.SetStateVariableRate ("ContainerUpdateIDs", TimeSpan.FromSeconds (2));
				service.SetStateVariable ("SystemUpdateID", "0");
				service.SetStateVariableRate ("SystemUpdateID", TimeSpan.FromSeconds (2));
				service.SetStateVariable ("SearchCapability", "@id,@refID,dc:title,upnp:class,upnp:genre,upnp:artist,upnp:author,upnp:author@role,upnp:album,dc:creator,res@size,res@duration,res@protocolInfo,res@protection,dc:publisher,dc:language,upnp:originalTrackNumber,dc:date,upnp:producer,upnp:rating,upnp:actor,upnp:director,upnp:toc,dc:description,microsoft:userRatingInStars,microsoft:userEffectiveRatingInStars,microsoft:userRating,microsoft:userEffectiveRating,microsoft:serviceProvider,microsoft:artistAlbumArtist,microsoft:artistPerformer,microsoft:artistConductor,microsoft:authorComposer,microsoft:authorOriginalLyricist,microsoft:authorWriter,upnp:userAnnotation,upnp:channelName,upnp:longDescription,upnp:programTitle");
				service.SetStateVariable ("SortCapability", "dc:title,upnp:genre,upnp:album,dc:creator,res@size,res@duration,res@bitrate,dc:publisher,dc:language,upnp:originalTrackNumber,dc:date,upnp:producer,upnp:rating,upnp:actor,upnp:director,upnp:toc,dc:description,microsoft:year,microsoft:userRatingInStars,microsoft:userEffectiveRatingInStars,microsoft:userRating,microsoft:userEffectiveRating,microsoft:serviceProvider,microsoft:artistAlbumArtist,microsoft:artistPerformer,microsoft:artistConductor,microsoft:authorComposer,microsoft:authorOriginalLyricist,microsoft:authorWriter,microsoft:sourceUrl,upnp:userAnnotation,upnp:channelName,upnp:longDescription,upnp:programTitle");


				service = null;
			}

			{
				service = new PltService (this, "urn:schemas-upnp-org:service:ConnectionManager:1", "urn:upnp-org:serviceId:ConnectionManager", "ConnectionManager");
				Plt.CheckSevere (service.SetSCPDXML ((String)System.Text.Encoding.UTF8.GetString (MediaServerConnectionManagerSCPD.MS_ConnectionManagerSCPD)));
				Plt.CheckSevere (AddService (service));

				service.SetStateVariable ("CurrentConnectionIDs", "0");
				service.SetStateVariable ("SinkProtocolInfo", "");
				service.SetStateVariable ("SourceProtocolInfo", "http-get:*:*:*");


				service = null;
			}

			return true;
		}

		protected virtual Boolean OnAction (PltAction action, PltHttpRequestContext context)
		{
			/* parse the action name */
			String name = action.GetActionDesc ().GetName ();

			// ContentDirectory
			if (name.EqualsIgnoreCase ("Browse")) {
				return OnBrowse (action, context);
			}
			if (name.EqualsIgnoreCase ("Search")) {
				return OnSearch (action, context);
			}
			if (name.EqualsIgnoreCase ("GetSystemUpdateID")) {
				return OnGetSystemUpdateID (action, context);
			}
			if (name.EqualsIgnoreCase ("GetSortCapabilities")) {
				return OnGetSortCapabilities (action, context);
			}  
			if (name.EqualsIgnoreCase ("GetSearchCapabilities")) {
				return OnGetSearchCapabilities (action, context);
			}  

			// ConnectionMananger
			if (name.EqualsIgnoreCase ("GetCurrentConnectionIDs")) {
				return OnGetCurrentConnectionIDs (action, context);
			}
			if (name.EqualsIgnoreCase ("GetProtocolInfo")) {
				return OnGetProtocolInfo (action, context);
			}    
			if (name.EqualsIgnoreCase ("GetCurrentConnectionInfo")) {
				return OnGetCurrentConnectionInfo (action, context);
			}

			action.SetError (401, "No Such Action.");
			return true;
		}

		public override Boolean ProcessHttpGetRequest (HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
			if (m_Delegate != null)
				return m_Delegate.ProcessFileRequest (request, context, response);

			return false;//NPT_ERROR_NO_SUCH_ITEM;
		}

		// ConnectionManager
		protected virtual Boolean OnGetCurrentConnectionIDs (PltAction action, PltHttpRequestContext context)
		{
			return action.SetArgumentsOutFromStateVariable ();
		}

		protected virtual Boolean OnGetProtocolInfo (PltAction action, PltHttpRequestContext context)
		{			 
			return action.SetArgumentsOutFromStateVariable ();
		}

		protected virtual Boolean OnGetCurrentConnectionInfo (PltAction action, PltHttpRequestContext context)
		{
			if (Plt.IsFailed (action.VerifyArgumentValue ("ConnectionID", "0"))) {
				action.SetError (706, "No Such Connection.");
				return false;
			}

			if (Plt.IsFailed (action.SetArgumentValue ("RcsID", "-1"))) {
				return false;
			}
			if (Plt.IsFailed (action.SetArgumentValue ("AVTransportID", "-1"))) {
				return false;
			}
			if (Plt.IsFailed (action.SetArgumentValue ("ProtocolInfo", "http-get:*:*:*"))) {
				return false;
			}
			if (Plt.IsFailed (action.SetArgumentValue ("PeerConnectionManager", "/"))) {
				return false;
			}
			if (Plt.IsFailed (action.SetArgumentValue ("PeerConnectionID", "-1"))) {
				return false;
			}
			if (Plt.IsFailed (action.SetArgumentValue ("Direction", "Output"))) {
				return false;
			}
			if (Plt.IsFailed (action.SetArgumentValue ("Status", "Unknown"))) {
				return false;
			}

			return true;
		}


		// ContentDirectory
		protected virtual Boolean OnGetSortCapabilities (PltAction action, PltHttpRequestContext context)
		{
			return action.SetArgumentsOutFromStateVariable ();
		}

		protected virtual Boolean OnGetSearchCapabilities (PltAction action, PltHttpRequestContext context)
		{
			return action.SetArgumentsOutFromStateVariable ();
		}

		protected virtual Boolean OnGetSystemUpdateID (PltAction  action, PltHttpRequestContext context)
		{
			return action.SetArgumentsOutFromStateVariable ();
		}

		protected virtual Boolean OnBrowse (PltAction action, PltHttpRequestContext context)
		{
			Boolean res;
			String object_id;
			String browse_flag_val;    
			String filter;
			String start;
			String count;
			String sort;
			String[] sort_list;

			if (Plt.IsFailed (action.GetArgumentValue ("ObjectId", out object_id)) ||
			    Plt.IsFailed (action.GetArgumentValue ("BrowseFlag", out browse_flag_val)) ||
			    Plt.IsFailed (action.GetArgumentValue ("Filter", out filter)) ||
			    Plt.IsFailed (action.GetArgumentValue ("StartingIndex", out start)) ||
			    Plt.IsFailed (action.GetArgumentValue ("RequestedCount", out count)) ||
			    Plt.IsFailed (action.GetArgumentValue ("SortCriteria", out  sort))) {
				Plt.LogWarn ("Missing arguments");
				action.SetError (402, "Invalid args");
				return true;
			}

			/* extract flag */
			BrowseFlags flag;
			if (Plt.IsFailed (ParseBrowseFlag (browse_flag_val, out flag))) {
				/* error */
				Plt.LogWarn (String.Format ("BrowseFlag value not allowed ({0})", browse_flag_val));
				action.SetError (402, "Invalid args");
				return true;
			}

			/* convert index and counts to int */
			int starting_index, requested_count;
			if (Plt.IsFailed (int.TryParse (start, out starting_index)) || Plt.IsFailed (Int32.TryParse (count, out requested_count)) || PltDidl.ConvertFilterToMask (filter) == 0) {       
				Plt.LogWarn (String.Format ("Invalid arguments ({0}, {1}, {2})", start, count, filter));
				action.SetError (402, "Invalid args");
				return false;
			}

			/* parse sort criteria for validation */
			if (Plt.IsFailed (ParseSort (sort, out sort_list))) {
				Plt.LogWarn (String.Format ("Unsupported or invalid sort criteria error ({0})", sort));
				action.SetError (709, "Unsupported or invalid sort criteria error");
				return false;
			}

			Plt.LogFine (String.Format ("Processing {0} from {1} with id=\"{2}\", filter=\"{3}\", start={4}, count={5}", browse_flag_val, context.RemoteAddress.Address.ToString (), object_id, filter, starting_index, requested_count));

			/* Invoke the browse function */
			if (flag == BrowseFlags.BROWSEMETADATA) {
				res = OnBrowseMetadata (action, object_id, filter, starting_index, requested_count, sort, context);
			} else {
				res = OnBrowseDirectChildren (action, object_id, filter, starting_index, requested_count, sort, context);
			}

			if (Plt.IsFailed (res) && (action.GetErrorCode () == 0)) {
				action.SetError (800, "Internal error");
			}

			return res;
		}

		protected virtual Boolean OnSearch (PltAction action, PltHttpRequestContext context)
		{
							 

			Boolean res;
			String container_id;
			String search;
			String filter;
			String start;
			String count;
			String sort;
			String[] sort_list;

			if (Plt.IsFailed (action.GetArgumentValue ("ContainerId", out container_id)) ||
			    Plt.IsFailed (action.GetArgumentValue ("SearchCriteria", out search)) ||
			    Plt.IsFailed (action.GetArgumentValue ("Filter", out filter)) ||
			    Plt.IsFailed (action.GetArgumentValue ("StartingIndex", out start)) ||
			    Plt.IsFailed (action.GetArgumentValue ("RequestedCount", out count)) ||
			    Plt.IsFailed (action.GetArgumentValue ("SortCriteria", out sort))) {
				Plt.LogWarn ("Missing arguments");
				action.SetError (402, "Invalid args");
				return true;
			}

			/* convert index and counts to int */
			int starting_index, requested_count;
			if (Plt.IsFailed (Int32.TryParse (start, out starting_index)) || Plt.IsFailed (Int32.TryParse (count, out requested_count))) {       
				Plt.LogWarn (String.Format ("Invalid arguments ({0}, {1})", start, count));
				action.SetError (402, "Invalid args");
				return false;
			}

			/* parse sort criteria */
			if (Plt.IsFailed (ParseSort (sort, out sort_list))) {
				Plt.LogWarn (String.Format ("Unsupported or invalid sort criteria error ({0})", sort));
				action.SetError (709, "Unsupported or invalid sort criteria error");
				return false;
			}

			Plt.LogFine (String.Format ("Processing Search from {0} with id=\"{1}\", search=\"{2}\", start={3}, count={4}", context.RemoteAddress.Address.ToString (), container_id, search, starting_index, requested_count));

			if (String.IsNullOrEmpty (search) || search.Equals ("*")) {
				res = OnBrowseDirectChildren (action, container_id, filter, starting_index, requested_count, sort, context);
			} else {
				res = OnSearchContainer (action, container_id, search, filter, starting_index, requested_count, sort, context);
			}

			if (Plt.IsFailed (res) && (action.GetErrorCode () == 0)) {
				action.SetError (800, "Internal error");
			}

			return res;
		}

		// overridable methods
		protected virtual Boolean OnBrowseMetadata (PltAction action, String object_id, String filter, int starting_index, int requested_count, String sort_criteria, PltHttpRequestContext context)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnBrowseMetadata (action, object_id, filter, starting_index, requested_count, sort_criteria, context);
			}
			return false;//NPT_ERROR_NOT_IMPLEMENTED; 
		}

		protected virtual Boolean OnBrowseDirectChildren (PltAction action, String object_id, String filter, int starting_index, int requested_count, String sort_criteria, PltHttpRequestContext context)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnBrowseDirectChildren (action, object_id, filter, starting_index, requested_count, sort_criteria, context);
			}
			return false;// NPT_ERROR_NOT_IMPLEMENTED;
		}

		protected virtual Boolean OnSearchContainer (PltAction action, String  container_id, String search_criteria, String filter, int starting_index, int requested_count, String sort_criteria, PltHttpRequestContext context)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnSearchContainer (action, container_id, search_criteria, filter, starting_index, requested_count, sort_criteria, context);
			}
			return false;// NPT_ERROR_NOT_IMPLEMENTED;
		}

		 

	}


	public enum BrowseFlags
	{
		BROWSEMETADATA,
		BROWSEDIRECTCHILDREN
	}

	public interface  PltMediaServerDelegate
	{

		Boolean OnBrowseMetadata (PltAction action, String  object_id, String filter, int starting_index,	int requested_count, String sort_criteria, PltHttpRequestContext context);

		Boolean OnBrowseDirectChildren (PltAction action, String object_id, String filter, int starting_index, int requested_count, String sort_criteria, PltHttpRequestContext context);

		Boolean OnSearchContainer (PltAction action, String  container_id, String search_criteria, String filter, int starting_index, int requested_count, String sort_criteria, PltHttpRequestContext context);

		Boolean ProcessFileRequest (HttpRequest request, HttpRequestContext context, HttpResponse response);
	}
}
     
