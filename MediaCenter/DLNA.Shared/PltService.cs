﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml;
using System.Net;

namespace Platinum
{
	public class PltService
	{

 
		protected PltDeviceData                           m_Device;
		protected String                              m_ServiceType;
		protected String                              m_ServiceID;
		protected String                              m_ServiceName;
		protected String                              m_SCPDURL;
		protected String                              m_ControlURL;
		protected String                              m_EventSubURL;
		protected PltServiceEventTask                   m_EventTask;
		protected List<PltActionDesc>              m_ActionDescs;
		protected List<PltStateVariable>            m_StateVars;
		protected Object                               m_Lock = new Object();
		protected List<PltStateVariable>            m_StateVarsChanged;
		protected List<PltStateVariable>            m_StateVarsToPublish;
		protected List<PltEventSubscriber>  m_Subscribers;
		protected Boolean                                    m_EventingPaused;
		protected String                              m_LastChangeNamespace;


		public PltService (PltDeviceData device, String type, String id, String name, String lastChangeNamespace = null)
		{
			m_Device = device;
			m_ServiceType = type;
			m_ServiceID = id;
			m_ServiceName = name;
			m_EventTask = null;
			m_EventingPaused = false;
			m_LastChangeNamespace = lastChangeNamespace;

			m_ActionDescs = new List<PltActionDesc> ();
			m_StateVars = new List<PltStateVariable> ();
			m_Subscribers = new List<PltEventSubscriber> ();
			m_StateVarsToPublish = new List<PltStateVariable> ();
			m_StateVarsChanged = new List<PltStateVariable> ();

			if (!String.IsNullOrEmpty(name)) InitURLs(name);

		}

		public void Dispose()
		{
			Cleanup ();
		}

		public Boolean InitURLs(String service_name)
		{
			m_SCPDURL      = service_name;
			m_SCPDURL     += "/" + m_Device.UUID + "/scpd.xml";
			m_ControlURL   = service_name;
			m_ControlURL  += "/" + m_Device.UUID + "/control.xml";
			m_EventSubURL  = service_name;
			m_EventSubURL += "/" + m_Device.UUID + "/event.xml";

			return true;
		}

		public Boolean IsValid() { 
			return (m_ActionDescs.Count > 0); 
		}

		public Boolean PauseEventing(bool pause = true)
		{
			lock (m_Lock) {
				m_EventingPaused = pause;
				return true;
			}
		}


		public Boolean SetSCPDURL(String url)  
		{
			m_SCPDURL = url; 
			return true; 
		}

		public Boolean SetControlURL(String url) 
		{
			m_ControlURL = url; 
			return true; 
		}

		public Boolean SetEventSubURL(String url)
		{
			m_EventSubURL = url; 
			return true; 
		}
			
		public String GetSCPDURL(bool absolute = false)
		{
			HttpUri url = GetDevice().NormalizeURL(m_SCPDURL);   
			return absolute?url.Uri.AbsoluteUri:url.Uri.LocalPath;
		}

		public String GetControlURL(bool absolute = false)
		{
			HttpUri url = GetDevice().NormalizeURL(m_ControlURL);    
			return absolute?url.Uri.AbsoluteUri:url.Uri.LocalPath;
		}

		public String GetEventSubURL(bool absolute = false)
		{
			HttpUri url = GetDevice().NormalizeURL(m_EventSubURL);    
			return absolute?url.Uri.AbsoluteUri:url.Uri.LocalPath;
		}

		public String GetServiceID()
		{
			return m_ServiceID;   
		}

		public String GetServiceType() 
		{
			return m_ServiceType; 
		}

		public String GetServiceName() 
		{
			return m_ServiceName; 
		}

		public PltDeviceData GetDevice()
		{
			return m_Device;      
		}

		public Boolean ForceVersion(int version)
		{
			if (version < 1) return false;

			m_ServiceType = m_ServiceType.Substring(0, m_ServiceType.Length-1);
			m_ServiceType += version;
			return true;
		}

		public Boolean GetSCPDXML(out String xml)
		{
			xml = null; 
			Boolean res;

		// it is required to have at least 1 state variable
		if (m_StateVars.Count == 0) return false;
			XmlDocument doc = new XmlDocument ();
			XmlElement spec = null;
			XmlElement actionList = null;
			XmlElement top = doc.CreateElement("scpd","urn:schemas-upnp-org:service-1-0");
			XmlElement serviceStateTable = null;
		



		// add spec version
			spec = doc.CreateElement("","specVersion","urn:schemas-upnp-org:service-1-0");
			top.AppendChild(spec);
			
			if(Plt.IsFailed(res = PltXmlHelper.AddChildText(spec, "major", "1"))){
				return false;
			}
			if(Plt.IsFailed(res = PltXmlHelper.AddChildText(spec, "minor", "0"))){
				return false;
			}

		// add actions
			actionList = doc.CreateElement("","actionList","urn:schemas-upnp-org:service-1-0");
			top.AppendChild(actionList);
			
			if (Plt.IsFailed (res = m_ActionDescs.ApplyUntil (new PltActionGetSCPDXMLIterator (actionList), ApplyUntil.ResultSuccess))) {
				return res;
			}

		// add service state table
			serviceStateTable = doc.CreateElement("","serviceStateTable","urn:schemas-upnp-org:service-1-0");
			top.AppendChild(serviceStateTable);
		
			if (Plt.IsFailed (res = m_StateVars.ApplyUntil (new PltStateVariableGetSCPDXMLIterator (serviceStateTable), ApplyUntil.ResultSuccess))) {
				return res;
			}

		// serialize node
			Plt.CheckWarining(res = PltXmlHelper.Serialize(top, out xml, true, 2));

		
		return res;
		 
		}
		public Boolean SetSCPDXML(String scpd)
		{
			 
			if (scpd == null)
				throw new Exception ("NPT_ERROR_INVALID_PARAMETERS");

			Cleanup ();

			try {
		 
				XmlDocument tree = new XmlDocument ();
				Boolean res;
				List<XmlElement> stateVariables = new List<XmlElement> ();
				List<XmlElement> actions = new List<XmlElement> ();
				XmlElement root;
				XmlElement actionList;
				XmlElement stateTable;
				 
				tree.LoadXml (scpd);
				root = tree.DocumentElement;
		 
				if (root == null || !root.Name.Equals ("scpd")) {
					Plt.LogFatal ("Invalid scpd root tag name");
					throw new Exception ("NPT_ERROR_INVALID_SYNTAX");
				}

				// make sure we have required children presents
				stateTable = PltXmlHelper.GetChild (root, "serviceStateTable");
				if (stateTable==null || stateTable.ChildNodes.Count == 0 || Plt.IsFailed (PltXmlHelper.GetChildren (stateTable, out stateVariables, "stateVariable"))) {
					Plt.LogFatal ("No state variables found");
					throw new Exception ("NPT_ERROR_INVALID_SYNTAX");
				}

				for (int k = 0; k < stateVariables.Count; k++) {
					String name, type, send;
					PltXmlHelper.GetChildText (stateVariables [k], "name", out name);
					PltXmlHelper.GetChildText (stateVariables [k], "dataType", out type);
					PltXmlHelper.GetAttribute (stateVariables [k], "sendEvents", out send);

					if (name.Length == 0 || type.Length == 0) {
						Plt.LogFatal (String.Format ("Invalid state variable name or type at position %d", k + 1));
						throw new Exception ("NPT_ERROR_INVALID_SYNTAX");
					}

					PltStateVariable variable = new PltStateVariable (this);
					m_StateVars.Add (variable);

					variable.Name = name;
					variable.DataType = type;
					variable.IsSendingEvents = IsTrue (send);
					String adefaultValue;
					PltXmlHelper.GetChildText (stateVariables [k], "defaultValue",out adefaultValue );
					variable.DefaultValue = adefaultValue;

					XmlElement allowedValueList = PltXmlHelper.GetChild (stateVariables [k], "allowedValueList");
					if (allowedValueList!=null) {
						List<XmlElement> allowedValues = new List<XmlElement> ();
						PltXmlHelper.GetChildren (allowedValueList, out allowedValues, "allowedValue");
						for (int l = 0; l < allowedValues.Count; l++) {
							String text = allowedValues [l].InnerText;
							if (text != null) {
								variable.AllowedValues.Add (text);
							}
						}
					} else {
						XmlElement allowedValueRange = PltXmlHelper.GetChild (stateVariables [k], "allowedValueRange");
						if (allowedValueRange != null) {
							String min, max, step;
							PltXmlHelper.GetChildText (allowedValueRange, "minimum", out min);
							PltXmlHelper.GetChildText (allowedValueRange, "maximum", out max);
							PltXmlHelper.GetChildText (allowedValueRange, "step", out step);

							// these are required but try to be nice
							// in case bad devices provide nothing
							if (min.Length == 0) {
								if (variable.DataType.Equals ("ui1") ||
								    variable.DataType.Equals ("ui2") ||
								    variable.DataType.Equals ("ui4")) {
									min = "0";
								} else if (variable.DataType.Equals ("i1")) {
									min = "-128";
								} else if (variable.DataType.Equals ("i2")) {
									min = "-32768";
								} else if (variable.DataType.Equals ("i4") ||
								           variable.DataType.Equals ("int")) {
									min = "-2147483647";
								} else {
									Plt.LogFatal (String.Format ("Invalid variable data type %s", variable.DataType));
									throw new Exception ("NPT_ERROR_INVALID_SYNTAX");
								}
							}
							if (max.Length == 0) {
								if (variable.DataType.Equals ("ui1")) {
									max = 0xff.ToString ();
								} else if (variable.DataType.Equals ("ui2")) {
									max = 0xffff.ToString ();
								} else if (variable.DataType.Equals ("ui4")) {
									max = 0xffffffff.ToString ();
								} else if (variable.DataType.Equals ("i1")) {
									max = 0x7f.ToString ();
								} else if (variable.DataType.Equals ("i2")) {
									max = 0x7fff.ToString ();
								} else if (variable.DataType.Equals ("i4") ||
								           variable.DataType.Equals ("int")) {
									max = 0x7fffffff.ToString ();
								} else {
									Plt.LogFatal (String.Format ("Invalid variable data type %s", variable.DataType));
									throw new Exception ("NPT_ERROR_INVALID_SYNTAX");
								}
							}

							variable.AllowedValueRange = new AllowedValueRange ();
							variable.AllowedValueRange.MinValue = Int32.Parse (min);
							variable.AllowedValueRange.MaxValue = Int32.Parse (max);
							variable.AllowedValueRange.Step = -1;
					

							if (step.Length != 0) {
								variable.AllowedValueRange.Step = Int32.Parse (step);
						
							}
						}
					}
				}

				// actions
				actionList = PltXmlHelper.GetChild (root, "actionList");
				if (actionList != null) {
					res = PltXmlHelper.GetChildren (actionList, out actions, "action");
					if (Plt.IsFailed(res))
						Plt.Severe ();

					for (int i = 0; i < (int)actions.Count; i++) {
						String action_name;
						PltXmlHelper.GetChildText (actions [i], "name", out action_name);
						if (action_name.Length == 0) {
							Plt.LogFatal (String.Format ("Invalid action name for action number %d", i + 1));
							throw new Exception ("NPT_ERROR_INVALID_SYNTAX");
						}

						PltActionDesc action_desc = new PltActionDesc (action_name, this);
						m_ActionDescs.Add (action_desc);        

						// action arguments
						XmlElement argumentList = PltXmlHelper.GetChild (actions [i], "argumentList");
						if (argumentList == null || argumentList.ChildNodes.Count == 0)
							continue; // no arguments is ok I guess

						List<XmlElement> arguments = new List<XmlElement> ();
						Plt.CheckSevere (PltXmlHelper.GetChildren (argumentList, out arguments, "argument"));

						bool ret_value_found = false;
						for (int j = 0; j < (int)arguments.Count; j++) {
							String name, direction, relatedStateVar;
							PltXmlHelper.GetChildText (arguments [j], "name", out name);
							PltXmlHelper.GetChildText (arguments [j], "direction", out direction);
							PltXmlHelper.GetChildText (arguments [j], "relatedStateVariable", out relatedStateVar);

							if (name.Length == 0 || direction.Length == 0 || relatedStateVar.Length  == 0) {
								Plt.LogFatal (String.Format ("Invalid argument for action %s", action_name));
								throw new Exception ("NPT_ERROR_INVALID_SYNTAX");
							}

							// make sure the related state variable exists
							PltStateVariable variable = FindStateVariable (relatedStateVar);
							if (variable == null) {
								Plt.LogFatal (String.Format ("State variable not found %s", relatedStateVar));
								throw new Exception ("NPT_ERROR_INVALID_SYNTAX");
							}

							bool ret_value = false;
							XmlElement ret_val_node = PltXmlHelper.GetChild (arguments [j], "retVal");
							if (ret_val_node != null) {
								// verify this is the only retVal we've had
								if (ret_value_found) {
									Plt.LogFatal ("Return argument already found");
									throw new Exception ("NPT_ERROR_INVALID_SYNTAX");
								} else {
									ret_value = true;
									ret_value_found = true;
								}
							}
							action_desc.GetArgumentDescs ().Add (new PltArgumentDesc (name, j, direction, variable, ret_value));
						}
					}
				}

			} catch (Exception e) {
				Console.Error.WriteLine (e);
				Plt.LogFatal (String.Format ("Failed to parse scpd: {0}", scpd));

				return false;
			}
		
			return true;		 
		}

		public Boolean GetDescription(XmlElement parent, out XmlElement service)
		{
			 
			service = parent.OwnerDocument.CreateElement (parent.Prefix,"service",parent.NamespaceURI);
		
			parent.AppendChild (service);

			Plt.CheckSevere (PltXmlHelper.AddChildText (service, "serviceType", m_ServiceType));
			Plt.CheckSevere (PltXmlHelper.AddChildText (service, "serviceId", m_ServiceID));
			Plt.CheckSevere (PltXmlHelper.AddChildText (service, "SCPDURL", GetSCPDURL ()));
			Plt.CheckSevere (PltXmlHelper.AddChildText (service, "controlURL", GetControlURL ()));
			Plt.CheckSevere (PltXmlHelper.AddChildText (service, "eventSubURL", GetEventSubURL ()));

			return true;
		
		}

		public Boolean SetStateVariable(String name, String value)
		{
			PltStateVariable stateVariable = null;
			Plt.ContainerFind(m_StateVars, new PltStateVariableNameFinder(name), out stateVariable);
			if (stateVariable == null)
				return false;

			return stateVariable.SetValue(value);
		}

		public Boolean SetStateVariableRate(String name, TimeSpan rate)
		{
			PltStateVariable stateVariable = null;
			Plt.ContainerFind(m_StateVars, new PltStateVariableNameFinder(name), out stateVariable);
			if (stateVariable == null)
				return false;

			return stateVariable.SetRate(rate);
		}

		public Boolean SetStateVariableExtraAttribute(String name, String key, String value)
		{
			PltStateVariable stateVariable = null;
			Plt.ContainerFind(m_StateVars, new PltStateVariableNameFinder(name), out stateVariable);
			if (stateVariable == null)
				return false;

			return stateVariable.SetExtraAttribute(key, value);
		}

		public Boolean IncStateVariable(String name)
		{
			PltStateVariable stateVariable = null;
			Plt.ContainerFind(m_StateVars, new PltStateVariableNameFinder(name), out stateVariable);
			if (stateVariable == null)
				return false;

			String value = stateVariable.GetValue();
			int num;
			if (value.Length == 0 || Plt.IsFailed(Int32.TryParse(value, out num))) {
				return false;
			}

			// convert value to int
			return stateVariable.SetValue((num+1).ToString());
		}

		public PltStateVariable FindStateVariable(String name)
		{
			PltStateVariable stateVariable = null;
			Plt.ContainerFind(m_StateVars, new PltStateVariableNameFinder(name), out stateVariable);
			return stateVariable;
		}

		public Boolean GetStateVariableValue(String name, out String value)
		{
			PltStateVariable stateVariable = FindStateVariable(name);
			Plt.CheckPointerSevere(stateVariable);
			value = stateVariable.GetValue();
			return true;
		}

		public Boolean IsSubscribable()
		{
			foreach(PltStateVariable avar in m_StateVars){
				if (avar.IsSendingEvents) return true;
			}

			return false;
		}

		public List<PltStateVariable> GetStateVariables() 
		{
			return m_StateVars; 
		}

		public PltActionDesc FindActionDesc(String name)
		{
			PltActionDesc action = null;
			Plt.ContainerFind(m_ActionDescs, new PltActionDescNameFinder(name), out action);
			return action;
		}

		public List<PltActionDesc> GetActionDescs()
		{
			return m_ActionDescs; 
		}

		public static bool IsTrue(String value) {
			if (value.EqualsIgnoreCase("1")    ||
				value.EqualsIgnoreCase("true") ||
				value.EqualsIgnoreCase("yes")) {
				return true;
			}
			return false;
		}

		private void Cleanup()
		{
			m_ActionDescs.Apply(new DisposerApplier<PltActionDesc>());
			m_StateVars.Apply(new DisposerApplier<PltStateVariable>());

			m_ActionDescs.Clear();
			m_StateVars.Clear();
			m_Subscribers.Clear();
		}

		public  Boolean AddChanged(PltStateVariable avar)
		{
			lock(m_Lock){

				// no event task means no subscribers yet, so don't bother
				// Note: this will take care also when setting default state 
				//       variables values during init and avoid being published
				if (m_EventTask==null) return true;

				if (avar.IsSendingEvents) {
					if (!m_StateVarsToPublish.Contains(avar)) m_StateVarsToPublish.Add(avar);
				} else if (avar.IsSendingEventsIndirecly) {
					if (!m_StateVarsChanged.Contains(avar)) m_StateVarsChanged.Add(avar);

					UpdateLastChange(m_StateVarsChanged);
				}

			}
			return true;
		}

		private Boolean UpdateLastChange(List<PltStateVariable> vars)
		{
			PltStateVariable avar = FindStateVariable("LastChange");
			if (avar == null) return false;

			Plt.Assert(m_LastChangeNamespace.Length > 0);

			if (vars.Count == 0) {
				// no vars to update, remove LastChange from vars to publish
				m_StateVarsToPublish.Remove(avar);
				return true;
			}
			XmlDocument doc = new XmlDocument ();
			XmlElement top = doc.CreateElement ("Event",m_LastChangeNamespace);// new NPT_XmlElementNode("Event"));

			 


			XmlElement instance = doc.CreateElement("InstanceID");
			top.AppendChild(instance);
			instance.SetAttribute("val", "0");

			// build list of changes
			Plt.CheckSevere(vars.ApplyUntil(new PltLastChangeXMLIterator(instance),ApplyUntil.ResultSuccess));

			// serialize node
			String value;
			Plt.CheckSevere(PltXmlHelper.Serialize(top, out value, false));

			// set the state change directly instead of calling SetValue
			// to avoid recursive lock, instead add LastChange var directly
			avar.SetValue(value);

			// add to list of vars scheduled to be published next time if not already there
			if (!m_StateVarsToPublish.Contains(avar)) m_StateVarsToPublish.Add(avar);
			return true;
		}


		public Boolean NotifyChanged()
		{
			lock(m_Lock){

			// no eventing for now
			if (m_EventingPaused) return true;

			// pick the vars that are ready to be published
			// based on their moderation rate and last publication
				List<PltStateVariable> vars_ready = new List<PltStateVariable>();
				for(int i =0 ;i < m_StateVarsToPublish.Count;i++){
					PltStateVariable avar = m_StateVarsToPublish[i];
					if (avar.IsReadyToPublish()) {
						vars_ready.Add(avar);
						m_StateVarsToPublish.Remove(avar);
						i--;

						// clear last changed list if we're about to send LastChange var
						if (avar.Name.Equals("LastChange")) m_StateVarsChanged.Clear();

						continue;
					}						
				}
			

			// if nothing to publish then bail out now
			// we'll clean up expired subscribers when we have something to publish
			if (vars_ready.Count == 0) return true;

			// send vars that are ready to go and remove old subscribers 
			for(int k=0;k<m_Subscribers.Count;k++){
					PltEventSubscriber sub = m_Subscribers[k];
					DateTime now = DateTime.Now;
					DateTime expiration;

					expiration = sub.GetExpirationTime();

					// forget sub if it didn't renew subscription in time or if notification failed
					if (expiration == new DateTime() || now.CompareTo(expiration.AddSeconds(30))<0) {
						// TODO: Notification is asynchronous, so we won't know if it failed until
						// the subscriber m_SubscriberTask is done
						Boolean res = vars_ready.Count!=0?sub.Notify(vars_ready):true;
						if (Plt.IsSucceded(res)) {							 
							continue;
						}
					}

					m_Subscribers.RemoveAt(k--);
				}
			}

				 
			return true;
		}


		public Boolean ProcessNewSubscription(PltTaskManager task_manager, IPAddress addr, String callback_urls, int timeout, HttpResponse response)
		{
			Plt.LogFine(String.Format("New subscription for {0} (timeout = {1})", m_EventSubURL, timeout));
			 

			//    // first look if we don't have a subscriber with same callbackURL
			//    PLT_EventSubscriber* subscriber = null;
			//    if (NPT_SUCCEEDED(NPT_ContainerFind(m_Subscribers, PLT_EventSubscriberFinderByCallbackURL(strCallbackURL),
			//        subscriber))) {
			//        // update local interface and timeout
			//        subscriber->m_local_if.SetIpAddress((unsigned long) addr.GetIpAddress());
			//        subscriber->m_ExpirationTime = NPT_Time(null) + timeout;
			//
			//        PLT_UPnPMessageHelper::SetSID("uuid:" + subscriber->m_sid);
			//        PLT_UPnPMessageHelper::SetTimeOut(timeout);
			//        return NPT_SUCCESS;
			//    }
			//
			// reject if we have too many subscribers already
			if (m_Subscribers.Count > 30) {
				response.SetStatus(500, "Internal Server Error");
				return false;
			}

			//TODO: prevent hacking by making sure callbackurl is not ourselves?

			// generate a unique subscriber ID
			String sid;
			PltUPnPMessageHelper.GenerateGUID(out sid);
			sid = "uuid:" + sid;

			PltEventSubscriber subscriber = new PltEventSubscriber(task_manager, this, sid, timeout);
			// parse the callback URLs
			bool reachable = false;
			if (callback_urls[0] == '<') {
				char[] szURLs = callback_urls.ToCharArray();
				int brackL = 0;
				int brackR = 0;
				while (++brackR < callback_urls.Length) {
					if (szURLs[brackR] == '>') {
						String strCallbackURL = callback_urls.Substring(brackL+1,brackR-brackL-1);
						HttpUri url = new HttpUri(strCallbackURL);


						subscriber.AddCallbackURL(strCallbackURL);
						reachable = true;

						brackL = ++brackR;
					}
				}
			}

			if (reachable == false) {
				response.SetStatus(412, "Precondition Failed");
				return false;
			}

			// keep track of which interface we receive the request, we will use this one
			// when notifying
			subscriber.SetLocalIf(addr);

			PltUPnPMessageHelper.SetSID(response, subscriber.GetSID());
			PltUPnPMessageHelper.SetTimeOut(response, timeout);

			 
			lock(m_Lock){

				// new subscriber should get all vars in the LastChange var
				UpdateLastChange(m_StateVars);

				// send all state vars to sub immediately
				Boolean res = subscriber.Notify(m_StateVars);

				// reset LastChange var to what was really just changed
				UpdateLastChange(m_StateVarsChanged);

				// make sure the event worked before spawning our recurrent task
				if (Plt.IsFailed (res)) {
					response.SetStatus(412, "Precondition Failed");
					return false;
				}

				// schedule a recurring event notification task if not running already
				if (m_EventTask==null) {
					PltServiceEventTask task = new PltServiceEventTask(this);
					Plt.CheckSevere(task_manager.StartTask(task));

					m_EventTask = task;
				}

				m_Subscribers.Add(subscriber);
			}

			return true;						 
		}


		public Boolean ProcessRenewSubscription(IPEndPoint addr, String sid, int timeout_secs, HttpResponse response)
		{
			lock (m_Lock) {

				Plt.LogFine (String.Format ("Renewing subscription for %s (sub=%s)", m_EventSubURL, sid));

				// first look if we don't have a subscriber with same callbackURL
				PltEventSubscriber subscriber;
				if (Plt.IsSucceded (Plt.ContainerFind (m_Subscribers, new PltEventSubscriberFinderBySID (sid), out subscriber))) {

					DateTime now = DateTime.Now;
					DateTime expiration;
				
					expiration = subscriber.GetExpirationTime ();

					// renew subscriber if it has not expired
					if (expiration.CompareTo (now) > 0) {
						// update local interface and timeout
						subscriber.SetLocalIf (addr.Address);
						subscriber.SetTimeout (timeout_secs);

						PltUPnPMessageHelper.SetSID (response, subscriber.GetSID ());
						PltUPnPMessageHelper.SetTimeOut (response, timeout_secs);
						return true;
					} else {
						Plt.LogFine (String.Format ("Subscriber \"%s\" didn't renew in time", subscriber.GetSID ()));
						m_Subscribers.Remove (subscriber);
					}
				}

				Plt.LogWarn (String.Format ("Failed to renew subscription for %s!", sid));

				// didn't find a valid Subscriber in our list
				response.SetStatus (412, "Precondition Failed");
				return false;
			}
		}

		public Boolean ProcessCancelSubscription(EndPoint addr, String sid, HttpResponse response)
		{
			lock (m_Lock) {

				// first look if we don't have a subscriber with same callbackURL
				PltEventSubscriber sub;
				if (Plt.IsSucceded (Plt.ContainerFind (m_Subscribers, new PltEventSubscriberFinderBySID (sid), out sub))) {
					Plt.LogFine (String.Format ("Cancelling subscription for %s (sub=%s)", m_EventSubURL, sid));

					// remove sub
					m_Subscribers.Remove (sub);
					return true;
				}

				Plt.LogWarn (String.Format ("Cancelling subscription for unknown subscriber %s!", sid));

				// didn't find a valid Subscriber in our list
				response.SetStatus (412, "Precondition Failed");
				return false;
			}
		}
	}
	  
	  














	public class PltServiceSCPDURLFinder:IFinder<PltService>
	{
		public string Url{get;set;}
		public PltServiceSCPDURLFinder(String url)
		{
			Url = url;
		}

		public Boolean IsFound(PltService service)
		{
			HttpUri uri = new HttpUri (Url);
			HttpUri scpd = new HttpUri(service.GetSCPDURL (Url.StartsWith ("http://") ? true : false));
			return uri.Uri.LocalPath.Equals (scpd.Uri.LocalPath);
		}
	}

	public class PltServiceControlURLFinder:IFinder<PltService>
	{
		public string Url{get;set;}
		public PltServiceControlURLFinder(String url)
		{
			Url = url;
		}

		public Boolean IsFound(PltService service)
		{
			HttpUri uri = new HttpUri (Url);
			HttpUri scpd = new HttpUri(service.GetControlURL (Url.StartsWith ("http://") ? true : false));
			return uri.Uri.LocalPath.Equals (scpd.Uri.LocalPath);
		}
	}
	public class PltServiceEventSubURLFinder:IFinder<PltService>
	{
		public string Url{get;set;}
		public PltServiceEventSubURLFinder(String url)
		{
			Url = url;
		}

		public Boolean IsFound(PltService service)
		{
			HttpUri uri = new HttpUri (Url);
			HttpUri scpd = new HttpUri(service.GetEventSubURL (Url.StartsWith ("http://") ? true : false));
			return uri.Uri.LocalPath.Equals (scpd.Uri.LocalPath);			 
		}
	}
	public class PltServiceIDFinder:IFinder<PltService>
	{
		public string Id{get;set;}
		public PltServiceIDFinder(String id)
		{
			Id = id;
		}

		public Boolean IsFound(PltService service)
		{
			return Id.Equals (service.GetServiceID ());
		}
	}
	public class PltServiceTypeFinder:IFinder<PltService>
	{
		public string Type{get;set;}
		public PltServiceTypeFinder(String type)
		{
			Type = type;
		}

		public Boolean IsFound(PltService service)
		{
			// DLNA: match any version if last char is '*'
			if (Type.EndsWith("*")) {
				return true;
			}

			return Type.Equals(service.GetServiceType());
		}
	}
	public class PltServiceNameFinder:IFinder<PltService>
	{
		public string ServiceName{get;set;}
		public PltServiceNameFinder(String serviceName)
		{
			ServiceName = serviceName;
		}

		public Boolean IsFound(PltService service)
		{
			return ServiceName.Equals (service.GetServiceName ());
		}
	}
	public class PltLastChangeXMLIterator:IApplier<PltStateVariable>
	{
		public XmlElement Node{get;set;}
		public PltLastChangeXMLIterator(XmlElement node)
		{
			Node = node;
		}

		public Boolean Apply(PltStateVariable avar)
		{
			// only add vars that are indirectly evented
			if (!avar.IsSendingEventsIndirecly)
				return true;

			XmlElement variable = Node.OwnerDocument.CreateElement (avar.Name);
			Node.AppendChild (variable);
			avar.Serialize (variable);
							
			return true;
		}
	}
 
	 



	public class PltServiceEventTask:PltThreadTask
	{
		public PltService Service{ get; set; }

		public PltServiceEventTask(PltService service)
		{
			Service = service;
		}

		protected override void DoRun()
		{
			while (!IsAborting(100)) Service.NotifyChanged();
		}
	}
}
   