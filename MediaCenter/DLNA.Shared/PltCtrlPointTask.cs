﻿using System;


namespace Platinum
{
	public class PltCtrlPointGetDescriptionTask:PltHttpClientSocketTask
	{
		protected PltCtrlPoint   m_CtrlPoint;
		protected TimeSpan m_LeaseTime;
		protected String       m_UUID;

		public PltCtrlPointGetDescriptionTask (HttpUri url,PltCtrlPoint ctrl_point,TimeSpan   leasetime,String uuid):base(new HttpRequest(url, "GET", HttpMessage.NPT_HTTP_PROTOCOL_1_1))
		{
			m_CtrlPoint = ctrl_point;
			m_LeaseTime = leasetime;
			m_UUID = uuid;
		}

		protected override Boolean ProcessResponse(Boolean res, HttpRequest request,HttpRequestContext context,HttpResponse response)
		{
			return m_CtrlPoint.ProcessGetDescriptionResponse(res, request, context,	response, m_LeaseTime,	m_UUID);
		}
	}

	public class PltCtrlPointGetSCPDRequest:HttpRequest
	{
		public PltDeviceData m_Device;

		public PltCtrlPointGetSCPDRequest(PltDeviceData device,String url):this(device,url,"GET", HttpMessage.NPT_HTTP_PROTOCOL_1_1)
		{
		}

		public PltCtrlPointGetSCPDRequest(PltDeviceData device,String url,String method,String protocol):base(new HttpUri(url), method, protocol)
		{
			m_Device = device;
		}
	}

	public class PltCtrlPointGetSCPDsTask: PltHttpClientSocketTask
	{
		protected PltCtrlPoint          m_CtrlPoint;
		protected PltDeviceData m_RootDevice;

		public PltCtrlPointGetSCPDsTask(PltCtrlPoint ctrl_point, PltDeviceData root_device)
		{
			m_CtrlPoint = ctrl_point;
			m_RootDevice = root_device;
		}

		public Boolean AddSCPDRequest(PltCtrlPointGetSCPDRequest request) {
			return base.AddRequest((HttpRequest)request);
		}

		// override to prevent calling this directly
		public override Boolean AddRequest(HttpRequest request) {
			// only queuing PLT_CtrlPointGetSCPDRequest allowed
			throw new Exception ("Not supported");
		}

		protected  override Boolean ProcessResponse(Boolean res, HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
			return m_CtrlPoint.ProcessGetSCPDResponse(res,request, 	context, response,	((PltCtrlPointGetSCPDRequest)request).m_Device);
		}


	}

	class PltCtrlPointInvokeActionTask :  PltHttpClientSocketTask
	{
		protected PltCtrlPoint      m_CtrlPoint;
		protected PltAction m_Action;
		protected byte[]               m_Userdata;

		public	PltCtrlPointInvokeActionTask(HttpRequest request,PltCtrlPoint ctrl_point, PltAction action,byte[] userdata) : base(request)
		{
			m_CtrlPoint = ctrl_point;
			m_Action = action;
			m_Userdata = userdata;
		}


		protected override Boolean ProcessResponse(Boolean res,HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
		
			return m_CtrlPoint.ProcessActionResponse (res, request, context, response, m_Action, m_Userdata);
		}


	}

	class PltCtrlPointHouseKeepingTask : PltThreadTask
	{
		protected PltCtrlPoint   m_CtrlPoint;
		protected TimeSpan m_Timer;

		public PltCtrlPointHouseKeepingTask(PltCtrlPoint ctrl_point):this( ctrl_point,TimeSpan.FromSeconds(5))
		{
		}

		public PltCtrlPointHouseKeepingTask(PltCtrlPoint ctrl_point, TimeSpan timer)
		{
			m_CtrlPoint = ctrl_point;
			m_Timer = timer;
		}

	 
		// PLT_ThreadTask methods
		protected override void DoRun()
		{
			while (!IsAborting ((int)(m_Timer.TotalSeconds * 1000))) {
				if (m_CtrlPoint!=null) {
					m_CtrlPoint.DoHouseKeeping ();
				}
			}
		}

	
	}


	class PltCtrlPointSubscribeEventTask : PltHttpClientSocketTask
	{
		protected PltCtrlPoint          m_CtrlPoint;
		protected PltService            m_Service;
		protected PltDeviceData m_Device; // force to keep a reference to device owning m_Service
		protected byte[]                   m_Userdata;

		public	PltCtrlPointSubscribeEventTask(HttpRequest request, PltCtrlPoint ctrl_point, PltDeviceData device, PltService service,byte[] userdata = null) : base(request){
		m_CtrlPoint = ctrl_point;
		m_Service = service;
		m_Device = device;
		m_Userdata = userdata;
	
		}
		 

		protected override Boolean ProcessResponse(Boolean res, HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
		
			return m_CtrlPoint.ProcessSubscribeResponse (res, request, context, response, m_Service, m_Userdata);
		}
	}
}
   
 