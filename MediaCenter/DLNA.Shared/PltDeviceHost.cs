﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Xml;
using System.Net.NetworkInformation;

namespace Platinum
{

	class PltDeviceHostRequestHandler:HttpRequestHandler
	{
		private PltDeviceHost device;

		public PltDeviceHostRequestHandler (PltDeviceHost device)
		{
			this.device = device;
		}


		public override Boolean SetupResponse (HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
			// get the address of who sent us some data back*/
			String ip_address = context.RemoteAddress.Address.ToString ();
			String method = request.Method;
			String protocol = request.Protocol; 

			PltHttp.Log ("PltDeviceHost::SetupResponse:", request);

			if (method.Equals ("POST")) {
				return device.ProcessHttpPostRequest (request, context, response);
			} else if (method.Equals ("SUBSCRIBE") || method.Equals ("UNSUBSCRIBE")) {
				return device.ProcessHttpSubscriberRequest (request, context, response);
			} else if (method.Equals ("GET") || method.Equals ("HEAD")) {
				// process SCPD requests
				PltService service;
				if (Plt.IsSucceded (device.FindServiceBySCPDURL (request.Url.Uri.AbsoluteUri, out service, true))) {
					return device.ProcessGetSCPD (service, request, context, response);
				}

				// process Description document requests
				if (request.Url.Uri.LocalPath.Equals (device.URLDescription.Uri.LocalPath)) {
					return device.ProcessGetDescription (request, context, response);
				}

				// process other requests
				return device.ProcessHttpGetRequest (request, context, response);
			}

			response.SetStatus (405, "Bad Request");
			return true;

		}

	}



	/**
 UPnP Device Host.
 The PltDeviceHost class is a base class for implementing a UPnP Device. It handles 
 network announcements and responses to searches from ControlPoints. ControlPoint
 action invocations are also received and delegated to derived classes. A 
 PltDeviceHost also takes care of eventing when services state variables change.
 */
	public class PltDeviceHost:PltDeviceData, PltSsdpPacketListener
	{
		/*
		extern NPT_UInt8 Platinum_120x120_jpg[16096];
		extern NPT_UInt8 Platinum_120x120_png[26577];
		extern NPT_UInt8 Platinum_48x48_jpg[3041];
		extern NPT_UInt8 Platinum_48x48_png[4681];
*/
		private PltTaskManager taskManager;
		private PltHttpServer httpServer;
		private bool extraBroascast;
		private int port;
		private bool portRebind;
		private bool byeByeFirst;
		private bool started;

		/*
		protected PltTaskManager m_TaskManager;
		protected PltHttpServer m_HttpServer;
		protected bool m_ExtraBroascast;
		protected int m_Port;
		protected bool m_PortRebind;
		protected bool m_ByeByeFirst;
		protected bool m_Started;
*/
		public PltDeviceHost (String descriptionPath, String uuid, String deviceType, String friendlyName, bool showIp, int port, bool portRebind) : base (new HttpUri (null, 0, descriptionPath), uuid, PltConstants.GetInstance ().DefaultDeviceLease, deviceType, friendlyName)
		{
			taskManager = null;
			httpServer = null;
			extraBroascast = false;
			this.port = port;
			this.portRebind = portRebind;
			byeByeFirst = false;
			started = false;

			if (showIp) {
				List<IPAddress> ips = new List<IPAddress> ();
				PltUPnPMessageHelper.GetIPAddresses (out ips);
				if (ips.Count > 0) {
					friendlyName += " (" + ips [0].ToString () + ")";
				}
			}
		}

		public virtual void SetExtraBroadcast (bool broadcast)
		{
			extraBroascast = broadcast; 
		}

		public virtual void SetByeByeFirst (bool bye_bye_first)
		{
			byeByeFirst = bye_bye_first; 
		}

		public virtual int GetPort ()
		{
			return port; 
		}

		public Boolean AddIcon (PltDeviceIcon icon, String fileroot, String urlroot = "/")
		{
			// verify the url of the icon starts with the url root
			if (!icon.UrlPath.StartsWith (urlroot))
				throw new Exception ("NPT_ERROR_INVALID_PARAMETERS");

			HttpFileRequestHandler icon_handler = new HttpFileRequestHandler (urlroot, fileroot);
			httpServer.AddRequestHandler (icon_handler, icon.UrlPath, false, true);
			Icons.Add (icon);
			return true;
		}

		public Boolean AddIcon (PltDeviceIcon icon, String data, int size, bool copy = true)
		{
			HttpStaticRequestHandler icon_handler = new HttpStaticRequestHandler (data, size, icon.MimeType, copy);
			httpServer.AddRequestHandler (icon_handler, icon.UrlPath, false, true);
			Icons.Add (icon);
			return true;
		}



		public static Boolean Announce (PltDeviceData device, HttpRequest req, UdpClient socket, PltSsdpAnnounceType type)
		{
			Boolean res = true;

			// target address
			IPAddress ip;
			IPAddress[] addresses = Dns.GetHostAddresses (req.Url.Uri.Host);
			Plt.Assert (addresses.Length > 0);
			ip = addresses [0];
			IPEndPoint addr = new IPEndPoint (ip, req.Url.Uri.Port);

			//    // UPnP 1.1 BOOTID.UPNP.ORG header
			//    PltUPnPMessageHelper::SetBootId(req, device.m_BootId);
			//    
			//    // UPnP 1.1 CONFIGID.UPNP.ORG header
			//    if (device.m_ConfigId > 0) {
			//        PltUPnPMessageHelper::SetConfigId(req, devic.>m_ConfigId);
			//    }

			// NTS header
			String nts = "";
			switch (type) {
			case PltSsdpAnnounceType.PLT_ANNOUNCETYPE_ALIVE:
				nts = "ssdp:alive";
				PltUPnPMessageHelper.SetLeaseTime (req, device.LeaseTime);
				PltUPnPMessageHelper.SetServer (req, PltHttp.PLT_HTTP_DEFAULT_SERVER, false);
				break;

			case PltSsdpAnnounceType.PLT_ANNOUNCETYPE_BYEBYE:
				nts = "ssdp:byebye";
				break;

			case PltSsdpAnnounceType.PLT_ANNOUNCETYPE_UPDATE:
				nts = "ssdp:update";
				// update requires valid UPNP 1.1 NEXTBOOTID.UPNP.ORG Header
				if (device.NextBootId == 0) {
					throw new Exception ("NPT_ERROR_INTERNAL");
				}
				PltUPnPMessageHelper.SetNextBootId (req, device.NextBootId);
				break;

			default:
				break;
			}
			PltUPnPMessageHelper.SetNTS (req, nts);

			Plt.LogFine (String.Format ("Sending SSDP NOTIFY ({0}) Request to {1} ({2})", nts, req.Url.ToString (), PltUPnPMessageHelper.GetLocation (req) != null ? PltUPnPMessageHelper.GetLocation (req) : ""));

			// upnp:rootdevice
			if (String.IsNullOrEmpty (device.ParentUUID)) {
				PltSsdpSender.SendSsdp (req,	"uuid:" + device.UUID + "::upnp:rootdevice", "upnp:rootdevice",	socket,	true, addr);
			}

			// on byebye, don't sleep otherwise it hangs when we stop upnp
			if (type != PltSsdpAnnounceType.PLT_ANNOUNCETYPE_BYEBYE) {
				Thread.Sleep (TimeSpan.FromSeconds (PltUPnP.PLT_DLNA_SSDP_DELAY));
			}

			// uuid:device-UUID
			PltSsdpSender.SendSsdp (req,	"uuid:" + device.UUID, "uuid:" + device.UUID, socket, true, addr);

			// on byebye, don't sleep otherwise it hangs when we stop upnp
			if (type != PltSsdpAnnounceType.PLT_ANNOUNCETYPE_BYEBYE) {
				Thread.Sleep (TimeSpan.FromSeconds (PltUPnP.PLT_DLNA_SSDP_DELAY));
			}

			// uuid:device-UUID::urn:schemas-upnp-org:device:deviceType:ver
			PltSsdpSender.SendSsdp (req, "uuid:" + device.UUID + "::" + device.DeviceType, device.DeviceType, socket, true, addr);

			// on byebye, don't sleep otherwise it hangs when we stop upnp
			if (type != PltSsdpAnnounceType.PLT_ANNOUNCETYPE_BYEBYE) {
				Thread.Sleep (TimeSpan.FromSeconds (PltUPnP.PLT_DLNA_SSDP_DELAY));
			}

			// services
			for (int i = 0; i < device.Services.Count; i++) {
				// uuid:device-UUID::urn:schemas-upnp-org:service:serviceType:ver
				PltSsdpSender.SendSsdp (req, "uuid:" + device.UUID + "::" + device.Services [i].GetServiceType (), device.Services [i].GetServiceType (),	socket,	true, addr); 

				// on byebye, don't sleep otherwise it hangs when we stop upnp
				if (type != PltSsdpAnnounceType.PLT_ANNOUNCETYPE_BYEBYE) {
					Thread.Sleep (TimeSpan.FromSeconds (PltUPnP.PLT_DLNA_SSDP_DELAY));
				}
			}

			// embedded devices
			for (int j = 0; j < (int)device.EmbeddedDevices.Count; j++) {
				Announce (device.EmbeddedDevices [j],	req, socket, type);
			}

			return res;
		}

		public Boolean Announce (HttpRequest request, UdpClient socket, PltSsdpAnnounceType type)
		{
			return Announce (this, request, socket, type);
		}

		public virtual Boolean OnSsdpPacket (HttpRequest request, HttpRequestContext context)
		{
			// get the address of who sent us some data back*/
			String ip_address = context.RemoteAddress.Address.ToString ();
			String method = request.Method;
			String url = request.Url.ToString ();
			String protocol = request.Protocol;
			int remote_port = context.RemoteAddress.Port;
			String st = PltUPnPMessageHelper.GetST (request);

			if (method.Equals ("M-SEARCH")) {
				String prefix = String.Format ("PltDeviceHost::OnSsdpPacket M-SEARCH for {0} from {1}:{2}", !String.IsNullOrEmpty (st) ? st : "Unknown", ip_address, remote_port);
				PltHttp.Log (prefix, request);

				/*
        // DLNA 7.2.3.5 support
				if (remote_port < 1024 || remote_port == 1900) {
					NPT_LOG_INFO_2("Ignoring M-SEARCH from %s:%d (invalid source port)", 
						(const char*) ip_address,
						remote_port);
					return NPT_FAILURE;
				}
				*/

				Plt.CheckPointerSevere (st);

				if (url.Equals ("*") || !protocol.Equals ("HTTP/1.1"))
					return false;

				String man = PltUPnPMessageHelper.GetMAN (request);
				if (man == null || !man.EqualsIgnoreCase ("\"ssdp:discover\""))
					return false;

				int mx;
				Plt.CheckSevere (PltUPnPMessageHelper.GetMX (request, out mx));

				// create a task to respond to the request
				Random random = new Random ();
				TimeSpan timer = TimeSpan.FromSeconds ((mx == 0) ? 0 : (double)(random.Next () % (mx > 5 ? 5 : mx)));
				PltSsdpDeviceSearchResponseTask task = new PltSsdpDeviceSearchResponseTask (this, context.RemoteAddress, st);
				taskManager.StartTask (task, timer);
				return true;
			}

			return false;

		}

		public static Boolean SendSsdpSearchResponse (PltDeviceData device, HttpResponse response, UdpClient socket, String st, IPEndPoint addr = null)
		{
			// UPnP 1.1 BOOTID.UPNP.ORG header
			PltUPnPMessageHelper.SetBootId (response, device.BootId);

			// UPnP 1.1 CONFIGID.UPNP.ORG header
			if (device.ConfigId > 0) {
				PltUPnPMessageHelper.SetConfigId (response, device.ConfigId);
			}

			// ssdp:all or upnp:rootdevice
			if ("ssdp:all".Equals (st) || "upnp:rootdevice".Equals (st)) {

				if (String.IsNullOrEmpty (device.ParentUUID)) {
					Plt.LogFine (String.Format ("Responding to a M-SEARCH request for %s", st));

					// upnp:rootdevice
					PltSsdpSender.SendSsdp (response, "uuid:" + device.UUID + "::upnp:rootdevice", "upnp:rootdevice", socket,	false,	addr);
				}
			}

			// uuid:device-UUID
			if ("ssdp:all".Equals (st) || ("uuid:" + device.UUID).Equals (st)) {

				Plt.LogFine (String.Format ("Responding to a M-SEARCH request for %s", st));

				// uuid:device-UUID
				PltSsdpSender.SendSsdp (response, "uuid:" + device.UUID, "uuid:" + device.UUID, socket, false,	addr);
			}

			// urn:schemas-upnp-org:device:deviceType:ver
			if ("ssdp:all".Equals (st) || device.DeviceType.Equals (st)) {

				Plt.LogFine (String.Format ("Responding to a M-SEARCH request for %s", st));

				// uuid:device-UUID::urn:schemas-upnp-org:device:deviceType:ver
				PltSsdpSender.SendSsdp (response, "uuid:" + device.UUID + "::" + device.DeviceType, device.DeviceType, socket,	false,	addr);
			}

			// services
			for (int i = 0; i < device.Services.Count; i++) {
				if ("ssdp:all".Equals (st) ||	device.Services [i].GetServiceType ().Equals (st)) {

					Plt.LogFine (String.Format ("Responding to a M-SEARCH request for %s", st));

					// uuid:device-UUID::urn:schemas-upnp-org:service:serviceType:ver
					PltSsdpSender.SendSsdp (response, "uuid:" + device.UUID + "::" + device.Services [i].GetServiceType (), device.Services [i].GetServiceType (), socket, false, addr);
				}
			}

			// embedded devices
			for (int j = 0; j < device.EmbeddedDevices.Count; j++) {
				SendSsdpSearchResponse (device.EmbeddedDevices [j], response, socket, st, addr);
			}

			return true;
		}

		public virtual Boolean SendSsdpSearchResponse (HttpResponse response, UdpClient socket, String st, IPEndPoint addr = null)
		{
			return SendSsdpSearchResponse (this, response, socket, st, addr);
		}

		protected virtual Boolean SetupServices ()
		{
			Plt.CheckWarining (SetupServices ());
			Plt.CheckWarining (SetupIcons ());
			return true;

		}

		protected virtual Boolean SetupIcons ()
		{
			/*if (m_Icons.GetItemCount() == 0) {
		AddIcon(
			PltDeviceIcon("image/jpeg", 120, 120, 24, "/images/platinum-120x120.jpg"),
			Platinum_120x120_jpg, sizeof(Platinum_120x120_jpg), false);
		AddIcon(
			PltDeviceIcon("image/jpeg", 48, 48, 24, "/images/platinum-48x48.jpg"),
			Platinum_48x48_jpg, sizeof(Platinum_48x48_jpg), false);
		AddIcon(
			PltDeviceIcon("image/png", 120, 120, 24, "/images/platinum-120x120.png"),
			Platinum_120x120_png, sizeof(Platinum_120x120_png), false);
		AddIcon(
			PltDeviceIcon("image/png", 48, 48, 24, "/images/platinum-48x48.png"),
			Platinum_48x48_png, sizeof(Platinum_48x48_png), false);
	}*/
			return true;

		}

		protected virtual Boolean SetupDevice ()
		{
			Plt.CheckWarining (SetupServices ());
			Plt.CheckWarining (SetupIcons ());
			return true;
		}

		public virtual Boolean Start (PltSsdpListenTask task)
		{
			Boolean result;

			if (started)
				Plt.CheckWarining (false/*NPT_ERROR_INVALID_STATE*/);

			// setup
			taskManager = new PltTaskManager ();
			httpServer = new PltHttpServer (IPAddress.Any, port, portRebind, 100); // limit to 100 clients max  
			if (Plt.IsFailed (result = httpServer.Start ())) {
				taskManager = null;
				httpServer = null;
				Plt.CheckSevere (result);
			}

			// read back assigned port in case we passed 0 to randomly select one
			port = httpServer.GetPort ();
			UriBuilder builder = new UriBuilder (URLDescription.Uri);
			builder.Port = port;
			URLDescription = new HttpUri (builder.Uri);

			// callback to initialize the device
			if (Plt.IsFailed (result = SetupDevice ())) {
				taskManager = null;
				httpServer = null;
				Plt.CheckSevere (result);
			}

			// all other requests including description document
			// and service control are dynamically handled
			httpServer.AddRequestHandler (new PltHttpRequestHandler (new PltDeviceHostRequestHandler (this)), "/", true, true);

			// we should not advertise right away
			// spec says randomly less than 100ms
			Random rand = new Random ();
			TimeSpan delay = TimeSpan.FromMilliseconds (rand.Next (100));

			// calculate when we should send another announcement
			// we announce a bit before half way through leasetime to make sure
			// clients don't expire us.
			int leaseTime = (int)LeaseTime.TotalSeconds;
			TimeSpan repeat = TimeSpan.FromSeconds (leaseTime > 0 ? (int)(leaseTime - 10) : 30);
			 

			PltThreadTask announce_task = new PltSsdpDeviceAnnounceTask (this, repeat, byeByeFirst, extraBroascast);
			taskManager.StartTask (announce_task, delay);

			// register ourselves as a listener for SSDP search requests
			task.AddListener (this);

			started = true;
			return true;
		}

		public virtual Boolean Stop (PltSsdpListenTask task)
		{
			if (!started)
				Plt.CheckWarining (false /*NPT_ERROR_INVALID_STATE*/);

			// mark immediately we're stopping
			started = false;

			// unregister ourselves as a listener for ssdp requests
			task.RemoveListener (this);

			// remove all our running tasks
			taskManager.Abort ();

			// stop our internal http server
			httpServer.Stop ();

			// announce we're leaving
			List<NetworkInterface> if_list;
			PltUPnPMessageHelper.GetNetworkInterfaces (out if_list, true);
			if_list.Apply (new PltSsdpAnnounceInterfaceIterator (this, PltSsdpAnnounceType.PLT_ANNOUNCETYPE_BYEBYE, extraBroascast));
			//if_list.Apply(new PltObjectDeleter<NetworkInterface>());

			// Cleanup all services and embedded devices
			Cleanup ();

			httpServer = null;
			taskManager = null;

			return true;

		}

		protected virtual Boolean OnAction (PltAction action, HttpRequestContext context)
		{			 
			action.SetError (401, "Invalid Action");
			return false;
		}

		internal virtual Boolean ProcessGetDescription (HttpRequest request, HttpRequestContext context, HttpResponse response)
		{			 
			String doc;
			Plt.CheckSevere (GetDescription (out doc));
			Plt.LogFine (String.Format ("Returning description to {0}: {1}", context.RemoteAddress.Address.ToString (), doc));

			HttpEntity entity;
			PltHttpHelper.SetBody (response, doc, out entity);    
			entity.SetContentType ("text/xml; charset=\"utf-8\"");
			return true;

		}

		internal virtual Boolean ProcessGetSCPD (PltService service, HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
			 
			Plt.CheckPointerSevere (service);

			String doc;
			Plt.CheckSevere (service.GetSCPDXML (out doc));
			Plt.LogFine (String.Format ("Returning SCPD to {0}: {1}", context.RemoteAddress.Address.ToString (), doc));

			HttpEntity entity;
			PltHttpHelper.SetBody (response, doc, out entity);    
			entity.SetContentType ("text/xml; charset=\"utf-8\"");
			return true;

		}

		public virtual Boolean ProcessHttpGetRequest (HttpRequest request, HttpRequestContext context, HttpResponse    response)
		{
			return false; //NPT_ERROR_NO_SUCH_ITEM
		}

	
	

		internal virtual Boolean ProcessHttpSubscriberRequest (HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
			String ip_address = context.RemoteAddress.Address.ToString ();
			String method = request.Method;
			String url = request.Url.ToString ();
			String protocol = request.Protocol;

			String nt = PltUPnPMessageHelper.GetNT (request);
			String callback_urls = PltUPnPMessageHelper.GetCallbacks (request);
			String sid = PltUPnPMessageHelper.GetSID (request);

			PltService service;
			try {
				Plt.CheckSevere (FindServiceByEventSubURL (url, out service, true));

				if (method.Equals ("SUBSCRIBE")) {
					// Do we have a sid ?
					if (sid != null) {
						// make sure we don't have a callback nor a nt
						Plt.Assert (nt == null && callback_urls == null);

						// default lease
						TimeSpan timeout = PltConstants.GetInstance ().DefaultSubscribeLease;

						// subscription renewed
						// send the info to the service
						service.ProcessRenewSubscription (context.LocalAddress, sid, (int)timeout.TotalSeconds, response);
						return true;
					} else {
						// new subscription ?
						// verify nt is present and valid
						if (nt == null || !nt.Equals ("upnp:event")) {
							response.SetStatus (412, "Precondition failed");
							return true;
						}
						// verify callback is present
						if (callback_urls == null) {
							response.SetStatus (412, "Precondition failed");
							return true;
						}

						// default lease time
						TimeSpan timeout = PltConstants.GetInstance ().DefaultSubscribeLease;

						// send the info to the service
						service.ProcessNewSubscription (taskManager, context.LocalAddress.Address, callback_urls, (int)timeout.TotalSeconds, response);
						return true;
					}
				} else if (method.Equals ("UNSUBSCRIBE")) {
					// Do we have a sid ?
					if (sid != null && sid.Length > 0) {
						// make sure we don't have a callback nor a nt
						if (nt == null || callback_urls == null) {
							Plt.Severe ();
						}

						// subscription cancelled
						// send the info to the service
						service.ProcessCancelSubscription (context.LocalAddress, sid, response);
						return true;
					}

					response.SetStatus (412, "Precondition failed");
					return true;
				}
			} catch (Exception e) {
				//cleanup
				response.SetStatus (400, "Bad Request");
			}
			 
		
			return true;

		}


		 
		internal Boolean ProcessHttpPostRequest (HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
			Boolean res;
			String service_type;
			String str;
			XmlElement xml = null;
			String soap_action_header;
			PltService service;
			XmlElement soap_body;
			XmlElement soap_action;
			PltActionDesc action_desc;
			PltAction action = null;
			MemoryStream resp = new MemoryStream ();
			String ip_address = context.RemoteAddress.Address.ToString ();
			String method = request.Method;
			String url = request.Url.ToString ();
			String protocol = request.Protocol;
			String[] components;
			String soap_action_name;

			//#if defined(PLATINUM_UPNP_SPECS_STRICT)
			String attr;
			//#endif
			try {
				Plt.CheckSevere (FindServiceByControlURL (url, out service, true));


				Plt.CheckPointerSevere (request.Headers.GetHeaderValue ("SOAPAction"));


				// extract the soap action name from the header
				soap_action_header = request.Headers.GetHeaderValue ("SOAPAction");
				soap_action_header = soap_action_header.TrimStart ('"');
				soap_action_header = soap_action_header.TrimEnd ('"');

				components = soap_action_header.Split ('#');
				Plt.Assert (components.Length == 2);


				soap_action_name = components [1];

				// read the xml body and parse it
				Plt.CheckSevere (PltHttpHelper.ParseBody (request, out xml));


				// check envelope
				Plt.Assert (xml.LocalName.EqualsIgnoreCase ("Envelope"));


				//#if defined(PLATINUM_UPNP_SPECS_STRICT)
				// check namespace
				if (String.IsNullOrEmpty (xml.NamespaceURI) || !xml.NamespaceURI.Equals ("http://schemas.xmlsoap.org/soap/envelope/"))
					Plt.Severe ();

				// check encoding
				attr = xml.GetAttribute ("encodingStyle", "http://schemas.xmlsoap.org/soap/envelope/");
				if (attr == null || !attr.Equals ("http://schemas.xmlsoap.org/soap/encoding/"))
					Plt.Severe ();
				//#endif

				// read action
				soap_body = PltXmlHelper.GetChild (xml, "Body");
				if (soap_body == null)
					Plt.Severe ();

				PltXmlHelper.GetChild (soap_body, out soap_action);
				if (soap_action == null)
					Plt.Severe ();

				// verify action name is identical to SOAPACTION header*/
				if (soap_action.Name.EqualsIgnoreCase (soap_action_name))
					Plt.Severe ();

				// verify namespace
				if (String.IsNullOrEmpty (soap_action.NamespaceURI) || !soap_action.NamespaceURI.Equals (service.GetServiceType ()))
					Plt.Severe ();
				try {
					// create a buffer for our response body and call the service
					if ((action_desc = service.FindActionDesc (soap_action_name)) == null) {
						// create a bastard soap response
						PltAction.FormatSoapError (401, "Invalid Action", resp);
						Plt.Severe ();
					}

					// create a new action object
					action = new PltAction (action_desc);

					// read all the arguments if any
					foreach (XmlElement child in soap_action.ChildNodes) {
						// Total HACK for xbox360 upnp uncompliance!
						String name = child.Name;
						if (action_desc.GetName ().Equals ("Browse") && name.Equals ("ContainerID")) {
							name = "ObjectID";
						}

						res = action.SetArgumentValue (name,	!String.IsNullOrEmpty (child.InnerText) ? child.InnerText : "");

						// test if value was correct
						if (res == false/*NPT_ERROR_INVALID_PARAMETERS*/) {
							action.SetError (701, "Invalid Name");
							Plt.Severe ();
						}

					}
			 

					// verify all required arguments were passed
					if (Plt.IsFailed (action.VerifyArguments (true))) {
						action.SetError (402, "Invalid or Missing Args");
						Plt.Severe ();
					}

					Plt.LogFine (String.Format ("Processing action \"{0}\" from {1}", action.GetActionDesc ().GetName (), context.RemoteAddress.Address.ToString ()));

					// call the virtual function, it's all good
					if (Plt.IsFailed (OnAction (action, new PltHttpRequestContext (request, context)))) {
						Plt.Severe ();
					}

					// create the soap response now
					action.FormatSoapResponse (resp);
				} catch (Exception err) {
					//error:
					if (action != null) {
						// set the error in case it wasn't done already
						if (action.GetErrorCode () == 0) {
							action.SetError (501, "Action Failed");
						}
						Plt.LogWarn (String.Format ("Error while processing action {0}: {1} {2}", action.GetActionDesc ().GetName (), action.GetErrorCode (), action.GetError ()));

						action.FormatSoapResponse (resp);
					}
					response.SetStatus (500, "Internal Server Error");
				} finally {
					long resp_body_size = resp.Length;    

					if (resp_body_size > 0) {
						HttpEntity entity;
						PltHttpHelper.SetBody (response, resp, out entity);
						entity.SetContentType ("text/xml; charset=\"utf-8\"");
						response.Headers.SetHeader ("Ext", ""); // should only be for M-POST but oh well
					}    
				}

			

			
				 
				return true;

			} catch (Exception e) {

				//bad_request:
				//delete xml;
				Console.Error.Write ("Errore post");
				response.SetStatus (500, "Bad Request");
				return true;
			}
		}



	}
}

 
       

