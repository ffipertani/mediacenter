﻿using System;
using System.Threading;

namespace Platinum
{
	public class SharedVariable
	{
		private int value;

		public SharedVariable(int value)
		{
			this.value = value;
		}

		public int GetValue()
		{
			return value;
		}

		public void SetValue(int value)
		{
			this.value = value;
		}

		public Boolean WaitUntilEquals(int value) 
		{ 
			return WaitUntilEquals (value, TimeSpan.MaxValue);
		}

		public Boolean WaitUntilEquals(int value, TimeSpan timeout) { 
			double totalMillis = timeout.TotalMilliseconds;
			while (!this.value.Equals (value)) {
				if (totalMillis <= 0) {
					return false;
				}
				Thread.Sleep (100);
				totalMillis -= 100;
			}
			return true;
		}

		public Boolean WaitWhileEquals(int value) 
		{ 
			return WaitWhileEquals (value, TimeSpan.MaxValue);
		}

		public Boolean WaitWhileEquals(int value, TimeSpan timeout) { 
			while (this.value.Equals (value)) {
				Thread.Sleep (100);
				timeout.Subtract (TimeSpan.FromTicks (100));
				if (timeout.Ticks <= 0) {
					return false;
				}
			}
			return true;
		}

	}
}

