﻿using System;
using System.Collections.Generic;
using Platinum;

namespace MoviePlayer
{
	public class DeviceManager
	{
		private static object monitor = new object();
		private static DeviceManager instance;

		PltCtrlPoint ctrlPoint;
		MediaController controller;
		PltUPnP pp;

		private DeviceManager ()
		{
			ctrlPoint = new PltCtrlPoint ();
			//ctrlPoint.AddListener (new MyCtrlPointListener ());
			controller = new MediaController (ctrlPoint, new MediaControllerDelegate (this));
			pp = new PltUPnP();			 
			pp.AddCtrlPoint (ctrlPoint);
			pp.Start ();

			Refresh ();

		}

		public void Refresh()
		{
			ctrlPoint.Search ();
			ctrlPoint.Discover ();
		}

		public void Play(PltDeviceData device, String url){
			controller.Stop (device, 0, null);
			controller.SetAVTransportURI (device, 0, "http://t66.coolcdn.ch/dl/9e76aa101e2bfcf78f2b7ef35046a112/54a30ea1/ff877d64ff2f9ac2bd1f0126dab8762844.flv?client=FLASH", "", null);
			//http://spider2.akstream.net/vtmp/00/bx33xxbg.mp4
			controller.Play (device, 0, "1", null);
		}

		public List<PltDeviceData> Renderers{ get; private set;} = new List<PltDeviceData>();

		public static DeviceManager Instance{ 
			get{
				lock (monitor) {
					if (instance == null) {
						instance = new DeviceManager ();
					}
					return instance;
				}
			}
		}
	}


	class MediaControllerDelegate:PltMediaControllerDelegate
	{
		private DeviceManager viewModel;
		public MediaControllerDelegate(DeviceManager viewModel)
		{
			this.viewModel = viewModel;
		}


		public override bool OnMRAdded (PltDeviceData device)
		{
			viewModel.Renderers.Add (device);
			return base.OnMRAdded (device);
		}


	}
}

