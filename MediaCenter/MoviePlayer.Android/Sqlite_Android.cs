﻿using System;
using System.IO;
using SQLite.Net;

namespace MoviePlayer.Android
{
	public class SQLite_Android : ISQLite {
		public SQLite_Android () {}

		public SQLiteConnection GetConnection () {
			var sqliteFilename = "TodoSQLite.db3";
			string documentsPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal); // Documents folder
			var path = Path.Combine(documentsPath, sqliteFilename);
			// Create the connection
			var plat = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
			var conn = new SQLite.Net.SQLiteConnection(plat, path);
			// Return the database connection 
			return conn;
		}}
}

