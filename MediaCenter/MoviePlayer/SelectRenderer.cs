﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovieData;
using MovieData.Model;
using Xamarin.Forms;
using System.IO;
using System.Diagnostics;

namespace MoviePlayer
{
	public partial class MovieDetail:ContentPage
    {
		Image originalBackdrop;
		Xamarin.Forms.Image backdrop;
		AbsoluteLayout layout;
		StackLayout stageLayout;
		Size originalSize;
		bool sizeSetted = false;
		public MovieDetail ()
		{
			InitializeComponent ();


		}
		 
		public MovieDetail(Movie movie)
		{


			//backdropView = new ImageView (imgBackdrop);
			InitializeComponent ();

			((MovieDetailViewModel)BindingContext).MovieDetail = this;
			((MovieDetailViewModel)BindingContext).Movie = movie;


			backdrop = this.FindByName <Image>("image");
			stageLayout = this.FindByName<StackLayout> ("stage");
			//image.WidthRequest = this.Width;
			//image.HeightRequest = this.Height;
			layout = this.FindByName<AbsoluteLayout> ("absoluteLayout");


			SizeChanged += myPage_SizeChanged;
		}

		void myPage_SizeChanged(object sender, EventArgs e)
		{
			Debug.WriteLine(Width + " " + Height);
			if (!sizeSetted) {
				originalSize = backdrop.Bounds.Size;
				sizeSetted = true;
			}

			Size backdropSize = new Size();
			double widthProportion = this.Bounds.Width / backdrop.Width;
			double heightProportion = this.Bounds.Height / backdrop.Height;
			if (widthProportion > heightProportion) {
				backdropSize.Height = backdrop.Height * widthProportion;
				backdropSize.Width = backdrop.Width * widthProportion;			 
			} else {
				backdropSize.Height = backdrop.Height * heightProportion;
				backdropSize.Width = backdrop.Width * heightProportion;
			}
			backdrop.HeightRequest = backdropSize.Height;
			backdrop.WidthRequest = backdropSize.Width;
			 
			 
			double x = (backdrop.Width - Width) / 2;
			backdrop.TranslationX = -x;

			double y = (backdrop.Height - Height) / 2;
			backdrop.TranslationY = -y;
 

			AbsoluteLayout.SetLayoutBounds (stageLayout, new Rectangle (0, 0, Width, Height));
			  
		}

		public Grid Grid
		{
			get{
				return this.FindByName<Grid> ("mygrid");
			}
		}

 
    }
}
