﻿using System;
using System.Collections.Generic;


namespace MoviePlayer
{
	public interface IDeviceManager
	{
		void Refresh();

		void Play(Object device, String url);

		List<Object> Renderers{ get;}

	}


}

