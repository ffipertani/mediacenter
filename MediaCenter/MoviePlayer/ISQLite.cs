﻿using System;
using SQLite.Net;


namespace MoviePlayer
{
	public interface ISQLite
	{
		SQLiteConnection GetConnection();
	}
}

