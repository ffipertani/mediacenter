﻿using System;
using System.Drawing;
using MonoMac.Foundation;
using MonoMac.AppKit;
using MonoMac.ObjCRuntime;
using Vlc.DotNet.Core;
using Xwt;

namespace MediaCenter.Mac
{
	class MainClass
	{
		static void Main (string[] args)
		{
 
			string version = typeof(Application).Assembly.GetName ().Version.ToString ();
			Application.Initialize ("MediaCenter.Mac.MyMacEngine, MediaCenter.Mac, Version=" + version);

			MyMainWindow w = new MyMainWindow ();
			w.Show ();

			Application.Run ();
		 
			w.Dispose ();


			VlcContext.CloseAll();
			Application.Dispose ();
		
		}
	}
}

