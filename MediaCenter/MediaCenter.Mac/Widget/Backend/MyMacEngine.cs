﻿using System;
using Xwt.Mac;
using MonoMac.AppKit;
using Xwt.Backends;

namespace MediaCenter.Mac
{
	public class MyMacEngine:MacEngine
	{
		public MyMacEngine ()
		{
		}
		 

		public override void InitializeBackends ()
		{
			base.InitializeBackends ();
		 
			RegisterBackend <IMyWindowBackend, MyWindowBackend> ();
			RegisterBackend <IToolbarBackend, ToolbarBackend> ();
			RegisterBackend <IScrollViewBackend, MyScrollViewBackend> (); 
			RegisterBackend <IMediaPlayerBackend, MediaPlayerBackend> (); 
			RegisterBackend <IPanedBackend, MyPanedBackend> (); 
			RegisterBackend <IBoxBackend, MyBoxBackend> ();
			RegisterBackend <IMyLabelBackend, MyLabelBackend> ();
			RegisterBackend <ITextEntryBackend, MyTextEntryBackend> ();


			//RegisterBackend <IStageBackend, StageBackend> ();

		}
	}
}

