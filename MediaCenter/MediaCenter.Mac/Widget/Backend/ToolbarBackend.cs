﻿using System;
using Xwt.Mac;
using MediaCenter;
using MonoMac.AppKit;
using Xwt.Backends;
using MonoMac.CoreGraphics;
using System.Drawing;

namespace MediaCenter.Mac
{
	public class ToolbarBackend:BoxBackend, IToolbarBackend
	{
		public override void Initialize ()
		{
			ViewObject = new MyBaseWidgetView (EventSink, ApplicationContext);
			 
		}
  
	}
}