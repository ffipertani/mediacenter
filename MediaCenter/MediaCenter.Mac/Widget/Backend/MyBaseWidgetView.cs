﻿using System;
using Xwt.Backends;
using MonoMac.AppKit;
using Xwt.Mac;
using MonoMac.CoreGraphics;
using Xwt;
using MonoMac.Foundation;
using System.Drawing;

namespace MediaCenter.Mac
{
	public class MyBaseWidgetView: NSView, IViewObject
	{
		IWidgetEventSink eventSink;
		protected ApplicationContext context;
		NSTrackingArea trackingArea;
		// Captures Mouse Entered, Exited, and Moved events
		NSCursor ctype = NSCursor.CurrentCursor;
		 
		public MyBaseWidgetView (IWidgetEventSink eventSink, ApplicationContext context)
		{
			this.context = context;
			this.eventSink = eventSink;




			//AddCursorRect (Bounds, NSCursor.PointingHandCursor);
			//NSCursor.PointingHandCursor.SetOnMouseEntered (true);
		
		}


		public void SetCursor (CursorType cursor)
		{

			if (cursor == CursorType.Arrow)
				ctype = NSCursor.ArrowCursor;
			else if (cursor == CursorType.Crosshair)
				ctype = NSCursor.CrosshairCursor;
			else if (cursor == CursorType.Hand)
				ctype = NSCursor.ClosedHandCursor;
			else if (cursor == CursorType.IBeam)
				ctype = NSCursor.IBeamCursor;
			else if (cursor == CursorType.ResizeDown)
				ctype = NSCursor.ResizeDownCursor;
			else if (cursor == CursorType.ResizeUp)
				ctype = NSCursor.ResizeUpCursor;
			else if (cursor == CursorType.ResizeLeft)
				ctype = NSCursor.ResizeLeftCursor;
			else if (cursor == CursorType.ResizeRight)
				ctype = NSCursor.ResizeRightCursor;
			else if (cursor == CursorType.ResizeLeftRight)
				ctype = NSCursor.ResizeLeftRightCursor;
			else if (cursor == CursorType.ResizeUpDown)
				ctype = NSCursor.ResizeUpDownCursor;
			else
				ctype = NSCursor.ArrowCursor;
		}

		public override void ResetCursorRects ()
		{
			if (ctype != null) {
				AddCursorRect (Bounds, ctype);
			}
		}


		public ViewBackend Backend { get; set; }

		public NSView View {
			get { return this; }
		}

		public override bool IsFlipped {
			get {
				return true;
			}
		}

		public override bool AcceptsFirstResponder ()
		{
			return Backend.CanGetFocus;
		}

		public override void DrawRect (System.Drawing.RectangleF dirtyRect)
		{
			//CGContext ctx = NSGraphicsContext.CurrentContext.GraphicsPort;

			//fill BackgroundColor
			//ctx.SetFillColor (Backend.Frontend.BackgroundColor.ToCGColor ());
			//ctx.FillRect (Bounds);
		
		}

		public override void UpdateTrackingAreas ()
		{
			if (trackingArea != null) {
				RemoveTrackingArea (trackingArea);
				trackingArea.Dispose ();
			}
			System.Drawing.RectangleF viewBounds = this.Bounds;
			var options = NSTrackingAreaOptions.MouseMoved | NSTrackingAreaOptions.ActiveInKeyWindow | NSTrackingAreaOptions.MouseEnteredAndExited;
			trackingArea = new NSTrackingArea (viewBounds, options, this, null);
			AddTrackingArea (trackingArea);
		}

		public override void RightMouseDown (NSEvent theEvent)
		{
			var p = ConvertPointFromView (theEvent.LocationInWindow, null);
			ButtonEventArgs args = new ButtonEventArgs ();
			args.X = p.X;
			args.Y = p.Y;
			args.Button = PointerButton.Right;
			context.InvokeUserCode (delegate {
				eventSink.OnButtonPressed (args);
			});
		}

		public override void RightMouseUp (NSEvent theEvent)
		{
			var p = ConvertPointFromView (theEvent.LocationInWindow, null);
			ButtonEventArgs args = new ButtonEventArgs ();
			args.X = p.X;
			args.Y = p.Y;
			args.Button = PointerButton.Right;
			context.InvokeUserCode (delegate {
				eventSink.OnButtonReleased (args);
			});
		}

		public override void MouseDown (NSEvent theEvent)
		{

			var p = ConvertPointFromView (theEvent.LocationInWindow, null);
			ButtonEventArgs args = new ButtonEventArgs ();
			args.X = p.X;
			args.Y = p.Y;
			args.Button = PointerButton.Left;
			context.InvokeUserCode (delegate {
				eventSink.OnButtonPressed (args);
			});
		}

		public override void MouseUp (NSEvent theEvent)
		{
			var p = ConvertPointFromView (theEvent.LocationInWindow, null);
			ButtonEventArgs args = new ButtonEventArgs ();
			args.X = p.X;
			args.Y = p.Y;
			args.Button = (PointerButton)theEvent.ButtonNumber + 1;
			context.InvokeUserCode (delegate {
				eventSink.OnButtonReleased (args);
			});
		}

		public override void MouseEntered (NSEvent theEvent)
		{
			context.InvokeUserCode (delegate {
				eventSink.OnMouseEntered ();
			});
		}

		public override void MouseExited (NSEvent theEvent)
		{

			context.InvokeUserCode (delegate {
				eventSink.OnMouseExited ();
			});
		}

		public override void MouseMoved (NSEvent theEvent)
		{
			var p = ConvertPointFromView (theEvent.LocationInWindow, null);
			MouseMovedEventArgs args = new MouseMovedEventArgs ((long)TimeSpan.FromSeconds (theEvent.Timestamp).TotalMilliseconds, p.X, p.Y);
			context.InvokeUserCode (delegate {
				eventSink.OnMouseMoved (args);
			});
		}

		public override void MouseDragged (NSEvent theEvent)
		{
			var MouseDragDetectionThreshold = 1;
			if (Window.MovableByWindowBackground ||
				(Window.StyleMask & NSWindowStyle.FullScreenWindow) == NSWindowStyle.FullScreenWindow)
			{
				base.MouseDragged(theEvent);
				return;
			}

			var @where = Window.ConvertBaseToScreen(theEvent.LocationInWindow);
			var origin = Window.Frame.Location;
			var deltaX = 0.0f;
			var deltaY = 0.0f;

			while ((theEvent != null) && (theEvent.Type != NSEventType.LeftMouseUp))
			{
				using (new NSAutoreleasePool())
				{
					var now = Window.ConvertBaseToScreen(theEvent.LocationInWindow);
					deltaX += now.X - @where.X;
					deltaY += now.Y - @where.Y;

					if (Math.Abs(deltaX) >= MouseDragDetectionThreshold ||
						Math.Abs(deltaY) >= MouseDragDetectionThreshold)
					{
						origin.X += deltaX;
						origin.Y += deltaY;

						Window.SetFrameOrigin(origin);
						deltaX = 0f;
						deltaY = 0f;
					}

						@where = now;
					theEvent = NSApplication.SharedApplication.NextEvent(NSEventMask.LeftMouseDown | NSEventMask.LeftMouseDragged | NSEventMask.LeftMouseUp, NSDate.DistantFuture, NSRunLoopMode.EventTracking.ToString(), true);
				}
			}

			var p = ConvertPointFromView (theEvent.LocationInWindow, null);
			MouseMovedEventArgs args = new MouseMovedEventArgs ((long)TimeSpan.FromSeconds (theEvent.Timestamp).TotalMilliseconds, p.X, p.Y);
			context.InvokeUserCode (delegate {
				eventSink.OnMouseMoved (args);
			});
		}

		public override void SetFrameSize (System.Drawing.SizeF newSize)
		{
			bool changed = !newSize.Equals (Frame.Size);
			base.SetFrameSize (newSize);
			if (changed) {
				context.InvokeUserCode (delegate {
					eventSink.OnBoundsChanged ();
				});
			}
		}
	}
}