﻿using System;
using Xwt.Mac;

namespace MediaCenter.Mac
{
	public class MyBoxBackend:BoxBackend
	{

		public override void Initialize ()
		{
			ViewObject = new MyBaseWidgetView (EventSink, ApplicationContext);
		}
	}
}

