﻿using System;
using Xwt.Mac;
using MonoMac.AppKit;
using System.Drawing;
using Xwt.Backends;
using System.Timers;

namespace MediaCenter.Mac
{
	public class MyPanedBackend: ViewBackend<NSSplitView,IPanedEventSink>, IPanedBackend
	{
		 
		CustomSplitView split;
		SplitViewDelegate viewDelegate;
		internal NSView view1;
		internal NSView view2;
		internal double dividerPosition = 40;
		internal double maxDividerPosition = 300;
		internal double minDividerPosition = 150;

		public MyPanedBackend ()
		{
		}

		public void Initialize (Orientation dir)
		{
			split = new CustomSplitView (){ PanedBackend = this };
			ViewObject = split;
			if (dir == Orientation.Horizontal)
				Widget.IsVertical = true;
			viewDelegate = new SplitViewDelegate () { PanedBackend = this };
			Widget.Delegate = viewDelegate;
			split.DividerStyle = NSSplitViewDividerStyle.Thin;
			Widget.SetPositionOfDivider ((float)dividerPosition, 0);
			 

		}

	
		 
		public void SetPanel (int panel, IWidgetBackend widget, bool resize, bool shrink)
		{
			ViewBackend view = (ViewBackend)widget;
			var w = GetWidgetWithPlacement (view);
			RemovePanel (panel);
			Widget.AddSubview (w);
			Widget.AdjustSubviews ();
			if (panel == 1)
				view1 = w;
			else
				view2 = w;
			view.NotifyPreferredSizeChanged ();

			//split.SetPositionOfDivider (0.2f, 1);
			//	if (view2 != null) {
			//		view2.Bounds = new RectangleF (view2.Bounds.X, view2.Bounds.Y, view2.Bounds.Width -4, view2.Bounds.Height);
			//	}
		}

	 
		public void UpdatePanel (int panel, bool resize, bool shrink)
		{
		}

		public void RemovePanel (int panel)
		{
			if (panel == 1) {
				if (view1 != null) {
					view1.RemoveFromSuperview ();
					RemoveChildPlacement (view1);
					view1 = null;
				}
			} else {
				if (view2 != null) {
					view2.RemoveFromSuperview ();
					RemoveChildPlacement (view2);
					view2 = null;
				}
			}
		}

		public override void ReplaceChild (NSView oldChild, NSView newChild)
		{
			if (view1 != null)
				view1.RemoveFromSuperview ();
			if (view2 != null)
				view2.RemoveFromSuperview ();

			if (oldChild == view1)
				view1 = newChild;
			else
				view2 = newChild;

			if (view1 != null)
				Widget.AddSubview (view1);
			if (view2 != null)
				Widget.AddSubview (view2);
		}

		public double Position {
			get {
				return  dividerPosition;
			}
			set {
				 
				dividerPosition = value;
				Widget.SetPositionOfDivider ((float)value, 0);
 
			}
		}

		internal void DidResizeSubviews ()
		{
			EventSink.OnPositionChanged ();
			 
		}



	}

	class CustomSplitView: NSSplitView, IViewObject
	{
		internal MyPanedBackend PanedBackend;
		internal Boolean Autohide{ get; set;}

	 


		public NSView View {
			get {
				return this;
			}
		}

	 
		public override void DrawRect (RectangleF rect)
		{

		}

		 
		public override void ResizeSubviewsWithOldSize(SizeF oldSize)
		{
			 
			Console.WriteLine ("SIZE"+oldSize);
			if (oldSize.Width > 130 && oldSize.Width < 300) {
			
			} else if (oldSize.Width < PanedBackend.minDividerPosition) {
				oldSize.Width = (float) PanedBackend.minDividerPosition;
			} else if (oldSize.Width >  PanedBackend.maxDividerPosition) {
				oldSize.Width = (float) PanedBackend.maxDividerPosition;
			} else {
				SetPositionOfDivider (oldSize.Width, 0);
				AdjustSubviews ();
				return;
			}
			base.ResizeSubviewsWithOldSize (oldSize);
				
			SetPositionOfDivider (oldSize.Width, 0);
			 
			//PanedBackend.dividerPosition = oldSize.Width;

		}
 

		public ViewBackend Backend { get; set; }
	}

 

	class SplitViewDelegate: NSSplitViewDelegate
	{

		public MyPanedBackend PanedBackend;

		 
		public CustomSplitView View {
			get {
				return (CustomSplitView)PanedBackend.Widget;
			}
		}

		public override void SplitViewWillResizeSubviews (MonoMac.Foundation.NSNotification notification)
		{

		}

		public override void DidResizeSubviews (MonoMac.Foundation.NSNotification notification)
		{
			PanedBackend.DidResizeSubviews ();
			Console.WriteLine ("RESIZED");
			 

		}
			 
		public override float ConstrainSplitPosition (NSSplitView sview, float proposed, int index)
		{
			 
			if (proposed < PanedBackend.minDividerPosition) {
				proposed =  (float)PanedBackend.minDividerPosition;
			} else if (proposed > PanedBackend.maxDividerPosition) {
				proposed = (float)PanedBackend.maxDividerPosition;
			}
			PanedBackend.dividerPosition = proposed;
			 
			return proposed;
		}
		  
	}
}

