﻿using System;
using Xwt.Mac;
using MonoMac.AppKit;
using Xwt.Drawing;
using System.Drawing;
using Xwt.Backends;
using MonoMac.Foundation;

namespace MediaCenter.Mac
{
	public class MyScrollViewBackend:ScrollViewBackend,IMyScrollViewBackend
	{
		private RectangleF lastBounds = new RectangleF(-1,-1,-1,-1);
		CustomScrollView scrollView;
		NormalClipView clipView;

		public MyScrollViewBackend ()
		{

		}
		 
		public Xwt.Size Size {
			get {
				SizeF size = scrollView.ContentView.DocumentRect.Size;
				return new Xwt.Size ((int)size.Width, (int)size.Height);
			}
		}
 


		public override void Initialize ()
		{
			scrollView = new CustomScrollView (this);
			ViewObject = scrollView;
			Widget.HasHorizontalScroller = false;
			Widget.HasVerticalScroller = false;
			Widget.AutoresizesSubviews = true;
			//Widget.VerticalScroller.WantsLayer = true;
			//Widget.WantsLayer = true;
			//Widget.Layer.BackgroundColor = NSColor.Black.CGColor;
			//Widget.BackgroundColor = NSColor.Red;
			Widget.AutoresizingMask = NSViewResizingMask.HeightSizable | NSViewResizingMask.WidthSizable;
			Widget.AutohidesScrollers = true;
			Widget.VerticalScroller = new MyScroller (this);
			Widget.HorizontalScroller = new MyScroller (this);

			clipView = new NormalClipView ();
			clipView.Scrolled += delegate {
				Console.WriteLine("SCROLLED");
			};

			scrollView.ContentView = clipView;
			//scrollView.DocumentView = Table;

			 



		 
			//Widget.ScrollerKnobStyle = NSScrollerKnobStyle.Light;

			//Widget.VerticalScroller.WantsLayer = true;			 
				

			//Widget.VerticalScroller = null;
			//Widget.HorizontalScroller = null;


		}


		public new Xwt.Rectangle VisibleRect {
			get {
				RectangleF source = scrollView.DocumentVisibleRect;
				return new Xwt.Rectangle((int)source.X,(int)source.Y,(int)source.Width,(int)source.Height);
			}
		}

		//Fix for color the bottom right corner when scrollbars are visibile
		internal void onDrawView(RectangleF rect)
		{

		
			NSBezierPath path = new NSBezierPath ();
			path.MoveTo (new PointF(0,0));
			path.LineTo (new PointF(Widget.VisibleRect().Width,0));
			path.LineTo (new PointF(Widget.VisibleRect().Width,Widget.VisibleRect().Height));
			path.LineTo (new PointF(0,Widget.VisibleRect().Height));			 
			path.ClosePath ();			 
			BackgroundColor.ToNSColor ().Set ();
			path.Fill ();

			 path = new NSBezierPath ();
			path.MoveTo (new PointF(0,0));
			path.LineTo (new PointF(Widget.VisibleRect().Width+20,0));
			path.LineTo (new PointF(Widget.VisibleRect().Width+20,Widget.VisibleRect().Height+20));
			path.LineTo (new PointF(0,Widget.VisibleRect().Height+20));			 
			path.ClosePath ();			 
			NSColor.Red.ColorWithAlphaComponent(0f).Set ();
			path.Fill ();


			//Widget.ContentView.Bounds = Widget.Bounds;

		 /*
			int factor = 20;
			path = new NSBezierPath ();
			path.MoveTo (rect.Location);
			path.LineTo (new PointF(rect.Width-factor,rect.Height-factor));
			path.LineTo (new PointF(rect.Width,rect.Height-factor));
			path.LineTo (new PointF(rect.Width,rect.Height));
			//path.LineTo (new PointF(rect.Width-factor,rect.Height));
			path.ClosePath ();			 
			KnobBackgroundColor.Set ();
			path.Fill ();
 
	*/

			int factor = 20;
			path = NSBezierPath.FromRect (new RectangleF(rect.Width,rect.Height,rect.Width+factor,rect.Height+factor));			
			KnobBackgroundColor.Set ();
			path.Fill ();





			//------------
			/*
			float position = -1f;
			float height = -1f;
			float width = -1f;
			NSColor color = KnobBackgroundColor;
			if (Widget.HorizontalScroller.Enabled) {
				//color = BackgroundColor.ToNSColor ().ColorWithAlphaComponent (0.0f);
				RectangleF knobSlot = new RectangleF(0,rect.Height-15,rect.Width,15);
				color.Set ();
				NSBezierPath.FillRect (knobSlot);
			}
			if (Widget.VerticalScroller.Enabled) {
				//color = BackgroundColor.ToNSColor ().ColorWithAlphaComponent (0.0f);
				RectangleF knobSlot = new RectangleF(rect.Width-15,0,15,rect.Height);
				color.Set ();
				NSBezierPath.FillRect (knobSlot);

				position = scrollView.DocumentVisibleRect.Y;
				height = scrollView.DocumentVisibleRect.Height * scrollView.VerticalScroller.KnobProportion;
				//height=	scrollView.ContentView.DocumentRect.Size.Height * scrollView.VerticalScroller.KnobProportion;;
				width = 7;
				float margin = width / 2;
				Console.WriteLine ("proportion:" + scrollView.VerticalScroller.KnobProportion + " height:" + height);
				Console.WriteLine ("Rect" + rect);
				RectangleF knobRect = new RectangleF (rect.Width-15+margin, position, width, height);
				Console.WriteLine ("knobRect" + knobRect);
				NSColor.Red.Set ();
				NSBezierPath.FillRect (knobRect);

			}

		 
			 

*/





			if (lastBounds.Width==-1) {
				lastBounds = rect;
			} else {
				if (!lastBounds.Equals(rect)) {
					boundsChanged ();
					lastBounds = rect;

				}
			}
		}

		internal void onDrawKnobSlot(MyScroller sender, RectangleF rect, bool highlight)
		{
			 
			NSColor color = KnobBackgroundColor;
			 
			if (rect.Width > rect.Height) {
				if (!Widget.HorizontalScroller.Enabled) {
					color = BackgroundColor.ToNSColor ().ColorWithAlphaComponent (0.0f);
				}
			} else {
				if (!Widget.VerticalScroller.Enabled) {
					color = BackgroundColor.ToNSColor ().ColorWithAlphaComponent (0.0f);
				}
			}
		
			/*
			NSBezierPath path = new NSBezierPath ();
			path.MoveTo (rect.Location);
			path.LineTo (new PointF(rect.Location.X+rect.Width,rect.Location.Y));
			path.LineTo (new PointF(rect.Location.X+rect.Width,rect.Location.Y+rect.Height ));
			path.LineTo (new PointF(rect.Location.X,rect.Location.Y+rect.Height ));
			path.LineTo (new PointF(rect.Location.X,rect.Location.Y));
			path.ClosePath ();
			 
			color.Set ();
			path.Fill ();
			*/
			color.Set ();
			NSBezierPath.FillRect (rect);
 
			float position = -1f;
			float height = -1f;
			float width = -1f;
			if (sender == scrollView.VerticalScroller) {

				position = scrollView.DocumentVisibleRect.Y;
				//position = scrollView.DocumentVisibleRect.Height*position/scrollView.ContentView.DocumentRect.Height;

				position = position * scrollView.VerticalScroller.KnobProportion;
				position = position + 8;
				height = scrollView.DocumentVisibleRect.Height * scrollView.VerticalScroller.KnobProportion;
				height = height - 18;
				//height=	scrollView.ContentView.DocumentRect.Size.Height * scrollView.VerticalScroller.KnobProportion;;
				width = (int)rect.Width / 2;
				width = width + 1;
				float margin = width / 2;
				Console.WriteLine ("proportion:" + scrollView.VerticalScroller.KnobProportion + " height:" + height);
				Console.WriteLine ("Rect" + rect);
				RectangleF knobRect = new RectangleF (rect.Location.X + margin, position, width, height);
				Console.WriteLine ("knobRect" + knobRect);
				Console.WriteLine ("Highlight" + highlight);
				NSColor.Red.Set ();
				NSBezierPath.FillRect (knobRect);

				NSBezierPath top = new NSBezierPath ();
				top.AppendPathWithOvalInRect (new RectangleF (rect.Location.X + margin, position-4, width, 10));
				top.Fill ();
				NSBezierPath bottom = new NSBezierPath ();
				bottom.AppendPathWithOvalInRect (new RectangleF (rect.Location.X + margin, position +height-4, width, 10));
				bottom.Fill ();
 /*
				path = new NSBezierPath ();
				path.MoveTo (new PointF(rect.Location.X+margin,position));
				path.LineTo (new PointF(rect.Width-margin,position));
				path.LineTo (new PointF(rect.Width-margin,position+height ));
				path.LineTo (new PointF(rect.Location.X+margin,position+height ));
				path.LineTo (new PointF(rect.Location.X+margin,position));
				path.ClosePath ();
					path.Fill ();
 
*/
			
			} else {
				position = scrollView.DocumentVisibleRect.X;
				height = scrollView.DocumentVisibleRect.Width * scrollView.HorizontalScroller.KnobProportion;
			}

 

		}

		internal void boundsChanged()
		{
			//Widget.AutohidesScrollers = false;
			ApplicationContext.InvokeUserCode (delegate {
				((IScrollViewEventSink)EventSink).OnVisibleRectChanged ();
			});
			 
		}

		internal NSColor KnobBackgroundColor {
			get {
				return NSColor.FromDeviceRgba (0.33f, 0.33f, 0.33f, 0.4f);
			}
		}
	}


	class CustomScrollView: NSScrollView, IViewObject
	{

		MyScrollViewBackend parent;
		public CustomScrollView(MyScrollViewBackend parent)
		{
			this.parent = parent;

		}



		public NSView View {
			get {
				return this;
			}
		}

		public override void DrawRect(RectangleF rect){
			//base.DrawRect (rect);
			parent.onDrawView (rect);
		}

		public ViewBackend Backend { get; set; }

		public override bool IsFlipped {
			get {
				return true;
			}
		}
	}

	class MyScroller:NSScroller
	{
		MyScrollViewBackend parent;

		public MyScroller(MyScrollViewBackend parent)
		{
			this.parent = parent;
			ControlSize = NSControlSize.Regular;
			ControlTint = NSControlTint.Clear;
			ScrollerStyle = NSScrollerStyle.Overlay;
		}
			
		 
		public override void DrawRect(RectangleF rect){
			//base.DrawRect (rect);
			DrawKnobSlot (rect, true);
		}
 
		public override void DrawKnobSlot(RectangleF rect, bool highlight)
		{
			this.parent.onDrawKnobSlot (this, rect, highlight);

		}

		 
	}

	class NormalClipView: NSClipView
	{
		public event EventHandler Scrolled;

		public override void ScrollToPoint (System.Drawing.PointF newOrigin)
		{
			base.ScrollToPoint (newOrigin);
			if (Scrolled != null)
				Scrolled (this, EventArgs.Empty);
		}
	}
}

