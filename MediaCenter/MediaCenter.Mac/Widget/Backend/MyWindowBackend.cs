﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;

using MonoMac.Foundation;
using MonoMac.AppKit;
using MonoMac.CoreGraphics;
using MonoMac.CoreText;

using Xwt.Mac;
using Xwt.Drawing;
using AshokGelal.AppStoreWindow;
 
namespace MediaCenter.Mac
{
 
	[Register ("MyWindowBackend")]
	public class MyWindowBackend:AppStoreWindow, IMyWindowBackend
	{
		private IToolbarBackend toolbar;
		private NSObject o = new NSObject();

		public MyWindowBackend ()
		{	 
			Title = "Window";
			Visible = false;
			BaselineSeparatorColor = NSColor.Yellow;
			ShowBaselineSeparator = true;
			DrawGradientBackground = true;
			TitleTextColor = NSColor.DarkGray;
			TitleBarStartColor = NSColor.Black;
			TitleBarEndColor = NSColor.Black;
			//BackgroundColor = Xwt.Drawing.Colors.Black;


			StyleMask = MonoMac.AppKit.NSWindowStyle.Closable | MonoMac.AppKit.NSWindowStyle.Miniaturizable | MonoMac.AppKit.NSWindowStyle.Resizable | MonoMac.AppKit.NSWindowStyle.TexturedBackground | MonoMac.AppKit.NSWindowStyle.Titled | MonoMac.AppKit.NSWindowStyle.UnifiedTitleAndToolbar;

			DidBecomeKey += delegate {


				TitleBarView.DrawRect(TitleBarView.Bounds);


				NSShadow shadow = new NSShadow ();
				shadow.ShadowColor=NSColor.Black.ColorWithAlphaComponent(0.5f);
				shadow.ShadowOffset=new SizeF(0f, 0f);
				shadow.ShadowBlurRadius=0f;
				shadow.Set();
				TitleTextShadow = shadow;
				TitleBarHeight = (float)60;
				ShowsTitle = true;
				CenterTrafficLightButtons = false;
				//IsOpaque = true;


			};
			
			this.Level = NSWindowLevel.Floating;
			this.Level = NSWindowLevel.Normal;
		
		}

		public void Invoke(InvokeAction del)
		{
 
			NSAction action = new NSAction (delegate() {
				del();
			});
			this.InvokeOnMainThread (action);
		}

		public new Boolean Visible
		{
			set{
				if (value) {
					MakeKeyAndOrderFront(this);
				}
				base.Visible = value;
			}
			get{
				return base.Visible;
			}
		}

		public new IToolbarBackend Toolbar {
			get {
				return toolbar;
			}
			set{

				if (toolbar != null && ((ToolbarBackend)toolbar).Widget!=null) {
					((ToolbarBackend)toolbar).Widget.RemoveFromSuperview ();
				}
				this.toolbar = value;
				//toolbar.Widget.Bounds = new RectangleF (0, 0, 400, 10);
				TitleBarView.Superview.AddSubview (((ToolbarBackend)toolbar).Widget);
				((ToolbarBackend)toolbar).Widget.AutoresizingMask = NSViewResizingMask.MaxXMargin | NSViewResizingMask.WidthSizable;
				((ToolbarBackend)toolbar).Widget.Frame = new RectangleF (((ToolbarBackend)toolbar).Widget.Frame.X, ((ToolbarBackend)toolbar).Widget.Frame.Y, ((ToolbarBackend)toolbar).Widget.Frame.Width, 40);
			}
		}

	 
	
		public new Xwt.Drawing.Color BackgroundColor{
			get{
				NSColor bc = base.BackgroundColor;
				Xwt.Drawing.Color c = new Xwt.Drawing.Color (bc.RedComponent, bc.GreenComponent, bc.BlueComponent);
				return c;
			}
			set{
				NSColor nc = NSColor.FromDeviceRgba ((float)value.Red, (float)value.Green, (float)value.Blue, 1);
				base.BackgroundColor = nc;
			}
		}

	   
	}


 
	 
}

