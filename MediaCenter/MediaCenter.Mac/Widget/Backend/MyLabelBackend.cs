﻿using System;
using Xwt.Mac;
using MonoMac.AppKit;
using System.Drawing;

namespace MediaCenter.Mac
{
	public class MyLabelBackend:LabelBackend, IMyLabelBackend
	{
		Boolean shadow = false;

		public override void Initialize ()
		{
			base.Initialize ();
			NSTextField myview = (NSTextField)ViewObject.View.Subviews[0];

			NSShadow shadow = new NSShadow ();
			shadow.ShadowColor=NSColor.Black.ColorWithAlphaComponent(0.8f);
			shadow.ShadowOffset=new SizeF(1f,1f);
			shadow.ShadowBlurRadius=1f;
			myview.Shadow = shadow;
		}

		public Boolean Shadow{
			get{
				return shadow;
			}
			set{
				shadow = value;
			}
		}

	}
}

