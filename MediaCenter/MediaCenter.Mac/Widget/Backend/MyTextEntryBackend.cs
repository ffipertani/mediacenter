﻿using System;
using Xwt.Mac;
using MonoMac.AppKit;
using Xwt.Backends;
using MonoMac.ObjCRuntime;
using Xwt;

namespace MediaCenter.Mac
{

	/*
#define KEY_A 0
#define KEY_S 1
#define KEY_D 2
#define KEY_F 3
#define KEY_H 4
#define KEY_G 5
#define KEY_Z 6
#define KEY_X 7
#define KEY_C 8
#define KEY_V 9

#define KEY_B 11
#define KEY_Q 12
#define KEY_W 13
#define KEY_E 14
#define KEY_R 15
#define KEY_Y 16
#define KEY_T 17
#define KEY_1 18
#define KEY_2 19
#define KEY_3 20
#define KEY_4 21
#define KEY_6 22
#define KEY_5 23
#define KEY_EQUALS 24
#define KEY_9 25
#define KEY_7 26
#define KEY_MINUS 27
#define KEY_8 28
#define KEY_0 29
#define KEY_RIGHTBRACKET 30
#define KEY_O 31
#define KEY_U 32
#define KEY_LEFTBRACKET 33
#define KEY_I 34
#define KEY_P 35
#define KEY_RETURN 36
#define KEY_L 37
#define KEY_J 38
#define KEY_APOSTROPHE 39
#define KEY_K 40
#define KEY_SEMICOLON 41
#define KEY_FRONTSLASH 42
#define KEY_COMMA 43
#define KEY_BACKSLASH 44
#define KEY_N 45
#define KEY_M 46
#define KEY_PERIOD 47
#define KEY_TAB 48

#define KEY_BACKAPOSTROPHE 50
#define KEY_DELETE 51

#define KEY_ESCAPE 53

#define KEY_COMMAND 55
#define KEY_SHIFT 56
#define KEY_CAPSLOCK 57
#define KEY_OPTION 58
#define KEY_CONTROL 59

#define KEY_UP 126
#define KEY_DOWN 125
#define KEY_LEFT 123
#define KEY_RIGHT 124
*/
	public class MyTextEntryBackend: ViewBackend<NSView,ITextEntryEventSink>, ITextEntryBackend
	{
		int cacheSelectionStart, cacheSelectionLength;
		bool checkMouseSelection;

		public MyTextEntryBackend ()
		{
		}

		public override void Initialize ()
		{
			base.Initialize ();
			 
			var view = new CustomTextField (EventSink, ApplicationContext);
			view.TextEntryBackend = this;
			ViewObject = new CustomAlignedContainer (EventSink, ApplicationContext, (NSView)view);
			MultiLine = false;
			 

			Frontend.MouseEntered += delegate {
				checkMouseSelection = true;
			};
			Frontend.MouseExited += delegate {
				checkMouseSelection = false;
				HandleSelectionChanged ();
			};
			Frontend.MouseMoved += delegate {
				if (checkMouseSelection)
					HandleSelectionChanged ();
			};
		}
		 
		protected override void OnSizeToFit ()
		{
			Container.SizeToFit ();
		}

		CustomAlignedContainer Container {
			get { return base.Widget as CustomAlignedContainer; }
		}

		public new NSTextField Widget {
			get { return (NSTextField) Container.Child; }
		}

		protected override Size GetNaturalSize ()
		{
			var s = base.GetNaturalSize ();
			return new Size (EventSink.GetDefaultNaturalSize ().Width, s.Height);
		}

		#region ITextEntryBackend implementation
		public string Text {
			get {
				return Widget.StringValue;
			}
			set {
				Widget.StringValue = value ?? string.Empty;
			}
		}

		public Alignment TextAlignment {
			get {
				return Widget.Alignment.ToAlignment ();
			}
			set {
				Widget.Alignment = value.ToNSTextAlignment ();
			}
		}

		public bool ReadOnly {
			get {
				return !Widget.Editable;
			}
			set {
				Widget.Editable = !value;
			}
		}

		public bool ShowFrame {
			get {
				return Widget.Bordered;
			}
			set {
				Widget.Bordered = value;
			}
		}

		public string PlaceholderText {
			get {
				return ((NSTextFieldCell) Widget.Cell).PlaceholderString;
			}
			set {
				((NSTextFieldCell) Widget.Cell).PlaceholderString = value;
			}
		}

		public bool MultiLine {
			get {				 
				return Widget.Cell.UsesSingleLineMode;
			}
			set {				 
				if (value) {
					Widget.Cell.UsesSingleLineMode = false;
					Widget.Cell.Scrollable = false;
					Widget.Cell.Wraps = true;
				} else {
					Widget.Cell.UsesSingleLineMode = true;
					Widget.Cell.Scrollable = true;
					Widget.Cell.Wraps = false;
				}
				Container.ExpandVertically = value;
			}
		}

		public int CursorPosition { 
			get {
				if (Widget.CurrentEditor == null)
					return 0;
				return Widget.CurrentEditor.SelectedRange.Location;
			}
			set {
				Widget.CurrentEditor.SelectedRange = new MonoMac.Foundation.NSRange (value, SelectionLength);
				HandleSelectionChanged ();
			}
		}

		public int SelectionStart { 
			get {
				if (Widget.CurrentEditor == null)
					return 0;
				return Widget.CurrentEditor.SelectedRange.Location;
			}
			set {
				Widget.CurrentEditor.SelectedRange = new MonoMac.Foundation.NSRange (value, SelectionLength);
				HandleSelectionChanged ();
			}
		}

		public int SelectionLength { 
			get {
				if (Widget.CurrentEditor == null)
					return 0;
				return Widget.CurrentEditor.SelectedRange.Length;
			}
			set {
				Widget.CurrentEditor.SelectedRange = new MonoMac.Foundation.NSRange (SelectionStart, value);
				HandleSelectionChanged ();
			}
		}

		public string SelectedText { 
			get {
				if (Widget.CurrentEditor == null)
					return String.Empty;
				int start = SelectionStart;
				int end = start + SelectionLength;
				if (start == end) return String.Empty;
				try {
					return Text.Substring (start, end - start);
				} catch {
					return String.Empty;
				}
			}
			set {
				int cacheSelStart = SelectionStart;
				int pos = cacheSelStart;
				if (SelectionLength > 0) {
					Text = Text.Remove (pos, SelectionLength).Insert (pos, value);
				}
				SelectionStart = pos;
				SelectionLength = value.Length;
				HandleSelectionChanged ();
			}
		}

		void HandleSelectionChanged ()
		{
			if (cacheSelectionStart != SelectionStart ||
				cacheSelectionLength != SelectionLength) {
				cacheSelectionStart = SelectionStart;
				cacheSelectionLength = SelectionLength;
				ApplicationContext.InvokeUserCode (delegate {
					EventSink.OnSelectionChanged ();
				});
			}
		}

		public override void SetFocus ()
		{
			Widget.BecomeFirstResponder ();
		}
		#endregion

		 
		public Key getKey(int val)
		{
			 
		 
			switch (val) {
			case 0:
				return Key.A;
			case 1:
				return Key.S;
			case 3:
				return Key.F;
			case 4:
				return Key.H;
			case 5:
				return Key.G;
			case 6:
				return Key.Z;
			case 7:
				return Key.X;
			case 8:
				return Key.C;
			case 9:
				return Key.V;
			case 11:
				return Key.B;
			case 12:
				return Key.Q;
			case 13:
				return Key.W;
			case 14:
				return Key.E;
			case 15:
				return Key.R;
			case 16:
				return Key.Y;
			case 17:
				return Key.T;
			case 18:
				return Key.K1;
			case 19:
				return Key.K2;
			case 20:
				return Key.K3;
			case 21:
				return Key.K4;
			case 23:
				return Key.K5;
			case 22:
				return Key.K6;
			case 24:
				return Key.Equal;
			case 25:
				return Key.K9;
			case 26:
				return Key.K7;
			case 27:
				return Key.Minus;
			case 28:
				return Key.K8;
			case 29:
				return Key.K0;
			case 53:
				return Key.Escape;
			case 51:
				return Key.Delete;
			case 36:
				return Key.Return;
			default:
				return Key.ScrollLock;
			}

		}
	}

	sealed class CustomAlignedContainer: MyBaseWidgetView
	{
		public NSView Child;

		public CustomAlignedContainer (IWidgetEventSink eventSink, ApplicationContext context, NSView child) : base (eventSink, context)
		{
			Child = child;
			AddSubview (child);
			UpdateTextFieldFrame ();
		}

		static readonly Selector sizeToFitSel = new Selector ("sizeToFit");

		public void SizeToFit ()
		{
			if (Child.RespondsToSelector (sizeToFitSel))
				Messaging.void_objc_msgSend (Child.Handle, sizeToFitSel.Handle);
			else
				throw new NotSupportedException ();
			Frame = new System.Drawing.RectangleF (Frame.X, Frame.Y, Child.Frame.Width, Child.Frame.Height);
		}

		bool expandVertically;
		public bool ExpandVertically {
			get {
				return expandVertically;
			}
			set {
				expandVertically = value;
				UpdateTextFieldFrame ();
			}
		}

		public override void SetFrameSize (System.Drawing.SizeF newSize)
		{
			base.SetFrameSize (newSize);
			UpdateTextFieldFrame ();
		}

		void UpdateTextFieldFrame ()
		{
			if (expandVertically)
				Child.Frame = new System.Drawing.RectangleF (0, 0, Frame.Width, Frame.Height);
			else
				Child.Frame = new System.Drawing.RectangleF (0, (Frame.Height - Child.Frame.Height) / 2, Frame.Width, Child.Frame.Height);
		}
	}


	class CustomTextField: NSTextField, IViewObject
	{
		ITextEntryEventSink eventSink;
		ApplicationContext context;
		public MyTextEntryBackend TextEntryBackend;

		public CustomTextField (ITextEntryEventSink eventSink, ApplicationContext context)
		{
			this.context = context;
			this.eventSink = eventSink;
		}

		public NSView View {
			get {
				return this;
			}
		}

		public ViewBackend Backend { get; set; }

		public override void DidChange (MonoMac.Foundation.NSNotification notification)
		{
			base.DidChange (notification);
			context.InvokeUserCode (delegate {
				//	eventSink.OnChanged ();
				//	eventSink.OnSelectionChanged ();
			});
		}

		int cachedCursorPosition;
		public override void KeyUp (NSEvent theEvent)
		{

			Console.WriteLine (theEvent.KeyCode);
			base.KeyUp (theEvent);
			context.InvokeUserCode (delegate {

				eventSink.OnKeyPressed(new KeyEventArgs(TextEntryBackend.getKey(theEvent.KeyCode),NSEvent.CurrentModifierFlags.ToXwtValue (),false,new DateTime().Millisecond));				 
			});

			if (cachedCursorPosition != CurrentEditor.SelectedRange.Location)
				context.InvokeUserCode (delegate {
					eventSink.OnSelectionChanged ();
				});
			cachedCursorPosition = CurrentEditor.SelectedRange.Location;
		}


		 
	}
}

