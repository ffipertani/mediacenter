﻿using System;
using MonoMac.CoreGraphics;
using MonoMac.AppKit;

namespace MediaCenter.Mac
{
	public class Utils
	{
		public Utils ()
		{
		}

		public static CGColor NSColorToCGColor(NSColor color)
		{
			int numberOfComponents = color.ComponentCount;
			float[] components = new float[numberOfComponents];

			CGColorSpace colorSpace = color.ColorSpace.ColorSpace;
			//CGColorSpaceRef colorSpace = [[color colorSpace] CGColorSpace];
			color.GetComponents (out components);

			CGColor cgColor = new CGColor(color.RedComponent,color.GreenComponent,color.BlueComponent);
			return new CGColor (cgColor, (float)1.0);

		}

		public static NSColor NSColorFromHex(String hex)
		{
			int colorCode = int.Parse(hex, System.Globalization.NumberStyles.HexNumber);
			char redByte		= (char) (colorCode >> 16);
			char greenByte	= (char) (colorCode >> 8);
			char blueByte	= (char) (colorCode);	// masks off high bits
			NSColor result = NSColor.FromCalibratedRgba ((float)redByte	/ 0xff, (float)greenByte / 0xff, (float)blueByte	/ 0xff, (float)1.0);

			return result;
		}

		 

	}
}

