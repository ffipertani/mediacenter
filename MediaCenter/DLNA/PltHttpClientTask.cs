﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;

namespace Platinum
{
	public class PltHttpClientSocketTask:PltThreadTask
	{
		protected  HttpClient              m_Client = new HttpClient();
		protected bool                        m_WaitForever;
		protected Queue<HttpRequest>  m_Requests = new Queue<HttpRequest>();

		public PltHttpClientSocketTask (HttpRequest request = null, bool wait_forever = false)
		{
			m_WaitForever  = wait_forever;
			m_Client.SetUserAgent(PltConstants.GetInstance().DefaultUserAgent);
			m_Client.SetTimeouts(TimeSpan.FromMilliseconds(60000), TimeSpan.FromMilliseconds(60000),TimeSpan.FromMilliseconds(60000));
			if (request!=null) m_Requests.Enqueue(request);
		}

		public override void Dispose()
		{
			HttpRequest request = null;
			try{
				do{
				request = m_Requests.Dequeue ();
							
				}while(request!=null);
			}catch(Exception e){}
		}

		public virtual Boolean AddRequest(HttpRequest request)
		{
		 m_Requests.Enqueue(request);
			return true;
		}

		//public virtual Boolean SetHttpClientConfig( HttpClient.Config config)
		public virtual Boolean SetHttpClientConfig()
		{
			//return m_Client.SetConfig(config);
			return true;
		}

		protected override void DoAbort()
		{
			m_Client.Abort();
		}

		protected override void DoRun()
		{
			HttpRequest       request = null;
			HttpResponse      response = null;
			HttpRequestContext context = null;
			Boolean             res;
			DateTime          watchdog;

			watchdog = DateTime.Now;


			do {
				// pop next request or wait for one for 100ms
				while (Plt.IsSucceded(GetNextRequest(out request, TimeSpan.FromSeconds(100)))) {
					response = null;

					if (IsAborting(0)) return;

					// send request
					res = m_Client.SendRequest(request, out response, out context);

					String prefix = String.Format("PLT_HttpClientSocketTask::DoRun (res = %d):", res);
					PltHttp.Log( prefix, response);

					// process response
					ProcessResponse(res, request, context, response);

					// cleanup

					response = null;
					request = null;
				}

				// DLNA requires that we abort unanswered/unused sockets after 60 secs
				DateTime now = DateTime.Now;

				if (now.CompareTo(watchdog.AddSeconds(60))>0) {
					//HttpConnectionManager.GetInstance()->Recycle(NULL);
					watchdog = now;
				}

			} while (m_WaitForever && !IsAborting(0));
							 
		}

		protected virtual Boolean ProcessResponse(Boolean res, HttpRequest request, HttpRequestContext context, HttpResponse response)
		{

			Plt.LogFine(String.Format("PLT_HttpClientSocketTask::ProcessResponse (result=%d)", res));
			Plt.CheckWarining(res);

			// check if there's a body to read
			HttpEntity entity = response.Entity;
			Stream body;
			if (entity==null || Plt.IsFailed(entity.GetInputStream(out body)) || body==null) {
				return true;
			}

			// dump body into ether  
			// (if no content-length specified, read until disconnected)
			MemoryStream ms = new MemoryStream ();
			body.CopyTo(ms);
		
			return true;
		}

		private  Boolean GetNextRequest(out HttpRequest request, TimeSpan timeout_ms)
		{

				double totoalmillis = timeout_ms.TotalMilliseconds;
				request = null;
				do {
					try {
					lock (m_Requests) {
						request = m_Requests.Dequeue ();
						return true;
					}
					} catch {
						Thread.Sleep (100);
						totoalmillis -= 100;
					}
				} while(totoalmillis > 0);
				return false;
			
		}
	}
	/*
UNUSED
	public class PltHttpClientTask<T> : PltHttpClientSocketTask
	{
		T m_Data;

		public PltHttpClientTask(HttpUri url, T data) : base(new HttpRequest(url, "GET", HttpMessage.NPT_HTTP_PROTOCOL_1_1))
		{
			m_Data = data;
		}
	 

		protected override Boolean ProcessResponse(Boolean res, HttpRequest request, HttpRequestContext context, HttpResponse response) {
			return m_Data.ProcessResponse(res, request, context, response);
		}
		 

	}
*/
}
   
   
   