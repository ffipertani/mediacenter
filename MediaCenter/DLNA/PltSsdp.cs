﻿using System;
using System.Net.Sockets;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Collections.Generic;

namespace Platinum
{
	public interface PltSsdpPacketListener
	{
		Boolean OnSsdpPacket (HttpRequest request, HttpRequestContext context);
	}


	public interface PltSsdpSearchResponseListener
	{
		Boolean ProcessSsdpSearchResponse (Boolean res, HttpRequestContext context, HttpResponse response);
	}

	class PltSsdpSender
	{
		 
		public static Boolean SendSsdp (HttpRequest request, String  usn, String nt, UdpClient socket, bool notify, IPEndPoint addr = null)
		{
			Plt.CheckSevere (FormatPacket (request, usn, nt, notify));

			// logging
			String prefix = String.Format ("Sending SSDP {0} packet for {1}", request.Method, usn);
			PltHttp.Log ( prefix, request);

			// use a memory stream to write all the data
			MemoryStream stream = new MemoryStream (); 
			Boolean res = request.Emit (stream);
			Plt.CheckWarining (res);

			// copy stream into a data packet and send it
			long size = stream.Length;

			if (size != (long)size)
				throw new Exception (" NPT_CHECK(NPT_ERROR_OUT_OF_RANGE)");
			byte[] packet = stream.GetBuffer ();
			//NPT_DataBuffer packet(stream.GetData(), (NPT_Size)size);
			if (socket.Send (packet,(int) size, addr) != size) {
				throw new Exception ("Error sending SSD");
			}

			return true;
		}

		public static Boolean SendSsdp (HttpResponse  response,
		                               String usn,
		                               String nt,
		                               UdpClient socket,
		                               bool               notify, 
		                               IPEndPoint addr = null)
		{
			Plt.CheckSevere (FormatPacket (response, usn, nt, notify));

			// logging
			String prefix = String.Format ("Sending SSDP Response:");
			PltHttp.Log ( prefix, response);

			// use a memory stream to write all the data
			MemoryStream stream = new MemoryStream ();
			Boolean res = response.Emit (stream);
			if (Plt.IsFailed (res))
				return res;

			// copy stream into a data packet and send it
			long size = stream.Length;

			addr = new IPEndPoint(IPAddress.Parse("239.255.255.250"),1900);

			byte[] packet = stream.GetBuffer ();
			if (socket.Send (packet, packet.Length, addr) != packet.Length) {
				throw new Exception ("Error sending SSD");
			}
			 
			return true;
		}

		 
		private static Boolean FormatPacket (HttpMessage message, String usn, String nt, bool notify)
		{
			PltUPnPMessageHelper.SetUSN (message, usn);
			if (notify) {
				PltUPnPMessageHelper.SetNT (message, nt);
			} else {
				PltUPnPMessageHelper.SetST (message, nt);
				PltUPnPMessageHelper.SetDate (message);
			}
			//PLT_HttpHelper::SetContentLength(message, 0);

			return true;
		}
	}


	public class PltSsdpDeviceSearchResponseInterfaceIterator:IApplier<NetworkInterface>
	{
		PltDeviceHost m_Device;
		EndPoint m_RemoteAddr;
		String m_ST;

		public PltSsdpDeviceSearchResponseInterfaceIterator (PltDeviceHost device, EndPoint remote_addr, String st)
		{
			m_Device = device;
			m_RemoteAddr = remote_addr;
			m_ST = st;
		}


		public Boolean Apply (NetworkInterface net_if)
		{
			IPEndPoint remote_addr = (IPEndPoint)m_RemoteAddr;

			List<IPAddress> address = new List<IPAddress> ();
			PltUPnPMessageHelper.GetIPAddresses (net_if, out address);
			if (address.Count == 0) {
				return true;
			}

			// don't try to bind on port 1900 or connect will fail later
			UdpClient socket = new UdpClient (new IPEndPoint (IPAddress.Any, 0));
			//socket.Bind(NPT_SocketAddress(NPT_IpAddress::Any, 1900), true);

			// by connecting, the kernel chooses which interface to use to route to the remote
			// this is the IP we should use in our Location URL header


			IPEndPoint info = (IPEndPoint) socket.Client.LocalEndPoint;

			// did we successfully connect and found out which interface is used?
			if (info.Address != null) {
				// check that the interface the kernel chose matches the interface
				// we wanted to send on
				if (address [0].Equals (info.Address)) {
					return true;
				}

				// socket already connected, so we don't need to specify where to go
				//remote_addr = null;
			}

			HttpResponse response = new HttpResponse (200, "OK", HttpMessage.NPT_HTTP_PROTOCOL_1_1);
			PltUPnPMessageHelper.SetLocation (response, m_Device.GetDescriptionUrl (info.Address.ToString()));
			PltUPnPMessageHelper.SetLeaseTime (response, m_Device.LeaseTime);
			PltUPnPMessageHelper.SetServer (response, PltHttp.PLT_HTTP_DEFAULT_SERVER, false);
			response.Headers.SetHeader ("EXT", "");

			// process search response twice to be DLNA compliant

			 
			//NPT_UdpSocket socket;
			Plt.CheckSevere (m_Device.SendSsdpSearchResponse (response, socket, m_ST, remote_addr));
			
			 
			Thread.Sleep (TimeSpan.FromMilliseconds((int)(PltUPnP.PLT_DLNA_SSDP_DELAY_GROUP*1000)));
		 
			 
			//NPT_UdpSocket socket;
			Plt.CheckSevere (m_Device.SendSsdpSearchResponse (response, socket, m_ST, remote_addr));
				
			 

			return true;
		}

	 

	}

	class PltSsdpDeviceSearchResponseTask : PltThreadTask
	{

		protected PltDeviceHost m_Device;
		protected EndPoint m_RemoteAddr;
		protected String m_ST;

		public PltSsdpDeviceSearchResponseTask (PltDeviceHost device, EndPoint remote_addr, String st)
		{
			m_Device = device;
			m_RemoteAddr = remote_addr;
			m_ST = st; 
		}

		protected override void DoRun ()
		{
			List<NetworkInterface> if_list;
			if (Plt.IsFailed (PltUPnPMessageHelper.GetNetworkInterfaces (out if_list, true)))
				return;

			if_list.Apply (new PltSsdpDeviceSearchResponseInterfaceIterator (m_Device, m_RemoteAddr, m_ST));
		}

	
	}


	class PltSsdpAnnounceInterfaceIterator:IApplier<NetworkInterface>
	{
		PltDeviceHost m_Device;
		PltSsdpAnnounceType m_Type;
		bool m_Broadcast;

		public PltSsdpAnnounceInterfaceIterator (PltDeviceHost device, PltSsdpAnnounceType type, bool broadcast = false)
		{
			m_Device = device;
			m_Type = type;
			m_Broadcast = broadcast;
		}

		public Boolean Apply (NetworkInterface net_if)
		{
			// don't use this interface address if it's not broadcast capable
			if (m_Broadcast && !net_if.SupportsMulticast) {
				return false;
			}
			IPAddress address = PltUPnPMessageHelper.GetIPAddress (net_if);
			if (address == null) {
				return false;
			}
				
			// Remove disconnected interfaces

			if (address.ToString ().Equals ("0.0.0.0"))
				return false;

			if (!m_Broadcast && !net_if.SupportsMulticast && net_if.NetworkInterfaceType != NetworkInterfaceType.Loopback) {
				Plt.LogWarn (String.Format ("Not a valid interface: %s (flags: %d)", address.ToString (), net_if.Description));
				return false;
			}

			HttpUri url;
			UdpClient multicast_socket;
			UdpClient broadcast_socket;
			UdpClient socket;

			if (m_Broadcast) {
				broadcast_socket = new UdpClient ();
				url = new HttpUri (PltUPnPMessageHelper.getBroadcastIP (net_if).ToString (), 1900, "*");
				socket = broadcast_socket;
			} else {
				multicast_socket = new UdpClient ();
				url = new HttpUri ("239.255.255.250", 1900, "*");    
				multicast_socket.Client.Bind (new IPEndPoint (address, 0));
				multicast_socket.Ttl = (short) PltConstants.GetInstance ().AnnounceMulticastTimeToLive;
				socket = multicast_socket;			 
			}

			HttpRequest req = new HttpRequest (url, "NOTIFY", HttpMessage.NPT_HTTP_PROTOCOL_1_1);
			PltHttpHelper.SetHost (req, "239.255.255.250:1900");

			// Location header valid only for ssdp:alive or ssdp:update messages
			if (m_Type != PltSsdpAnnounceType.PLT_ANNOUNCETYPE_BYEBYE) {
				PltUPnPMessageHelper.SetLocation (req, m_Device.GetDescriptionUrl (address.ToString ()));
			}

			Plt.CheckSevere (m_Device.Announce (req, socket, m_Type));

			 
			// delay alive only as we don't want to delay when stopping
			if (m_Type != PltSsdpAnnounceType.PLT_ANNOUNCETYPE_BYEBYE) {
				Thread.Sleep (TimeSpan.FromMilliseconds(PltUPnP.PLT_DLNA_SSDP_DELAY_GROUP*100));
			}

			Plt.CheckSevere (m_Device.Announce (req, socket, m_Type));
			 

			return true;
		}

		 
	}

	public class PltSsdpInitMulticastIterator:IApplier<IPAddress>
	{
		Socket m_Socket;

		public PltSsdpInitMulticastIterator (Socket socket)
		{
			m_Socket = socket;
		}

		public Boolean Apply (IPAddress if_addr)
		{
			IPAddress addr = IPAddress.Parse ("239.255.255.250");
		 
			// OSX bug, since we're reusing the socket, we need to leave group first
			// before joining it
		//	m_Socket.SetSocketOption(SocketOptionLevel.IP,	SocketOptionName.DropMembership, new MulticastOption(addr));

			m_Socket.SetSocketOption(SocketOptionLevel.IP,	SocketOptionName.AddMembership, new MulticastOption(addr,if_addr));
			m_Socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 4);
			//m_Socket.Connect(
			return true;
		}
	}

	public class PltSsdpDeviceAnnounceTask : PltThreadTask
	{
		PltDeviceHost m_Device;
		TimeSpan m_Repeat;
		bool m_IsByeByeFirst;
		bool m_ExtraBroadcast;

		public PltSsdpDeviceAnnounceTask (PltDeviceHost  device, TimeSpan repeat, bool is_byebye_first = false, bool extra_broadcast = false)
		{
			m_Device = device;
			m_Repeat = repeat;
			m_IsByeByeFirst = is_byebye_first;
			m_ExtraBroadcast = extra_broadcast;
		}

		 
		 
		protected override void DoRun ()
		{
			List<NetworkInterface> if_list;

			while (true) {
				Plt.CheckWarining (PltUPnPMessageHelper.GetNetworkInterfaces (out if_list, false));

				// if we're announcing our arrival, sends a byebye first (NMPR compliance)
				if (m_IsByeByeFirst == true) {
					m_IsByeByeFirst = false;

					if (m_ExtraBroadcast) {
						if_list.Apply (new PltSsdpAnnounceInterfaceIterator (m_Device, PltSsdpAnnounceType.PLT_ANNOUNCETYPE_BYEBYE, true));
					}

					// multicast now
					if_list.Apply (new PltSsdpAnnounceInterfaceIterator (m_Device, PltSsdpAnnounceType.PLT_ANNOUNCETYPE_BYEBYE, false));

					// schedule to announce alive in 200 ms
					if (IsAborting (200))
						break;
				}

				if (m_ExtraBroadcast) {
					if_list.Apply (new PltSsdpAnnounceInterfaceIterator (m_Device, PltSsdpAnnounceType.PLT_ANNOUNCETYPE_ALIVE, true));
				}

				// multicast now
				if_list.Apply (new PltSsdpAnnounceInterfaceIterator (m_Device, PltSsdpAnnounceType.PLT_ANNOUNCETYPE_ALIVE, false));


				 
				if_list.Apply (new DisposerApplier<NetworkInterface> ());
				if_list.Clear ();

				if (IsAborting (m_Repeat))
					break;
			}

		}

		 
	}

	public class PltNetworkInterfaceAddressSearchIterator:IFinder<NetworkInterface>
	{
		String m_Ip;

		public PltNetworkInterfaceAddressSearchIterator (String ip)
		{
			m_Ip = ip;
		}


		public Boolean IsFound (NetworkInterface iface)
		{
			List<IPAddress> addresses = new List<IPAddress> ();
			PltUPnPMessageHelper.GetIPAddresses (iface, out addresses);
			foreach (IPAddress addr in addresses) {
				if (addr.ToString().Equals (m_Ip)) {
					return true;
				}
			}
			return false;
		}

		 
	}

	public class PltSsdpPacketListenerIterator:IApplier<PltSsdpPacketListener>
	{
		HttpRequest m_Request;
		HttpRequestContext m_Context;

		public PltSsdpPacketListenerIterator (HttpRequest request, HttpRequestContext context)
		{
			m_Request = request;
			m_Context = context;
		}

		public Boolean Apply (PltSsdpPacketListener listener)
		{
			return listener.OnSsdpPacket (m_Request, m_Context);
		}
		 
	}

	public class PltSsdpListenTask :  PltHttpServerSocketTask
	{
		protected PltInputDatagramStream m_Datagram;
		protected List<PltSsdpPacketListener> m_Listeners;
		protected Object m_Mutex;

		public PltSsdpListenTask (Socket socket) : base (socket, true)
		{
			// Change read time out for UDP because iPhone 3.0 seems to hang
			// after reading everything from the socket even though
			// more stuff arrived

			//#if defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE
			//m_Socket->SetReadTimeout(10000);
			//#endif
			m_Mutex = new Object ();
			m_Listeners = new List<PltSsdpPacketListener> ();
			m_Datagram = new PltInputDatagramStream (socket);

			 
		}

		public Boolean AddListener (PltSsdpPacketListener listener)
		{
			lock (m_Mutex) {
				m_Listeners.Add (listener);
				return true;
			}
		}

		public Boolean RemoveListener (PltSsdpPacketListener listener)
		{
			lock (m_Mutex) {
				m_Listeners.Remove (listener);
				return true;
			}
		}

		protected override void DoAbort ()
		{
			base.DoAbort ();
		}

		 

		// PLT_HttpServerSocketTask methods
		protected override Boolean GetInputStream (out Stream stream)
		{
			if (m_Datagram != null) {
				stream = m_Datagram;
				return true;
			} else {

				m_Datagram = new PltInputDatagramStream (m_Socket);
				stream = m_Datagram;
				return true;
			}
		}


		protected override Boolean GetInfo (out SocketInfo info)
		{
			info = null;
			if (m_Datagram == null)
				return false;
			return m_Datagram.GetInfo (out info);
		}

		protected override Boolean SetupResponse (HttpRequest request, HttpRequestContext context, HttpResponse response)
		{		
			lock (m_Mutex) {
				m_Listeners.Apply (new PltSsdpPacketListenerIterator (request, context));

				// return error since we don't have anything to respond
				// as we use a separate task to respond with ssdp
				return false;
			}
		}

		 

	}

	public class PltSsdpSearchTask : PltThreadTask
	{
		PltSsdpSearchResponseListener m_Listener;
		HttpRequest m_Request;
		TimeSpan m_Frequency;
		bool m_Repeat;
		Socket m_Socket;

		public PltSsdpSearchTask (Socket socket, PltSsdpSearchResponseListener listener, HttpRequest request):this(socket,listener,request,new TimeSpan (0))
		{
		}

		public PltSsdpSearchTask (Socket socket, PltSsdpSearchResponseListener listener, HttpRequest request,	TimeSpan frequency) // pass 0 for one time
		{
			m_Listener = listener;
			m_Request = request;
			m_Frequency = frequency.TotalMilliseconds!=0 ? frequency : TimeSpan.FromSeconds (30);
			m_Repeat = frequency.TotalSeconds != 0;
			m_Socket = socket;

			m_Socket.ReceiveTimeout = (int)m_Frequency.TotalMilliseconds;
			m_Socket.SendTimeout = 10000;

		}

		 
	 

		// PLT_ThreadTask methods
		protected override void DoAbort ()
		{
			m_Socket.Close ();
		}

		protected override void DoRun ()
		{
			HttpResponse response = null;
			int timeout = 30000;
			HttpRequestContext context = new HttpRequestContext();

			do {
				// get the address of the server
				IPAddress server_address;
				//IPHostEntry hostEntry = Dns.GetHostEntry (m_Request.Url.Uri.Host);



				//Plt.Assert (hostEntry != null && hostEntry.AddressList.Length > 0);
				//server_address = hostEntry.AddressList[0];
				IPEndPoint address = new IPEndPoint (IPAddress.Parse(m_Request.Url.Uri.Host),	m_Request.Url.Uri.Port);

				// send 2 requests in a row
				Stream output_stream = new PltOutputDatagramStream (m_Socket, 4096, address);

				if (Plt.IsFailed (HttpClient.WriteRequest (output_stream, m_Request, false)))
					return;

				// keep track of when we sent the request
				DateTime last_send = DateTime.Now;


				while (!IsAborting (0)) {
					// read response
					PltInputDatagramStream input_stream = new PltInputDatagramStream (m_Socket);

					Stream stream = input_stream;
					try {
						Boolean res = HttpClient.ReadResponse (stream, false, false, out response);
						// callback to process response
						if (Plt.IsSucceded (res)) {
							// get source info    
							SocketInfo info;
							input_stream.GetInfo (out info);

							context.LocalAddress = (IPEndPoint)info.LocalEndPoint;
							context.RemoteAddress = (IPEndPoint)info.RemoteEndPoint;

							// process response
							ProcessResponse (true, m_Request, context, response);
							response = null;
						} 
						input_stream = null;

						// check if it's time to resend request
						DateTime now = DateTime.Now;

						if (now.CompareTo (last_send.AddMilliseconds (m_Frequency.TotalMilliseconds)) >= 0)
							break;
					} catch (Exception ex) {
						if (!ex.Message.Equals ("NPT_ERROR_TIMEOUT")) {
							Plt.LogWarn ("PLT_SsdpSearchTask got an error waiting for response");
							if (IsAborting (0))
								break;

							Thread.Sleep (TimeSpan.FromMilliseconds (15));
						}
					}
				}
			} while (!IsAborting (0) && m_Repeat);

			 
		}

		protected virtual Boolean ProcessResponse (Boolean res, HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
			return m_Listener.ProcessSsdpSearchResponse (res, context, response);
		}

		 

	}



	public enum PltSsdpAnnounceType
	{
		PLT_ANNOUNCETYPE_BYEBYE,
		PLT_ANNOUNCETYPE_ALIVE,
		PLT_ANNOUNCETYPE_UPDATE
	}

}

   
       