﻿using System;
using System.Xml;

namespace Platinum
{
	public class PltArgument
	{
		protected PltArgumentDesc m_ArgDesc;
		protected String m_Value;

		public PltArgument (PltArgumentDesc argDesc)
		{
			m_ArgDesc = argDesc;
		}

		public static Boolean CreateArgument (PltActionDesc action_desc, String arg_name, String arg_value, out PltArgument arg)
		{
			// reset output params first
			arg = null;

			PltArgumentDesc arg_desc = action_desc.GetArgumentDesc (arg_name);
			if (arg_desc == null) {
				Plt.LogWarn (String.Format ("Invalid argument {0} for action {1}", arg_name, action_desc.GetName ()));
				throw new Exception ("NPT_ERROR_NO_SUCH_NAME");
			}

			Boolean res;
			PltArgument new_arg = new PltArgument (arg_desc);
			if (Plt.IsFailed (res = new_arg.SetValue (arg_value))) {
				new_arg = null;

				Plt.LogWarn (String.Format ("Invalid value of {0} for argument {1} of action {2}", arg_value, arg_name, action_desc.GetName ()));
				return res;
			}

			arg = new_arg;
			return true;
		}

		// accessor methods
		public PltArgumentDesc  GetDesc ()
		{ 
			return m_ArgDesc; 
		}

		public int GetPosition ()
		{ 
			return m_ArgDesc.GetPosition (); 
		}

		public Boolean SetValue (String value)
		{
			Plt.CheckSevere (ValidateValue (value));

			m_Value = value;
			return true;
		}

		public String  GetValue ()
		{
			return m_Value;
		}

		 
		private Boolean ValidateValue (String value)
		{
			if (m_ArgDesc.GetRelatedStateVariable () != null) {
				return m_ArgDesc.GetRelatedStateVariable ().ValidateValue (value);
			}
			return true;    
		}


	}

	public class PltArgumentDesc
	{
		protected String m_Name;
		protected int m_Position;
		protected String m_Direction;
		protected PltStateVariable m_RelatedStateVariable;
		protected bool m_HasReturnValue;

		public PltArgumentDesc (String name, int position, String direction = "in",	PltStateVariable variable = null, bool has_ret = false)
		{
			m_Name = name;
			m_Position = position;
			m_Direction = direction;
			m_RelatedStateVariable = variable;
			m_HasReturnValue = has_ret;
		}

		public Boolean GetSCPDXML (XmlElement node)
		{
			XmlElement argument = node.OwnerDocument.CreateElement (node.Prefix,"argument",node.NamespaceURI);
			 

			node.AppendChild (argument);

			Plt.CheckSevere (PltUtilities.AddChildText (argument, "name", m_Name));
			Plt.CheckSevere (PltUtilities.AddChildText (argument, "direction", m_Direction));
			Plt.CheckSevere (PltUtilities.AddChildText (argument, "relatedStateVariable", m_RelatedStateVariable.Name));

			if (m_HasReturnValue) {
				argument.AppendChild (node.OwnerDocument.CreateElement ("retval"));
			}

			return true;
		}

		public String  GetName ()
		{ 
			return m_Name; 
		}

		public String  GetDirection ()
		{
			return m_Direction; 
		}

		public int GetPosition ()
		{
			return m_Position; 
		}

		public PltStateVariable GetRelatedStateVariable ()
		{
			return m_RelatedStateVariable; 
		}

		public bool HasReturnValue ()
		{
			return m_HasReturnValue; 
		}
	}

 

	public class PltArgumentNameFinder:IFinder<PltArgument>
	{
		private String Name{ get; set; }

		public PltArgumentNameFinder (String name)
		{
			Name = name;
		}

		public Boolean IsFound (PltArgument argument)
		{
			return argument.GetDesc ().GetName ().Equals (Name);
		}
	}

 
	public class PltArgumentDescNameFinder:IFinder<PltArgumentDesc>
	{
		private String Name{ get; set; }

		public PltArgumentDescNameFinder (String name)
		{
			Name = name;
		}

		public Boolean IsFound (PltArgumentDesc argDesc)
		{
			return argDesc.GetName ().Equals (Name);
		}
			
	}

	    


	 
}

