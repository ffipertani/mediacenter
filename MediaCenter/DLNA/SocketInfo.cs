﻿using System;
using System.Net;

namespace Platinum
{
	public class SocketInfo
	{

		public EndPoint LocalEndPoint{ get; set;}
		public EndPoint RemoteEndPoint{ get; set;}
		public SocketInfo ()
		{
		}
	}
}

