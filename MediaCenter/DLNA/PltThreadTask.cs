﻿using System;
using System.Threading;

namespace Platinum
{
	public class PltThreadTask
	{
		public bool AutoDestroy{ get; set; }


		protected PltTaskManager m_TaskManager;
		private SharedVariable m_Started;
		private SharedVariable m_Abort;
		private Thread m_Thread;

		private TimeSpan m_Delay;

		public PltThreadTask ()
		{
			m_Started = new SharedVariable (0);
			m_Abort = new SharedVariable (1);
			m_TaskManager = null;
			m_Thread = null;
			AutoDestroy = false;
		}

		public virtual void Dispose ()
		{
			if (!AutoDestroy) {
				m_Thread.Abort ();
			}
		}

		public Boolean Kill ()
		{
			Stop ();

			// A task can only be destroyed manually
			// when the m_AutoDestroy is false
			// otherwise the Task Manager takes
			// care of deleting it when the thread exits
			Plt.Assert (AutoDestroy == false);
			if (!AutoDestroy)
				Dispose ();

			return true;
		}

		public virtual bool IsAborting (int milliseconds)
		{
			return m_Abort.WaitUntilEquals (1, TimeSpan.FromMilliseconds(milliseconds));
		}

		public virtual bool IsAborting (TimeSpan timeout)
		{
			return m_Abort.WaitUntilEquals (1, timeout);
		}


		public Boolean Start ()
		{
			return Start (null, default(TimeSpan), true);
		}


		public Boolean Start (PltTaskManager task_manager, TimeSpan delay, bool auto_destroy = true)
		{
			m_Abort = new SharedVariable (0);
			AutoDestroy = auto_destroy;
			m_Delay = delay ;
			m_TaskManager = task_manager;

			if (m_TaskManager!=null) {
				Plt.CheckSevere (m_TaskManager.AddTask (this));
				return true;
			} else {
				Boolean result = StartThread ();

				// suicide now if task is to auto destroy when finish
				if (Plt.IsFailed (result) && AutoDestroy) {
					Dispose ();
				}
				return result;
			}
		}

		public Boolean Stop (bool blocking = true)
		{
			// keep variable around in case
			// we get destroyed
			bool auto_destroy = AutoDestroy;

			// tell thread we want to die
			m_Abort.SetValue (1);
			DoAbort ();

			// return without waiting if non blocking or not started
			if (!blocking || m_Thread==null)
				return true;

			// if auto-destroy, the thread may be already dead by now 
			// so we can't wait on m_Thread.
			// only Task Manager will know when task is finished
			if (auto_destroy) {
				return false;
			}

			m_Thread.Join ();
			return true;
		}

		protected virtual void DoInit ()
		{
		}

		protected virtual void DoAbort ()
		{
		}

		protected virtual void DoRun ()
		{
			Console.WriteLine ("NOT IMPLEMENTED");
		}

		public Boolean StartThread ()
		{
			try {
				m_Started.SetValue (0);

				m_Thread = new Thread (new ThreadStart (Run));
				m_Thread.Start ();

				return m_Started.WaitUntilEquals (1);
			} catch (Exception e) {
				// delete thread manually in case m_AutoDestroy was true
				if (AutoDestroy) {
					m_Thread = null;
				}	
				return false;
			}
		}

		private void Run ()
		{
			try{
			m_Started.SetValue (1);

			// wait before starting task if necessary
			if (m_Delay.Ticks > 0) {
				// more than 100ms, loop so we can abort it
				if (m_Delay.TotalMilliseconds > 100) {
					DateTime start = DateTime.Now;
					DateTime now;		
					Boolean isAborting = false;
					do {
						now = DateTime.Now;
						if (now.Ticks >= start.Ticks + m_Delay.Ticks)
							break;
						isAborting = IsAborting (100);
					} while (!isAborting);
				} else {
					Thread.Sleep (m_Delay);
				}
			}

			// loop
			if (!IsAborting (0)) {
				DoInit ();
				DoRun ();
			}

			// notify the Task Manager we're done
			// it will destroy us if m_AutoDestroy is true
			if (m_TaskManager!=null) {
				m_TaskManager.RemoveTask (this);
			} else if (AutoDestroy) {
				// destroy ourselves otherwise
				Dispose ();
			}
		
		}catch(Exception e){
				Console.WriteLine(e);
		}
		 
	}

	}

	  
}

