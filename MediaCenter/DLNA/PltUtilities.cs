﻿using System;
using System.Xml;
using System.Globalization;
using System.Net.NetworkInformation;
using System.Net;
using System.Collections.Generic;
using System.IO;

namespace Platinum
{
	public class PltUtilities
	{
		public static Boolean AddChildText (XmlNode node, String childName, String text)
		{
			foreach (XmlNode child in node.ChildNodes) {
				if (child.Name.Equals (childName)) {
					child.InnerText = text;
					return true;
				}
			}
			XmlElement newNode = node.OwnerDocument.CreateElement (node.Prefix,childName,node.NamespaceURI);
			newNode.InnerText = text;
			node.AppendChild(newNode);
			 
			return true;

		}
	}

	public class PltUPnPMessageHelper
	{
		static CultureInfo cinfo = new CultureInfo( "en-US", false );
		static Random random = new Random ();
		public static String NPT_XML_ANY_NAMESPACE = "*";

		public static String GetST (HttpMessage message)
		{ 
			return getAttr (message, "ST");
		}

		public static Boolean SetST (HttpMessage message, String st)
		{ 
			return setAttr (message, "ST", st); 
		}

		public static String GetNT (HttpMessage message)
		{
			return getAttr (message, "NT");
		}

		public static Boolean SetNT (HttpMessage message, String nt)
		{ 
			return setAttr (message, "NT", nt); 
		}

		public static String GetNTS (HttpMessage message)
		{
			return getAttr (message, "NTS"); 
		}

		public static Boolean SetNTS (HttpMessage message, String nts)
		{ 
			return setAttr (message, "NTS", nts); 
		}

		public static String GetMAN (HttpMessage message)
		{
			return getAttr (message, "MAN"); 
		}

		public static Boolean SetMAN (HttpMessage message, String man)
		{ 
			return setAttr (message, "MAN", man); 
		}

		public static String GetLocation (HttpMessage message)
		{
			return getAttr (message, "Location"); 
		}

		public static Boolean SetLocation (HttpMessage message, String location)
		{ 
			return setAttr (message, "Location", location); 
		}

		public static  String GetServer (HttpMessage message)
		{
			return getAttr (message, "Server"); 
		}

		public static Boolean SetServer (HttpMessage message, String server, bool replace = true)
		{ 
			return setAttr (message, "Server", server, replace); 
		}

		public static  String GetUSN (HttpMessage message)
		{
			return getAttr (message, "USN"); 
		}

		public static Boolean SetUSN (HttpMessage message, String usn)
		{ 
			return setAttr (message, "USN", usn); 
		}

		public static  String GetCallbacks (HttpMessage message)
		{
			return getAttr (message, "CALLBACK"); 
		}

		public static Boolean SetCallbacks (HttpMessage message, String callbacks)
		{
			return setAttr (message, "CALLBACK", callbacks); 
		}

		public static  String GetSID (HttpMessage message)
		{
			return getAttr (message, "SID"); 
		}

		public static Boolean SetSID (HttpMessage message, String sid)
		{ 
			return setAttr (message, "SID", sid); 
		}

		public static Boolean GetLeaseTime (HttpMessage message, out TimeSpan lease)
		{
			String cc = getAttr (message, "Cache-Control");
			Plt.CheckPointer (cc);
			return ExtractLeaseTime (cc, out lease);
		}

		public static Boolean SetLeaseTime (HttpMessage message, TimeSpan lease)
		{
			return setAttr (message, "Cache-Control", "max-age=" + lease.TotalSeconds); 
		}

		public static Boolean GetBootId (HttpMessage message, out int bootId)
		{
			bootId = 0;
			String bid = getAttr (message, "BOOTID.UPNP.ORG");
			Plt.CheckPointer (bid);
			return Int32.TryParse (bid, out bootId);
		}

		public static Boolean SetBootId (HttpMessage message, int bootId)
		{
			return setAttr (message, "BOOTID.UPNP.ORG", bootId.ToString ());
		}

		public static Boolean GetNextBootId (HttpMessage message, out int nextBootId)
		{
			nextBootId = 0;
			String nbid = getAttr (message, "NEXTBOOTID.UPNP.ORG");
			Plt.CheckPointer (nbid);
			return Int32.TryParse (nbid, out nextBootId);
		}

		public static Boolean SetNextBootId (HttpMessage  message, int nextBootId)
		{
			return setAttr (message, "NEXTBOOTID.UPNP.ORG", nextBootId.ToString ());
		}

		public static Boolean GetConfigId (HttpMessage message, out int configId)
		{
			configId = 0;
			String cid = getAttr (message, "CONFIGID.UPNP.ORG");
			Plt.CheckPointer (cid);
			return Int32.TryParse (cid, out configId);
		}

		public static Boolean SetConfigId (HttpMessage  message, int configId)
		{
			return setAttr (message, "CONFIGID.UPNP.ORG", configId.ToString ());
		}

		public static Boolean GetTimeOut (HttpMessage message, out int seconds)
		{
			seconds = 0;
			String timeout = getAttr (message, "TIMEOUT"); 
			Plt.CheckPointer (timeout);
			return ExtractTimeOut (timeout, out seconds); 
		}

		public static Boolean SetTimeOut (HttpMessage message, Int32 seconds)
		{
			if (seconds >= 0) {
				return setAttr (message, "TIMEOUT", "Second-" + seconds);
			} else {
				return setAttr (message, "TIMEOUT", "Second-infinite");
			}
		}

		public static Boolean SetDate (HttpMessage message)
		{
			DateTime now = DateTime.Now;

			Console.WriteLine ("TODO CHECK DATE ");
			 

			return setAttr (message, "Date", TimeZoneInfo.ConvertTime(now,TimeZoneInfo.Utc).ToString (cinfo.DateTimeFormat.RFC1123Pattern,cinfo)); 
		}

		public static Boolean GetIfModifiedSince (HttpMessage message, out DateTime date)
		{
			date = default(DateTime);
			String value = getAttr (message, "If-Modified-Since");
			if (String.IsNullOrEmpty(value))
				return false;

			// Try RFC 1123, RFC 1036, then ANSI

			if (Plt.IsSucceded (DateTime.TryParseExact (value, DateTimeFormatInfo.CurrentInfo.RFC1123Pattern,null,DateTimeStyles.None,out date)))
				return true;

			if (Plt.IsSucceded (DateTime.TryParseExact (value,"dddd, dd-MMM-yy HH:mm:ss z",null,DateTimeStyles.None,out date)))
				return true;

			return DateTime.TryParse ( value, out date);
		}

		public static Boolean SetIfModifiedSince (HttpMessage message, DateTime date)
		{
			return setAttr (message, "If-Modified-Since", date.ToString (DateTimeFormatInfo.CurrentInfo.RFC1123Pattern));
		}

		public static Boolean GetMX (HttpMessage message, out int value)
		{
			value = 0;
			String mx = getAttr (message, "MX");
			Plt.CheckPointer (mx);
			return Int32.TryParse (mx, out value);
		}

		public static Boolean SetMX (HttpMessage message, int mx)
		{
			return setAttr (message, "MX", mx.ToString ()); 
		}

		public static Boolean GetSeq (HttpMessage message, out int value)
		{
			value = 0;
			String seq = getAttr (message, "SEQ");
			Plt.CheckPointer (seq);
			return Int32.TryParse (seq,  out value);
		}

		public static Boolean SetSeq (HttpMessage message, int seq)
		{
			return setAttr (message, "SEQ", seq.ToString ()); 
		}

		public static String GenerateUUID (int count, out String uuid)
		{
			uuid = "";			 
			for (int i = 0; i < (count < 100 ? count : 100); i++) {
				int arandom = random.Next ();
				uuid += (char)((arandom % 25) + 66);
			}
			return uuid;
		}

		public static String GenerateSerialNumber (out String sn, int count = 40)
		{
			sn = "{";		 
			for (int i = 0; i < count; i++) {
				char nibble = (char)(random.Next () % 16);
				sn += (nibble < 10) ? ('0' + nibble) : ('a' + (nibble - 10));
			}
			sn += "}";
			return sn;
		}

		public static String GenerateGUID (out String guid)
		{
			guid = Guid.NewGuid ().ToString();
			return guid;
		}

		public static Boolean ExtractLeaseTime (String cache_control, out TimeSpan lease)
		{
			lease = new TimeSpan(0);
			int value;
			if (cache_control.StartsWith ("max-age=") && Plt.IsSucceded (Int32.TryParse (cache_control.Substring (8), out value))) {
				lease = TimeSpan.FromSeconds (value);
				return true;
			}
			return false;
		}

		public static Boolean ExtractTimeOut (String timeout, out int len)
		{
			String temp = timeout;
			if (!temp.StartsWith ("Second-")) {
				throw new Exception ("NPT_ERROR_INVALID_FORMAT");
			}

			if (temp.Equals ("Second-infinite")) {
				len = (int) TimeSpan.MaxValue.TotalSeconds;
				return true;
			}
			temp = temp.Substring (7);
			return Int32.TryParse (temp, out len);
		}

		public static Boolean GetIPAddresses (out List<IPAddress> ips, bool with_localhost = false)
		{
			Boolean localhostAdded = false;
			ips = new List<IPAddress> ();
			foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces()) {
				if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet) {							 
					foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses) {
						if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
							Console.WriteLine (ip.Address.ToString ());
							if (ip.Address.ToString ().Equals ("127.0.0.1")) {
								localhostAdded = true;
							}
							if (!ip.Address.ToString ().Equals ("0.0.0.0") && (with_localhost || !ip.Address.ToString ().Equals ("127.0.0.1")))
								ips.Add (ip.Address);
						}
					}
				}  
			}


			if (with_localhost && !localhostAdded) {
				IPAddress localhost = IPAddress.Parse ("127.0.0.1");
				ips.Add (localhost);
			}
			return true;
		}

		public static Boolean GetIPAddresses (NetworkInterface ni, out List<IPAddress> ips)
		{		 
			ips = new List<IPAddress> ();
			foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses) {
				if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {						 
					ips.Add (ip.Address);
				}
			}		 

			return true;
		}

		public static IPAddress GetIPAddress (NetworkInterface ni)
		{		 
			foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses) {
				if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {						 
					return ip.Address;
				}
			}		 

			return null;
		}

		public static Boolean GetNetworkInterfaces (out List<NetworkInterface> if_list, bool with_localhost = false)
		{
			_GetNetworkInterfaces ( out if_list, with_localhost, false);

			// if no valid interfaces or if requested, add localhost interface
			if (if_list.Count == 0) {
				_GetNetworkInterfaces (out if_list, true, true);
			}
			return true;
		}

		public static Boolean GetMACAddresses (out List<String> addresses)
		{
			addresses = new List<String> ();

			foreach (NetworkInterface iface in NetworkInterface.GetAllNetworkInterfaces()) {
				Boolean isLocalhost = false;
				foreach (UnicastIPAddressInformation ip in iface.GetIPProperties().UnicastAddresses) {
					if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {								 
						if (ip.Address.ToString ().Equals ("0.0.0.0") || ip.Address.ToString ().Equals ("127.0.0.1"))
							isLocalhost = true;														
					}
				}
				if (!isLocalhost) {
					addresses.Add (iface.GetPhysicalAddress ().ToString ());
				}						 
			}

			return true;
		}

		public static bool IsLocalNetworkAddress (IPAddress address)
		{
			if (address.ToString ().Equals ("127.0.0.1"))
				return true;

			List<IPAddress> addresses = new List<IPAddress> ();
			GetIPAddresses (out addresses, false);
			foreach (IPAddress addr in addresses) {
				if (addr.IsInSameSubnet (address, IPAddress.Parse ("255.255.255.0")))
					return true;								
			}

			return false;
		}

		private static String getAttr (HttpMessage message, String attr)
		{
			String res = message.Headers.GetHeaderValue (attr);
			return res;
		}

		private static Boolean setAttr (HttpMessage message, String attr, String value,Boolean replace=true)
		{
			 message.Headers.SetHeader (attr, value,replace);
			return true;
		}


		private static Boolean _GetNetworkInterfaces (out List<NetworkInterface> if_list, bool include_localhost = false, bool only_localhost = false)
		{
			Boolean localhostAdded = false;
			if_list = new List<NetworkInterface> ();
			foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces()) {
				Boolean isLocalhost = false;
				if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet) {							 
					foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses) {
						if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
							if (ip.Address.ToString ().Equals ("127.0.0.1")) {
								isLocalhost = true;
							}
						}
					}

					if (isLocalhost) {
						if (only_localhost || include_localhost) {
							if_list.Add (ni);
						}
					} else {
						if_list.Add (ni);
					}


				}
			}
			return true;
		}



		public static IPAddress getBroadcastIP(NetworkInterface ni)
		{
			IPAddress maskIP = getHostMask(ni);
			IPAddress hostIP = GetIPAddress (ni);

			if (maskIP==null || hostIP == null)
				return null;

			byte[] complementedMaskBytes = new byte[4];
			byte[] broadcastIPBytes = new byte[4];

			for (int i = 0; i < 4; i++)
			{
				complementedMaskBytes[i] =  (byte) maskIP.GetAddressBytes()[i];
				broadcastIPBytes[i] = (byte) (hostIP.GetAddressBytes()[i]| complementedMaskBytes[i]);
			}

			return new IPAddress(broadcastIPBytes);

		}


		private static IPAddress getHostMask(NetworkInterface ni)
		{
				 

			UnicastIPAddressInformationCollection UnicastIPInfoCol = ni.GetIPProperties ().UnicastAddresses;

			foreach (UnicastIPAddressInformation UnicatIPInfo in UnicastIPInfoCol) {
				return UnicatIPInfo.IPv4Mask; 
			}


			return null;
		}

		 

	}

	public enum ApplyUntil
	{
		ResultSuccess,
		ResultNotSuccess
	}

	public static class StringExtension
	{
		public static Boolean EqualsIgnoreCase(this String cur, String other)
		{
			return string.Equals (cur, other, StringComparison.OrdinalIgnoreCase);
		}
	}

	public static class ListExtension
	{
		public static void Apply<T> (this List<T> list, IApplier<T> applier)
		{
			foreach (T o in list) {
				if (!applier.Apply (o)) {
					Plt.LogWarn("Warning in applier " +applier);
				}
			}
		}

		public static Boolean ApplyUntil<T>(this List<T> list, IApplier<T> applier, ApplyUntil predicate)
		{                          
			bool match;
			return ApplyUntil (list, applier, predicate, out match);
		}

		public static Boolean ApplyUntil<T>(this List<T> list, IApplier<T> applier, ApplyUntil predicate, out bool match)
		{                          
			foreach(T item in list){			
				Boolean return_value;
				return_value = applier.Apply (item);
				if (predicate == ApplyUntil.ResultSuccess) {
					if (return_value != true) {
						match = true;
						return true;
					}
				}
				if (predicate == ApplyUntil.ResultNotSuccess) {
					if (return_value != false) {
						match = true;
						return true;
					}
				}


			}

			match = false;
			return true;
		}


	}



	public static class IPAddressExtensions
	{
		public static IPAddress GetBroadcastAddress (this IPAddress address, IPAddress subnetMask)
		{
			byte[] ipAdressBytes = address.GetAddressBytes ();
			byte[] subnetMaskBytes = subnetMask.GetAddressBytes ();

			if (ipAdressBytes.Length != subnetMaskBytes.Length)
				throw new ArgumentException ("Lengths of IP address and subnet mask do not match.");

			byte[] broadcastAddress = new byte[ipAdressBytes.Length];
			for (int i = 0; i < broadcastAddress.Length; i++) {
				broadcastAddress [i] = (byte)(ipAdressBytes [i] | (subnetMaskBytes [i] ^ 255));
			}
			return new IPAddress (broadcastAddress);
		}

		public static IPAddress GetNetworkAddress (this IPAddress address, IPAddress subnetMask)
		{
			byte[] ipAdressBytes = address.GetAddressBytes ();
			byte[] subnetMaskBytes = subnetMask.GetAddressBytes ();

			if (ipAdressBytes.Length != subnetMaskBytes.Length)
				throw new ArgumentException ("Lengths of IP address and subnet mask do not match.");

			byte[] broadcastAddress = new byte[ipAdressBytes.Length];
			for (int i = 0; i < broadcastAddress.Length; i++) {
				broadcastAddress [i] = (byte)(ipAdressBytes [i] & (subnetMaskBytes [i]));
			}
			return new IPAddress (broadcastAddress);
		}

		public static bool IsInSameSubnet (this IPAddress address2, IPAddress address, IPAddress subnetMask)
		{
			IPAddress network1 = address.GetNetworkAddress (subnetMask);
			IPAddress network2 = address2.GetNetworkAddress (subnetMask);

			return network1.Equals (network2);
		}
	}


	public class PltXmlHelper
	{
		 

		// static methods

		public static Boolean Parse (String xml, out XmlElement tree)
		{
			// reset tree
			tree = null;
			XmlDocument doc = new XmlDocument ();
			doc.LoadXml (xml);
			tree = doc.DocumentElement;

			return true;
		}

		public static Boolean GetChildText (XmlElement node, 
		                                    String         tag, 
		                                    out String         value,
		                                    String         namespc = "",
		                                    int        max_size = 1024)
		{
			value = "";

			if (node == null)
				return false;

			// special case "" means we look for the same namespace as the parent
			if (String.IsNullOrEmpty (namespc)) {
				namespc = !String.IsNullOrEmpty(node.NamespaceURI) ? node.NamespaceURI : null;
			}

			XmlNode child = null;
			foreach (XmlNode achild in node.ChildNodes) {
				if (achild.Name.Equals (tag) && (String.IsNullOrEmpty(namespc) || achild.NamespaceURI.Equals (namespc))) {
					child = achild;
					break;
				}
			}

			if (child == null)
				return false;

			value = child.InnerText;
			// DLNA 7.3.17
			if (!String.IsNullOrEmpty (value) && value.Length>max_size) {
				value = value.Substring (0, max_size);
			}
			return true;
		}

		public static Boolean RemoveAttribute (XmlElement node, 
		                                       String         name,
		                                       String         namespc = "")
		{
			if (node == null)
				return false;

			// special case "" means we look for the same namespace as the parent
			if (!String.IsNullOrEmpty (namespc) && namespc [0] == '\0') {
				namespc = !String.IsNullOrEmpty(node.NamespaceURI) ? node.NamespaceURI : null;
			}

			String attribute = null;
			if (namespc != null) {
				node.GetAttribute (name, namespc);
			} else {
				node.GetAttribute (name);
			}
			if (attribute == null) {
				return false;
			}
			if (namespc != null) {
				node.RemoveAttribute (name, namespc);
			} else {
				node.RemoveAttribute (name);
			}
			 
			return true;
		}

		public static Boolean GetAttribute (XmlElement node, 
		                                    String        name,
		                                    out XmlAttribute  attr,
		                                    String         namespc = "")
		{
			attr = null;

			if (node == null)
				return false;

			// special case "" means we look for the same namespace as the parent
			if (String.IsNullOrEmpty (namespc) ) {
				namespc = !String.IsNullOrEmpty(node.NamespaceURI) ? node.NamespaceURI : null;
			}
			foreach (XmlAttribute attribute in node.Attributes) {
				attr = attribute;
				return true;
			}
			return false;
		}

		public static Boolean GetAttribute (XmlElement node, 
		                                    String         name,
		                                    out String value,
		                                    String         namespc = "",
		                                    int        max_size = 1024)
		{
			value = "";

			XmlAttribute attribute = null;
			Boolean result = GetAttribute (node, name, out attribute, namespc);
			if (Plt.IsFailed (result))
				return result;

			if (attribute == null)
				return false;

			value = attribute.Value;

			// DLNA 7.3.17 truncate to 1024 bytes
			if (value != null && value.Length > max_size) {
				value = value.Substring (0, max_size);
			}

			return true;
		}

		public static Boolean SetAttribute (XmlElement node, 
		                                    String         name,
		                                    String         value,
		                                    String         namespc = "")
		{
			XmlAttribute attribute = null;
			Plt.CheckWarining (GetAttribute (node, name, out attribute, namespc));
			if (attribute == null)
				return false;

			attribute.Value = value;
			return true;
		}

		public static Boolean AddChildText (XmlElement node,
		                                    String         tag,
		                                    String         text,
		                                    String         prefix = null)
		{
			if (node == null)
				return false;
			XmlElement child = node.OwnerDocument.CreateElement (prefix, tag, node.NamespaceURI);
			child.InnerText = text;
			node.AppendChild (child);
			return true;
		}

		public static bool IsMatch (XmlNode node, String tag, String namespc_mapped)
		{
			// if m_Namespace is NULL, we're looking for ANY namespace
			// if m_Namespace is '\0', we're looking for NO namespace
			// if m_Namespace is non-empty, look for that SPECIFIC namespace

			XmlElement element = (XmlElement)node;
			// is tag the same (case sensitive)?
			if (element!=null && element.LocalName.Equals (tag)) {
				if (!String.IsNullOrEmpty (namespc_mapped)) {
					// look for a SPECIFIC namespace or NO namespace
					String namespc = element.NamespaceURI;
					if (!String.IsNullOrEmpty (namespc)) {
						// the element has a namespace, match if it is equal to
						// what we're looking for
						return namespc.Equals (namespc_mapped);
					} else {
						// the element does not have a namespace, match if we're
						// looking for NO namespace
						return namespc_mapped.Equals ("");
					}
				} else {
					// ANY namespace will match
					return true;
				}
			}
			return false;
		}

		public static Boolean GetChildren (XmlElement node,	out  List<XmlElement>  children, String tag, String namespc = "")
		{
			children = new List<XmlElement> ();
			if (node == null)
				return false;

			// special case "" means we look for the same namespace as the parent
			if (namespc != null && namespc.Equals ("")) {
				namespc = node.NamespaceURI;
			}

			String namespc_mapped = (namespc == null) ? "" : (namespc.ToCharArray() [0] == '*' && namespc.ToCharArray ()[1] == '\0') ? null : namespc;

			// get all children first
			children = new List<XmlElement> ();
			foreach (XmlElement child in node.ChildNodes) {
				if (IsMatch (child, tag, namespc_mapped)) {
					children.Add (child);
				}
			}
			return true;			 
		}

		public static XmlElement GetChild (XmlElement node,
		                                   String         tag,
		                                   String         namespc = "")
		{
			if (node == null)
				return null;

			// special case "" means we look for the same namespace as the parent
			if (namespc != null && namespc.Equals ("")) {
				namespc = node.NamespaceURI;
			}
			foreach (XmlElement child in node.ChildNodes) {
				if (IsMatch (child, tag, namespc)) {
					return child;
				}
			}
			return null;
		}

		public static Boolean GetChild (XmlElement  parent, out XmlElement child, int n = 0)
		{
			child = null;

			if (parent == null)
				return false;

			 
			// get all children first
			foreach (XmlElement achild in parent.ChildNodes) {
				if (n-- == 0) {
					child = achild;
					return true;
				}
			}
			return false;
		}

		public static Boolean Serialize (XmlNode node, out String xml, bool add_header = true, int indentation = 0)
		{
			MemoryStream ms = new MemoryStream ();
			XmlWriterSettings setti = new XmlWriterSettings ();

			setti.Indent = indentation>0;
			setti.OmitXmlDeclaration = !add_header;
		
			XmlWriter aw = XmlWriter.Create (ms, setti);
			node.WriteTo (aw);
			aw.Flush ();
			ms.Position = 0;
			StreamReader reader = new StreamReader (ms);
			xml = reader.ReadToEnd ();
			 

			return true;
		}

		public static String Serialize (XmlNode node, bool add_header = true, int indentation = 0)
		{
			String res;
			if (!Serialize (node, out res, add_header, indentation)) {
				Plt.LogWarn ("Failed to serialize xml node");
				return "";
			}
			return res;
		}

	}
}

