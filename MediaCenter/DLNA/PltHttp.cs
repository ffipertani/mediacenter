﻿using System;
using System.IO;
using System.Xml;

namespace Platinum
{
	public class PltHttp
	{
		public static String PLT_HTTP_DEFAULT_USER_AGENT = "UPnP/1.0 DLNADOC/1.50 Platinum/" + Plt.PLT_PLATINUM_SDK_VERSION_STRING;
		public static String PLT_HTTP_DEFAULT_SERVER = "UPnP/1.0 DLNADOC/1.50 Platinum/" + Plt.PLT_PLATINUM_SDK_VERSION_STRING;

		public static void Log (String prefix, HttpMessage message)
		{
			Console.WriteLine ("-------");
			Console.WriteLine (prefix);

			Console.WriteLine (message);
			Console.WriteLine ("-------");
		}
	}


	public enum PltDeviceSignature
	{
		PLT_DEVICE_UNKNOWN,
		PLT_DEVICE_XBOX,
		PLT_DEVICE_PS3,
		PLT_DEVICE_WMP,
		PLT_DEVICE_SONOS,
		PLT_DEVICE_MAC,
		PLT_DEVICE_WINDOWS,
		PLT_DEVICE_VLC
	}

 
	public class PltHttpHelper
	{

		public static bool IsConnectionKeepAlive (HttpMessage message)
		{
			String connection = message.Headers.GetHeaderValue (HttpMessage.NPT_HTTP_HEADER_CONNECTION);

			// the DLNA says that all HTTP 1.0 requests should be closed immediately by the server
			String protocol = message.Protocol;
			if (protocol.Equals (HttpMessage.NPT_HTTP_PROTOCOL_1_0))
				return false;

			// all HTTP 1.1 requests without a Connection header 
			// or with a keep-alive Connection header should be kept alive if possible 
			return (connection == null || connection.Equals ("keep-alive"));
		}


		public static bool IsBodyStreamSeekable (HttpMessage message)
		{
			HttpEntity entity = message.Entity;
			Stream stream;

			if (entity == null || Plt.IsFailed (entity.GetInputStream (out stream)) || stream == null) {
				return true;
			}

			return stream.CanSeek;
		}





		public static Boolean GetContentType (HttpMessage message, out String type)
		{
			type = "";

			String val = message.Headers.GetHeaderValue (HttpMessage.NPT_HTTP_HEADER_CONTENT_TYPE);
			Plt.CheckPointer (val);

			type = val;
			return true;
		}

		public static Boolean GetContentLength (HttpMessage message, out UInt64 len)
		{
			len = 0;

			String val = message.Headers.GetHeaderValue (HttpMessage.NPT_HTTP_HEADER_CONTENT_LENGTH);
			Plt.CheckPointer (val);
			return UInt64.TryParse (val, out len);
		}

		public static Boolean GetHost (HttpRequest request, out String value)
		{
			value = "";

			String val = request.Headers.GetHeaderValue (HttpMessage.NPT_HTTP_HEADER_HOST);
			Plt.CheckPointer (val);

			value = val;
			return true;
		}

		public  static void	SetHost (HttpRequest request, String host)
		{
			request.Headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_HOST, host); 
		}

		public static PltDeviceSignature GetDeviceSignature (HttpRequest request)
		{
			String agent = request.Headers.GetHeaderValue (HttpMessage.NPT_HTTP_HEADER_USER_AGENT);
			String hdr = request.Headers.GetHeaderValue ("X-AV-Client-Info");
			String server = request.Headers.GetHeaderValue (HttpMessage.NPT_HTTP_HEADER_SERVER);

			if ((!String.IsNullOrEmpty (agent) && (agent.IndexOf ("XBox") >= 0 || agent.IndexOf ("Xenon") >= 0)) ||
			    (!String.IsNullOrEmpty (server) && server.IndexOf ("Xbox") >= 0)) {
				return PltDeviceSignature.PLT_DEVICE_XBOX;
			} else if (!String.IsNullOrEmpty (agent) && (agent.IndexOf ("Windows Media Player") >= 0 || agent.IndexOf ("Windows-Media-Player") >= 0 || agent.IndexOf ("Mozilla/4.0") >= 0 || agent.IndexOf ("WMFSDK") >= 0)) {
				return PltDeviceSignature.PLT_DEVICE_WMP;
			} else if (!String.IsNullOrEmpty (agent) && (agent.IndexOf ("Sonos") >= 0)) {
				return PltDeviceSignature.PLT_DEVICE_SONOS;
			} else if ((!String.IsNullOrEmpty (agent) && agent.IndexOf ("PLAYSTATION 3") >= 0) || (!String.IsNullOrEmpty (hdr) && hdr.IndexOf ("PLAYSTATION 3") >= 0)) {
				return PltDeviceSignature.PLT_DEVICE_PS3;
			} else if (!String.IsNullOrEmpty (agent) && agent.IndexOf ("Windows") >= 0) {
				return PltDeviceSignature.PLT_DEVICE_WINDOWS;
			} else if (!String.IsNullOrEmpty (agent) && (agent.IndexOf ("Mac") >= 0 || agent.IndexOf ("OS X") >= 0 || agent.IndexOf ("OSX") >= 0)) {
				return PltDeviceSignature.PLT_DEVICE_MAC;
			} else if (!String.IsNullOrEmpty (agent) && (agent.IndexOf ("VLC") >= 0 || agent.IndexOf ("VideoLan") >= 0)) {
				return PltDeviceSignature.PLT_DEVICE_VLC;
			} else {
				Plt.LogFine (String.Format ("Unknown device signature (ua=%s)", !String.IsNullOrEmpty (agent) ? agent : "none"));
			}

			return PltDeviceSignature.PLT_DEVICE_UNKNOWN;
		}

		public  static Boolean SetBody (HttpMessage message, String text, out HttpEntity entity)
		{
			byte[] buffer = System.Text.Encoding.UTF8.GetBytes (text);
			return SetBody (message, buffer, buffer.Length, out entity);
		}

		public  static Boolean SetBody (HttpMessage message, byte[] body, long len, out HttpEntity entity)
		{
			entity = new HttpEntity ();
			if (len == 0)
				return true;

			// dump the body in a memory stream
			MemoryStream ms = new MemoryStream ();
			ms.Write (body, 0, (int)len);
			 
			// set content length
			return SetBody (message, ms, out entity);
		}

		public  static Boolean SetBody (HttpMessage message, Stream stream, out HttpEntity entity)
		{
			HttpEntity _entity = message.Entity;
			if (_entity == null) {
				// no entity yet, create one
				_entity = new HttpEntity ();
				message.Entity = _entity;
			}
		
			entity = _entity;

			// set the entity body
			return _entity.SetInputStream (stream, true);
		}

		public  static Boolean GetBody (HttpMessage message, out String body)
		{
			Stream stream;
			body = null;
			try{
			
			// get stream
			HttpEntity entity = message.Entity;
			if (entity.GetContentLength () == 0) {
				body = "";
				return true;
			}
			if (entity == null || Plt.IsFailed (entity.GetInputStream (out stream)) || stream == null) {
				return false;
			}

			
				byte[] buffer= new byte[entity.GetContentLength()];
				stream.Read(buffer,0,buffer.Length);
				MemoryStream ms = new MemoryStream (buffer);
		 
			 
			StreamReader reader = new StreamReader (ms);
			body = reader.ReadToEnd ();
		
			return true;
			}catch(Exception e){
				Console.Error.WriteLine ("error while gettin body");
				return false;
			}
		}

		public  static Boolean ParseBody (HttpMessage message, out XmlElement xml)
		{
			xml = null;

			// read body
			String body;
			Plt.CheckWarining (GetBody (message, out body));

			return PltXmlHelper.Parse (body, out xml);
		}

		public static void	SetBasicAuthorization (HttpRequest request, String username, String password)
		{
			String encoded;
			String cred = username + ":" + password;
			var plainTextBytes = System.Text.Encoding.UTF8.GetBytes (cred);
			encoded = System.Convert.ToBase64String (plainTextBytes);
	
			request.Headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_AUTHORIZATION, "Basic " + encoded); 
		}
	}

	public class PltHttpRequestContext : HttpRequestContext
	{
		private HttpRequest m_Request;


		public  PltHttpRequestContext (HttpRequest request)
		{ 
			m_Request = request;
		}

		public PltHttpRequestContext (HttpRequest request, HttpRequestContext context) : base (context.LocalAddress, context.RemoteAddress)
		{
			m_Request = request;
		}

    
		public HttpRequest GetRequest ()
		{ 
			return m_Request; 
		}

		public PltDeviceSignature GetDeviceSignature ()
		{ 
			return PltHttpHelper.GetDeviceSignature (m_Request); 
		}
     
	}

	public class PltHttpRequestHandler : HttpRequestHandler
	{
		HttpRequestHandler m_Delegate;

		public PltHttpRequestHandler (HttpRequestHandler adelegate)
		{
			m_Delegate = adelegate;
		}

		public override Boolean SetupResponse (HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
			return m_Delegate.SetupResponse (request, context, response);
		}
   
	}


	class HttpHeaderFinder: IFinder<HttpHeader>
	{
		String m_Name;

		public HttpHeaderFinder (String name)
		{
			m_Name = name;
		}

		public Boolean IsFound (HttpHeader header)
		{
			if (header.Name.Equals (m_Name)) {
				return true;
			} else {
				return false;
			}
		}
	}


	class HttpHeaderPrinter: IApplier<HttpHeader>
	{
		Stream m_Stream;
		StreamWriter writer;

		public HttpHeaderPrinter (Stream stream)
		{
			m_Stream = stream;
			writer = new StreamWriter (stream);
		}

		public Boolean Apply (HttpHeader header)
		{
			writer.Write (header.Name);
			writer.Write (": ");
			writer.Write (header.Value);
			writer.Write ("\r\n");
			writer.Flush ();
			return true;
		}
	}

	/* TODO
	class HttpHeaderLogger
	{
		public:
		// methods
		public HttpHeaderLogger(NPT_LoggerReference& logger, int level) : 
		m_Logger(logger), m_Level(level) {}
		NPT_Result operator()(NPT_HttpHeader*& header) const {
			NPT_COMPILER_UNUSED(header);

			NPT_LOG_L2(m_Logger, m_Level, "%s: %s", 
				(const char*)header->GetName(), 
				(const char*)header->GetValue());
			return NPT_SUCCESS;
		}

		NPT_LoggerReference& m_Logger;
		int                  m_Level;
	};
	*/
	 

}
  