﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

 

namespace Platinum
{
	public class PltAction
	{
		protected PltActionDesc m_ActionDesc;
		protected List<PltArgument> m_Arguments = new List<PltArgument> ();
		protected int m_ErrorCode;
		protected String m_ErrorDescription;
		protected PltDeviceData m_RootDevice;

		public PltAction (PltActionDesc desc)
		{
			m_ActionDesc = desc;
			m_ErrorCode = 0;
		}


		public PltAction (PltActionDesc desc, PltDeviceData root_device) : this (desc)
		{
			m_RootDevice = root_device;
		}

		public void Dispose ()
		{
			m_Arguments.Apply (new DisposerApplier<PltArgument> ());
		}

		public PltActionDesc GetActionDesc ()
		{
			return m_ActionDesc; 
		}

		public Boolean GetArgumentValue (String name, out String value)
		{
			value = null;
			PltArgument arg = GetArgument (name);
			if (arg == null) {
				return false;
			}
			value = arg.GetValue ();
			return true;
		}

		public Boolean GetArgumentValue (String name, out int value)
		{
			String tmp_value;
			Plt.CheckWarining (GetArgumentValue (name, out tmp_value));
			return Int32.TryParse (tmp_value, out value);
		}


		public Boolean GetArgumentValue (String name, out bool value)
		{
			String tmp_value;
			value = true;
			Plt.CheckWarining (GetArgumentValue (name, out tmp_value));
			if (tmp_value.EqualsIgnoreCase ("1") ||
			    tmp_value.EqualsIgnoreCase ("TRUE") ||
			    tmp_value.EqualsIgnoreCase ("YES")) {
				value = true;
			} else if (tmp_value.EqualsIgnoreCase ("0") ||
			           tmp_value.EqualsIgnoreCase ("FALSE") ||
			           tmp_value.EqualsIgnoreCase ("NO")) {
				value = false;
			} else {
				return false;
			}
			return true;
		}

		public Boolean VerifyArgumentValue (String name, String value)
		{
			String str;
			Plt.CheckSevere (GetArgumentValue (name, out str));

			return str.EqualsIgnoreCase (value) ? false : true;
		}

		public Boolean VerifyArguments (bool input)
		{
			int count = 0;

			// Check we have all the required parameters (in or out)
			for (int i = 0; i < m_ActionDesc.GetArgumentDescs ().Count; i++) {
				PltArgumentDesc arg_desc = m_ActionDesc.GetArgumentDescs () [i];

				// only input arguments are needed
				if (!arg_desc.GetDirection ().Equals (input ? "in" : "out"))
					continue;

				// look for this argument in the list we received
				PltArgument arg = null;
				if (Plt.IsFailed (Plt.ContainerFind<PltArgument> (m_Arguments, new PltArgumentNameFinder (arg_desc.GetName ()), out arg))) {
					Plt.LogWarn (String.Format ("Argument {0} for action {1} not found", arg_desc.GetName (), m_ActionDesc.GetName ()));
					return false;
				}
				++count;
			}

			SetError (0, "");
			return true;
		}

		public Boolean SetArgumentOutFromStateVariable (String name)
		{
			// look for this argument in the action list of arguments
			PltArgumentDesc arg_desc = null;
			Plt.CheckSevere (Plt.ContainerFind<PltArgumentDesc> (m_ActionDesc.GetArgumentDescs (), new PltArgumentDescNameFinder (name), out arg_desc));

			return SetArgumentOutFromStateVariable (arg_desc);
		}

		public Boolean SetArgumentsOutFromStateVariable ()
		{
			// go through the list of the action output arguments
			for (int i = 0; i < m_ActionDesc.GetArgumentDescs ().Count; i++) {
				PltArgumentDesc arg_desc = m_ActionDesc.GetArgumentDescs () [i];

				// only output arguments are needed
				if (!arg_desc.GetDirection ().Equals ("out"))
					continue;

				Plt.CheckSevere (SetArgumentOutFromStateVariable (arg_desc));
			}

			return true;
		}

		public Boolean SetArgumentValue (String name, String value)
		{
			// look for this argument in our argument list
			// and replace the value if we found it 
//			PltArguments::Iterator iter = null;
			PltArgument iter;

			if (Plt.IsSucceded (Plt.ContainerFind<PltArgument> (m_Arguments, new PltArgumentNameFinder (name), out iter))) {
				Boolean res = iter.SetValue (value);

				// remove argument from list if failed
				// so that when we verify arguments later, 
				// we don't use a previously set value
				if (Plt.IsFailed (res))
					m_Arguments.Remove (iter);
				return res;
			}

			// since we didn't find it, create a clone 
			PltArgument arg;
			Plt.CheckSevere (PltArgument.CreateArgument (m_ActionDesc, name, value, out arg));

			// insert it at the right position
			for (int i = 0; i < m_Arguments.Count; i++) {
				iter = m_Arguments [i];
				if (iter.GetPosition () > arg.GetPosition ()) {
					m_Arguments.Insert (i, arg);
					return true;
				}
			}

			m_Arguments.Add (arg);
			return true;
		}

		public Boolean SetError (int code, String description)
		{
			m_ErrorCode = code;
			m_ErrorDescription = description;
			return true;
		}

		public String GetError (out int code)
		{
			code = m_ErrorCode;
			return m_ErrorDescription;
		}

		public String GetError ()
		{
			return m_ErrorDescription;
		}


		public int GetErrorCode ()
		{
			return m_ErrorCode;
		}

		public Boolean FormatSoapRequest (Stream stream)
		{
			String str;
			Boolean res;
			XmlElement body = null;
			XmlElement request = null;
			XmlDocument doc = new XmlDocument ();

			XmlElement envelope = doc.CreateElement ("s","Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
			 
			doc.AppendChild (envelope);
			envelope.SetAttribute("encodingStyle", "http://schemas.xmlsoap.org/soap/envelope/", "http://schemas.xmlsoap.org/soap/encoding/");

			body = doc.CreateElement ("s","Body","http://schemas.xmlsoap.org/soap/envelope/");
			envelope.AppendChild (body);

			request = doc.CreateElement ("u",  m_ActionDesc.GetName (), m_ActionDesc.GetService ().GetServiceType ());
		 
			body.AppendChild (request);

			for (int i = 0; i < m_Arguments.Count; i++) {
				PltArgument argument = m_Arguments [i];
				if (argument.GetDesc ().GetDirection ().Equals ("in")) {
					if (Plt.IsFailed (res = PltXmlHelper.AddChildText (request, argument.GetDesc ().GetName (), argument.GetValue ()))) {
						return res;
					}
				}
			}

			if (Plt.IsFailed (res = PltXmlHelper.Serialize (envelope, out str))) {
				return res;
			}

			StreamWriter writer = new StreamWriter (stream);
			writer.Write (str);
			writer.Flush ();
			return true;
		}

		public Boolean FormatSoapResponse (Stream stream)
		{
			if (m_ErrorCode != 0)
				return FormatSoapError (m_ErrorCode, m_ErrorDescription, stream);
			 

			XmlDocument doc = new XmlDocument ();

			String str;
			Boolean res;
			XmlElement body = null;
			XmlElement response = null;
			XmlElement node = null;
			XmlElement envelope = doc.CreateElement ("s", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");

   
			envelope.SetAttribute("encodingStyle", "http://schemas.xmlsoap.org/soap/envelope/", "http://schemas.xmlsoap.org/soap/encoding/");

			body = doc.CreateElement ("s","Body","http://schemas.xmlsoap.org/soap/envelope/");
			envelope.AppendChild (body);        

			response = doc.CreateElement ("u", m_ActionDesc.GetName () + "Response", m_ActionDesc.GetService ().GetServiceType ());
   
			body.AppendChild (response);

			for (int i = 0; i < m_Arguments.Count; i++) {
				PltArgument argument = m_Arguments [i];
				if (argument.GetDesc ().GetDirection ().Equals ("out")) {
					node = doc.CreateElement (argument.GetDesc ().GetName ());
					node.InnerText = argument.GetValue ();
					response.AppendChild (node);
					/*
#ifndef REMOVE_WMP_DATATYPE_EXTENSION
            PltStateVariable var = argument.GetDesc().GetRelatedStateVariable();
            if (var!=null) {
                node->SetNamespaceUri("dt", "urn:schemas-microsoft-com:datatypes");
                node->SetAttribute("dt", "dt", var->GetDataType());
            }
#endif
*/
				}
			}

			// this will xmlescape any values that contain xml characters
			str = PltXmlHelper.Serialize (envelope);
    
			StreamWriter writer = new StreamWriter (stream);
			writer.Write (str);
			writer.Flush ();
			return true;
			//return stream.Write (str, str.Length);

		}

   

		public static Boolean FormatSoapError (int code, String desc, Stream stream)
		{
			String str;
			Boolean res;
			XmlElement body = null;
			XmlElement fault = null;
			XmlElement detail = null;
			XmlElement UPnPError = null;
			XmlDocument doc = new XmlDocument ();
			XmlElement envelope = doc.CreateElement ("s", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
			envelope.SetAttribute("encodingStyle", "http://schemas.xmlsoap.org/soap/envelope/", "http://schemas.xmlsoap.org/soap/encoding/");

			body = doc.CreateElement ("s","Body","http://schemas.xmlsoap.org/soap/envelope/");
			envelope.AppendChild (body);        

			fault = doc.CreateElement ("s","Fault","http://schemas.xmlsoap.org/soap/envelope/");
			body.AppendChild (fault);

			if (Plt.IsFailed (res = PltXmlHelper.AddChildText (fault, "faultcode", "Client","s"))) {
				return res;
			}
			if (Plt.IsFailed (res = PltXmlHelper.AddChildText (fault, "faultstring", "UPnPError"))) {
				return res;
			}

			detail = doc.CreateElement ("detail");
			fault.AppendChild (detail);

			UPnPError = doc.CreateElement ("UPnPError","urn:schemas-upnp-org:control-1-0");
			 			 
			detail.AppendChild (UPnPError);

			if (Plt.IsFailed (res = PltXmlHelper.AddChildText (UPnPError, "errorCode", code.ToString ()))) {
				return res;
			}
			if (Plt.IsFailed (res = PltXmlHelper.AddChildText (UPnPError, "errorDescription", desc))) {
				return res;
			}


			if (Plt.IsFailed (res = PltXmlHelper.Serialize (envelope, out str))) {
				return res;
			}

			StreamWriter writer = new StreamWriter (stream);
			writer.Write (str);
			writer.Flush ();
			return true;
		}


		private Boolean SetArgumentOutFromStateVariable (PltArgumentDesc arg_desc)
		{
			// only output arguments can use a state variable
			if (!arg_desc.GetDirection ().Equals ("out")) {
				return false;
			}

			PltStateVariable variable = arg_desc.GetRelatedStateVariable ();
			if (variable == null)
				return false;

			// assign the value to an argument
			Plt.CheckSevere (SetArgumentValue (arg_desc.GetName (), variable.GetValue ()));
			return true;
		}

		private PltArgument GetArgument (String name)
		{
			PltArgument argument = null;
			Plt.ContainerFind (m_Arguments, new PltArgumentNameFinder (name), out argument);
			return argument;
		}
	}

	 
	 
	 

	public class PltActionDesc
	{
		protected String m_Name;
		protected PltService m_Service;
		protected List<PltArgumentDesc> m_ArgumentDescs;


		public PltActionDesc (String name, PltService service)
		{
			m_Name = name;
			m_Service = service;
			m_ArgumentDescs = new List<PltArgumentDesc> ();
		}

		public void Dispose ()
		{
			m_ArgumentDescs.Apply (new DisposerApplier<PltArgumentDesc> ());
		}

		public List<PltArgumentDesc> GetArgumentDescs ()
		{ 
			return m_ArgumentDescs; 
		}

		public String GetName ()
		{
			return m_Name;
		}

		public PltArgumentDesc GetArgumentDesc (String name)
		{
			PltArgumentDesc arg_desc = null;
			Plt.ContainerFind (m_ArgumentDescs, new PltArgumentDescNameFinder (name), out arg_desc);
			return arg_desc;
		}

		public Boolean GetSCPDXML (XmlElement node)
		{
			XmlElement action = node.OwnerDocument.CreateElement (node.Prefix, "action", node.NamespaceURI);
			node.AppendChild (action);
			Plt.CheckSevere (PltXmlHelper.AddChildText (action, "name", m_Name));

			XmlElement argumentList = node.OwnerDocument.CreateElement (node.Prefix, "argumentList", node.NamespaceURI);
			action.AppendChild (argumentList);

			// no arguments is ok
			if (m_ArgumentDescs.Count == 0)
				return true;

			return m_ArgumentDescs.ApplyUntil<PltArgumentDesc> (new PltArgumentGetSCPDXMLIterator (argumentList), ApplyUntil.ResultSuccess);
		}

		public PltService GetService ()
		{
			return m_Service;
		}
	}


	 
	public class PltActionDescNameFinder:IFinder<PltActionDesc>
	{
		private String	Name{ get; set; }

		public PltActionDescNameFinder (String name)
		{
			Name = name;
		}

		public Boolean IsFound (PltActionDesc action_desc)
		{
			return action_desc.GetName ().Equals (Name);
		}
	}

	public class PltArgumentGetSCPDXMLIterator:IApplier<PltArgumentDesc>
	{
		XmlElement m_Node;

		public PltArgumentGetSCPDXMLIterator (XmlElement node)
		{
			m_Node = node;
		}

		public Boolean Apply (PltArgumentDesc data)
		{
			return data.GetSCPDXML (m_Node);
		}
	}

	public class PltActionGetSCPDXMLIterator:IApplier<PltActionDesc>
	{
		XmlElement m_Node;

		public PltActionGetSCPDXMLIterator (XmlElement node)
		{
			m_Node = node;
		}

		public Boolean Apply (PltActionDesc data)
		{
			return data.GetSCPDXML (m_Node);
		}
	}

	public class PltStateVariableGetSCPDXMLIterator:IApplier<PltStateVariable>
	{
		XmlElement m_Node;

		public PltStateVariableGetSCPDXMLIterator (XmlElement node)
		{
			m_Node = node;
		}

		public Boolean Apply (PltStateVariable data)
		{
			return data.GetSCPDXML (m_Node);
		}
	}




}
 
   