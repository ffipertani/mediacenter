﻿using System;

namespace Platinum
{
	public class MediaRenderer: PltDeviceHost
	{
		private MediaRendererDelegate m_Delegate;

	 
			
		public MediaRenderer (String  friendly_name, bool show_ip = false, String  uuid = null, int port = 0, bool port_rebind = false)
			: base ("/", uuid, "urn:schemas-upnp-org:device:MediaRenderer:1", friendly_name, show_ip, port, port_rebind)
		{
			m_Delegate = null;
			ModelDescription = "Plutinosoft AV Media Renderer Device";
			ModelName = "AV Renderer Device";
			ModelURL = "http://www.plutinosoft.com/platinum";
			DlnaDoc = "DMR-1.50";
		}



		// methods
		public 	virtual void SetDelegate (MediaRendererDelegate adelegate)
		{
			m_Delegate = adelegate; 
		}

		// PLT_DeviceHost methods
		protected override Boolean SetupServices ()
		{
			PltService service;

			{
				/* AVTransport */
				service = new PltService (this, "urn:schemas-upnp-org:service:AVTransport:1", "urn:upnp-org:serviceId:AVTransport", "AVTransport", "urn:schemas-upnp-org:metadata-1-0/AVT/");
				Plt.CheckSevere (service.SetSCPDXML ((String)System.Text.Encoding.UTF8.GetString(AVTransportSCPD.RDRAVTransportSCPD)));
				Plt.CheckSevere (AddService (service));

				service.SetStateVariableRate ("LastChange", TimeSpan.FromMilliseconds (200));
				service.SetStateVariable ("A_ARG_TYPE_InstanceID", "0"); 

				// GetCurrentTransportActions
				service.SetStateVariable ("CurrentTransportActions", "Play,Pause,Stop,Seek,Next,Previous");

				// GetDeviceCapabilities
				service.SetStateVariable ("PossiblePlaybackStorageMedia", "NONE,NETWORK,HDD,CD-DA,UNKNOWN");
				service.SetStateVariable ("PossibleRecordStorageMedia", "NOT_IMPLEMENTED");
				service.SetStateVariable ("PossibleRecordQualityModes", "NOT_IMPLEMENTED");

				// GetMediaInfo
				service.SetStateVariable ("NumberOfTracks", "0");
				service.SetStateVariable ("CurrentMediaDuration", "00:00:00");
				service.SetStateVariable ("AVTransportURI", "");
				service.SetStateVariable ("AVTransportURIMetadata", "");

				service.SetStateVariable ("NextAVTransportURI", "NOT_IMPLEMENTED");
				service.SetStateVariable ("NextAVTransportURIMetadata", "NOT_IMPLEMENTED");
				service.SetStateVariable ("PlaybackStorageMedium", "NONE");
				service.SetStateVariable ("RecordStorageMedium", "NOT_IMPLEMENTED");
				service.SetStateVariable ("RecordMediumWriteStatus", "NOT_IMPLEMENTED");

				// GetPositionInfo
				service.SetStateVariable ("CurrentTrack", "0");
				service.SetStateVariable ("CurrentTrackDuration", "00:00:00");
				service.SetStateVariable ("CurrentTrackMetadata", "");
				service.SetStateVariable ("CurrentTrackURI", "");
				service.SetStateVariable ("RelativeTimePosition", "00:00:00"); 
				service.SetStateVariable ("AbsoluteTimePosition", "00:00:00");
				service.SetStateVariable ("RelativeCounterPosition", "2147483647"); // means NOT_IMPLEMENTED
				service.SetStateVariable ("AbsoluteCounterPosition", "2147483647"); // means NOT_IMPLEMENTED

				// disable indirect eventing for certain state variables
				PltStateVariable avar;
				avar = service.FindStateVariable ("RelativeTimePosition");
				if (avar != null)
					avar.DisableIndirectEventing ();
				avar = service.FindStateVariable ("AbsoluteTimePosition");
				if (avar != null)
					avar.DisableIndirectEventing ();
				avar = service.FindStateVariable ("RelativeCounterPosition");
				if (avar != null)
					avar.DisableIndirectEventing ();
				avar = service.FindStateVariable ("AbsoluteCounterPosition");
				if (avar != null)
					avar.DisableIndirectEventing ();

				// GetTransportInfo
				service.SetStateVariable ("TransportState", "NO_MEDIA_PRESENT");
				service.SetStateVariable ("TransportStatus", "OK");
				service.SetStateVariable ("TransportPlaySpeed", "1");

				// GetTransportSettings
				service.SetStateVariable ("CurrentPlayMode", "NORMAL");
				service.SetStateVariable ("CurrentRecordQualityMode", "NOT_IMPLEMENTED");

				//service.Detach ();
				//service = null;
			}

			{
				/* ConnectionManager */
				service = new PltService (this, "urn:schemas-upnp-org:service:ConnectionManager:1", "urn:upnp-org:serviceId:ConnectionManager", "ConnectionManager");
				Plt.CheckSevere (service.SetSCPDXML ((String)System.Text.Encoding.UTF8.GetString(ConnectionManagerSCPD.RDRConnectionManagerSCPD)));
				Plt.CheckSevere (AddService (service));

				service.SetStateVariable ("CurrentConnectionIDs", "0");

				// put all supported mime types here instead
				service.SetStateVariable ("SinkProtocolInfo", "http-get:*:video/x-ms-wmv:DLNA.ORG_PN=WMVMED_PRO,http-get:*:video/x-ms-asf:DLNA.ORG_PN=MPEG4_P2_ASF_SP_G726,http-get:*:video/x-ms-wmv:DLNA.ORG_PN=WMVMED_FULL,http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_MED,http-get:*:video/x-ms-wmv:DLNA.ORG_PN=WMVMED_BASE,http-get:*:audio/L16;rate=44100;channels=1:DLNA.ORG_PN=LPCM,http-get:*:video/mpeg:DLNA.ORG_PN=MPEG_PS_PAL,http-get:*:video/mpeg:DLNA.ORG_PN=MPEG_PS_NTSC,http-get:*:video/x-ms-wmv:DLNA.ORG_PN=WMVHIGH_PRO,http-get:*:audio/L16;rate=44100;channels=2:DLNA.ORG_PN=LPCM,http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_SM,http-get:*:video/x-ms-asf:DLNA.ORG_PN=VC1_ASF_AP_L1_WMA,http-get:*:audio/x-ms-wma:DLNA.ORG_PN=WMDRM_WMABASE,http-get:*:video/x-ms-wmv:DLNA.ORG_PN=WMVHIGH_FULL,http-get:*:audio/x-ms-wma:DLNA.ORG_PN=WMAFULL,http-get:*:audio/x-ms-wma:DLNA.ORG_PN=WMABASE,http-get:*:video/x-ms-wmv:DLNA.ORG_PN=WMVSPLL_BASE,http-get:*:video/mpeg:DLNA.ORG_PN=MPEG_PS_NTSC_XAC3,http-get:*:video/x-ms-wmv:DLNA.ORG_PN=WMDRM_WMVSPLL_BASE,http-get:*:video/x-ms-wmv:DLNA.ORG_PN=WMVSPML_BASE,http-get:*:video/x-ms-asf:DLNA.ORG_PN=MPEG4_P2_ASF_ASP_L5_SO_G726,http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_LRG,http-get:*:audio/mpeg:DLNA.ORG_PN=MP3,http-get:*:video/mpeg:DLNA.ORG_PN=MPEG_PS_PAL_XAC3,http-get:*:audio/x-ms-wma:DLNA.ORG_PN=WMAPRO,http-get:*:video/mpeg:DLNA.ORG_PN=MPEG1,http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_TN,http-get:*:video/x-ms-asf:DLNA.ORG_PN=MPEG4_P2_ASF_ASP_L4_SO_G726,http-get:*:audio/L16;rate=48000;channels=2:DLNA.ORG_PN=LPCM,http-get:*:audio/mpeg:DLNA.ORG_PN=MP3X,http-get:*:video/x-ms-wmv:DLNA.ORG_PN=WMVSPML_MP3,http-get:*:video/x-ms-wmv:*");
				service.SetStateVariable ("SourceProtocolInfo", "");

				//service.Detach ();
				//service = null;
			}

			{
				/* RenderingControl */
				service = new PltService (this, "urn:schemas-upnp-org:service:RenderingControl:1", "urn:upnp-org:serviceId:RenderingControl", "RenderingControl", "urn:schemas-upnp-org:metadata-1-0/RCS/");
				Plt.CheckSevere (service.SetSCPDXML ((String)System.Text.Encoding.UTF8.GetString(RenderingControlSCPD.RDRRenderingControlSCPD)));
				Plt.CheckSevere (AddService (service));

				service.SetStateVariableRate ("LastChange", TimeSpan.FromMilliseconds (200));

				service.SetStateVariable ("Mute", "0");
				service.SetStateVariableExtraAttribute ("Mute", "Channel", "Master");
				service.SetStateVariable ("Volume", "100");
				service.SetStateVariableExtraAttribute ("Volume", "Channel", "Master");
				service.SetStateVariable ("VolumeDB", "0");
				service.SetStateVariableExtraAttribute ("VolumeDB", "Channel", "Master");

				service.SetStateVariable ("PresetNameList", "FactoryDefaults");

				//service.Detach ();
				//service = null;
			}

			return true;
		}

		protected override Boolean OnAction (PltAction action, HttpRequestContext context)
		{
			/* parse the action name */
			String name = action.GetActionDesc ().GetName ();

			// since all actions take an instance ID and we only support 1 instance
			// verify that the Instance ID is 0 and return an error here now if not
			String serviceType = action.GetActionDesc ().GetService ().GetServiceType ();
			if (serviceType.EqualsIgnoreCase ("urn:schemas-upnp-org:service:AVTransport:1")) {
				if (Plt.IsFailed (action.VerifyArgumentValue ("InstanceID", "0"))) {
					action.SetError (718, "Not valid InstanceID");
					return false;
				}
			}
			serviceType = action.GetActionDesc ().GetService ().GetServiceType ();
			if (serviceType.EqualsIgnoreCase ("urn:schemas-upnp-org:service:RenderingControl:1")) {
				if (Plt.IsFailed (action.VerifyArgumentValue ("InstanceID", "0"))) {
					action.SetError (702, "Not valid InstanceID");
					return false;
				}
			}

			/* Is it a ConnectionManager Service Action ? */
			if (name.EqualsIgnoreCase ("GetCurrentConnectionInfo")) {
				return OnGetCurrentConnectionInfo (action);
			}  

			/* Is it a AVTransport Service Action ? */
			if (name.EqualsIgnoreCase ("Next")) {
				return OnNext (action);
			}
			if (name.EqualsIgnoreCase ("Pause")) {
				return OnPause (action);
			}
			if (name.EqualsIgnoreCase ("Play")) {
				return OnPlay (action);
			}
			if (name.EqualsIgnoreCase ("Previous")) {
				return OnPrevious (action);
			}
			if (name.EqualsIgnoreCase ("Seek")) {
				return OnSeek (action);
			}
			if (name.EqualsIgnoreCase ("Stop")) {
				return OnStop (action);
			}
			if (name.EqualsIgnoreCase ("SetAVTransportURI")) {
				return OnSetAVTransportURI (action);
			}
			if (name.EqualsIgnoreCase ("SetPlayMode")) {
				return OnSetPlayMode (action);
			}

			/* Is it a RendererControl Service Action ? */
			if (name.EqualsIgnoreCase ("SetVolume")) {
				return OnSetVolume (action);
			}
			if (name.EqualsIgnoreCase ("SetVolumeDB")) {
				return OnSetVolumeDB (action);
			}
			if (name.EqualsIgnoreCase ("GetVolumeDBRange")) {
				return OnGetVolumeDBRange (action);

			}
			if (name.EqualsIgnoreCase ("SetMute")) {
				return OnSetMute (action);
			}

			// other actions rely on state variables
			if (!Plt.IsFailed (action.SetArgumentsOutFromStateVariable ())) {
				return true;
			}

			 
			action.SetError (401, "No Such Action.");
			return false;
		}

			 


		// PLT_MediaRendererInterface methods
		// ConnectionManager
		protected	virtual Boolean OnGetCurrentConnectionInfo (PltAction action)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnGetCurrentConnectionInfo (action);
			}

			if (Plt.IsFailed (action.VerifyArgumentValue ("ConnectionID", "0"))) {
				action.SetError (706, "No Such Connection.");
				return false;
			}

			if (Plt.IsFailed (action.SetArgumentValue ("RcsID", "0"))) {
				return false;
			}
			if (Plt.IsFailed (action.SetArgumentValue ("AVTransportID", "0"))) {
				return false;
			}
			if (Plt.IsFailed (action.SetArgumentOutFromStateVariable ("ProtocolInfo"))) {
				return false;
			}
			if (Plt.IsFailed (action.SetArgumentValue ("PeerConnectionManager", "/"))) {
				return false;
			}
			if (Plt.IsFailed (action.SetArgumentValue ("PeerConnectionID", "-1"))) {
				return false;
			}
			if (Plt.IsFailed (action.SetArgumentValue ("Direction", "Input"))) {
				return false;
			}
			if (Plt.IsFailed (action.SetArgumentValue ("Status", "Unknown"))) {
				return false;
			}

			return true;
		}

		// AVTransport
		protected	virtual Boolean OnNext (PltAction action)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnNext (action);
			}
			throw new NotImplementedException ();
		}

		protected virtual Boolean OnPause (PltAction action)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnPause (action);
			}
			throw new NotImplementedException ();
		}

		protected virtual Boolean OnPlay (PltAction action)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnPlay (action);
			}
			throw new NotImplementedException ();
		}

		protected	virtual Boolean OnPrevious (PltAction action)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnPrevious (action);
			}
			throw new NotImplementedException ();
		}

		protected	virtual Boolean OnSeek (PltAction action)
		{
			if (m_Delegate!=null) {
				return m_Delegate.OnSeek (action);
			}
			throw new NotImplementedException ();
		}

		protected	virtual Boolean OnStop (PltAction action)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnStop (action);
			}
			throw new NotImplementedException ();
		}

		protected virtual Boolean OnSetAVTransportURI (PltAction action)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnSetAVTransportURI (action);
			}

			// default implementation is using state variable
			String uri;
			Plt.CheckWarining (action.GetArgumentValue ("CurrentURI", out uri));

			String metadata;
			Plt.CheckWarining (action.GetArgumentValue ("CurrentURIMetaData", out metadata));

			PltService serviceAVT;
			Plt.CheckWarining (FindServiceByType ("urn:schemas-upnp-org:service:AVTransport:1", out serviceAVT));

			// update service state variables
			serviceAVT.SetStateVariable ("AVTransportURI", uri);
			serviceAVT.SetStateVariable ("AVTransportURIMetaData", metadata);

			return true;
		}

		protected	virtual Boolean OnSetPlayMode (PltAction action)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnSetPlayMode (action);
			}
			throw new NotImplementedException ();
		}

		// RenderingControl
		protected	virtual Boolean OnSetVolume (PltAction action)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnSetVolume (action);
			}
			throw new NotImplementedException ();
		}

		protected	virtual Boolean OnSetVolumeDB (PltAction action)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnSetVolumeDB (action);
			}
			throw new NotImplementedException ();
		}

		protected	virtual Boolean OnGetVolumeDBRange (PltAction action)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnGetVolumeDBRange (action);
			}
			throw new NotImplementedException ();
		}

		protected	virtual Boolean OnSetMute (PltAction action)
		{
			if (m_Delegate != null) {
				return m_Delegate.OnSetMute (action);
			}
			throw new NotImplementedException ();
		}

		 

	}

	public interface MediaRendererDelegate
	{
		 
		Boolean OnGetCurrentConnectionInfo (PltAction action);

		// AVTransport
		Boolean OnNext (PltAction action);

		Boolean OnPause (PltAction action);

		Boolean OnPlay (PltAction action);

		Boolean OnPrevious (PltAction action);

		Boolean OnSeek (PltAction action);

		Boolean OnStop (PltAction action);

		Boolean OnSetAVTransportURI (PltAction action);

		Boolean OnSetPlayMode (PltAction action);

		// RenderingControl
		Boolean OnSetVolume (PltAction action);

		Boolean OnSetVolumeDB (PltAction action);

		Boolean OnGetVolumeDBRange (PltAction action);

		Boolean OnSetMute (PltAction action);
	}
}

 
     