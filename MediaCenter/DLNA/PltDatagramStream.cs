﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Net;

namespace Platinum
{
	public class PltInputDatagramStream:BaseStream
	{
		Socket Socket;
		SocketInfo Info;
		byte[] Buffer;
		int BufferOffset;
		int BufferSize;

		public PltInputDatagramStream (Socket socket, int buffer_size = 2000)
		{
			Socket = socket;
			BufferOffset = 0;
			BufferSize = 0;
			Buffer = new byte[buffer_size]; 
			Info = new SocketInfo ();
		}

	 

		public Boolean GetInfo (out SocketInfo info)
		{

			try {
				Info.LocalEndPoint = Socket.LocalEndPoint;
			} catch {
			}
			try {
				Info.RemoteEndPoint = Socket.RemoteEndPoint;
			} catch {
			}
			info = Info;
			return true;
		}

		public override bool CanRead {
			get {
				return true;
			}
		}

		public override void SetLength (long value)
		{
			BufferSize = 0;
			BufferOffset = 0;
		}
		/*
	BUFFER		---------------------
				   |		|		|
				   BO       BS   BL  
		*/
		public override int Read (byte[] buffer, int offset, int count)
		{
			 
			EndPoint ip = new IPEndPoint (IPAddress.Any, 0);
			int bytesRead = 0;
			int available = BufferSize - BufferOffset;
			// always try to read from socket if needed even if bytes_to_read is 0
			if (available == 0) {        
				// read data into it now
				SocketFlags flags = SocketFlags.None;
			
				int toreadfromnet = Buffer.Length - BufferSize;// count > available ? available : count;
				try {
					bytesRead = Socket.ReceiveFrom (Buffer, BufferSize, toreadfromnet, flags, ref ip);

					//bytesRead = Socket.Receive(buffer,offset,count,flags);
				} catch (Exception e){
					//Console.WriteLine (e);
					return 0;
				}

				 

				BufferSize += bytesRead;
			}

			available = BufferSize - BufferOffset;
			int toread = count > available ? available : count;
			for (int i = 0; i < toread; i++) {
				buffer [i + offset] = Buffer [i + BufferOffset];
			}
			BufferOffset += toread;
			// read buffer entirety, reset for next time
			if (BufferOffset == BufferSize) {
				BufferOffset = 0;
				BufferSize = 0;
			}



			Info.RemoteEndPoint = ip;
			GetInfo (out Info);	

			if (true) {
				return toread;
			}

			if (bytesRead == 0)
				return bytesRead;
				
			 
			int _bytes_to_read = bytesRead < available ? bytesRead : available;
			//NPT_CopyMemory(buffer, m_Buffer.UseData()+m_BufferOffset, _bytes_to_read);

			int index = offset;
			for (int i = BufferOffset; i < _bytes_to_read; i++) {
				buffer [index++] = Buffer [i];
			}

		 
			BufferOffset += _bytes_to_read;
			if (bytesRead > 0)
				bytesRead = _bytes_to_read;

			// read buffer entirety, reset for next time
			if (BufferOffset == BufferSize) {
				BufferOffset = 0;
				BufferSize = 0;
			}
			 

			return bytesRead;
		}

		/*
		// NPT_InputStream methods
		public Boolean Read (byte[] buffer, int  bytes_to_read, out int bytes_read)
		{
			Boolean res = true;

			bytes_read = 0;

			// always try to read from socket if needed even if bytes_to_read is 0
			if (m_Buffer.Length == 0) {        
				// read data into it now
				bytes_read = m_Socket.Receive(buffer,bytes_to_read,SocketFlags.None);

				// update info
				GetInfo (out m_Info);				 
			}

			if (bytes_to_read == 0)
				return res;

			if (Plt.IsSucceded (res)) {
				int available = m_Buffer.Length - m_BufferOffset;
				int _bytes_to_read = bytes_to_read < available ? bytes_to_read : available;
				//NPT_CopyMemory(buffer, m_Buffer.UseData()+m_BufferOffset, _bytes_to_read);
				buffer.CopyTo (m_Buffer, m_BufferOffset);
				m_BufferOffset += _bytes_to_read;

				if (bytes_read>0)
					bytes_read = _bytes_to_read;

				// read buffer entirety, reset for next time
				if (m_BufferOffset == m_Buffer.Length) {
					m_BufferOffset = 0;
					m_Buffer = new byte[0];
				}
			}

			return res;
		}
		 */
	}

	public class PltOutputDatagramStream:BaseStream
	{
		protected Socket m_Socket;
		protected IPEndPoint m_Address;
		protected byte[] Buffer;
		protected int BufferSize;

		public PltOutputDatagramStream (Socket socket, int size = 4096, IPEndPoint address = null)
		{
			m_Socket = socket;
			m_Address = address != null ? new IPEndPoint (address.Address, address.Port) : null;
			 
		 
			BufferSize = 0;
			Buffer = new byte[size]; 
		}


		public override void Write (byte[] buffer, int offset, int count)
		{
			// calculate if we need to increase the buffer
			byte[] tmpBuffer;
			int overflow = count - (Buffer.Length - BufferSize);
			if (overflow > 0) {
				tmpBuffer = new byte[Buffer.Length + overflow];
				Buffer.CopyTo (tmpBuffer, 0);
			} else {
				tmpBuffer = Buffer;
			}
			// copy data in place at the end of what we have there already
			//buffer.CopyTo (tmpBuffer,BufferSize);
			for (int i = 0; i < count; i++) {
				tmpBuffer [i + BufferSize] = buffer [i + offset];
			}
			//NPT_CopyMemory(m_Buffer.UseData() + m_Buffer.GetDataSize(), buffer, bytes_to_write);
			//m_Buffer.SetDataSize(m_Buffer.GetDataSize() + bytes_to_write);
			BufferSize += count;	
			Buffer = tmpBuffer;
		}

		public override void Flush ()
		{
			// send buffer now
			if (BufferSize == 0) {
				return;
			}
			m_Socket.SendTo (Buffer, BufferSize, SocketFlags.None, m_Address);

			// reset buffer
			Buffer = new byte[0];
			BufferSize = 0;
		}

		 

		public override bool CanWrite {
			get {
				return true;
			}
		}

		public override long Length {
			get {
				return BufferSize;
			}
		}


		 
	}



	public abstract class BaseStream:Stream
	{
		public override void Flush ()
		{
			throw new NotImplementedException ();
		}

		public override bool CanRead {
			get {
				return false;
			}
		}

		public override bool CanWrite {
			get {
				return false;
			}
		}


		public override bool CanSeek {
			get {
				return false;
			}
		}

		public override long Seek (long offset, SeekOrigin origin)
		{
			throw new NotImplementedException ();
		}

		public override long Position {
			get {
				throw new NotImplementedException ();
			}
			set {
				throw new NotImplementedException ();
			}
		}

		public override void SetLength (long value)
		{
			throw new NotImplementedException ();
		}

		public override long Length {
			get {
				throw new NotImplementedException ();
			}
		}

		public override int Read (byte[] buffer, int offset, int count)
		{
			throw new NotImplementedException ();
		}

		public override void Write (byte[] buffer, int bytes_to_write, int bytes_written)
		{
			throw new NotImplementedException ();
		}
	}


}
  
 