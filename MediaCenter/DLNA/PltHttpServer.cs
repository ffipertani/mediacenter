﻿using System;
using System.Net;
using System.IO;
using System.Globalization;
using System.Net.Sockets;
using System.Collections.Generic;

namespace Platinum
{
	public class PltHttpServer:HttpRequestHandler
	{
		private PltTaskManager        m_TaskManager;
		private TcpListener				    m_Server;
		private IPAddress                   m_Address;
		private int                      m_Port;
		private bool                            m_AllowRandomPortOnBindFailure;
		private bool                            m_ReuseAddress;
		private bool                            m_Running;
		private bool                            m_Aborted;
		private List<HandlerConfig> m_RequestHandlers;

		public PltHttpServer ():this(IPAddress.Any, 0, false, 50, false)
		{
			 
		}


		public PltHttpServer (IPAddress address, int    port, bool allow_random_port_on_bind_failure = false, int  max_clients = 50, bool reuse_address = false)
		{
			m_TaskManager = new PltTaskManager(max_clients);
			m_RequestHandlers = new List<HandlerConfig> ();
			m_Address  = address;
			m_Port = port;
			m_AllowRandomPortOnBindFailure = allow_random_port_on_bind_failure;
			m_ReuseAddress = reuse_address;
			m_Running = false;
			m_Aborted = false;
		}

		public Boolean AddRequestHandler(HttpRequestHandler handler, String path, bool include_children,bool transfer_ownership)
		{
			m_RequestHandlers.Add(new HandlerConfig(handler, path, include_children, transfer_ownership));
			return true;
		}

		public void Dispose()
		{
			Stop();
		}

		public static Boolean ServeFile(HttpRequest request, HttpRequestContext context, HttpResponse response, String file_path)
		{
			Stream stream;
			FileInfo             file_info = new FileInfo(file_path);

			// prevent hackers from accessing files outside of our root
			if ((file_path.IndexOf("/..") >= 0) || (file_path.IndexOf("\\..") >= 0) ||
				Plt.IsFailed(File.Exists(file_path))) {
				throw new Exception("NPT_ERROR_NO_SUCH_ITEM");
			}

			// check for range requests
			String range_spec = request.Headers.GetHeaderValue(HttpMessage.NPT_HTTP_HEADER_RANGE);

			// handle potential 304 only if range header not set
			DateTime  date;
			long timestamp;
			if (Plt.IsSucceded(PltUPnPMessageHelper.GetIfModifiedSince(request, out date)) && String.IsNullOrEmpty(range_spec)) {
				timestamp = date.ToFileTime ();
				Plt.LogFine(String.Format("File %s timestamps: request=%d (%s) vs file=%d (%s)", request.Url.Uri.AbsolutePath, timestamp, date.ToString(), file_info.LastWriteTime, file_info.LastWriteTime.ToString()));

					if (timestamp >= file_info.LastWriteTime.ToFileTime()) {
					// it's a match
					Plt.LogFine(String.Format("Returning 304 for %s", request.Url.Uri.LocalPath));
					response.SetStatus(304, "Not Modified", HttpMessage.NPT_HTTP_PROTOCOL_1_1);
					return true;
				}
			}

			// open file
				stream = File.OpenRead(file_path);
					if (stream==null) {
					 throw new Exception("NPT_ERROR_NO_SUCH_ITEM");
			}

			// set Last-Modified and Cache-Control headers
					
			DateTime last_modified = new DateTime(file_info.LastWriteTime.Ticks);
			response.Headers.SetHeader("Last-Modified", last_modified.ToString( DateTimeFormatInfo.CurrentInfo.RFC1123Pattern), true);
			response.Headers.SetHeader("Cache-Control", "max-age=0,must-revalidate", true);
				//response.GetHeaders().SetHeader("Cache-Control", "max-age=1800", true);


			PltHttpRequestContext tmp_context = new PltHttpRequestContext(request, context);
			return ServeStream(request, context, response, stream, PltMimeType.GetMimeType(file_path, tmp_context));
		}

		public static Boolean ServeStream(HttpRequest request, HttpRequestContext context, HttpResponse response,Stream body, String content_type)
		{
			if (body==null) return false;

			// set date
			DateTime now = DateTime.Now;
			
			response.Headers.SetHeader("Date", now.ToString( DateTimeFormatInfo.CurrentInfo.RFC1123Pattern), true);

			// get entity
			HttpEntity entity = response.Entity;
			Plt.CheckPointerSevere(entity);

			// set the content type
			entity.SetContentType(content_type);

			// check for range requests
			String range_spec = request.Headers.GetHeaderValue(HttpMessage.NPT_HTTP_HEADER_RANGE);

			// setup entity body
			Plt.CheckWarining(HttpFileRequestHandler.SetupResponseBody(response, body, range_spec));

			// set some default headers
			if (!response.Entity.GetTransferEncoding().Equals(HttpMessage.NPT_HTTP_TRANSFER_ENCODING_CHUNKED)) {
				// set but don't replace Accept-Range header only if body is seekable
				int offset = (int)body.Position;
				try{
					body.Seek(offset,SeekOrigin.Begin);
					response.Headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_ACCEPT_RANGES, "bytes", false); 
				}catch{}

			}

			// set getcontentFeatures.dlna.org
			String value = request.Headers.GetHeaderValue("getcontentFeatures.dlna.org");
			if (!String.IsNullOrEmpty(value)) {
				PltHttpRequestContext tmp_context = new PltHttpRequestContext(request, context);
				String dlna = PltProtocolInfo.GetDlnaExtension(entity.GetContentType(),tmp_context);
				if (dlna!=null) response.Headers.SetHeader("ContentFeatures.DLNA.ORG", dlna, false);
			}

			// transferMode.dlna.org
			value = request.Headers.GetHeaderValue("transferMode.dlna.org");
			if (value!=null) {
				// Interactive mode not supported?
				/*if (value->Compare("Interactive", true) == 0) {
            response.SetStatus(406, "Not Acceptable");
            return NPT_SUCCESS;
        }*/

				response.Headers.SetHeader("TransferMode.DLNA.ORG", value, false);
			} else {
				response.Headers.SetHeader("TransferMode.DLNA.ORG", "Streaming", false);
			}

			if (request.Headers.GetHeaderValue("TimeSeekRange.dlna.org")!=null) {
				response.SetStatus(406, "Not Acceptable");
				return true;
			}

			return true;
		}

		public override Boolean SetupResponse(HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
			String prefix = String.Format("PLT_HttpServer::SetupResponse {0} request from {1} for \"{2}\"", request.Method, context.RemoteAddress.ToString(),request.Url.ToString());
			PltHttp.Log(prefix, request);

			List<HttpRequestHandler> handlers = FindRequestHandlers(request);
			if (handlers.Count == 0) throw new Exception("NPT_ERROR_NO_SUCH_ITEM");

			// ask the handler to setup the response
			Boolean result = handlers[0].SetupResponse(request, context, response);

			// DLNA compliance
			PltUPnPMessageHelper.SetDate(response);
			if (request.Headers.GetHeader("Accept-Language")!=null) {
				response.Headers.SetHeader("Content-Language", "en");
			}
			return result;
		}

		protected List<HttpRequestHandler> FindRequestHandlers(HttpRequest request){
			List<HttpRequestHandler> handlers = new List<HttpRequestHandler> ();

			foreach (HandlerConfig config in  m_RequestHandlers) {
				if (config.IncludeChildren) {
					if (request.Url.Uri.AbsolutePath.StartsWith(config.Path)) {
						handlers.Add(config.Handler);
					}  
				} else {
					if (request.Url.Uri.AbsolutePath.Equals(config.Path)) {
						handlers.Insert(0, config.Handler);
					}
				}
			}
			return handlers;
		}

 


		// methods
		public virtual Boolean   Start()
		{
			Boolean res = false;
			int port = m_Port;
			// we can't start an already running server or restart an aborted server
			// because the socket is shared create a new instance
			if (m_Running || m_Aborted) throw new Exception("NPT_CHECK_WARNING(NPT_ERROR_INVALID_STATE)");

			// if we're given a port for our http server, try it
			if (m_Port!=0) {				 
				m_Server = new TcpListener (IPAddress.Any, m_Port);
				m_Server.ExclusiveAddressUse = !m_ReuseAddress;

			}

			Random randomgenerator = new Random ();
			// try random port now
			if (m_Port ==0 || Plt.IsFailed(res)) {
				int retries = 100;
				do {    
					int random = randomgenerator.Next();
					port = ( short)(1024 + (random % 1024));
					try{
						m_Server = new TcpListener (IPAddress.Any, port);
						m_Server.ExclusiveAddressUse = !m_ReuseAddress;
						break;
					}catch(Exception e){

					}
					 
				} while (--retries > 0);

				if (retries == 0) throw new Exception("NPT_FAILURE");
			}

			// keep track of port server has successfully bound
			m_Port = port;
			 
			// Tell server to try to listen to more incoming sockets
			// (this could fail silently)
			if (m_TaskManager.GetMaxTasks () > 20) {
				m_Server.Start (m_TaskManager.GetMaxTasks ());
			} else {
				m_Server.Start ();
			}

			// start a task to listen for incoming connections
			PltHttpListenTask task = new PltHttpListenTask(this, m_Server, false);
			Plt.CheckSevere(m_TaskManager.StartTask(task));

			SocketInfo info = new SocketInfo();
			info.LocalEndPoint = m_Server.LocalEndpoint;
		
			Plt.LogFine(String.Format("HttpServer listening on {0}", info.LocalEndPoint));

			m_Running = true;
			return true;
		}


		public virtual Boolean   Stop()
		{
			// we can't restart an aborted server
			if (m_Aborted || !m_Running) throw new Exception("NPT_ERROR_INVALID_STATE");

			// stop all other pending tasks 
			m_TaskManager.Abort();

			m_Running = false;
			m_Aborted = true;

			return true;
		}

		public virtual int GetPort()
					{ 
						return m_Port;
					}
	}


	class HandlerConfig
	{
		public HttpRequestHandler Handler{ get;set;}
		public String Path{get;set;}
		public Boolean IncludeChildren{ get; set;}
		public Boolean HandlerIsOwned{ get; set;}

		public HandlerConfig(HttpRequestHandler handler,
			String             path,
			bool                    include_children,
			bool                    transfer_ownership = false)
		{
			Handler = handler;
			Path = path;
			IncludeChildren = include_children;
			HandlerIsOwned = transfer_ownership;
		}

		  

	}
}

 
    