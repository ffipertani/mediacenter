﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Platinum
{
	public class Plt
	{
		public static int INVALID_PORT = 0;
		public static String PLT_PLATINUM_SDK_VERSION_STRING = "";
		public static int NPT_BUFFERED_BYTE_STREAM_DEFAULT_SIZE = 4096;

		public static Boolean ContainerFind<T> (List<T> list, IFinder<T> comparator, out T result)
		{
			result = default(T);
			foreach (T data in list) {
				if (comparator.IsFound (data)) {
					result = data;
					return true;
				}
			}
			return false;
		}

		public static Boolean Iterate<T> (List<T> list, IIterator<T> iterator)
		{
			foreach (T o in list) {
				iterator.Iterate (o);
			}
			return true;
		}

		public static void CheckSevere (Boolean res)
		{
			if (!res) {
				throw new Exception ("Check severe error");
			}
		}

		public static void Severe ()
		{
			throw new Exception ("Check severe error");
		}

		public static void CheckPointer (Object o)
		{
			if (o == null) {
				Console.Error.WriteLine ("CHECK POINTER");
			}
		}

		public static void CheckPointerSevere (Object o)
		{
			if (o == null) {
				throw new Exception ("CHECK POINTER SEVERE");
			}
		}

		public static void CheckWarining (Boolean res)
		{
			if (!res) {
				Console.Error.WriteLine ("Warning");
			}
		}

		public static Boolean IsFailed (Boolean res)
		{
			return !res;
		}

		public static Boolean IsSucceded (Boolean res)
		{
			return res;
		}

		public static void LogWarn (String message)
		{
			Console.WriteLine (message);
		}

		public static void LogFatal (String message)
		{
			Console.Error.WriteLine (message);
		}

		public static void LogFine (String message)
		{
			Console.WriteLine (message);
		}

		public static void Assert (Boolean val)
		{
			if (!val) {
				throw new Exception ("Assert");
			}
		}
	}

	public interface IFinder<T>
	{
		Boolean IsFound (T dev1);
	}


	public interface IIterator<T>
	{
		Boolean Iterate (T o);
	}

	public interface IApplier<T>
	{
		Boolean Apply (T o);
	}

	public class DisposerApplier<T>:IApplier<T>
	{
		public Boolean Apply (T o)
		{
			if (o is IDisposable) {
				((IDisposable)o).Dispose ();
			}
			return true;
		}
	}

	public class StringFinder:IFinder<String>
	{
		public String Str{ get; set; }

		public StringFinder (String str)
		{
			Str = str;
		}


		public Boolean IsFound (String str)
		{
			return Str.Equals (str);
		}
	}
		
}

