﻿using System;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace Platinum
{
	public abstract class PltHttpServerSocketTask:PltThreadTask
	{
		const String PLT_HTTP_DEFAULT_403_HTML = "<html><head><title>403 Forbidden</title></head><body><h1>Forbidden</h1><p>Access to this URL is forbidden.</p></html>";
		const String PLT_HTTP_DEFAULT_404_HTML = "<html><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL was not found on this server.</p></html>";
		const String PLT_HTTP_DEFAULT_500_HTML = "<html><head><title>500 Internal Error</title></head><body><h1>Internal Error</h1><p>The server encountered an unexpected condition which prevented it from fulfilling the request.</p></html>";

		protected Socket         m_Socket;
		protected bool           m_StayAliveForever;
		 

		public PltHttpServerSocketTask (Socket socket, bool stay_alive_forever = false)
		{
			m_Socket = socket;
			m_StayAliveForever = stay_alive_forever;

				// needed for PS3 that is some case will request data every 35 secs and 
				// won't like it if server disconnected too early
			if (m_Socket.IsBound) {
				m_Socket.ReceiveTimeout = 60000;
				m_Socket.SendTimeout = 600000;
			}
		}

		public override void Dispose()
		{
			if (m_Socket!=null) {
				m_Socket.Close();
			}
		}


		protected abstract Boolean SetupResponse(HttpRequest  request, HttpRequestContext context, HttpResponse response);

		// overridables
		protected virtual Boolean GetInputStream(out Stream stream)
		{
			stream = new NetworkStream (m_Socket);//new PltInputDatagramStream (m_Socket);
			return true;
		}

		protected virtual Boolean GetInfo(out SocketInfo info)
		{
			info = new SocketInfo ();
			try{
			info.LocalEndPoint = m_Socket.LocalEndPoint;
			}catch{
			}
			try{
			info.RemoteEndPoint = m_Socket.RemoteEndPoint;
			}catch{
			}
			return true;
		}

		// PLT_ThreadTask methods
		protected override void DoAbort() 
		{
			if (m_Socket!=null) m_Socket.Close(); 
		}

		protected override void DoRun()
		{
		//	BufferedStream buffered_input_stream;
			HttpRequestContext context;
			Boolean res = true;
			bool headers_only;
			bool keep_alive = false;

			// create a buffered input stream to parse HTTP request
			Stream inputStream;
			GetInputStream (out inputStream);
			 
			if (inputStream == null)
				return;

		//	buffered_input_stream = new BufferedStream (inputStream);

			while (!IsAborting (0)) {
				HttpRequest request = null;
				HttpResponse response = null;

				// reset keep-alive to exit task on read failure
				keep_alive = false;

				// wait for a request
				try{
					res = Read (inputStream, out request, out context);
				}catch(Exception e){
					Console.Error.WriteLine ("Connection reset");
					return;
				}
				if (Plt.IsFailed (res) || (request == null))
					return;

				// process request and setup response
				res = RespondToClient (request, context, out response);
				if (Plt.IsFailed (res) || (response == null))
					return;

				// check if client requested keep-alive
				keep_alive = PltHttpHelper.IsConnectionKeepAlive (request);
				headers_only = request.Method.Equals (HttpMessage.NPT_HTTP_METHOD_HEAD);

				// send response, pass keep-alive request from client
				// (it can be overridden if response handler did not allow it)
				res = Write (response, keep_alive, headers_only,context.RemoteAddress);

				// on write error, reset keep_alive so we can close this connection
				if (Plt.IsFailed (res))
					keep_alive = false;

				 

				if (!keep_alive && !m_StayAliveForever) {
					return;
				}
			}
		 
			return;
		}



		protected virtual Boolean Read(Stream buffered_input_stream, out HttpRequest request, out HttpRequestContext context)
		{
			SocketInfo info;

			GetInfo(out info);

			// update context with socket info if needed
			context = new HttpRequestContext ();
			 



			context.LocalAddress  = (IPEndPoint)info.LocalEndPoint;
			context.RemoteAddress = (IPEndPoint)info.RemoteEndPoint;
			 

			// put back in buffered mode to be able to parse HTTP request properly
			//	buffered_input_stream.SetLength(Plt.NPT_BUFFERED_BYTE_STREAM_DEFAULT_SIZE);

			// parse request
			Boolean res = HttpRequest.Parse(buffered_input_stream, (IPEndPoint)info.LocalEndPoint, out request);
			if (Plt.IsFailed(res) || request==null) {
				// only log if not timeout
				res = Plt.IsFailed(res)?res:false;
				//if (res != NPT_ERROR_TIMEOUT && res != NPT_ERROR_EOS) Plt.CheckWarining(res);
				return res;
			}

			// update context with socket info again 
			// to refresh the remote address in case it was a non connected udp socket
			GetInfo(out info);
			 
			context.LocalAddress = (IPEndPoint)info.LocalEndPoint;
			context.RemoteAddress = (IPEndPoint)info.RemoteEndPoint;
			 

			// return right away if no body is expected
			if (request.Method.Equals(HttpMessage.NPT_HTTP_METHOD_GET) || 
				request.Method.Equals(HttpMessage.NPT_HTTP_METHOD_HEAD)) {
				return true;
			}

			// create an entity
			HttpEntity request_entity = new HttpEntity(request.Headers);
			request.Entity = request_entity;

			MemoryStream body_stream = new MemoryStream();
			request_entity.SetInputStream(body_stream);

			// unbuffer the stream to read body fast
			//buffered_input_stream.SetLength(0);

			// check for chunked Transfer-Encoding
			if ("chunked".Equals(request_entity.GetTransferEncoding()) ) {
				buffered_input_stream = new HttpChunkedStreamWriter (buffered_input_stream);
				buffered_input_stream.CopyTo (body_stream);


				request_entity.SetTransferEncoding(null);
			} else if (request_entity.GetContentLength()>0) {
				// a request with a body must always have a content length if not chunked
				byte[] buffer = new byte[request_entity.GetContentLength ()];
				buffered_input_stream.Read (buffer, 0, buffer.Length);
				request_entity.SetInputStream(new MemoryStream(buffer));
			} else {
				request.Entity = null;
			}

			// rebuffer the stream
			//buffered_input_stream.SetBufferSize(Plt.NPT_BUFFERED_BYTE_STREAM_DEFAULT_SIZE);

			return true;
		}

		protected virtual Boolean Write(HttpResponse response, bool keep_alive, bool headers_only = false,IPEndPoint endpoint = null)
		{
			// get the socket output stream

			PltOutputDatagramStream output_stream = new PltOutputDatagramStream(m_Socket,4096,endpoint);

			// send headers
			Plt.CheckWarining(SendResponseHeaders(response, output_stream, keep_alive));

			// send the body
			if (!headers_only) {
				Plt.CheckWarining(SendResponseBody(response, output_stream));
			}

			// flush
			try{
			output_stream.Flush();
			}catch{
				Console.WriteLine ("Socket already close");
			}

			return true;
		}

		protected virtual Boolean RespondToClient(HttpRequest request, HttpRequestContext context, out HttpResponse response)
		{
			Boolean result = false;//NPT_ERROR_NO_SUCH_ITEM;

			// reset output params first
			response = null;

			// prepare the response body
			HttpEntity body = new HttpEntity();
			response = new HttpResponse(200, "OK", HttpMessage.NPT_HTTP_PROTOCOL_1_1);
			response.Entity = body;

			// ask to setup the response
			try{
				result = SetupResponse(request, context, response);
			}catch(Exception e){
				Console.WriteLine ("ATTENZIONE SISTEMARE QUESTI");
				/*
				// handle result
				if (result == NPT_ERROR_NO_SUCH_ITEM) {
					body.SetInputStream(PLT_HTTP_DEFAULT_404_HTML);
					body.SetContentType("text/html");
					response.SetStatus(404, "Not Found");
				} else if (result == NPT_ERROR_PERMISSION_DENIED) {
					body.SetInputStream(PLT_HTTP_DEFAULT_403_HTML);
					body.SetContentType("text/html");
					response.SetStatus(403, "Forbidden");
				} else if (result == NPT_ERROR_TERMINATED) {
					// mark that we want to exit
					response = null;
				} else if (Plt.IsFailed(result)) {
					body.SetInputStream(PLT_HTTP_DEFAULT_500_HTML);
					body.SetContentType("text/html");
					response.SetStatus(500, "Internal Error");
				}
				*/

			}


			return true;
		}

		protected virtual Boolean SendResponseHeaders(HttpResponse response, Stream output_stream, bool keep_alive)
		{
			// add any headers that may be missing
			HttpHeaders headers = response.Headers;

			// get the request entity to set additional headers
			Stream body_stream;
			HttpEntity entity = response.Entity;
			if (entity!=null && Plt.IsSucceded(entity.GetInputStream(out body_stream))) {
				// set the content length if known
				if (entity.ContentLengthIsKnown()) {
					headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_CONTENT_LENGTH,entity.GetContentLength().ToString());
				}

				// content type
				String content_type = entity.GetContentType();
				if (!String.IsNullOrEmpty(content_type)) {
					headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_CONTENT_TYPE, content_type);
				}

				// content encoding
				String content_encoding = entity.GetContentEncoding();
				if (!String.IsNullOrEmpty(content_encoding)) {
					headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_CONTENT_ENCODING, content_encoding);
				}

				// transfer encoding
				String entity_transfer_encoding = entity.GetTransferEncoding();
				if (!String.IsNullOrEmpty(entity_transfer_encoding)) {
					headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_TRANSFER_ENCODING, entity_transfer_encoding);
				}

			} else if (headers.GetHeader(HttpMessage.NPT_HTTP_HEADER_CONTENT_LENGTH)==null) {
				// force content length to 0 if there is no message body
				// (necessary for 1.1 or 1.0 with keep-alive connections)
				headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_CONTENT_LENGTH, "0");
			}

			String content_length  = headers.GetHeaderValue(HttpMessage.NPT_HTTP_HEADER_CONTENT_LENGTH);
			String transfer_encoding  = headers.GetHeaderValue(HttpMessage.NPT_HTTP_HEADER_TRANSFER_ENCODING);
			String connection_header  = headers.GetHeaderValue(HttpMessage.NPT_HTTP_HEADER_CONNECTION);

			if (keep_alive) {
				if (connection_header!=null && connection_header.Equals("close")) {
					keep_alive = false;
				} else {
					// the request says client supports keep-alive
					// but override if response has content-length header or
					// transfer chunked encoding
					keep_alive = content_length!=null || (transfer_encoding!=null && transfer_encoding.Equals(HttpMessage.NPT_HTTP_TRANSFER_ENCODING_CHUNKED));
				}
			}

			// only write keep-alive header for 1.1 if it's close
			String protocol = response.Protocol;
			if (protocol.Equals(HttpMessage.NPT_HTTP_PROTOCOL_1_0) || !keep_alive) {
				headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_CONNECTION, keep_alive?"keep-alive":"close", true);
			}
			headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_SERVER, PltHttp.PLT_HTTP_DEFAULT_SERVER, false); // set but don't replace

			PltHttp.Log("Sended response", response);

			// create a memory stream to buffer the headers
			MemoryStream header_stream = new MemoryStream();
			//response.Emit(header_stream);
			// send the headers
			//header_stream.CopyTo (output_stream);

			response.Emit(output_stream);




			return true;
		}

		protected virtual Boolean SendResponseBody(HttpResponse response, Stream output_stream)
		{
			HttpEntity entity = response.Entity;
			if (entity==null) return true;

			Stream body_stream;
			entity.GetInputStream(out body_stream);
			if (body_stream==null) return true;

			// check for chunked transfer encoding
			Stream dest = output_stream;
			if (HttpMessage.NPT_HTTP_TRANSFER_ENCODING_CHUNKED.Equals(entity.GetContentEncoding())) {
				dest = new HttpChunkedStreamWriter(output_stream);
			}

			// send body
			Plt.LogFine(String.Format("sending body stream, {0} bytes", entity.GetContentLength()));
			long bytes_written = 0;
			body_stream.Position = 0;
			long length = dest.Length;
			body_stream.CopyTo (dest);
			bytes_written = dest.Length-length;
			//Boolean result = NPT_StreamToStreamCopy(*body_stream, *dest, 0, entity.GetContentLength(), &bytes_written); /* passing 0 if content length is unknown will read until nothing is left */
			Boolean result = true;
			if (bytes_written!=entity.GetContentLength()) {
				Plt.LogFine(String.Format("body stream only partially sent, {0} bytes ({1})",bytes_written,	result));
				result = false;
			}

			// flush to write out any buffered data left in chunked output if used
			dest.Flush();

			// cleanup (this will send zero size chunk followed by CRLF)
			if (dest != output_stream)dest.Close();

			return result;
		}
	}


	public class PltHttpServerTask : PltHttpServerSocketTask
	{
		protected HttpRequestHandler m_Handler;

		public PltHttpServerTask(HttpRequestHandler handler, Socket socket, bool keep_alive = false) : base(socket, keep_alive)
		{
			m_Handler = handler;
		}
		protected virtual Boolean Read(Stream buffered_input_stream, out HttpRequest request, out HttpRequestContext context)
		{
			return base.Read (buffered_input_stream, out request, out context);
		}

		protected override Boolean  SetupResponse(HttpRequest request, HttpRequestContext context, HttpResponse response) {
			return m_Handler.SetupResponse(request, context, response); 
		}

		 

	}


	public class PltHttpListenTask : PltThreadTask
	{
		protected HttpRequestHandler m_Handler;
		protected TcpListener    m_Socket;
		protected bool                    m_OwnsSocket;

		public	PltHttpListenTask(HttpRequestHandler handler, TcpListener socket, bool owns_socket = true) 
	 {
			m_Handler= handler;
			m_Socket = socket;
			m_OwnsSocket = owns_socket;
		}

		public override void Dispose() { 
			if (m_OwnsSocket && m_Socket!=null) m_Socket.Stop();
		}

		 
		protected override void DoAbort()
		{
			if (m_Socket!=null) m_Socket.Stop(); 
		}

		protected override void DoRun()
		{
			while (!IsAborting(0)) {
				Socket client = null;
				client = m_Socket.AcceptSocket();
				if (client==null) {
				 
					// exit on other errors ?
					Plt.LogWarn("PLT_HttpListenTask exiting...");
					break;
				} else {
					PltThreadTask task = new PltHttpServerTask(m_Handler, client);
					m_TaskManager.StartTask(task);
				}
			}
		}

	
	}
} 
    
 