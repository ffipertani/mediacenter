﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Net.Sockets;
using System.Net;

namespace Platinum
{

	/**
 	The PLT_UPnP class maintains a list of devices (PLT_DeviceHost) to advertise and/or 
	 control points (PLT_CtrlPoint).
 	*/
	public class PltUPnP
	{
		public const float PLT_DLNA_SSDP_DELAY = 0.05f;
		public const float PLT_DLNA_SSDP_DELAY_GROUP = 0.2f;

		 
		private List<PltDeviceHost> devices = new List<PltDeviceHost> ();
		private List<PltCtrlPoint> ctrlPoints = new List<PltCtrlPoint> ();
		private PltTaskManager taskManager;
		private Object m_Lock = new Object ();

		// Since we can only have one socket listening on port 1900,
		// we create it in here and we will attach every control points
		// and devices to it when they're added
		private bool started;
		private PltSsdpListenTask ssdpListenTask;
		private bool ignoreLocalUUIDs;

		public PltUPnP ()
		{
			taskManager = null;
			started = false;
			ssdpListenTask = null;
			ignoreLocalUUIDs = true;

		}

		public void Dispose ()
		{
			Stop ();

			ctrlPoints.Clear ();
			devices.Clear ();
		}

		 
		public Boolean Start ()
		{
			lock (m_Lock) {
				Console.WriteLine ("Starting UPnP...");
			 
				
				if (started) {
					return false;
				}

				/* Create multicast socket and bind on 1900. If other apps didn't
       play nicely by setting the REUSE_ADDR flag, this could fail */

				IPEndPoint local = new IPEndPoint (IPAddress.Any, 1900);

				UdpClient udpclient = new UdpClient ();
				udpclient = new UdpClient (local.AddressFamily);
				try {
					udpclient.ExclusiveAddressUse = false;
				} catch (SocketException) {
				}
				try {
					udpclient.Client.SetSocketOption (SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
				} catch (SocketException) {
				}
				try {
					udpclient.Client.SetSocketOption (SocketOptionLevel.Socket, SocketOptionName.ExclusiveAddressUse, false);
				} catch (SocketException) {
				}
				try {
					udpclient.EnableBroadcast = true;
				} catch (SocketException) {
				}
				 
					if (local.AddressFamily == AddressFamily.InterNetwork)
						udpclient.Client.Bind (new IPEndPoint (IPAddress.Any, local.Port));
					if (local.AddressFamily == AddressFamily.InterNetworkV6)
						udpclient.Client.Bind (new IPEndPoint (IPAddress.IPv6Any, local.Port));
				 
				if (local.AddressFamily == AddressFamily.InterNetwork)
					udpclient.JoinMulticastGroup (IPAddress.Parse ("239.255.255.250"), ((IPEndPoint)udpclient.Client.LocalEndPoint).Address);
				if (local.AddressFamily == AddressFamily.InterNetworkV6) {
					if (local.Address.IsIPv6LinkLocal)
						udpclient.JoinMulticastGroup ((int)local.Address.ScopeId, IPAddress.Parse ("FF02::C"));
					else
						udpclient.JoinMulticastGroup ((int)local.Address.ScopeId, IPAddress.Parse ("FF05::C"));
				}
				//udpclient.BeginReceive (new AsyncCallback (OnReceiveSink), local);



				Socket client = new Socket (AddressFamily.InterNetwork, SocketType.Dgram,	ProtocolType.Udp);
				client.ExclusiveAddressUse = false;
				//	client.Bind (new IPEndPoint (IPAddress.Any,1900));
				client.Bind (new IPEndPoint (IPAddress.Any, 0));



				PltSsdpInitMulticastIterator applier = new PltSsdpInitMulticastIterator (client);

				foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces()) {
					if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet) {
						foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses) {
							if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
								Console.WriteLine (ip.Address.ToString ());

								 
								if (applier.Apply (ip.Address)) {
									break;
								}
								 
							}
						}
					}  
				}
				 
				/* create the ssdp listener */
				ssdpListenTask = new PltSsdpListenTask (udpclient.Client);

				taskManager = new PltTaskManager ();
				taskManager.StartTask (ssdpListenTask);
				 
				foreach (PltDeviceHost dh in devices) {
					dh.BootId = dh.GenerateNextBootId ();
					dh.NextBootId = 0;
					dh.Start (ssdpListenTask);
				}
				foreach (PltCtrlPoint cp in ctrlPoints) {
					cp.Start (ssdpListenTask);
				}
				 
				started = true;
				return true;
			}
		}

		public Boolean Stop ()
		{
			lock (m_Lock) {
				if (!started) {
					return false;
				}

				Console.WriteLine ("Stopping UPnP...");

				foreach (PltDeviceHost dh in devices) {
					dh.Stop (ssdpListenTask);
				}
				foreach (PltCtrlPoint cp in ctrlPoints) {
					cp.Stop (ssdpListenTask);
				}
				
				// stop remaining tasks
				taskManager.Abort ();
				ssdpListenTask = null;
				taskManager = null;

				started = false;
				return true;
			}
		}

		 
		public Boolean AddDevice (PltDeviceHost device)
		{
			lock (m_Lock) {
				// tell all our controllers to ignore this device
				if (ignoreLocalUUIDs) {
					foreach (PltCtrlPoint cp in ctrlPoints) {
						cp.IgnoreUUID (device.UUID);
					}					
				}

				if (started) {
					Plt.LogFine ("Starting Device...");
					device.Start (ssdpListenTask);
				}

				devices.Add (device);
				return true;
			}
		}

		 
		public Boolean RemoveDevice (PltDeviceHost device)
		{
			lock (m_Lock) {
				if (started) {
					device.Stop (ssdpListenTask);
				}

				return devices.Remove (device);
			}
		}

		 
		public Boolean AddCtrlPoint (PltCtrlPoint ctrl_point)
		{
			lock (m_Lock) {
				// tell the control point to ignore our own running devices
				if (ignoreLocalUUIDs) {
					foreach (PltDeviceHost device in devices) {
						ctrl_point.IgnoreUUID (device.UUID);
					}		
				}

				if (started) {
					Console.WriteLine ("Starting Ctrlpoint...");
					ctrl_point.Start (ssdpListenTask);
				}

				ctrlPoints.Add (ctrl_point);
				return true;
			}
		}

		 
		public Boolean RemoveCtrlPoint (PltCtrlPoint ctrl_point)
		{
			lock (m_Lock) {
				if (started) {
					ctrl_point.Stop (ssdpListenTask);
				}

				return ctrlPoints.Remove (ctrl_point);
			}
		}


	}

	 
  
	 
}

