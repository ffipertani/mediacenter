﻿using System;

namespace Platinum
{
	public class HttpUri
	{
		public static String SCHEME_ID_HTTP = "http";
		public static String SCHEME_ID_HTTPS = "https";

		UriBuilder builder;


		public HttpUri (Uri uri)
		{
			builder = new UriBuilder(uri);			 
		}

		public HttpUri (String host, int  port, String path, String query = null, String fragment = null)
		{
			if (host == null) {
				host = "localhost";
			}
			builder = new UriBuilder("http",host,port,path,query);
			builder.Fragment = fragment;
		}

		public HttpUri(String url, bool ignore_scheme = false){
			if (String.IsNullOrEmpty(url)) {
				builder = new UriBuilder ();
			}else {
				
				if (!url.StartsWith ("http") && !url.StartsWith ("https")) {
					url = "http://localhost" + url;
				}
				builder = new UriBuilder (url);
			}
			if (!ignore_scheme) {
				if (!builder.Scheme.Equals(SCHEME_ID_HTTP) && !builder.Scheme.Equals(SCHEME_ID_HTTPS) ){
					builder = new UriBuilder ();
				}
			}

		}

		public Boolean IsValid()
		{
			if (Uri.Scheme.Equals ("http") || Uri.Scheme.Equals ("https")) {
				return true;
			}
			return false;
		}

		public Uri Uri
		{
			get{
				return builder.Uri;
			}
		}

		public override String ToString()
		{
			return builder.Uri.ToString ();
		}
	}
}

