﻿using System;
using System.IO;

namespace Platinum
{
	public class HttpEntity
	{
		static long NPT_INPUT_STREAM_LOAD_MAX_SIZE = 0x40000000;
		// 1GB
		static int NPT_INPUT_STREAM_LOAD_DEFAULT_READ_CHUNK = 4096;

		private Stream m_InputStream;
		private long m_ContentLength;
		private String m_ContentType;
		private String m_ContentEncoding;
		private String m_TransferEncoding;
		private Boolean m_ContentLengthIsKnown;

		private HttpHeaders headers;

		public HttpEntity ()
		{
			m_ContentLength = 0;
			m_ContentLengthIsKnown = false;
		}


		public HttpEntity (HttpHeaders headers) : this ()
		{
			SetHeaders (headers);
		}

		public Boolean SetInputStream (Stream stream, bool update_content_length = false)
		{
			m_InputStream = stream;

			// get the content length from the stream
			if (update_content_length && stream != null) {
				long length = stream.Length; 
				return SetContentLength (length);				 
			}
			return true;
		}

		public Boolean SetInputStream (byte[] data, long size)
		{
			MemoryStream memory_stream = new MemoryStream (data, 0, (int)size);
			return SetInputStream (memory_stream, true);
		}

		public Boolean SetInputStream (String str)
		{
			if (str == null)
				throw new Exception ("NPT_ERROR_INVALID_PARAMETERS");
			byte[] buffer = System.Text.Encoding.UTF8.GetBytes (str);
			MemoryStream memory_stream = new MemoryStream (buffer, 0, buffer.Length);
			return SetInputStream (memory_stream, true);
		}

		public Boolean GetInputStream (out Stream stream)
		{
			// reset output params first
			stream = null;

			if (m_InputStream == null)
				return false;

			stream = m_InputStream;
			return true;
		}

		public Boolean Load (out byte[] buffer)
		{
			// check that we have an input stream
			if (m_InputStream == null)
				throw new Exception ("NPT_ERROR_INVALID_STATE");

			// load the stream into the buffer
			if (m_ContentLength != (UInt32)m_ContentLength)
				throw new Exception ("NPT_ERROR_OUT_OF_RANGE");
			ReadAll (m_InputStream, out buffer, m_ContentLength);
			return true;
		}

		private Boolean ReadAll (Stream stream, out byte[] buffer, long max_read = 0)
		{
			Boolean result;
			long total_bytes_read;



			// check the limits
			if (max_read > NPT_INPUT_STREAM_LOAD_MAX_SIZE) {
				throw new Exception ("NPT_ERROR_INVALID_PARAMETERS");
			}

			// try to get the stream size
			long size = stream.Length;
			if (size > 0) { 
				// make sure we don't read more than max_read
				if (max_read > 0 && max_read < size)
					size = max_read;
				if (size > NPT_INPUT_STREAM_LOAD_MAX_SIZE) {
					throw new Exception ("NPT_ERROR_OUT_OF_RANGE");
				}
			} else {
				size = max_read;
			} 

			// pre-allocate the buffer
			if (size != 0)
				buffer = new byte[size];

			// read the data from the file
			total_bytes_read = 0;
			do {
				long available = 0;
				long bytes_to_read;
				int bytes_read;
				byte[] data;

				// check if we know how much data is available
				available = stream.Length - stream.Position;
				if (available > 0) {
					// we know how much is available
					bytes_to_read = available;
				} else {
					bytes_to_read = NPT_INPUT_STREAM_LOAD_DEFAULT_READ_CHUNK;
				}

				// make sure we don't read more than what was asked
				if (size != 0 && total_bytes_read + bytes_to_read > size) {
					bytes_to_read = size - total_bytes_read;
				}

				// stop if we've read everything
				if (bytes_to_read == 0)
					break;

				// ensure that the buffer has enough space
				if (total_bytes_read + bytes_to_read > NPT_INPUT_STREAM_LOAD_MAX_SIZE) {
					buffer = null;
					throw new Exception ("NPT_ERROR_OUT_OF_RANGE");
				}
				buffer = new byte[bytes_to_read];
				bytes_read = stream.Read (buffer, (int)total_bytes_read, (int)bytes_to_read);
				if (bytes_read != 0) {
					total_bytes_read += bytes_read;
				}
			} while(size == 0 || total_bytes_read < size);

			return true;
		}


		public Boolean SetHeaders (HttpHeaders headers)
		{
			HttpHeader header;

			// Content-Length
			header = headers.GetHeader (HttpMessage.NPT_HTTP_HEADER_CONTENT_LENGTH);
			if (header != null) {
				m_ContentLengthIsKnown = true;
				long length;
				if (Plt.IsSucceded (long.TryParse (header.Value, out length))) {
					m_ContentLength = length;
				} else {
					m_ContentLength = 0;
				}
			}

			// Content-Type
			header = headers.GetHeader (HttpMessage.NPT_HTTP_HEADER_CONTENT_TYPE);
			if (header != null) {
				m_ContentType = header.Value;
			}

			// Content-Encoding
			header = headers.GetHeader (HttpMessage.NPT_HTTP_HEADER_CONTENT_ENCODING);
			if (header != null) {
				m_ContentEncoding = header.Value;
			}

			// Transfer-Encoding
			header = headers.GetHeader (HttpMessage.NPT_HTTP_HEADER_TRANSFER_ENCODING);
			if (header != null) {
				m_TransferEncoding = header.Value;
			}

			return true;
		}

		 
		public Boolean SetContentLength (long length)
		{
			m_ContentLength = length;
			m_ContentLengthIsKnown = true;
			return true;
		}


		public Boolean SetContentType (String type)
		{
			m_ContentType = type;
			return true;
		}

		public Boolean SetContentEncoding (String encoding)
		{
			m_ContentEncoding = encoding;
			return true;
		}

		public Boolean SetTransferEncoding (String encoding)
		{
			m_TransferEncoding = encoding;
			return true;
		}

		public long GetContentLength ()
		{
			return m_ContentLength;
		}

		public String GetContentType ()
		{
			return m_ContentType;
		}

		public String GetContentEncoding ()
		{
			return m_ContentEncoding;
		}

		public String GetTransferEncoding ()
		{
			return m_TransferEncoding;
		}

		public Boolean ContentLengthIsKnown ()
		{
			return m_ContentLengthIsKnown;
		}
		 

	}
}

