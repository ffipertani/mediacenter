﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Collections.Generic;
using System.Collections;
using System.Threading;

namespace Platinum
{
	public class HttpConnectionManager
	{

		public static int NPT_HTTP_CONNECTION_MANAGER_MAX_CONNECTION_POOL_SIZE = 5;
		public static int NPT_HTTP_CONNECTION_MANAGER_MAX_CONNECTION_AGE = 50;
		// seconds
		public static int NPT_HTTP_MAX_RECONNECTS = 10;
		public static int NPT_HTTP_MAX_100_RESPONSES = 10;

		static HttpConnectionManager Instance;
		static Object mutex = new object ();

		// members
		Object m_Lock = new Object ();
		int m_MaxConnections;
		int m_MaxConnectionAge;
		SharedVariable m_Aborted = new SharedVariable(0);
		List<Connection> m_Connections = new List<Connection>();
		Dictionary<HttpClient, List<Connection>> m_ClientConnections = new Dictionary<HttpClient,List<Connection>>();
		Thread thread;



		public HttpConnectionManager ()
		{
			m_MaxConnections = NPT_HTTP_CONNECTION_MANAGER_MAX_CONNECTION_POOL_SIZE;
			m_MaxConnectionAge = NPT_HTTP_CONNECTION_MANAGER_MAX_CONNECTION_AGE;
			thread = new Thread (new ThreadStart (Run));
		}
		public void Start()
		{
			thread.Start ();
		}


		// singleton management
		public static HttpConnectionManager GetInstance ()
		{
			if (Instance!=null)
				return Instance;
			lock (mutex) {
				if (Instance == null) {
					// create the shared instance
					Instance = new HttpConnectionManager ();
					// Start shared instance
					Instance.Start ();
				}
			}
		 


			return Instance;

		}
		 

		// methods
		Connection FindConnection (EndPoint address)
		{

			lock (m_Lock) {
				Cleanup ();
				for (int i = 0; i < m_Connections.Count; i++) {
					Connection connection = m_Connections [i];
					SocketInfo info;
					if (Plt.IsFailed (connection.GetInfo (out info)))
						continue;

					if (info.RemoteEndPoint.Equals (address)) {
						m_Connections.RemoveAt (i);
						return connection;
					}
				}
			 
				// not found
				return null;
			}

		}

		public Boolean  Recycle (Connection connection)
		{

			lock (m_Lock) {
				Cleanup ();

				// remove older connections to make room
				while (m_Connections.Count >= m_MaxConnections) {
					if (m_Connections.Count > 0) {
						m_Connections.RemoveAt (0);
					}
					Plt.LogFine ("removing connection from pool to make some room");
				}

				if (connection != null) {
					// Untrack connection
					UntrackConnection (connection);

					// label this connection with the current timestamp and flag
					connection.m_TimeStamp = DateTime.Now;
					connection.m_IsRecycled = true;

					// add the connection to the pool
					m_Connections.Add (connection);
				}

				return true;
			}
		}

		public Boolean  Track (HttpClient client, Connection connection)
		{
			lock (m_Lock) {

				// look if already tracking client connections
				List<Connection> connections;
				if (Plt.IsSucceded (m_ClientConnections.TryGetValue(client, out connections))) {
					// return immediately if connection is already associated with client
					if (connections.Contains (connection)) {
						Plt.LogWarn ("Connection already associated to client.");
						return true;
					}
					connections.Add (connection);
					return true;
				}

				// new client connections
				List<Connection> new_connections = new List<Connection>();

				// add connection to new client connection list
				new_connections.Add (connection);

				// track new client connections
				m_ClientConnections.Add (client, new_connections);
				return true;
			}
		}

		public Boolean  AbortConnections (HttpClient client)
		{
			lock (m_Lock) {

				List<Connection> connections;
				if (Plt.IsSucceded (m_ClientConnections.TryGetValue (client, out connections))) {
					foreach (BaseConnection con in connections) {
						con.Abort ();
					}
				}
				return true;
			}
		}

		// class methods
		public static Boolean  Untrack (Connection connection)
		{
			// check first if ConnectionCanceller Instance has not been released already
			// with static finalizers
			if (Instance == null) return false;

			return GetInstance().UntrackConnection(connection);
		}

			
		 

	 
		 
		// NPT_Thread methods
		void Run ()
		{
			// try to cleanup every 5 secs
			while (m_Aborted.WaitUntilEquals (1, TimeSpan.FromMilliseconds( 5000)) /*== NPT_ERROR_TIMEOUT*/) {
				lock (m_Lock) {
					Cleanup ();
				}
			}

		}

		// methods
		public Boolean UntrackConnection (Connection connection)
		{
			lock (m_Lock) {
			 
				foreach (HttpClient client in m_ClientConnections.Keys) {				 				
					List<Connection> connections;

					if (Plt.IsFailed (m_ClientConnections.TryGetValue (client, out connections))) {
						m_ClientConnections.Remove (client);
						continue;
					}

					// look for connection in client connection list
					connections.Remove (connection);


					// untrack client if no more active connections for it
					if (connections.Count == 0) {
						m_ClientConnections.Remove (client);
					}

					return true;
				}

			}

			return false;//NPT_ERROR_NO_SUCH_ITEM;
		}

		public Boolean Cleanup ()
		{
			DateTime now = DateTime.Now;

			for (int i = 0; i < m_Connections.Count; i++) {
				Connection con = m_Connections [i];
				if (now.CompareTo (con.m_TimeStamp.Add (TimeSpan.FromSeconds (m_MaxConnectionAge)) )< 0)
					break;
				Plt.LogFine (String.Format ("cleaning up connection (%d remain)", m_Connections.Count ));
				m_Connections.Remove (con);
				i--;
			}
			 
			return true;

		}

	}

	public class Connection : BaseConnection
	{
		// members
		public HttpConnectionManager m_Manager;
		public Boolean m_IsRecycled;
		public DateTime m_TimeStamp;
		public Socket m_Socket;
		public Stream m_InputStream;
		public Stream m_OutputStream;

		public Connection (HttpConnectionManager manager, Socket socket, Stream input_stream, Stream output_stream)
		{
			m_Manager = manager;
			m_IsRecycled = false;
			m_Socket = socket;
			m_InputStream = input_stream;
			m_OutputStream = output_stream;
		}
		 

		// NPT_HttpClient::Connection methods
		public override Stream  GetInputStream ()
		{
			return m_InputStream;           
		}

		public override Stream GetOutputStream ()
		{
			return m_OutputStream;          
		}

		public override Boolean GetInfo (out SocketInfo info)
		{
			info = new SocketInfo ();
			info.LocalEndPoint = m_Socket.LocalEndPoint;
			info.RemoteEndPoint = m_Socket.RemoteEndPoint;
			return true; 
		}

		public override Boolean SupportsPersistence ()
		{
			return true;                    
		}

		public override Boolean IsRecycled ()
		{
			return m_IsRecycled;            
		}

		public override Boolean Recycle ()
		{
			return m_Manager.Recycle(this);
		}

		public override  Boolean Abort ()
		{
			 m_Socket.Close (); 
			return true;
		}


	};

}


 