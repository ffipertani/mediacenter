﻿using System;
using System.Net.Sockets;
using System.Net;
using System.IO;


namespace Platinum
{
	public class HttpRequest:HttpMessage
	{
		public String Method{ get; set; }

		public HttpUri Url{ get; set; }

		public HttpRequest (String url, String method, String  protocol) : this (new HttpUri (url),method,protocol)
		{		 
		}

		public HttpRequest (HttpUri url, String method, String  protocol) : base (protocol)
		{
			Url = url;
			Method = method;
		}

		 

		public override String ToString ()
		{
			return String.Format ("{0} {1} {2}", Method, Url.Uri.Fragment, Protocol);
		}

	
		public static Boolean Parse (Stream stream, IPEndPoint endpoint, out HttpRequest request)
		{
			// default return value
			request = null;
			int countEmptyLines = 5;
		//	StreamReader reader = new StreamReader (stream);
			String line="";
			Boolean skip_first_empty_line = false;
			while (!skip_first_empty_line) {
				// read the request line

				line = ReadLine (stream);
				//NPT_CHECK_FINER(stream.ReadLine(line, NPT_HTTP_PROTOCOL_MAX_LINE_LENGTH));
				Plt.LogFine (String.Format ("http request: {0}", line));
				// when using keep-alive connections, clients such as XBox 360
				// incorrectly send a few empty lines as body for GET requests
				// so we try to skip them until we find something to parse
				if (countEmptyLines-- == 0) {
					Console.Error.WriteLine ("Errore in parse request, empty lines");
					return false;
				}
				if (line!=null && line.Length > 0)
					break;
			}

			// check the request line
			int first_space = line.IndexOf (' ');
			if (first_space < 0) {
				Plt.LogFine (String.Format ("http request: {0}", line));
				throw new Exception ("NPT_ERROR_HTTP_INVALID_REQUEST_LINE");
			}
			int second_space = line.Substring (first_space + 1).IndexOf (' ');
			if (second_space < 0) {
				Plt.LogFine (String.Format ("http request: {0}", line));
				throw new Exception ("NPT_ERROR_HTTP_INVALID_REQUEST_LINE");
			}

			// parse the request line
			String method = line.Substring (0, first_space);
			String uri = line.Substring (first_space + 1, second_space );
			String protocol = line.Substring (first_space + second_space + 2);
			if (uri.Equals ("*")) {
				uri = "/";
			}
			// create a request
			bool proxy_style_request = true;
			if (uri.StartsWith ("http://")) {
				// proxy-style request with absolute URI
				request = new HttpRequest (uri, method, protocol);
				proxy_style_request = true;
			} else {
				// normal absolute path request
				request = new HttpRequest ("http://"+endpoint.Address+uri, method, protocol);
			}

			// parse headers
			Boolean result = request.ParseHeaders (stream);
			if (Plt.IsFailed (result)) {				 
				request = null;
				return result;
			}

			// update the URL
			if (!proxy_style_request) {
				UriBuilder builder = new UriBuilder (request.Url.Uri);
				builder.Scheme = "http";
				builder.Path = new Uri (uri).AbsoluteUri;
				builder.Port = HttpMessage.NPT_URL_DEFAULT_HTTP_PORT;


				// check for a Host: header
				 
				HttpHeader host_header = request.Headers.GetHeader (HttpMessage.NPT_HTTP_HEADER_HOST);
				if (host_header != null) {
					builder.Host = host_header.Value;

					// host sometimes doesn't contain port
					if (endpoint != null) {
						builder.Port = endpoint.Port;
					}
				} else {
					// use the endpoint as the host
					if (endpoint != null) {
						builder.Host = endpoint.ToString ();
					} else {
						// use defaults
						builder.Host = "localhost";
					}
				}
				request.Url = new HttpUri (builder.Uri.ToString ());
			}

			return true;
		}

		 
		public Boolean Emit (Stream stream, Boolean use_proxy = false)
		{
			 
			StreamWriter writer = new StreamWriter (stream);
			 

			// write the request line
			writer.Write (Method);
			writer.Write (" ");
			if (use_proxy) {
				writer.Write (Url.Uri.AbsoluteUri);
			} else {
				if (Url.Uri.AbsolutePath.Equals ("/*")) {
					writer.Write ("*");
				} else {
					writer.Write (Url.Uri.AbsolutePath);
				}

			}
			writer.Write (" ", 1);
			writer.Write (Protocol);
			writer.Write (HttpMessage.NPT_HTTP_LINE_TERMINATOR);
			writer.Flush ();
			// emit headers
			Headers.Emit (stream);

			// finish with an empty line
			writer.Write (HttpMessage.NPT_HTTP_LINE_TERMINATOR);
			writer.Flush ();
			 
			 
			return true;
		}

	}

	public class HttpRequestContext
	{
		public HttpRequestContext(){}
		public HttpRequestContext(IPEndPoint localAddres, IPEndPoint remoteAddress)
		{
			LocalAddress = localAddres;
			RemoteAddress = remoteAddress;
		}

		public IPEndPoint LocalAddress{ get; set; }

		public IPEndPoint RemoteAddress{ get; set; }

		 
	}
}



