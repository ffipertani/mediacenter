﻿using System;
using System.IO;

namespace Platinum
{
	public class HttpHeader
	{
		public String Name{ get; set;}
		public String Value{get; set;}

		public HttpHeader (String name, String value)
		{
			Name = name;
			Value = value;
		}

		public Boolean Emit(Stream stream)
		{
			StreamWriter writer = new StreamWriter (stream);

			writer.Write(Name);
			writer.Write(": ");
			writer.Write(Value);
			writer.Write(HttpMessage.NPT_HTTP_LINE_TERMINATOR);
			writer.Flush ();
			Plt.LogFine(String.Format("header {0}: {1}", Name, Value));

			return true;
		}
	}
}

