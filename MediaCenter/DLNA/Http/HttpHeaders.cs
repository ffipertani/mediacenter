﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Platinum
{
	public class HttpHeaders
	{
		private List<HttpHeader> headers = new List<HttpHeader> ();

		public HttpHeaders ()
		{
		}


		public Boolean Apply (IApplier<HttpHeader> applier)
		{
			headers.Apply (applier);
			return true;
		}

		private String readLine(Stream stream){
			byte[] buffer = new byte[9000];
			String line=null;
			int index = 0;
			while (true) {
				int res = stream.Read (buffer, index, 1);

				if (index>0 && ((char)buffer [index - 1]) == '\r' && (((char)buffer [index]) == '\n')) {
					line = System.Text.Encoding.UTF8.GetString (buffer, 0, index);
					line = line.Replace("\r","");
					break;
				}
				index++;
				if (res == 0) {
					break;
				}
			}
			return line;
		}

		public Boolean Parse (Stream stream)
		{
			String header_name = null;
			String header_value = "";
			bool header_pending = false;
			String line;
			StreamReader reader = new StreamReader (stream);
			 
			do {
				try {
					line = readLine(stream);
					if (line == null || line.Length == 0 || line.Length > 8192) {
						// empty line, end of headers
						break;
					}
					if (header_pending && (line [0] == ' ' || line [0] == '\t')) {
						// continuation (folded header)
						header_value += line.Substring (1);
					} else {
						// add the pending header to the list
						if (header_pending) {
							header_value = header_value.Trim ();
							AddHeader (header_name, header_value);
							header_pending = false;
							Plt.LogFine (String.Format ("header - {0}: {1}", header_name, header_value));
						}

						// find the colon separating the name and the value
						int colon_index = line.IndexOf (':');
						if (colon_index < 1) {
							// invalid syntax, ignore
							continue;
						}
						header_name = line.Substring (0, colon_index);

						// the field value starts at the first non-whitespace
						String value = line.Substring (colon_index + 1);
						while (value.StartsWith (" ") || value.StartsWith ("\t")) {
							value = value.Substring (1);
						}
						header_value = value;

						// the header is pending
						header_pending = true;
					}
				} catch (Exception e) {
					Console.Error.WriteLine (e);
					return false;
				}
			} while(true);

			// if we have a header pending, add it now
			if (header_pending) {
				header_value.Trim ();
				AddHeader (header_name, header_value);
				Plt.LogFine (String.Format ("header {0}: {1}", header_name,	header_value));
			}

			return true;
		}


		public HttpHeader GetHeader (String name)
		{
			foreach (HttpHeader header in headers) {
				if (header.Name.EqualsIgnoreCase (name))
					return header;
			}
			return null;
		}

		public void AddHeader (String name, String value)
		{
			headers.Add (new HttpHeader (name, value));
		}

		public void Remove (String name)
		{
			for (int i = 0; i < headers.Count; i++) {
				HttpHeader header = headers [i];
				if (header.Name.Equals (name)) {
					headers.Remove (header);			 
				}
			}
		}

		public void SetHeader (String name, String value, Boolean replace = true)
		{
			HttpHeader header = GetHeader (name);
			if (header == null) {
				AddHeader (name, value);
			} else if (replace) {
				header.Value = value; 
			}  
		}

		public String GetHeaderValue (String name)
		{
			HttpHeader header = GetHeader (name);				
			if (header == null) {
				return null;
			} else {
				return header.Value;
			}
		}

		public Boolean Emit (Stream stream)
		{
			foreach (HttpHeader header in headers) {
				Plt.CheckWarining (header.Emit (stream));
			}
			return true;
		}
	}
}

