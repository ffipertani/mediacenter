﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace Platinum
{
	public class HttpClient
	{

		static int NPT_HTTP_MAX_100_RESPONSES = 10;
		static int NPT_ERROR_BASE = -20000;
		static int NPT_ERROR_BASE_HTTP = NPT_ERROR_BASE - 800;
		static int NPT_ERROR_HTTP_TOO_MANY_RECONNECTS = NPT_ERROR_BASE_HTTP - 6;

		// members
		Config                 m_Config = new Config();
		HttpProxySelector m_ProxySelector;
		bool                   m_ProxySelectorIsOwned;
		Connector m_Connector = new Connector ();
		bool                   m_ConnectorIsOwned;
		Object              m_AbortLock = new Object();
		bool                   m_Aborted;

		// class methods
		/*
		static Boolean WriteRequest(Stream output_stream, HttpRequest request, bool should_persist,	bool use_proxy = false)
		{
			Boolean result = true;

			// add any headers that may be missing
			HttpHeaders headers = request.Headers;

			if (!should_persist) {
				headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_CONNECTION, "close", false); // set but don't replace
			}

			String host = request.Url.UriHost;
			int default_port = 0;
			if (request.Url.Uri.Scheme.Equals ("http")) {
				default_port = HttpMessage.NPT_HTTP_DEFAULT_PORT;
			} else if (request.Url.Uri.Scheme.Equals ("https")) {
				default_port = HttpMessage.NPT_HTTPS_DEFAULT_PORT;
			}
			 
			if (request.Url.Uri.Port != default_port) {
				host += ":";
				host += request.Url.Uri.Port;
			}
			headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_HOST, host, false); // set but don't replace

			// get the request entity to set additional headers
			Stream body_stream;
			HttpEntity entity = request.Entity;
			if (entity!=null && Plt.IsSucceded(entity.GetInputStream(out body_stream))) {
				// set the content length if known
				if (entity.ContentLengthIsKnown()) {
					headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_CONTENT_LENGTH, entity.GetContentLength().ToString());
				}

				// content type
				String content_type = entity.GetContentType();
				if (!String.IsNullOrEmpty(content_type)) {
					headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_CONTENT_TYPE, content_type);
				}

				// content encoding
				String content_encoding = entity.GetContentEncoding();
				if (!String.IsNullOrEmpty(content_encoding)) {
					headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_CONTENT_ENCODING, content_encoding);
				}

				// transfer encoding
				String transfer_encoding = entity.GetTransferEncoding();
				if (!String.IsNullOrEmpty(transfer_encoding)) {
					headers.SetHeader(HttpMessage.NPT_HTTP_HEADER_TRANSFER_ENCODING, transfer_encoding);
				}
			} else {
				//FIXME: We should only set content length of 0 for methods with expected entities.
				//headers.SetHeader(NPT_HTTP_HEADER_CONTENT_LENGTH, "0");
			}

			// create a memory stream to buffer the headers
			MemoryStream header_stream = new MemoryStream();

			// emit the request headers into the header buffer
			request.Emit(header_stream, use_proxy && request.Url.Uri.Scheme.Equals("http"));

			// send the headers
			output_stream.CopyTo(header_stream);
		 

			// send request body 
			if (entity && body_stream!=null) {
				// check for chunked transfer encoding
				Stream dest = output_stream;
				if (entity.GetTransferEncoding().Equals(HttpMessage.NPT_HTTP_TRANSFER_ENCODING_CHUNKED)) {
					dest = new HttpChunkedStreamWriter(output_stream);
				}

				Plt.LogFine(String.Format("sending body stream, %lld bytes", entity.GetContentLength())); //FIXME: Would be 0 for chunked encoding
				long bytes_written = 0;

				// content length = 0 means copy until input returns EOS
				body_stream.CopyTo(dest,entity.GetContentLength());



				// flush to write out any buffered data left in chunked output if used
				dest.Flush();  

				// cleanup (this will send zero size chunk followed by CRLF)
				if (dest != output_stream) dest.Close();
			}

			// flush the output stream so that everything is sent to the server
			output_stream.Flush();

			return result;

		}
*/
		/*
		static Boolean ReadResponse(Stream input_stream, bool should_persist, bool expect_entity, out HttpResponse response, Connection cref )
		{
			Boolean result;

			// setup default values
			response = null;

			// create a buffered stream for this socket stream
			BufferedStream buffered_input_stream = BufferedStream (input_stream);

			// parse the response
			for (int watchcat = 0; watchcat < HttpMessage.NPT_HTTP_MAX_100_RESPONSES; watchcat++) {
				// parse the response
				result = HttpResponse.Parse(buffered_input_stream, response);
				Plt.CheckWarining(result);

				if (response.GetStatusCode() >= 100 && response.GetStatusCode() < 200) {
					Plt.LogFine(String.Format("got %d response, continuing", response.GetStatusCode()));					 
					response = null;
					continue;
				}
				Plt.LogFine(String.Format("got response, code=%d, msg=%s", response.GetStatusCode(), response.GetReasonPhrase()));
				break;
			}

			// check that we have a valid response
			if (response == null) {
				Plt.LogFine("failed after max continuation attempts");
				return false;//NPT_ERROR_HTTP_TOO_MANY_RECONNECTS;
			}

			// unbuffer the stream
			buffered_input_stream.SetLength (0);

			// decide if we should still try to reuse this connection later on
			if (should_persist) {
				String connection_header = response.Headers.GetHeaderValue(HttpMessage.NPT_HTTP_HEADER_CONNECTION);
				if (response.Protocol.Equals(HttpMessage.NPT_HTTP_PROTOCOL_1_1)) {
					if (connection_header!=null && connection_header.Equals("close")) {
						should_persist = false;
					}
				} else {
					if (connection_header==null || !connection_header.Equals("keep-alive")) {
						should_persist = false;
					}
				}
			}

			// create an entity if one is expected in the response
			if (expect_entity) {
				HttpEntity response_entity = new HttpEntity(response.Headers);

				// check if the content length is known
				bool have_content_length = (response.Headers.GetHeaderValue(HttpMessage.NPT_HTTP_HEADER_CONTENT_LENGTH) != null);

				// check for chunked Transfer-Encoding
				bool chunked = false;
				if (response_entity.GetTransferEncoding().Equals(HttpMessage.NPT_HTTP_TRANSFER_ENCODING_CHUNKED)) {
					chunked = true;
					response_entity.SetTransferEncoding(null);
				}

				// prepare to transfer ownership of the connection if needed 
				Connection* connection = null;
				if (cref!=null) {
					connection = cref
					cref.Detach(); // release the internal ref
					// don't delete connection now so we can abort while readin response body, 
					// just pass ownership to NPT_HttpEntityBodyInputStream so it can recycle it
					// when done if connection should persist
				}

		

				// create the body stream wrapper
				Stream response_body_stream = 
					new NPT_HttpEntityBodyInputStream(buffered_input_stream, 
						response_entity->GetContentLength(),
						have_content_length,
						chunked,
						connection,
						should_persist);
				response_entity->SetInputStream(NPT_InputStreamReference(response_body_stream));
				response->SetEntity(response_entity);
			} else {
				if (should_persist && cref) {
					Connection* connection = cref->AsPointer();
					cref->Detach(); // release the internal ref
					connection->Recycle();
				}
			}

			return NPT_SUCCESS;
		}
*/
	 
		public HttpClient(Connector connector = null, bool transfer_ownership = true)
		{
			m_ProxySelector = HttpProxySelector.GetDefault ();
			m_ProxySelectorIsOwned = false;
			m_Connector = connector;
			m_ConnectorIsOwned = transfer_ownership;
			m_Aborted = false;

				if (connector == null) {
				//	m_Connector = new HttpTlsConnector();
					m_ConnectorIsOwned = true;
				}

		}



		// methods
		public Boolean SendRequest(HttpRequest request, out HttpResponse response)
		{
			HttpRequestContext context;
			return SendRequest (request, out response, out context);
		}


		public Boolean SendRequest(HttpRequest request, out HttpResponse response, out HttpRequestContext context)
		{
			int watchdog = m_Config.m_MaxRedirects+1;
			bool         keep_going;
			Boolean   result;

			// reset aborted flag
			m_Aborted = false;

			// default value
			response = null;

			// check that for GET requests there is no entity
			if (request.Entity != null && request.Method == HttpMessage.NPT_HTTP_METHOD_GET) {
				 throw new Exception("NPT_ERROR_HTTP_INVALID_REQUEST");
			}

			do {
				keep_going = false;
				result = SendRequestOnce(request, out response, out context);
				if (Plt.IsFailed(result)) break;
				if (response!=null && m_Config.m_MaxRedirects>0 && (request.Method.Equals(HttpMessage.NPT_HTTP_METHOD_GET) ||
					request.Method.Equals(HttpMessage.NPT_HTTP_METHOD_HEAD)) &&
					(response.GetStatusCode() == 301 ||
						response.GetStatusCode() == 302 ||
						response.GetStatusCode() == 303 ||
						response.GetStatusCode() == 307)) {
					// handle redirect
					String location = response.Headers.GetHeaderValue(HttpMessage.NPT_HTTP_HEADER_LOCATION);
					if (!String.IsNullOrEmpty(location)) {
						// check for location fields that are not absolute URLs 
						// (this is not allowed by the standard, but many web servers do it
						if (location.StartsWith("/") || 
							(!location.StartsWith("http://") &&
								!location.StartsWith("https://"))) {
							Plt.LogFine(String.Format("Location: header (%s) is not an absolute URL, using it as a relative URL", location));
							if (location.StartsWith("/")) {
								Plt.LogFine(String.Format("redirecting to absolute path %s", location));
								request.Url = new HttpUri(location);
							} else {
								String redirect_path = request.Url.Uri.AbsolutePath;
								int slash_pos = redirect_path.LastIndexOf('/');
								if (slash_pos >= 0) {
									redirect_path = redirect_path.Substring(slash_pos+1);
								} else {
									redirect_path = "/";
								}
								redirect_path += location;
								Plt.LogFine(String.Format("redirecting to absolute path %s", redirect_path));
								request.Url = new HttpUri(redirect_path);
							}
						} else {
							// replace the request url
							Plt.LogFine(String.Format("redirecting to %s", location));
							request.Url = new HttpUri(location);
							// remove host header so it is replaced based on new url
							request.Headers.Remove(HttpMessage.NPT_HTTP_HEADER_HOST);
						}
						keep_going = true;
						 
						response = null;
					}
				}       
			} while (keep_going && --watchdog>0 && !m_Aborted);

			// check if we were bitten by the watchdog
			if (watchdog == 0) {
				Plt.LogWarn("too many HTTP redirects");
				return false;//NPT_ERROR_HTTP_TOO_MANY_REDIRECTS;
			}

			return result;

		}

		public  Boolean Abort()
		{
			lock (m_AbortLock) {
				m_Aborted = true;

				HttpConnectionManager.GetInstance ().AbortConnections (this);
				return true;
			}
		}

		public Config GetConfig() 
		{
			return m_Config; 
		}

		Boolean SetConfig(Config config)
		{
			m_Config = config;

			return true;

		}

		public Boolean SetProxy(String http_proxy_hostname, int  http_proxy_port, String https_proxy_hostname = null, int  https_proxy_port = 0)
		{

			if (m_ProxySelectorIsOwned) {
				m_ProxySelector = null;
				m_ProxySelectorIsOwned = false;
			}

			// use a static proxy to hold on to the settings
			m_ProxySelector = new HttpStaticProxySelector(http_proxy_hostname, 
				http_proxy_port,
				https_proxy_hostname,
				https_proxy_port);
			m_ProxySelectorIsOwned = true;

			return true;

		}
		public Boolean SetProxySelector(HttpProxySelector selector)
		{
			if (m_ProxySelectorIsOwned && m_ProxySelector != selector) {
			}
			m_ProxySelector = selector;
			m_ProxySelectorIsOwned = false;

			return true;

		}
		public Boolean SetConnector(Connector connector)
		{
			if (m_ConnectorIsOwned && m_Connector != connector) {
	
			}
			m_Connector = connector;
			m_ConnectorIsOwned = false;

			return true;

		}
		public Boolean SetTimeouts(TimeSpan connection_timeout, TimeSpan io_timeout,TimeSpan name_resolver_timeout)
		{
			m_Config.m_ConnectionTimeout   = connection_timeout;
			m_Config.m_IoTimeout           = io_timeout;
			m_Config.m_NameResolverTimeout = name_resolver_timeout;

			return true;

		}
		public Boolean SetUserAgent(String user_agent)
		{
			m_Config.m_UserAgent = user_agent;
			return true;

		}
	//	Boolean SetOptions(Object options, bool on);

		 
	 
		public Boolean TrackConnection(Connection connection)
		{
			lock (m_AbortLock) {
				if (m_Aborted)
					return false;//NPT_ERROR_CANCELLED;
				return HttpConnectionManager.GetInstance ().Track (this, connection);
			}
		}
		protected Boolean SendRequestOnce(HttpRequest request, out HttpResponse response, out HttpRequestContext context )
		{
			// setup default values
			Boolean result = true;   
			response = null;
			context = new HttpRequestContext ();
			Plt.LogFine (String.Format ("requesting URL %s", request.Url.ToString ()));

			// get the address and port to which we need to connect
			HttpProxyAddress proxy = null;
			bool use_proxy = false;
			if (m_ProxySelector != null) {
				// we have a proxy selector, ask it to select a proxy for this URL
				result = m_ProxySelector.GetProxyForUrl (request.Url,out proxy);
				if (Plt.IsFailed (result) /*&& result != HttpMessage.NPT_ERROR_HTTP_NO_PROXY*/) {
					Plt.LogWarn (String.Format ("proxy selector failure (%d)", result));
					return result;
				}
				use_proxy = !String.IsNullOrEmpty(proxy.GetHostName ());
			}

			// connect to the server or proxy
			Connection connection = null;
			bool http_1_1 = (request.Protocol == HttpMessage.NPT_HTTP_PROTOCOL_1_1);
			Connection cref;

			// send the request to the server (in a loop, since we may need to reconnect with 1.1)
			bool reconnect = false;
			int watchdog = HttpConnectionManager.NPT_HTTP_MAX_RECONNECTS;
			do {
				cref = null;
				connection = null;
				Plt.LogFine (String.Format ("calling connector (proxy:{0}) (http 1.1:{1}) (url:{2})", 
					use_proxy ? "yes" : "no", http_1_1 ? "yes" : "no", request.Url.ToString ()));
				Plt.CheckWarining (m_Connector.Connect (request.Url, this,	use_proxy ? proxy : null, http_1_1,	out connection));
				Plt.LogFine (String.Format ("got connection (reused: %s)", connection.IsRecycled () ? "true" : "false"));

				Stream input_stream = connection.GetInputStream ();
				Stream output_stream = connection.GetOutputStream ();

				cref = connection;
				reconnect = connection.IsRecycled ();

				 
				 
					SocketInfo info;
					cref.GetInfo (out info);
					context.LocalAddress = (IPEndPoint)info.LocalEndPoint;
					context.RemoteAddress = (IPEndPoint)info.RemoteEndPoint;
				 

				HttpEntity entity = request.Entity;
				Stream body_stream = null;

				if (reconnect && entity!=null && Plt.IsSucceded (entity.GetInputStream (out body_stream))) {
					// if body is not seekable, we can't afford to reuse a connection
					// that could fail, so we reconnect a new one instead
					body_stream.Seek (0,SeekOrigin.Begin);
					Plt.LogFine ("rewinding body stream would fail ... create new connection");
					continue;
				}

				// decide if this connection should persist
				HttpHeaders headers = request.Headers;
				bool should_persist = http_1_1;
				if (!connection.SupportsPersistence ()) {
					should_persist = false;
				}
				if (should_persist) {
					String connection_header = headers.GetHeaderValue (HttpMessage.NPT_HTTP_HEADER_CONNECTION);
					if (!String.IsNullOrEmpty (connection_header) && (connection_header.Equals ("close"))) {
						should_persist = false;
					}        
				}

				if (m_Config.m_UserAgent.Length>0 ) {
					headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_USER_AGENT, m_Config.m_UserAgent, false); // set but don't replace 
				}

				result = WriteRequest (output_stream, request, should_persist, use_proxy);
				if (Plt.IsFailed (result)) {
					Plt.LogFine (String.Format ("failed to write request headers (%d)", result));
					if (reconnect && !m_Aborted) {
						if (body_stream != null) {
							// go back to the start of the body so that we can resend
							Plt.LogFine ("rewinding body stream in order to resend");
							body_stream.Seek (0,SeekOrigin.Begin);
							if (Plt.IsFailed (result)) {
								Console.WriteLine ("NPT_CHECK_FINE(NPT_ERROR_HTTP_CANNOT_RESEND_BODY)");
							}
						}
						continue;
					} else {
						return result;
					}
				}

				result = ReadResponse (input_stream,	should_persist,	!request.Method.Equals(HttpMessage.NPT_HTTP_METHOD_HEAD), out response);
				if (Plt.IsFailed (result)) {
					Plt.LogFine (String.Format ("failed to parse the response (%d)", result));
					if (reconnect && !m_Aborted /*&&
                (result == NPT_ERROR_EOS                || 
                 result == NPT_ERROR_CONNECTION_ABORTED ||
                 result == NPT_ERROR_CONNECTION_RESET   ||
                 result == NPT_ERROR_READ_FAILED) GBG: don't look for specific error codes */) {
						Plt.LogFine ("error is not fatal, retrying");
						if (body_stream != null) {
							// go back to the start of the body so that we can resend
							Plt.LogFine ("rewinding body stream in order to resend");
					 		body_stream.Seek (0, SeekOrigin.Begin);
							if (Plt.IsFailed (result)) {
								Console.WriteLine ("NPT_CHECK_FINE(NPT_ERROR_HTTP_CANNOT_RESEND_BODY)");
							}
						}
						continue;
					} else {
						// don't retry
						return result;
					}
				}			    
				break;
			} while (reconnect && --watchdog>0 && !m_Aborted);

			// check that we have a valid connection
			if (Plt.IsFailed (result) && !m_Aborted) {
				Plt.LogFine ("failed after max reconnection attempts");
				return false;//; NPT_ERROR_HTTP_TOO_MANY_RECONNECTS;
			}

			return result;

		}

	




		public HttpClient ()
		{
		}

		public static Boolean ReadResponse (Stream  input_stream, bool should_persist, bool expect_entity, out HttpResponse response)
		{
			Boolean result;

			// setup default values
			response = null;

			// create a buffered stream for this socket stream
			//MemoryStream ms = new MemoryStream( buffered_input_stream(new NPT_BufferedInputStream(input_stream));
				
			// parse the response
			for (int watchcat = 0; watchcat < NPT_HTTP_MAX_100_RESPONSES; watchcat++) {
				// parse the response
				result = HttpResponse.Parse (input_stream, out response);
				Plt.CheckWarining (result);
				if (response == null) {
					continue;
				}
				if (response.GetStatusCode () >= 100 && response.GetStatusCode () < 200) {
					Plt.LogFine (String.Format ("got {0} response, continuing", response.GetStatusCode ()));
					response = null;
					continue;
				}
				Plt.LogFine (String.Format ("got response, code={0}, msg={1}", response.GetStatusCode (), response.GetReasonPhrase ()));
				break;
			}

			// check that we have a valid response
			if (response == null) {
				Plt.LogFine ("failed after max continuation attempts");
				throw new Exception ("NPT_ERROR_HTTP_TOO_MANY_RECONNECTS");
			}
			 

			// decide if we should still try to reuse this connection later on
			if (should_persist) {
				String connection_header = response.Headers.GetHeaderValue (HttpMessage.NPT_HTTP_HEADER_CONNECTION);
				if (response.Protocol == HttpMessage.NPT_HTTP_PROTOCOL_1_1) {
					if (connection_header != null && (connection_header.Equals ("close"))) {
						should_persist = false;
					}
				} else {
					if (connection_header == null || !connection_header.Equals ("keep-alive")) {
						should_persist = false;
					}
				}
			}

			// create an entity if one is expected in the response
			if (expect_entity) {
				HttpEntity response_entity = new HttpEntity (response.Headers);

				// check if the content length is known
				bool have_content_length = (response.Headers.GetHeaderValue (HttpMessage.NPT_HTTP_HEADER_CONTENT_LENGTH) != null);

				// check for chunked Transfer-Encoding
				bool chunked = false;
				if (HttpMessage.NPT_HTTP_TRANSFER_ENCODING_CHUNKED.Equals(response_entity.GetTransferEncoding ())) {
					chunked = true;
					response_entity.SetTransferEncoding (null);
				}

				 

				// create the body stream wrapper
				//InputStream response_body_stream = new HttpEntityBodyInputStream(buffered_input_stream, response_entity.GetContentLength(), have_content_length, chunked, connection, should_persist);
				response_entity.SetInputStream (input_stream); //->SetInputStream(NPT_InputStreamReference(response_body_stream));
				response.Entity = response_entity;
			} else {

			}

			return true;
		}

		public static Boolean WriteRequest (Stream output_stream, HttpRequest  request, bool should_persist, bool use_proxy = false)
		{
			Boolean result = true;

			// add any headers that may be missing
			HttpHeaders headers = request.Headers;

			if (!should_persist) {
				headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_CONNECTION, "close", false); // set but don't replace
			}

			String host = request.Url.Uri.Host;
			int default_port = 0;
			if (request.Url.Uri.Scheme.Equals (HttpUri.SCHEME_ID_HTTP)) {			
				default_port = HttpMessage.NPT_URL_DEFAULT_HTTP_PORT;
			} else if (request.Url.Uri.Scheme.Equals (HttpUri.SCHEME_ID_HTTPS)) {					 
				default_port = HttpMessage.NPT_URL_DEFAULT_HTTPS_PORT;
			} 

			if (request.Url.Uri.Port != default_port) {
				host += ":";
				host += request.Url.Uri.Port;
			}
			headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_HOST, host, false); // set but don't replace

			// get the request entity to set additional headers
			Stream body_stream = new MemoryStream ();
			HttpEntity entity = request.Entity;
			if (entity != null && Plt.IsSucceded (entity.GetInputStream (out body_stream))) {
				// set the content length if known
				if (entity.ContentLengthIsKnown ()) {
					headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_CONTENT_LENGTH, entity.GetContentLength ().ToString ());
				}

				// content type
				String content_type = entity.GetContentType ();
				if (!String.IsNullOrEmpty (content_type)) {
					headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_CONTENT_TYPE, content_type);
				}

				// content encoding
				String content_encoding = entity.GetContentEncoding ();
				if (!String.IsNullOrEmpty (content_encoding)) {
					headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_CONTENT_ENCODING, content_encoding);
				}

				// transfer encoding
				String transfer_encoding = entity.GetTransferEncoding ();
				if (!String.IsNullOrEmpty (transfer_encoding)) {
					headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_TRANSFER_ENCODING, transfer_encoding);
				}
			} else {
				//FIXME: We should only set content length of 0 for methods with expected entities.
				//headers.SetHeader(NPT_HTTP_HEADER_CONTENT_LENGTH, "0");
			}

			// create a memory stream to buffer the headers
			MemoryStream header_stream = new MemoryStream ();

			// emit the request headers into the header buffer
			request.Emit (header_stream, use_proxy && request.Url.Uri.Scheme.Equals (HttpUri.SCHEME_ID_HTTP));

			// send the headers
			//StreamWriter writer= new StreamWriter(output_stream);
			header_stream.WriteTo (output_stream);
			//Plt.CheckWarining(output_stream.WriteFully(header_stream.GetData(), header_stream.GetDataSize()));

			// send request body 
			if (entity != null && body_stream != null) {
				// check for chunked transfer encoding
				Stream dest = output_stream;
				if (entity.GetTransferEncoding ().Equals ("NPT_HTTP_TRANSFER_ENCODING_CHUNKED")) {
					dest = new HttpChunkedStreamWriter (output_stream);
				}

				Plt.LogFine (String.Format ("sending body stream, %lld bytes", entity.GetContentLength ())); //FIXME: Would be 0 for chunked encoding
				long bytes_written = 0;

				// content length = 0 means copy until input returns EOS
				body_stream.CopyTo (dest);
				bytes_written = entity.GetContentLength ();
			
				//result = NPT_StreamToStreamCopy(*body_stream.AsPointer(), *dest, 0, entity->GetContentLength(), &bytes_written);
				if (Plt.IsFailed (result)) {
					Plt.LogFine (String.Format ("body stream only partially sent, %lld bytes (%d:%s)", bytes_written, result, result));
				}

				// flush to write out any buffered data left in chunked output if used
				dest.Flush ();  

				// cleanup (this will send zero size chunk followed by CRLF)
				if (dest != output_stream) {
					dest.Close ();
					dest.Dispose ();
				}
			}

			// flush the output stream so that everything is sent to the server
			output_stream.Flush ();

			return result;
		}
	}






	// types
	public class Config {
		public TimeSpan  m_ConnectionTimeout;
		public TimeSpan  m_IoTimeout;
		public TimeSpan  m_NameResolverTimeout;
		public int m_MaxRedirects;
		public String   m_UserAgent;

		public Config(){
			/*
			m_ConnectionTimeout = NPT_HTTP_CLIENT_DEFAULT_CONNECTION_TIMEOUT;
			m_IoTimeout = NPT_HTTP_CLIENT_DEFAULT_CONNECTION_TIMEOUT;
			m_NameResolverTimeout = NPT_HTTP_CLIENT_DEFAULT_NAME_RESOLVER_TIMEOUT;
			m_MaxRedirects = NPT_HTTP_CLIENT_DEFAULT_MAX_REDIRECTS;
			m_UserAgent = NPT_CONFIG_HTTP_DEFAULT_USER_AGENT;
			*/
		}
		
	}

	public abstract class BaseConnection {

		 
		public abstract Stream  GetInputStream();
		public abstract Stream GetOutputStream();
		public abstract Boolean GetInfo(out SocketInfo si);

		public virtual bool SupportsPersistence() 
		{
			return false;                    
		}
		public virtual bool IsRecycled(){ 
			return false;                    
		}
		public virtual Boolean Recycle(){ 
			return true; 
		}
		public virtual Boolean Abort(){ 
			return false; 
		}
	}

	public class  Connector {
		 
		 

		public Boolean Connect(HttpUri url,HttpClient client,HttpProxyAddress proxy, bool reuse, out Connection connection)
		{

			TcpClient tcpclient = new TcpClient();
			tcpclient.Connect (url.Uri.Host, url.Uri.Port);

			connection = new Connection (HttpConnectionManager.GetInstance(), tcpclient.Client, tcpclient.GetStream (), tcpclient.GetStream ());
			return true;
		}

		 
		protected Boolean TrackConnection(HttpClient client, Connection connection)
		{
			return client.TrackConnection(connection); 
		}

		public Connector() {} // don't instantiate directly
	}

	public abstract class HttpProxySelector
	{
		private static HttpProxySelector m_SystemDefault;

		public static bool 	HttpProxySelector_ConfigChecked   = false;
		public static int  	HttpProxySelector_Config          = 0;
		public static int   NPT_HTTP_PROXY_SELECTOR_CONFIG_NONE   = 0;
		public static int   NPT_HTTP_PROXY_SELECTOR_CONFIG_ENV    = 1;
		public static int   NPT_HTTP_PROXY_SELECTOR_CONFIG_SYSTEM = 2;


		// class methods
		public static HttpProxySelector GetDefault()
		{
			/*
			if (!HttpProxySelector_ConfigChecked) {
				String config;
				if (Plt.IsSucceded(NPT_Environment::Get("NEPTUNE_NET_CONFIG_PROXY_SELECTOR", config))) {
					if (config.Equals("noproxy")) {
						HttpProxySelector_Config = NPT_HTTP_PROXY_SELECTOR_CONFIG_NONE;
					} else if (config.Equals("env")) {
						HttpProxySelector_Config = NPT_HTTP_PROXY_SELECTOR_CONFIG_ENV;
					} else if (config.Equals("system")) {
						HttpProxySelector_Config = NPT_HTTP_PROXY_SELECTOR_CONFIG_SYSTEM;
					} else {
						HttpProxySelector_Config = NPT_HTTP_PROXY_SELECTOR_CONFIG_NONE;
					}
				}
				HttpProxySelector_ConfigChecked = true;
			} 

			switch (HttpProxySelector_Config) {
			case NPT_HTTP_PROXY_SELECTOR_CONFIG_NONE:
				// no proxy
				return null;

			case NPT_HTTP_PROXY_SELECTOR_CONFIG_ENV:
				// use the shared instance
				return HttpEnvProxySelector.GetInstance();

			case NPT_HTTP_PROXY_SELECTOR_CONFIG_SYSTEM:
				// use the sytem proxy selector
				return GetSystemSelector();

			default:
				return null;
			}
			*/
			return null;

		}
		public static HttpProxySelector GetSystemSelector()
		{
			return null;
		}

		 
		public abstract Boolean GetProxyForUrl(HttpUri url, out HttpProxyAddress proxy);
		 

	};

	public class HttpProxyAddress
	{
		String m_HostName;
		int m_Port;

		public HttpProxyAddress(){
			m_Port = HttpMessage.NPT_URL_INVALID_PORT ;
		}
		public HttpProxyAddress(String hostname, int port) 
		{
			m_HostName = hostname;
			m_Port = port;
		}

		public String GetHostName() 
		{
			return m_HostName; 
		}

		public void SetHostName(String hostname)
		{
			m_HostName = hostname; 
		}

		public int  GetPort() 
		{
			return m_Port; 
		}

		public void SetPort(int port) 
		{
			m_Port = port; 
		}
	}


	public class HttpStaticProxySelector : HttpProxySelector
	{
		HttpProxyAddress m_HttpProxy;
		HttpProxyAddress m_HttpsProxy;
		// constructor
		public HttpStaticProxySelector(String http_proxy_hostname, int  http_proxy_port, String https_proxy_hostname, int  https_proxy_port)
		{
			m_HttpProxy  = new HttpProxyAddress(http_proxy_hostname,  http_proxy_port);
			m_HttpsProxy = new HttpProxyAddress(https_proxy_hostname, https_proxy_port);

		}

		// NPT_HttpProxySelector methods
		public override Boolean GetProxyForUrl(HttpUri url, out HttpProxyAddress proxy)
		{
			proxy = null;
			if (url.Uri.Scheme.Equals ("http")) {			
				proxy = m_HttpProxy;
			} else if (url.Uri.Scheme.Equals ("http")) {
				proxy = m_HttpsProxy;
			} else {
				return false;// NPT_ERROR_HTTP_NO_PROXY;
			}
			return true;
		}
	}


	public class HttpEnvProxySelector : HttpProxySelector
	{
		private static HttpEnvProxySelector Instance;
		private static Object mutex = new object ();
		HttpProxyAddress m_HttpProxy;
		HttpProxyAddress m_HttpsProxy;
		List<String> m_NoProxy;
		HttpProxyAddress m_AllProxy;

		public static HttpEnvProxySelector GetInstance()
		{
			if (Instance!=null)
				return Instance;

			lock (mutex) {
				if (Instance == null) {
					// create the shared instance
					Instance = new HttpEnvProxySelector ();

				 

					// parse the http proxy settings
					/*
					String http_proxy;
					NPT_Environment::Get ("http_proxy", http_proxy);
					ParseProxyEnv (http_proxy, Instance.m_HttpProxy);
					Plt.LogFine (String.Format ("http_proxy: %s:%d", Instance.m_HttpProxy.GetHostName (), Instance.m_HttpProxy.GetPort ()));

					// parse the https proxy settings
					String https_proxy;
					if (Plt.IsFailed (NPT_Environment::Get ("HTTPS_PROXY", https_proxy))) {
						NPT_Environment::Get ("https_proxy", https_proxy);
					}
					ParseProxyEnv (https_proxy, Instance->m_HttpsProxy);
					Plt.LogFine (String.Format ("https_proxy: %s:%d", Instance.m_HttpsProxy.GetHostName (), Instance.m_HttpsProxy.GetPort ()));

					// parse the all-proxy settings
					String all_proxy;
					if (Plt.IsFailed (NPT_Environment::Get ("ALL_PROXY", all_proxy))) {
						NPT_Environment::Get ("all_proxy", all_proxy);
					}
					ParseProxyEnv (all_proxy, Instance->m_AllProxy);
					Plt.LogFine (String.Format ("all_proxy: %s:%d", Instance.m_AllProxy.GetHostName (), Instance.m_AllProxy.GetPort ()));

					// parse the no-proxy settings
					String no_proxy;
					if (Plt.IsFailed (NPT_Environment::Get ("NO_PROXY", no_proxy))) {
						//NPT_Environment::Get ("no_proxy", no_proxy);
					}
					if (no_proxy.GetLength ()) {
						Instance.m_NoProxy = no_proxy.Split (",");
					}
*/
				}
				
			 

				return Instance;
			}
		}

		static void ParseProxyEnv(String env, HttpProxyAddress proxy)
		{
			// ignore empty strings
			if (env.Length == 0) return;

			String proxy_spec;
			if (env.IndexOf("://") >= 0) {
				proxy_spec = env;
			} else {
				proxy_spec = "http://"+env;
			}
			Uri url = new Uri(proxy_spec);
			proxy.SetHostName(url.Host);
			proxy.SetPort(url.Port);

		}

		// NPT_HttpProxySelector methods
		public override Boolean  GetProxyForUrl(HttpUri url, out HttpProxyAddress proxy)
		{
			HttpProxyAddress protocol_proxy = null;
			proxy = null;

			if (url.Uri.Scheme.Equals ("http")) {			
				proxy = m_HttpProxy;
			} else if (url.Uri.Scheme.Equals ("http")) {
				proxy = m_HttpsProxy;
			} else {
				return false;// NPT_ERROR_HTTP_NO_PROXY;
			}

			 
			// check for no-proxy first
			if (m_NoProxy.Count>0) {
				foreach (String str in m_NoProxy) {
					if (str.Equals ("*")) {
						return false;//NPT_ERROR_HTTP_NO_PROXY;
					}
					if (url.Uri.Host.EndsWith (str)) {
						if (url.Uri.Host.Length == str.Length) {
							// exact match
							return false;//NPT_ERROR_HTTP_NO_PROXY;
						}
						if (url.Uri.Host.ToCharArray () [url.Uri.Host.Length - str.Length - 1] == '.') {
							// subdomain match
							return false;//NPT_ERROR_HTTP_NO_PROXY;
						}
					} 
				}				 
			}

			// check the protocol proxy
			if (protocol_proxy.GetHostName().Length>0) {
				proxy = protocol_proxy;
				return true;
			}

			// use the default proxy
			proxy = m_AllProxy;

			return proxy.GetHostName ().Length > 0 ? true : false;//NPT_ERROR_HTTP_NO_PROXY;
		}

	 
	};





}

