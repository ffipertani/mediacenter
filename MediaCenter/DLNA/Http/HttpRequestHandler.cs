﻿using System;

using System.IO;
using System.Collections.Generic;
using System.Web;

namespace Platinum
{
	public abstract class HttpRequestHandler
	{
		public HttpRequestHandler ()
		{
		}

		public abstract Boolean SetupResponse (HttpRequest request, HttpRequestContext context, HttpResponse response);

		 
		public virtual Boolean SendResponseBody (HttpRequestContext context, HttpResponse response, Stream output)
		{
			HttpEntity entity = response.Entity;
			if (entity == null)
				return true;

			Stream body_stream;
			entity.GetInputStream (out body_stream);
			if (body_stream == null)
				return true;

			// check for chunked transfer encoding
			Stream dest = output;
			if (entity.GetTransferEncoding ().Equals (HttpMessage.NPT_HTTP_TRANSFER_ENCODING_CHUNKED)) {
				dest = new HttpChunkedStreamWriter (output);
			}

			// send the body
			Plt.LogFine (String.Format ("sending body stream, %lld bytes", entity.GetContentLength ()));
			UInt64 bytes_written = 0;
			try {
				body_stream.CopyTo (dest, (int) entity.GetContentLength ());
			} catch (Exception e) {
				Console.WriteLine (e);
			}
			dest.Flush ();
			 

			// cleanup (this will send zero size chunk followed by CRLF)
			if (dest != output)
				dest.Close ();

			return true;
		}
	}

	public class HttpStaticRequestHandler : HttpRequestHandler
	{
		String m_MimeType;
		byte[] m_Buffer;

		public HttpStaticRequestHandler (String document, String mime_type = "text/html", bool        copy = true)
		{

			m_MimeType = mime_type;
			m_Buffer = System.Text.Encoding.Default.GetBytes (document);

		}

		public HttpStaticRequestHandler (String data, int size, String mime_type = "text/html", bool copy = true)
		{
			m_MimeType = mime_type;
			m_Buffer = System.Text.Encoding.Default.GetBytes (data);
		}

		// NPT_HttpRequestHandler methods
		public override Boolean SetupResponse (HttpRequest request, HttpRequestContext context, HttpResponse response)
		{
			HttpEntity entity = response.Entity;
			if (entity == null)
				throw new Exception ("NPT_ERROR_INVALID_STATE");

			entity.SetContentType (m_MimeType);
			entity.SetInputStream (m_Buffer, m_Buffer.Length);

			return true;

		}

		 
	}


	class HttpFileRequestHandler : HttpRequestHandler
	{
		private String m_UrlRoot;
		private String m_FileRoot;
		private Dictionary<String, String> m_FileTypeMap;
		private String m_DefaultMimeType;
		private bool m_UseDefaultFileTypeMap;
		private bool m_AutoDir;
		private String m_AutoIndex;

		public HttpFileRequestHandler (String url_root, String file_root, bool auto_dir = false, String auto_index = null)
		{
			m_UrlRoot = url_root;
			m_FileRoot = file_root;
			m_DefaultMimeType = "text/html";
			m_UseDefaultFileTypeMap = true;
			m_AutoDir = auto_dir;
			m_AutoIndex = auto_index;

		}

		// NPT_HttpRequestHandler methods
		public override Boolean SetupResponse (HttpRequest request, HttpRequestContext context, HttpResponse  response)
		{
			HttpEntity entity = response.Entity;
			if (entity == null)
				throw new Exception ("NPT_ERROR_INVALID_STATE");

			// check the method
			if (!request.Method.Equals (HttpMessage.NPT_HTTP_METHOD_GET) &&
			    !request.Method.Equals (HttpMessage.NPT_HTTP_METHOD_HEAD)) {
				response.SetStatus (405, "Method Not Allowed");
				return true;
			}

			// set some default headers
			response.Headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_ACCEPT_RANGES, "bytes");

			// declare HTTP/1.1 if the client asked for it
			if (request.Protocol.Equals (HttpMessage.NPT_HTTP_PROTOCOL_1_1)) {
				response.SetProtocol (HttpMessage.NPT_HTTP_PROTOCOL_1_1);
			}

			// TODO: we need to normalize the request path

			// check that the request's path is an entry under the url root
			if (!request.Url.Uri.AbsolutePath.StartsWith (m_UrlRoot)) {
				throw new Exception ("NPT_ERROR_INVALID_PARAMETERS");
			}
		
			// compute the filename
			String filename = m_FileRoot;
			FileAttributes attr = File.GetAttributes (filename);
			String relative_path = System.Net.WebUtility.UrlDecode (request.Url.Uri.AbsolutePath);//) NPT_Url::PercentDecode(request.GetUrl().GetPath().GetChars()+m_UrlRoot.GetLength());
			filename += "/";
			filename += relative_path;
			Plt.LogFine (String.Format ("filename = %s", filename));

			// get info about the file
			FileInfo info = new FileInfo (filename);
			File.OpenRead (filename);
			// check if this is a directory 
			if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
				Plt.LogFine ("file is a DIRECTORY");
				if (m_AutoDir) {
					if (m_AutoIndex.Length > 0) {
						Plt.LogFine ("redirecting to auto-index");
						filename += Path.DirectorySeparatorChar;//NPT_FilePath::Separator;
						filename += m_AutoIndex;
						if (File.Exists (filename)) {
							String location = m_UrlRoot + "/" + m_AutoIndex;
							response.SetStatus (302, "Found");
							response.Headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_LOCATION, location);
						} else {
							throw new Exception ("NPT_ERROR_PERMISSION_DENIED");
						}
					} else {
						Plt.LogFine ("doing auto-dir");

						// get the dir entries
						FileInfo[] entries = info.Directory.GetFiles ();

						String html="";
						 

						String html_dirname =  System.Net.WebUtility.HtmlEncode (relative_path);//, "<>&");
						html += "<hmtl><head><title>Directory Listing for /";
						html += html_dirname;
						html += "</title></head><body>";
						html += "<h2>Directory Listing for /";
						html += html_dirname;
						html += "</h2><hr><ul>\r\n";
						String url_base_path =  System.Net.WebUtility.HtmlEncode (request.Url.Uri.AbsolutePath);//, "<>&\"");
						foreach (FileInfo fi in entries) {
							String url_filename =  System.Net.WebUtility.HtmlEncode (fi.Name);// NPT_HtmlEncode(*i, "<>&");
							html += "<li><a href=\"";
							html += url_base_path;
							if (!url_base_path.EndsWith ("/"))
								html += "/";
							html += url_filename;
							html += "\">";
							html += url_filename;

							String full_path = filename;
							full_path += "/";
							full_path += fi.FullName;

							 
							if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
								html += "/";

							html += "</a><br>\r\n";
						}
						html += "</ul></body></html>";

						entity.SetContentType ("text/html");
						entity.SetInputStream (html);
						return true;
					}
				} else {
					throw new Exception ("NPT_ERROR_PERMISSION_DENIED");
				}
			}


			FileStream fs = File.Open (filename, FileMode.Open);
			if (fs == null) {
				Plt.LogFine ("file not found");
				throw new Exception ("NPT_ERROR_NO_SUCH_ITEM");
			}


			// check for range requests
			String range_spec = request.Headers.GetHeaderValue (HttpMessage.NPT_HTTP_HEADER_RANGE);

			// setup entity body
			Plt.CheckWarining (SetupResponseBody (response, fs, range_spec));

			// set the response body
			entity.SetContentType (GetContentType (filename));

			return true;
		}

		// class methods
		static String GetDefaultContentType (String extension)
		{
			for (int i=0; i<PltMimeType.PltHttpFileRequestHandler_DefaultFileTypeMap.Length; i++) {
				if (extension.Equals(PltMimeType.PltHttpFileRequestHandler_DefaultFileTypeMap[i].Extension)) {
					String type = PltMimeType.PltHttpFileRequestHandler_DefaultFileTypeMap[i].MimeType;
					Plt.LogFine(String.Format("using type from default list: %s", type)); 
					return type;
				}
			}

			return null;

		}

		// accessors
		public Dictionary<String,String> GetFileTypeMap ()
		{
			return m_FileTypeMap; 
		}

		public void SetDefaultMimeType (String mime_type)
		{
			m_DefaultMimeType = mime_type;
		}

		public void SetUseDefaultFileTypeMap (bool use_default)
		{
			m_UseDefaultFileTypeMap = use_default;
		}

		public static Boolean SetupResponseBody (HttpResponse response, Stream stream, String range_spec = null)
		{
			Boolean result = true;
			HttpEntity entity = response.Entity;
			if (entity == null)
				throw new Exception ("NPT_ERROR_INVALID_STATE");

			if (range_spec!=null) {    
				String accept_range = response.Headers.GetHeaderValue (HttpMessage.NPT_HTTP_HEADER_ACCEPT_RANGES);

				if (response.Entity.GetTransferEncoding ().Equals (HttpMessage.NPT_HTTP_TRANSFER_ENCODING_CHUNKED) ||
				    (accept_range != null && accept_range.Equals ("bytes"))) {
					Plt.LogFine ("range request not supported");
					response.SetStatus (416, "Requested Range Not Satisfiable");
					return true;            
				}

				// measure the stream size
				bool has_stream_size = false;
				long stream_size = 0;
				stream_size = stream.Length;
				if (stream_size >= 0) {
					has_stream_size = true;
					Plt.LogFine (String.Format ("body size=%lld", stream_size));
					if (stream_size == 0)
						return true;
				}

				if (!range_spec.StartsWith ("bytes=")) {
					Plt.LogFine ("unknown range spec");
					response.SetStatus (400, "Bad Request");
					return true;
				}
				String valid_range="";
				String range = range_spec.Substring (6);
				if (range.IndexOf (',') >= 0) {
					Plt.LogFine ("multi-range requests not supported");
					if (has_stream_size) {
						valid_range = "bytes */";
						valid_range += stream_size.ToString ();
						response.Headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_CONTENT_RANGE, valid_range);
					}
					response.SetStatus (416, "Requested Range Not Satisfiable");
					return true;            
				}
				int sep = range.IndexOf ('-');
				long range_start = 0;
				long range_end = 0;
				bool has_start = false;
				bool has_end = false;
				bool satisfied = false;

				if (sep < 0) {
					Plt.LogFine ("invalid syntax");
					response.SetStatus (400, "Bad Request");
					return true;
				} else {
					if (sep + 1 < range.Length) {
						result = long.TryParse (range.Substring (sep + 1), out range_end);
						if (Plt.IsFailed (result)) {
							Plt.LogFine ("failed to parse range end");
							return result;
						}
						range.Substring(0,sep);
						has_end = true;
					}
					if (sep > 0) {
						result = long.TryParse (range, out range_start);						 
						if (Plt.IsFailed (result)) {
							Plt.LogFine ("failed to parse range start");
							return result;
						}
						has_start = true;
					}

					if (!has_stream_size) {
						if (has_start && range_start == 0 && !has_end) {
							bool update_content_length = (!entity.GetTransferEncoding ().Equals (HttpMessage.NPT_HTTP_TRANSFER_ENCODING_CHUNKED));
							// use the whole file stream as a body
							return entity.SetInputStream (stream, update_content_length);
						} else {
							Plt.LogWarn ("file.GetSize() failed");
							Plt.LogFine ("range request not supported");
							response.SetStatus (416, "Requested Range Not Satisfiable");
							return true;
						}
					}

					if (has_start) {
						// some clients sends incorrect range_end equal to size
						// we try to handle it
						if (!has_end || range_end == stream_size)
							range_end = stream_size - 1; 
					} else {
						if (has_end) {
							if (range_end <= stream_size) {
								range_start = stream_size - range_end;
								range_end = stream_size - 1;
							}
						}
					}
					Plt.LogFine (String.Format("final range: start=%lld, end=%lld", range_start, range_end));
					if (range_start > range_end) {
						Plt.LogFine ("invalid range");
						response.SetStatus (400, "Bad Request");
						satisfied = false;
					} else if (range_end >= stream_size) {
						response.SetStatus (416, "Requested Range Not Satisfiable");
						Plt.LogFine ("out of range");
						satisfied = false;
					} else {
						satisfied = true;
					}
				} 
				if (satisfied && range_start != 0) {
					// seek in the stream
					 stream.Seek (range_start, SeekOrigin.Begin);
					if (Plt.IsFailed (result)) {
						Plt.LogWarn (String.Format ("stream.Seek() failed (%d)", result));
						satisfied = false;
					}
				}
				if (!satisfied) {
					if (!String.IsNullOrEmpty(valid_range))
						response.Headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_CONTENT_RANGE, valid_range);
					response.SetStatus (416, "Requested Range Not Satisfiable");
					return true;            
				}

				// use a portion of the file stream as a body
				entity.SetInputStream (stream, false);
				entity.SetContentLength (range_end - range_start + 1);
				response.SetStatus (206, "Partial Content");
				valid_range = "bytes ";
				valid_range += range_start;
				valid_range += "-";
				valid_range += range_end;
				valid_range += "/";
				valid_range += stream_size;
				response.Headers.SetHeader (HttpMessage.NPT_HTTP_HEADER_CONTENT_RANGE, valid_range);
			} else {
				bool update_content_length = (!entity.GetTransferEncoding ().Equals (HttpMessage.NPT_HTTP_TRANSFER_ENCODING_CHUNKED));
				// use the whole file stream as a body
				entity.SetInputStream (stream, update_content_length);
			}

			return true;



		}

		public String GetContentType(String filename)
		{
			int last_dot = filename.LastIndexOf('.');
			if (last_dot > 0) {
				String extension = filename.Substring(last_dot+1);
				extension = extension.ToLower ();

				Plt.LogFine(String.Format("extension=%s", extension));

				String mime_type;
				if (Plt.IsSucceded(m_FileTypeMap.TryGetValue(extension, out mime_type))) {
					Plt.LogFine(String.Format("found mime type in map: %s", mime_type));
					return mime_type;
				}

				// not found, look in the default map if necessary
				if (m_UseDefaultFileTypeMap) {
					String type = GetDefaultContentType(extension);
					if (type!=null) return type;
				}
			}

			Plt.LogFine("using default mime type");
			return m_DefaultMimeType;
		}


	 

		 
	}


}

