﻿using System;
using Xwt.Mac;
using MonoMac.AppKit;
using Xwt.Backends;

namespace MediaRenderer.Mac
{
	public class MediaRendererMacEngine:MacEngine
	{

		public override void InitializeBackends ()
		{
			base.InitializeBackends ();
		 			 
			RegisterBackend <IMediaRendererPlayer, MediaPlayerBackend> (); 			 
		}
	}
}

