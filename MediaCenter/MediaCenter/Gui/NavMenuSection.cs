﻿using System;
using System.Collections.Generic;
using Xwt;
using Xwt.Drawing;

namespace MediaCenter
{
	public class NavMenuSection
	{
		public VBox Content{ get; set; }
		private String title;
		private List<NavMenuItem> items = new List<NavMenuItem>();
		private Label tentry;

		public NavMenuSection ()
		{	
			Content = new VBox ();
			Content.Spacing = 0;
			Content.MarginBottom = 6;
			Content.BackgroundColor = Colors.Transparent;
		}

		public void AddItem(NavMenuItem item)
		{
			items.Add (item);
			item.NavMenuSection = this;
			Content.PackStart (item);
		}

		public String Title
		{ 
			get{ 
				return title;
			}
			set{
				title = value;
				Content.Clear ();
				redraw ();
			}
		}	

		public IEnumerator<NavMenuItem> Items {
			get{
				return items.GetEnumerator ();
			}
		}

		private void redraw()
		{
			if (title != null) {
			    tentry = new Label ();
				tentry.Margin = new WidgetSpacing (0, 0, 0, 0);
				tentry.ExpandVertical = false;
				tentry.ExpandHorizontal = false;
				tentry.Text = title.ToUpper();
				//tentry.MinWidth = 10;
				tentry.TextColor = new Color (0.43, 0.43, 0.46);
				tentry.Font = tentry.Font.WithSize (11).WithWeight(FontWeight.Bold);
				//tentry.BackgroundColor = Colors.Red;
				tentry.MarginLeft = 8;
				tentry.MarginTop = 8;
				tentry.MarginBottom = 4;

				HBox labelBox = new HBox ();
				labelBox.BackgroundColor = Colors.Transparent;
				labelBox.PackStart (tentry);
				Content.PackStart (labelBox);
				Content.ExpandHorizontal = false;
			}
			foreach (NavMenuItem item in items) {
				Content.PackStart (item);
			}
		}

	}
}

