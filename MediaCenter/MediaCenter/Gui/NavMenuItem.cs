﻿using System;
using Xwt;
using Xwt.Drawing;

namespace MediaCenter
{
	public class NavMenuItem:HBox
	{
		public NavMenuSection NavMenuSection{ get; set; }

		private Boolean selected = false;
		private Canvas canvasSelected;
		Label tentry;
		public NavMenuItem (String text, Image image):this (text, image, false){}

		public NavMenuItem (String text, Image image, Boolean selected)
		{
			this.selected = selected;
			canvasSelected = new Canvas ();
			canvasSelected.MinWidth = 3;
			canvasSelected.MarginTop = 3;
			canvasSelected.MarginBottom = 3;
			canvasSelected.MinHeight = 2;
			canvasSelected.ButtonPressed += OnButtonPressedOnChild;
			PackStart (canvasSelected);
			Widget wgt = null;

			if (image != null) {
				image = image.WithSize(18,18);
				var imageView = new ImageView ();
				imageView.Image = image;
				wgt = imageView;
			} else {
				wgt = new Canvas ();
				wgt.MinWidth = 18;
				wgt.MinWidth = 18;
			}

			wgt.MarginLeft = 4;
			//wgt.MarginTop = 6;
		//	wgt.MarginBottom = 6;

			wgt.ButtonPressed += OnButtonPressedOnChild;
			PackStart (wgt);
			 
			Margin = new WidgetSpacing (0, 0, 0, 0);

			tentry = new Label ();
			tentry.Margin = new WidgetSpacing (0, 0, 0, 0);
			tentry.ExpandVertical = false;
			tentry.ExpandHorizontal = false;
			tentry.Text = text;
			tentry.MinWidth = 50;
			tentry.Font = tentry.Font.WithSize (13).WithWeight(FontWeight.Bold).WithStyle(FontStyle.Normal);
			tentry.Cursor = CursorType.Hand;
			tentry.ButtonPressed += OnButtonPressedOnChild;
			//tentry.BackgroundColor = Colors.Red;
			tentry.MarginTop = 5;
			tentry.MarginBottom = 5;
			tentry.MarginLeft = 2;
			PackStart (tentry);

			if (selected) {
				selectItem ();
			} else {
				deselectItem ();
			}


		}

		private void OnButtonPressedOnChild(object sender, ButtonEventArgs e){
			OnButtonPressed (e);
		}

		private void selectItem()
		{
			canvasSelected.BackgroundColor = Colors.Yellow;
			//BackgroundColor = new Color(0.43,0.43,0.46);
			tentry.TextColor = Colors.White;
			BackgroundColor = new Color(0.2,0.2,0.23);
		}

		private void deselectItem()
		{
			tentry.TextColor = new Color (0.43, 0.43, 0.46);
			canvasSelected.BackgroundColor = Colors.Transparent;
			BackgroundColor =Colors.Transparent;
		}

		public Boolean Selected {
			get {
				return selected;
			}
			set{
				selected = value;
				if (selected) {
					selectItem ();
				} else {
					deselectItem ();
				}
			}
		}

	}
}

