﻿using System;
using Xwt;
using System.Collections;
using System.Collections.Generic;
using Xwt.Drawing;

namespace MediaCenter
{
	public class NavMenu:ScrollView
	{
		private VBox layout;
		private NavMenuItem selectedItem;
		private List<NavMenuSection> sections = new List<NavMenuSection> ();

		public NavMenu ()
		{
			BackgroundColor = new Xwt.Drawing.Color(0.11,0.11,0.12);
			BorderVisible = false;
			MinWidth = 250;
			HorizontalScrollPolicy = ScrollPolicy.Never;

			layout = new VBox ();
			layout.HorizontalPlacement = WidgetPlacement.Fill;
			layout.VerticalPlacement = WidgetPlacement.Fill;
			layout.BackgroundColor = new Xwt.Drawing.Color(0.11,0.11,0.12);
			layout.Spacing = 2;
			layout.WidthRequest = 250;
			layout.MinWidth = 250;


			Content = layout;		
		}

		private void onSelectItem(object sender,EventArgs e)
		{
			if(selectedItem!=null){
				selectedItem.Selected =false;
			}
			selectedItem = (NavMenuItem)sender;
			selectedItem.Selected = true;
			this.Show ();
		}

		private void deselectAll()
		{
			foreach (NavMenuSection section in sections) {
				IEnumerator<NavMenuItem> en = section.Items;
				while (en.MoveNext ()) {
					en.Current.Selected = false;
				}
			}
		}

		public void Add(NavMenuSection section)
		{
			IEnumerator<NavMenuItem> en = section.Items;
			while (en.MoveNext ()) {
				en.Current.ButtonPressed += onSelectItem;
			}

			layout.PackStart (section.Content);
		}

		public NavMenuItem SelectedItem
		{ 
			get{
				return selectedItem;
			}
			set{ 
				selectedItem = value;
			}
		}
	}
}

