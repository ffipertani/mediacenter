﻿using System;
using Xwt;
using Xwt.Drawing;

using Vlc.DotNet.Core.Medias;


namespace MediaCenter
{
	public class MyMainWindow: MyBaseWindow
	{
		MovieView view;
		MediaPlayer mp = new MediaPlayer ();
		private delegate void VoidDelegate();

		StatusIcon statusIcon;

		public MyMainWindow ()
		{
			Width = 1000;
			Height = 600;
		
			Title = "Xwt Demo Application";
			//BackgroundColor = Xwt.Drawing.Colors.Violet;
			//BackgroundColor = new Xwt.Drawing.Color(0.11,0.11,0.12);
			 
			Toolbar = new Toolbar ();
			//MyButton btnBack = new MyButton ("arrowhead.png");
			//MyButton btnForward = new MyButton ("chevron13.png");
			//Toolbar.PackStart (btnBack);
			//Toolbar.PackStart (btnForward);
			 
			 
			Button button = new Button ("Play");
			button.MarginTop = 0;
			button.Clicked += delegate {
				mp.Play();
			};
			Toolbar.PackStart (button);
			Button buttonStop = new Button ("Stop");
			buttonStop.MarginTop = 0;
			buttonStop.Clicked += delegate {
				view.Back();

			};
			Toolbar.PackStart (buttonStop);
 		 
			HPaned box = new HPaned ();
			//box.MinWidth = 800;
			//box.MinHeight = 600;
			//box.BackgroundColor = Xwt.Drawing.Colors.Black;
			 
			box.Cursor = CursorType.Move;
 /*
			ListBox listBox = new ListBox ();
			listBox.GridLinesVisible = true;
			ListStore store = new ListStore (name, icon);
			listBox.DataSource = store;
			TextCellView rowCell = new TextCellView (row);

			listBox.Views.Add (rowCell);
			listBox.Views.Add (new ImageCellView (icon));
			listBox.Views.Add (new TextCellView (name));


			listBox.Margin = new WidgetSpacing (4, 4, 4, 4);
			for (int n=0; n<100; n++) {
				var r = store.AddRow ();
				store.SetValue (r, row, r);
				store.SetValue (r, icon, png);
				store.SetValue (r, name, "Value " + n);
			}
			//listBox.Items.Add ("PIPPO");
			listBox.Font = Font.SystemSansSerifFont.WithSize (54).WithWeight(FontWeight.Bold).WithStyle(FontStyle.Italic);
		
			box.Panel1.Content = listBox;

*/

			box.BackgroundColor = Colors.Transparent;						 
			box.Panel1.Content = createMenu ();

			view = new MovieView ();
			box.Panel2.Content = view;
			 
		 


			Content = box;
			box.Position = 160;

			CloseRequested += HandleCloseRequested;

			 
		}

		private NavMenu createMenu()
		{
			Image png = Image.FromResource (this.GetType(), "class.png");
			NavMenu navMenu = new NavMenu ();

			NavMenuSection section = new NavMenuSection ();
			section.Title = "Menu principale";

			section.AddItem(new NavMenuItem("Film",png));
			section.AddItem(new NavMenuItem("Serie TV",null));
			section.AddItem(new NavMenuItem("Anime",png));
			section.AddItem(new NavMenuItem("Collezioni",null));	
			section.AddItem(new NavMenuItem("Attori",null));	
			navMenu.Add (section);


			NavMenuSection section2 = new NavMenuSection ();
			section2.Title = "Risorse";
			section2.AddItem(new NavMenuItem("Media locali",null));
			section2.AddItem(new NavMenuItem("Rete",null));
			section2.AddItem(new NavMenuItem("Dispositivi",null));
			section2.AddItem(new NavMenuItem("Pippo",null));
			section2.AddItem(new NavMenuItem("Pippo",null));
			section2.AddItem(new NavMenuItem("Pippo",null));
			section2.AddItem(new NavMenuItem("Pippo",null));
			section2.AddItem(new NavMenuItem("Pippo",null));
			section2.AddItem(new NavMenuItem("Pippo",null));
			section2.AddItem(new NavMenuItem("Pippo",null));
			navMenu.Add (section2);

			return navMenu;
		}


		 

		private void altro()
		{
			/*
			try {
				statusIcon = Application.CreateStatusIcon ();
				statusIcon.Menu = new Menu ();
				statusIcon.Menu.Items.Add (new MenuItem ("Test"));
				statusIcon.Image = Image.FromResource (GetType (), "package.png");
			} catch {
				Console.WriteLine ("Status icon could not be shown");
			}

			Menu menu = new Menu ();

			var file = new MenuItem ("_File");
			file.SubMenu = new Menu ();
			file.SubMenu.Items.Add (new MenuItem ("_Open"));
			file.SubMenu.Items.Add (new MenuItem ("_New"));
			MenuItem mi = new MenuItem ("_Close");
			mi.Clicked += delegate {
				Application.Exit();
			};
			file.SubMenu.Items.Add (mi);
			menu.Items.Add (file);

			var edit = new MenuItem ("_Edit");
			edit.SubMenu = new Menu ();
			edit.SubMenu.Items.Add (new MenuItem ("_Copy"));
			edit.SubMenu.Items.Add (new MenuItem ("Cu_t"));
			edit.SubMenu.Items.Add (new MenuItem ("_Paste"));
			menu.Items.Add (edit);

			MainMenu = menu;
*/
		}
		void HandleCloseRequested (object sender, CloseRequestedEventArgs args)
		{
			/*
			args.AllowClose = MessageDialog.Confirm ("Samples will be closed", Command.Ok);
			if (args.AllowClose)
				Application.Exit ();
				*/
		}

		protected override void Dispose (bool disposing)
		{
			base.Dispose (disposing);

			if (statusIcon != null) {
				statusIcon.Dispose ();
			}
		}
		  



		  
	}

	 
}

