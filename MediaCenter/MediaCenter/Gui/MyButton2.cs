﻿using System;
using Xwt;
using Xwt.Drawing;
using System.Drawing;
using System.Threading;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.IO;

namespace MediaCenter
{

	public class MyButton2:Canvas
	{
		public String Text{ get; set;}
		public Xwt.Drawing.Image Image{get; set;}
		public Xwt.Drawing.Color OriginalColor{get;set;}
		public Xwt.Drawing.Color DefaultColor{ get; set;}
		public Xwt.Drawing.Color HoverColor{ get; set;}
		public Xwt.Drawing.Color ClickColor{ get; set;}

		private ImageView view;

		public MyButton2 (String imagePath)
		{
			OriginalColor = Colors.Black;
			DefaultColor = Colors.LightCoral;
			HoverColor = Colors.Aquamarine;
			ClickColor = Colors.DarkOrchid;
			WidthRequest = 48;
			HeightRequest = 48;
			Image =  Xwt.Drawing.Image.FromResource(imagePath);
			Image = Image.WithSize (new Xwt.Size (48, 48));
			Image = Image.WithBoxSize(new Xwt.Size(48,48));

			view = new ImageView ( modifyColor (Image, DefaultColor));
			//view = new ImageView (  Image );
			view.ExpandVertical = true;
			view.ExpandHorizontal = true;
			AddChild (view);
			//SetChildBounds (view, this.Bounds);

			MarginTop = 0;

			view.MouseEntered+= delegate {
				view.Image = modifyColor(Image,HoverColor);
			};

			view.MouseExited+= delegate {
				view.Image = modifyColor(Image,DefaultColor);
			};

			view.ButtonPressed+= delegate {
				view.Image = modifyColor(Image,ClickColor);
			};

			view.ButtonReleased+= delegate {
				view.Image = modifyColor(Image,HoverColor);
			};

			/*
            view.MouseMoved+= delegate {
                view.Image = modifyColor(Image,HoverColor);
            };
            */
		}


		private Xwt.Drawing.Image modifyColor(Xwt.Drawing.Image image, Xwt.Drawing.Color newColor)
		{
			BitmapImage abmp = image.ToBitmap ();        
		//	Bitmap originalbmp = new Bitmap ((int)abmp.Width, (int)abmp.Height);
				//Bitmap originalbmp = new Bitmap (50,50);

			/*
			for(int i=0;i<abmp.Width;i++){
				for (int h = 0; h < abmp.Height; h++) {
					Xwt.Drawing.Color pixelColor = abmp.GetPixel (i, h);
					originalbmp.SetPixel (i, h, XDColorToSDColor (pixelColor));
					//originalbmp.SetPixel (i, h, System.Drawing.Color.FromArgb ((byte)pixelColor.Alpha,(byte)pixelColor.Red,(byte)pixelColor.Green,(byte)pixelColor.Blue));
				}
			}
*/

			/*
			ColorSubstitutionFilter filter = new ColorSubstitutionFilter ();
			filter.SourceColor = OriginalColor;//XDColorToSDColor (OriginalColor);// System.Drawing.Color.FromArgb (1, (byte)OriginalColor.Red, (byte)OriginalColor.Green, (byte)OriginalColor.Blue);

			filter.NewColor = newColor;// XDColorToSDColor (newColor);//System.Drawing.Color.FromArgb (1, (byte)newColor.Red, (byte)newColor.Green, (byte)newColor.Blue);
			filter.ThresholdValue = 10f;
			*/
			//abmp = abmp.ColorSubstitution (filter);
			return null;
			/*
			for(int i=0;i<abmp.Width;i++){
				for (int h = 0; h < abmp.Height; h++) {
					System.Drawing.Color pixelColor = originalbmp.GetPixel (i, h);
					//Xwt.Drawing.Color newpixel = Xwt.Drawing.Color.FromBytes ((byte)pixelColor.R, (byte)pixelColor.G, (byte)pixelColor.B);
					//newpixel.Alpha = pixelColor.A;
					if (pixelColor.R != 0) {
						Console.WriteLine ("ZERO");
					}
					Xwt.Drawing.Color newpixel = SDColorToXDColor (pixelColor);
					abmp.SetPixel (i, h, newpixel);
				}
			}
			return abmp;
			*/

		}


		private Xwt.Drawing.Color SDColorToXDColor(System.Drawing.Color color)
		{
			Console.WriteLine ("ORIGINAL COLOR" + color);
			float r = (float)color.R / 255;
			float g = (float)color.G / 255;
			float b = (float)color.B / 255;
			float a = (float)color.A / 255;

			Xwt.Drawing.Color newcolor =new Xwt.Drawing.Color(r, g, b,a);
			Console.WriteLine ("OTHER COLOR" + newcolor);
			return newcolor;
		}

		private System.Drawing.Color XDColorToSDColor(Xwt.Drawing.Color color)
		{
			System.Drawing.Color newcolor = System.Drawing.Color.FromArgb((byte)(color.Alpha * 255),(byte)(color.Red*255), (byte)(color.Green*255),(byte)(color.Blue*255));

			return newcolor;
		}
	}


	public static class ExtBitmap2
	{
		/*
		public static Xwt.Drawing.Image ColorSubstitution(this Xwt.Drawing.BitmapImage sourceBitmap, ColorSubstitutionFilter filterData)
		{
			Xwt.Drawing.BitmapImage resultBitmap = new Xwt.Drawing.Image (sourceBitmap).ToBitmap();
			 

			double sourceRed = 0, sourceGreen = 0, sourceBlue = 0, sourceAlpha = 0;
			double resultRed = 0, resultGreen = 0, resultBlue = 0;

			double newRedValue = (double)(filterData.NewColor.Red*255);
			double newGreenValue = (double)(filterData.NewColor.Green*255);
			double newBlueValue = (double)(filterData.NewColor.Blue*255);

			double redFilter = (double)(filterData.SourceColor.Red*255);
			double greenFilter = (double)(filterData.SourceColor.Green*255);
			double blueFilter = (double)(filterData.SourceColor.Blue*255);

			byte minValue = 0;
			byte maxValue = 255;
			MemoryStream st = new MemoryStream ();
			sourceBitmap.Save (st, ImageFileType.Bmp);
			 
			//byte[] resultBuffer = st.GetBuffer ();			 
			byte[] resultBuffer = sourceBitmap.GetRawData ();
			st.Close ();
			 

		
			for (int k = 0; k < resultBuffer.Length; k += 4)
			{
				sourceAlpha = resultBuffer[k + 3];

				if (sourceAlpha != 0)
				{
					sourceBlue = resultBuffer[k];
					sourceGreen = resultBuffer[k + 1];
					sourceRed = resultBuffer[k + 2];

					if ((sourceBlue < blueFilter + filterData.ThresholdValue &&
						sourceBlue > blueFilter - filterData.ThresholdValue) &&

						(sourceGreen < greenFilter + filterData.ThresholdValue &&
							sourceGreen > greenFilter - filterData.ThresholdValue) &&

						(sourceRed < redFilter + filterData.ThresholdValue &&
							sourceRed > redFilter - filterData.ThresholdValue))
					{
						resultBlue =(byte)( blueFilter - sourceBlue + newBlueValue);

						if (resultBlue > maxValue)
						{ resultBlue = maxValue; }
						else if (resultBlue < minValue)
						{ resultBlue = minValue; }

						resultGreen = (byte) (greenFilter - sourceGreen + newGreenValue);

						if (resultGreen > maxValue)
						{ resultGreen = maxValue; }
						else if (resultGreen < minValue)
						{ resultGreen = minValue; }

						resultRed = (byte)(redFilter - sourceRed + newRedValue);

						if (resultRed > maxValue)
						{ resultRed = maxValue; }
						else if (resultRed < minValue)
						{ resultRed = minValue; }

						resultBuffer[k] = (byte)resultBlue;
						resultBuffer[k + 1] = (byte)resultGreen;
						resultBuffer[k + 2] = (byte)resultRed;
						//resultBuffer[k + 3] = (byte)sourceAlpha;
					}
				}
			}
 */
			 /*
			for (int i = 0; i < resultBitmap.Width; i++) {
				for (int k = 0; k < resultBitmap.Height; k++) {

					Xwt.Drawing.Color originalPixel = sourceBitmap.GetPixel (i, k);
					if(originalPixel.Alpha!=0){
																
						sourceBlue = originalPixel.Blue;
						sourceGreen = originalPixel.Green;
						sourceRed = originalPixel.Red;
						sourceAlpha = originalPixel.Alpha;

						if ((sourceBlue < blueFilter + filterData.ThresholdValue &&
						   sourceBlue > blueFilter - filterData.ThresholdValue) &&

						   (sourceGreen < greenFilter + filterData.ThresholdValue &&
						   sourceGreen > greenFilter - filterData.ThresholdValue) &&

						   (sourceRed < redFilter + filterData.ThresholdValue &&
						   sourceRed > redFilter - filterData.ThresholdValue)) {
							resultBlue =(float) (blueFilter - sourceBlue + newBlueValue);

							if (resultBlue > maxValue) {
								resultBlue = maxValue;
							} else if (resultBlue < minValue) {
								resultBlue = minValue;
							}

							resultGreen = (float)(greenFilter - sourceGreen + newGreenValue);

							if (resultGreen > maxValue) {
								resultGreen = maxValue;
							} else if (resultGreen < minValue) {
								resultGreen = minValue;
							}

							resultRed =(float) (redFilter - sourceRed + newRedValue);

							if (resultRed > maxValue) {
								resultRed = maxValue;
							} else if (resultRed < minValue) {
								resultRed = minValue;
							}


							resultBitmap.SetPixel (i,k,new Xwt.Drawing.Color (resultRed, resultGreen, resultBlue,sourceAlpha/255));
						}
					}
				}
			}
	}
			  */
			//st.Position = 0;
			//return  BitmapImage.FromStream (new MemoryStream(resultBuffer)).WithBoxSize(new Xwt.Size(48,48)); //n BitmapImage.FromFile("/Users/francescofipertani/Downloads/Vlc.DotNet/MediaCenter/bin/Debug/img.bmp").WithBoxSize(new Xwt.Size(48,48));
			//eturn resultBitmap;
		 


		/*
		public static Bitmap ColorSubstitution(this Bitmap sourceBitmap, ColorSubstitutionFilter filterData)
		{
			Bitmap resultBitmap = new Bitmap(sourceBitmap.Width, sourceBitmap.Height, PixelFormat.Format32bppArgb);

			BitmapData sourceData = sourceBitmap.LockBits(new System.Drawing.Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
			BitmapData resultData = resultBitmap.LockBits(new System.Drawing.Rectangle(0, 0, resultBitmap.Width, resultBitmap.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

			byte[] resultBuffer = new byte[resultData.Stride * resultData.Height];
			Marshal.Copy(sourceData.Scan0, resultBuffer, 0, resultBuffer.Length);

			sourceBitmap.UnlockBits(sourceData);

			byte sourceRed = 0, sourceGreen = 0, sourceBlue = 0, sourceAlpha = 0;
			int resultRed = 0, resultGreen = 0, resultBlue = 0;

			byte newRedValue = filterData.NewColor.R;
			byte newGreenValue = filterData.NewColor.G;
			byte newBlueValue = filterData.NewColor.B;

			byte redFilter = filterData.SourceColor.R;
			byte greenFilter = filterData.SourceColor.G;
			byte blueFilter = filterData.SourceColor.B;

			byte minValue = 0;
			byte maxValue = 255;

			for (int k = 0; k < resultBuffer.Length; k += 4)
			{
				sourceAlpha = resultBuffer[k + 3];

				if (sourceAlpha != 0)
				{
					sourceBlue = resultBuffer[k];
					sourceGreen = resultBuffer[k + 1];
					sourceRed = resultBuffer[k + 2];

					if ((sourceBlue < blueFilter + filterData.ThresholdValue &&
						sourceBlue > blueFilter - filterData.ThresholdValue) &&

						(sourceGreen < greenFilter + filterData.ThresholdValue &&
							sourceGreen > greenFilter - filterData.ThresholdValue) &&

						(sourceRed < redFilter + filterData.ThresholdValue &&
							sourceRed > redFilter - filterData.ThresholdValue))
					{
						resultBlue = blueFilter - sourceBlue + newBlueValue;

						if (resultBlue > maxValue)
						{ resultBlue = maxValue; }
						else if (resultBlue < minValue)
						{ resultBlue = minValue; }

						resultGreen = greenFilter - sourceGreen + newGreenValue;

						if (resultGreen > maxValue)
						{ resultGreen = maxValue; }
						else if (resultGreen < minValue)
						{ resultGreen = minValue; }

						resultRed = redFilter - sourceRed + newRedValue;

						if (resultRed > maxValue)
						{ resultRed = maxValue; }
						else if (resultRed < minValue)
						{ resultRed = minValue; }

						resultBuffer[k] = (byte)resultBlue;
						resultBuffer[k + 1] = (byte)resultGreen;
						resultBuffer[k + 2] = (byte)resultRed;
						resultBuffer[k + 3] = sourceAlpha;
					}
				}
			}

			Marshal.Copy(resultBuffer, 0, resultData.Scan0, resultBuffer.Length);
			resultBitmap.UnlockBits(resultData);

			return resultBitmap;
		}

		public static Bitmap Format32bppArgbCopy(this Bitmap sourceBitmap)
		{
			Bitmap copyBitmap = new Bitmap(sourceBitmap.Width, sourceBitmap.Height, PixelFormat.Format32bppArgb);

			using (Graphics graphicsObject = Graphics.FromImage(copyBitmap))
			{
				graphicsObject.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
				graphicsObject.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
				graphicsObject.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
				graphicsObject.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

				graphicsObject.DrawImage(sourceBitmap, new System.Drawing.Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height), new System.Drawing.Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height), GraphicsUnit.Pixel);
			}

			return copyBitmap;
		}
		*/
	}

	public class ColorSubstitutionFilter2
	{
		private float thresholdValue = 0.1f;
		public float ThresholdValue
		{
			get { return thresholdValue; }
			set { thresholdValue = value; }
		}

		private Xwt.Drawing.Color sourceColor = Xwt.Drawing.Colors.White;
		public Xwt.Drawing.Color SourceColor
		{
			get { return sourceColor; }
			set { sourceColor = value; }
		}

		private Xwt.Drawing.Color newColor = Xwt.Drawing.Colors.White;
		public Xwt.Drawing.Color NewColor
		{
			get { return newColor; }
			set { newColor = value; }
		}
	}
}