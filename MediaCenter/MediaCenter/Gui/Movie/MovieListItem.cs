﻿using System;
using Xwt;
using Xwt.Drawing;
using MovieProvider.Model;
using MovieProvider;
using System.IO;
using System.Threading;

namespace MediaCenter
{

	class DownloadThread{
		private Thread t; 
		private String url;
		public EventHandler<EventArgs> Finished; 

		public DownloadThread(String url)
		{
			this.url = url;
			t = new Thread(new ThreadStart(download));
		}

		public void Start()
		{
			t.Start ();
		}

		public void Abort()
		{
			t.Abort();
		}

		public void Join()
		{
			t.Join ();
		}

		public Stream Stream{ get; set;}
		public Movie Movie{get;set;}

		private void download(){
			try{
			Console.WriteLine ("Downloading image " + url);
			StatefulHttpClient client = new StatefulHttpClient ();
			Stream = client.ReadGetResponse (url);
			if (Finished != null) {
				Console.WriteLine ("Image downloaded " + url);
				Finished (this, new EventArgs ());
			}
			}catch(Exception e){
				Console.WriteLine ("Errore in dowload immagine " + e);
			}
		}
	}

	public class MovieListItem:VBox
	{
		public event EventHandler<ButtonEventArgs> ItemClicked;
		private ImageView imageView;
		private Label titleLabel;
		private Label yearLabel;
		private int height=250;
		private int sectionHeight = 50;
		private int imageHeight=200;
		private int width=150;
		private Canvas imageContainer;
		private Canvas overlay;
		public Object Movie{ get; set; }
		public MovieListFromDatabase MovieList{ get; set; }
	 
		private DownloadThread downloadThread;

		public MovieListItem (Object amovie)
		{
			Movie movie = (Movie)amovie;
			Movie = movie;
			WidthRequest = width;
			MinWidth = width;
			MinHeight = height;
			HorizontalPlacement = WidgetPlacement.Center;
			VerticalPlacement = WidgetPlacement.Center;
			Spacing = 0;
			Cursor = CursorType.Hand;


			imageContainer = new Canvas();
			imageContainer.WidthRequest = width;
			imageContainer.HeightRequest =imageHeight;
			imageContainer.MouseEntered += delegate {
				addOverlay();
			};
			imageContainer.MouseExited += delegate {
				removeOverlay();
			};


		

			/*
			if (image != null) {
				imageView = new ImageView (image.WithSize(new Size(width,imageHeight)));

			} else {

			}
			*/
			imageView = new ImageView ();
			imageView.MinWidth = width;
			imageView.MinHeight = imageHeight;
			imageView.VerticalPlacement = WidgetPlacement.Center;
			imageView.HorizontalPlacement = WidgetPlacement.Center;
			imageContainer.AddChild (imageView);


			PackStart (imageContainer);


			Image image = null;


			if (movie.PosterUrl != null) {		
				downloadThread = new DownloadThread (movie.PosterUrl);
				 
				downloadThread.Finished += delegate {
					((MyBaseWindow)this.ParentWindow).Invoke(delegate {
						image = Image.FromStream (downloadThread.Stream);	
						imageView.Image = image.WithSize(new Size(width,imageHeight));
						imageView.WidthRequest = width;
						imageView.HeightRequest = imageHeight;
					});
					 
				};
				 
				downloadThread.Start ();
				 

			

			} 
 
			VBox container = new VBox ();
			container.Spacing = 0;
			container.Margin = new WidgetSpacing (0, 0, 0, 0);
			container.BackgroundColor =new Xwt.Drawing.Color(0.13,0.13,0.14);
			container.WidthRequest = width;
			container.HeightRequest = sectionHeight;
			PackStart (container);
		
			titleLabel = new Label ();
			titleLabel.Text = movie.Title;
			titleLabel.TextColor = Colors.White;
			titleLabel.Font = Font.WithWeight (FontWeight.Bold);
			titleLabel.Wrap = WrapMode.WordAndCharacter;			 
			container.PackStart (titleLabel);

			 

			yearLabel = new Label ();
			yearLabel.Text = movie.Year;
			yearLabel.TextColor = new Color (0.43, 0.43, 0.46);
			yearLabel.Font = Font.WithWeight (FontWeight.Bold).WithSize(11);
			yearLabel.Wrap = WrapMode.Character;
			yearLabel.Cursor = CursorType.Hand;
			container.PackEnd (yearLabel);
			 

		}

		private void addOverlay()
		{
			if (MovieList.HoverItem != null) {
				MovieList.HoverItem.removeOverlay ();
			}
			MovieList.HoverItem = this;
			if(overlay==null){
				overlay = new Canvas();
				overlay.ButtonPressed+= delegate(object sender, ButtonEventArgs e) {
					removeOverlay();
					if(ItemClicked!=null){
						ItemClicked(this,e);
					}
				};
				overlay.BackgroundColor = Colors.Gray.WithAlpha(0.4f);
				imageContainer.AddChild(overlay);
				imageContainer.SetChildBounds(overlay,new Rectangle(0,0,imageContainer.Bounds.Width,imageContainer.Bounds.Height));
				imageContainer.Cursor = CursorType.Hand;
			}

		}

		private void removeOverlay()
		{
			if(overlay!=null){
				imageContainer.RemoveChild(overlay);
				overlay.Dispose();
				overlay = null;
				MovieList.HoverItem = null;
			}
		}

		 
	}
}

