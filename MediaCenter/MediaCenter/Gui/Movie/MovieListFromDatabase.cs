﻿using System;
using Xwt;
 
using TMDbLib.Objects.General;
using System.Collections.Generic;
 
using Xwt.Drawing;
using System.Net;
using System.IO;
using SQLite;
using MovieProvider.Model;
using System.Threading;
using System.Runtime.CompilerServices;
using MovieProvider;


namespace MediaCenter
{


	public class MovieListFromDatabase:Table
	{
		 
		public event EventHandler<ItemSelectedEventArgs> ItemSelected;

		public MovieListItem HoverItem{ get; set;}
		private int itemsForRow = 3;
		private int pageSize = 10;
		private int currentPage=0;
		private String lastSearch="";
		private int currentX;
		private int currentY;
		private SQLiteConnection db;
		private MovieView movieView;

		public MovieListFromDatabase ()
		{
			db = new SQLiteConnection (AppDomain.CurrentDomain.BaseDirectory+"/db.data");

		 
			MinWidth = 300;
			MinHeight = 400;
			MarginLeft = 20;
			//MarginRight = 20;
			MarginTop = 40;
			MarginBottom = 20;
			DefaultRowSpacing = 5;
			DefaultColumnSpacing = 5;		 
			 
			Search ();			 
		}

		public MovieView MovieView
		{
			get{
				return movieView;
			}
			set{
				movieView = value;
				movieView.MouseScrolled+= delegate {
					Console.WriteLine("SCrolled");
				};

				movieView.VerticalScrollControl.ValueChanged+= delegate {
					if(movieView.Size.Height == movieView.VisibleRect.Height+movieView.VisibleRect.Y){
						currentPage++;
						Search(lastSearch,currentPage*pageSize,pageSize);

					}
					//Console.WriteLine("VChanged " + movieView.VerticalScrollControl.Value+" PAGESIZE "+movieView.VerticalScrollControl.PageSize+" this bounds " + +"/"+movieView.VisibleRect);
				
				};
			}
		}

		public void PrepareForAdd ()
		{		 
			currentPage = 0;
			currentX = 0;
			currentY = 0;
			Clear ();
		}

	
		public void Add (MovieListItem item)
		{			 
			if (currentX >= itemsForRow) {
				currentX = 0;
				currentY++;
			}
			this.Add (item, currentX++, currentY, 1, 1, false, false, WidgetPlacement.Center, WidgetPlacement.Center);
		}

		[MethodImpl (MethodImplOptions.Synchronized)]
		private void renderList (List<Movie> movies)
		{

			foreach (Movie movie in movies) {			 
				Add (CreateMovieListItem (movie));	
			}
		}

		 

		private MovieListItem CreateMovieListItem (Movie movie)
		{			 
			MovieListItem item = new MovieListItem (movie);
			item.MovieList = this;
			item.ItemClicked += delegate(object sender, ButtonEventArgs e) {
				if (ItemSelected != null) {
					MovieListItem senderItem = (MovieListItem)sender;
					ItemSelected (this, new ItemSelectedEventArgs{ Movie = senderItem.Movie });
				}
			};
			return item;
		}

		public void Search(){
			Search("");
		}

		public void Search(String text){
			PrepareForAdd ();
			Search(text,0,pageSize);
		}

		public void Search (String text,int fromIndex, int count)
		{
			//IEnumerator<Movie> enumerator = db.Query<Movie> ("select * from Movie where upper(Title) LIKE upper(?) or upper(Overview)  LIKE upper(?)and (select count(MovieId) from MovieFile where Message = 'OK'  and MovieId = _id  group by MovieId)>0 order by Year desc LIMIT "+count+" OFFSET "+fromIndex, new object[] {
			IEnumerator<Movie> enumerator = db.Query<Movie> ("select * from Movie, MovieFile where Movie._id = MovieFile.MovieId and (upper(Title) LIKE upper(?) or upper(Overview)  LIKE upper(?) ) and Moviefile.message = 'OK' and MovieFile.fileUrl is not null order by Movie.Year desc LIMIT "+count+" OFFSET "+fromIndex, new object[] {
				"%"+text+"%",
				"%"+text+"%"
			}).GetEnumerator ();
		 
		 
			List<Movie> movies = new List<Movie> ();
			while (enumerator.MoveNext ()) {
				Movie movie = enumerator.Current;
				IEnumerator<MovieFile> enumeratorFile = db.Query<MovieFile> ("select * from MovieFile where MovieId = ? and Moviefile.message = 'OK' and MovieFile.fileUrl is not null", new object[] {
					movie.Id					 
				}).GetEnumerator ();
				movie.MovieFiles = new List<MovieFile> ();
				while (enumeratorFile.MoveNext ()) {
					movie.MovieFiles.Add (enumeratorFile.Current);
				}
				movies.Add (movie);

			}	
 			renderList (movies);
			lastSearch = text;
		}

		public void VisibleRectChanged (Size size)
		{
			Console.WriteLine ("CHANGED" + size);
			if (size.Width > (DefaultColumnSpacing + 150) * (itemsForRow + 1) + MarginLeft + DefaultColumnSpacing + 10) {
				itemsForRow++;
				reposition ();

			} else if (size.Width < (DefaultColumnSpacing + 150) * (itemsForRow) + MarginLeft + DefaultColumnSpacing + 10) {
				if (itemsForRow > 3) {
					itemsForRow--;
					reposition ();
				}

			}
		}

	 

		private void reposition ()
		{
			IEnumerator<Widget> en = this.Children.GetEnumerator ();
			List<Widget> children = new List<Widget> ();
			while (en.MoveNext ()) {
				children.Add (en.Current);
			}
			this.Clear ();
			en = children.GetEnumerator ();
			for (int y = 0; y < 100; y++) {
				for (int x = 0; x < itemsForRow; x++) {
					if (!en.MoveNext ()) {
						return;
					}
					this.Add (en.Current, x, y, 1, 1, false, false, WidgetPlacement.Center, WidgetPlacement.Center);
				}
			}
		}
	}
}

