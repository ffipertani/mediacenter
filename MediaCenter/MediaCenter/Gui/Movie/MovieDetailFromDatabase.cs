﻿using System;
using Xwt;
using MovieProvider.Model;
using Xwt.Drawing;
using System.Net;
using System.Collections;
using System.IO;
using MovieProvider.Extractor;
using CreateCatalog;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Movies;

namespace MediaCenter
{
	public class StartPlayEventArgs
	{
		public String Url{get;set;}
	}

	public class MovieDetailFromDatabase:Canvas
	{
		public event EventHandler<StartPlayEventArgs> StartPlayClicked;

		private ImageView backdropView;
		private ImageView posterView;
		private Canvas overlay;
		private Canvas contentOverlay;
		private MovieProvider.Model.Movie movie;
		private Image originalBackdrop;
		private Label title;
		private Label year;
		private HBox titleBox;
		private Label labelDuration;
		private VBox contentBox;
		private Label labelGenre;
		private VBox movieKeyVals;
		private TMDbLib.Objects.Movies.Movie tmovie;
		private TMDbFinder finder = new TMDbFinder();
			 

		public MovieDetailFromDatabase ()
		{
				 
			ExpandHorizontal = true;
		}

		private void createBackground ()
		{
			overlay = new Canvas ();
			overlay.BackgroundColor = new Xwt.Drawing.Color (0.16, 0.16, 0.17).WithAlpha (0.4);
			contentOverlay = new Canvas ();
			contentOverlay.BackgroundColor = new Xwt.Drawing.Color (0.11, 0.11, 0.12).WithAlpha (0.4);
			Image imgBackdrop = null;
				 			 
		 
		 
			if (tmovie.BackdropPath != null) {
				originalBackdrop = finder.DownloadImage(tmovie.BackdropPath);			
				imgBackdrop = originalBackdrop.WithSize (this.Bounds.Size);
			} else {				 
				if (movie.PosterUrl != null) {
					originalBackdrop = downloadImage (movie.PosterUrl);	
					imgBackdrop = originalBackdrop.WithSize (this.Bounds.Size);
				} 
			}


			backdropView = new ImageView (imgBackdrop);


			backdropView = new ImageView (imgBackdrop);
			backdropView.VerticalPlacement = WidgetPlacement.Center;
			backdropView.HorizontalPlacement = WidgetPlacement.Center;
			AddChild (backdropView);
			SetChildBounds (backdropView, this.Bounds);
			AddChild (overlay);			 
			AddChild (contentOverlay);
			resizeBackdrop ();
			resizeOverlays ();
		}


		private void resizeOverlays ()
		{
			SetChildBounds (overlay, this.Bounds);
			SetChildBounds (contentOverlay, new Rectangle (0, 140, this.Bounds.Width, this.Bounds.Height - 140));
		}

		private void resizeBackdrop ()
		{
			if (originalBackdrop == null) {
				return;
			}
			Size backdropSize = new Size ();

			double widthProportion = this.Bounds.Width / originalBackdrop.Width;
			double heightProportion = this.Bounds.Height / originalBackdrop.Height;
			if (widthProportion > heightProportion) {
				backdropSize.Height = originalBackdrop.Height * widthProportion;
				backdropSize.Width = originalBackdrop.Width * widthProportion;			 
			} else {
				backdropSize.Height = originalBackdrop.Height * heightProportion;
				backdropSize.Width = originalBackdrop.Width * heightProportion;
			}

			backdropView.Image = backdropView.Image.WithSize (backdropSize);

		}

		private void createPoster ()
		{
			Image img = null;
			if (movie.PosterUrl != null) {
				img = downloadImage (movie.PosterUrl);					 					
			} 
			posterView = new ImageView (img.WithSize (new Size (250, 350)));
			posterView.WidthRequest = 250;
			posterView.HeightRequest = 3500;
			posterView.MinWidth = 250;
			posterView.MinHeight = 350;

			posterView.MarginTop = 100;
			posterView.ExpandVertical = false;
			posterView.ExpandHorizontal = false;
			posterView.HorizontalPlacement = WidgetPlacement.Start;
			posterView.VerticalPlacement = WidgetPlacement.Start;
			posterView.MarginLeft = 0;

			AddChild (posterView);
			Rectangle bounds = new Rectangle (20, 80, 250, 350);
			SetChildBounds (posterView, bounds);


			if (movie.MovieFiles.Count > 0) {
				Button b = new Button ("Guarda");
				b.Clicked += delegate {
					if (StartPlayClicked != null) {
						bool handled = false;
						foreach(MovieFile mf in movie.MovieFiles){
							SpeedVideoUrlExtractor ex = new SpeedVideoUrlExtractor();
							if(ex.CanHandle(mf.ProviderUrl)){
								handled = true;
								StartPlayClicked (this, new StartPlayEventArgs{ Url = ex.ExtractUrl(mf.ProviderUrl) });
							}
						}

						if(!handled){
							StartPlayClicked (this, new StartPlayEventArgs{ Url = movie.MovieFiles[0].FileUrl });
						}
					}			
				};
				AddChild (b);
				SetChildBounds (b, new Rectangle (50, 450, 100, 40));
			}
		}

		private void createTitleView ()
		{

			titleBox = new HBox ();
			titleBox.VerticalPlacement = WidgetPlacement.Start;
			titleBox.HorizontalPlacement = WidgetPlacement.Start;
			titleBox.ExpandVertical = false;
			titleBox.ExpandHorizontal = false;

			AddChild (titleBox);


			title = new MyLabel ();
			title.MarginLeft = 10;
			title.Text = movie.Title;
			title.TextColor = Colors.White;
			title.Font = Font.WithSize (28).WithWeight (FontWeight.Light);			
			title.VerticalPlacement = WidgetPlacement.Start;
			title.HorizontalPlacement = WidgetPlacement.Start;
			titleBox.PackStart (title);

			 
			year = new MyLabel ();
			year.Text = movie.Year;
			year.MarginRight = 20;
			year.VerticalPlacement = WidgetPlacement.Start;
			year.HorizontalPlacement = WidgetPlacement.Start;
			year.TextColor = Colors.LightGray;
			year.Font = Font.WithSize (28).WithWeight (FontWeight.Light);
			titleBox.PackEnd (year);

			resizeTitleView ();
		}

		private void createMovieInfo ()
		{
			contentBox = new VBox ();

			HBox durationBox = new HBox ();
			durationBox.VerticalPlacement = WidgetPlacement.Start;
			durationBox.HorizontalPlacement = WidgetPlacement.Fill;


			contentBox.PackStart (durationBox);
			durationBox.ExpandHorizontal = false;
			durationBox.ExpandVertical = false;
			labelDuration = new Label ();


			labelDuration.Text = "1 ora 30 minuti";
			labelDuration.MarginTop = 20;
			labelDuration.MarginLeft = 20;
			labelDuration.VerticalPlacement = WidgetPlacement.Start;
			labelDuration.HorizontalPlacement = WidgetPlacement.Start;
			labelDuration.ExpandHorizontal = false;
			labelDuration.ExpandVertical = false;
			labelDuration.TextColor = Colors.WhiteSmoke;
			labelDuration.Font = Font.WithSize (18).WithWeight (FontWeight.Light);
			durationBox.PackStart (labelDuration);


			labelGenre = new Label ();
			labelGenre.MarginTop = 20;
			labelGenre.MarginRight = 20;
			labelGenre.VerticalPlacement = WidgetPlacement.Start;
			labelGenre.HorizontalPlacement = WidgetPlacement.Start;
			labelGenre.ExpandHorizontal = false;
			labelGenre.ExpandVertical = false;			 
		 	labelGenre.Text = movie.Genre;
			labelGenre.TextColor = Colors.WhiteSmoke;
			labelGenre.Font = Font.WithSize (18).WithWeight (FontWeight.Light);
			durationBox.PackEnd (labelGenre);

			createCrew ();

			AddChild (contentBox);
			resizeMovieInfo ();

		}

		private void createCrew()
		{
			//movieKeyVals = new VBox ();
			//movieKeyVals.WidthRequest = this.Bounds.Width - 270;


			String registaStr = "";
			String autoreStr = "";
			String produttoreStr = "";
			String musicaStr = "";
			String direttoreFoto = "";
			String editoreStr = "";
			String artDirectoStr = "";
			foreach (Crew c in tmovie.Credits.Crew) {
				if (c.Job.Equals ("Director")) {
					registaStr += c.Name + ", ";
				}else if (c.Job.Equals ("Producer")) {
					produttoreStr += c.Name + ", ";
				}else if (c.Job.Equals ("Music")) {
					musicaStr += c.Name + ", ";
				}else if (c.Job.Equals ("Director of Photography")) {
					direttoreFoto += c.Name + ", ";
				}else if (c.Job.Equals ("Screenplay")||c.Job.Equals ("Writer")) {
					autoreStr += c.Name + ", ";
				}else if (c.Job.Equals ("Editor")) {
					editoreStr += c.Name + ", ";
				}else if (c.Job.Equals ("Art Director")) {
					artDirectoStr += c.Name + ", ";
				} else {
					Console.WriteLine ("Attribute not managed: " + c.Job);
				}
			}

			if (!registaStr.Equals ("")) {
				registaStr = removeLastComma (registaStr);
				MovieKeyVal regista = new MovieKeyVal ("Regista", registaStr);
				regista.MarginTop = 30;
				contentBox.PackStart (regista);
			}
			if (!autoreStr.Equals ("")) {
				autoreStr = removeLastComma (autoreStr);
				MovieKeyVal autore = new MovieKeyVal ("Sceneggiatore", autoreStr);				 
				contentBox.PackStart (autore);
			}
			if (!produttoreStr.Equals ("")) {
				produttoreStr = removeLastComma (produttoreStr);
				MovieKeyVal produttore = new MovieKeyVal ("Produttore", produttoreStr);				 
				contentBox.PackStart (produttore);
			}
			if (!musicaStr.Equals ("")) {
				musicaStr = removeLastComma (musicaStr);
				MovieKeyVal musica = new MovieKeyVal ("Musica", musicaStr);				 
				contentBox.PackStart (musica);
			}
			if (!direttoreFoto.Equals ("")) {
				direttoreFoto = removeLastComma (direttoreFoto);
				MovieKeyVal foto = new MovieKeyVal ("Direttore fotografia", direttoreFoto);				 
				contentBox.PackStart (foto);
			}
			if (!editoreStr.Equals ("")) {
				editoreStr = removeLastComma (editoreStr);
				MovieKeyVal foto = new MovieKeyVal ("Editore", editoreStr);				 
				contentBox.PackStart (foto);
			}
			if (!artDirectoStr.Equals ("")) {
				artDirectoStr = removeLastComma (artDirectoStr);
				MovieKeyVal foto = new MovieKeyVal ("Editore", artDirectoStr);				 
				contentBox.PackStart (foto);
			}


			String castStr = "";

			foreach (Cast c in tmovie.Credits.Cast) {
				castStr += c.Name + ", ";			 
			}

			if (!castStr.Equals ("")) {
				castStr = removeLastComma (castStr);
				MovieKeyVal cast = new MovieKeyVal ("Cast", castStr);			
				contentBox.PackStart (cast);
			}


		}

		private void createDescription ()
		{
			Label overview = new Label ();
			overview.VerticalPlacement = WidgetPlacement.Start;
			overview.VerticalPlacement = WidgetPlacement.Start;
			overview.MarginLeft = 20;
			overview.MarginRight = 20;
			overview.MarginTop = 20;
			overview.Text = movie.Overview;
			overview.Font = Font.WithSize (14);
			overview.TextColor = Colors.White;
			overview.Wrap = WrapMode.WordAndCharacter;
			contentBox.PackStart (overview);
		}




		private void updateTextChildren (Box box)
		{
			if (box == null || box.Children == null) {
				return;
			}
			IEnumerator en = box.Children.GetEnumerator ();
			while (en.MoveNext ()) {
				Widget current = (Widget)en.Current;
				if (current is MovieKeyVal) {
					updateTextChildren ((MovieKeyVal)current);
				} else if (current is Label) {
					((Label)current).Text = ((Label)current).Text;
				} else if (current is Box) {
					updateTextChildren ((Box)current);
				} else if (current is Canvas) {
					updateTextChildren ((Canvas)current);
				}
			}
		}

		private void updateTextChildren (Canvas canvas)
		{
			if (canvas == null || canvas.Children == null) {
				return;
			}
			IEnumerator en = canvas.Children.GetEnumerator ();
			while (en.MoveNext ()) {
				Widget current = (Widget)en.Current;
				if (current is MovieKeyVal) {
					updateTextChildren ((MovieKeyVal)current);
				} else if (current is Label) {
					((Label)current).Text = ((Label)current).Text;
				} else if (current is Box) {
					updateTextChildren ((Box)current);
				} else if (current is Canvas) {
					updateTextChildren ((Canvas)current);
				} 
			}
		}

		private void updateTextChildren (MovieKeyVal item)
		{
			if (item == null) {
				return;
			}
			//item.WidthRequest = this.Bounds.Width - 270;
			item.labelVal.WidthRequest = Bounds.Width / 2;
			item.labelVal.Text = item.labelVal.Text;
			item.labelKey.Text = item.labelKey.Text;

		}


		private String removeLastComma (String str)
		{
			if (str.EndsWith (", ")) {
				str = str.Substring (0, str.Length - 2);
			}
			return str;
		}

		private void resizeMovieInfo ()
		{
			SetChildBounds (contentBox, new Rectangle (20 + 250, 140, this.Bounds.Width - 270, this.Bounds.Height - 140));
		}

		private void resizeTitleView ()
		{		
			SetChildBounds (titleBox, new Rectangle (20 + 250, 100, this.Bounds.Width - 270, 40));
		}


		public MovieProvider.Model.Movie Movie {
			get {
				return movie;
			}
			set {
				this.movie = value;
				this.Clear ();
				String imdb = finder.FindImdb (movie);
				if (imdb != null) {
					tmovie = finder.FindByImdb (imdb);
				} else {
					tmovie = null;
				}

				this.BoundsChanged += delegate {
					resizeBackdrop ();

					resizeTitleView ();

					resizeOverlays ();
					resizeMovieInfo ();
					updateTextChildren (this);
				};

				createBackground ();

				createPoster ();

				createTitleView ();		
				createMovieInfo ();
				createDescription ();
				updateTextChildren (this);
			}
		}

		private Image downloadImage (String url)
		{
			HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create (url); 							 
			HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse (); 							 
			Stream receiveStream = myHttpWebResponse.GetResponseStream ();
			return Image.FromStream (receiveStream);							 					
		}

	
	}


}

