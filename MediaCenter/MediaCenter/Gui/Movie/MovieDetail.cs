﻿using System;
using TMDbLib.Objects.Movies;
using Xwt;
using TMDbLib.Client;
using Xwt.Drawing;
using System.Net;
using System.IO;
using TMDbLib.Objects.General;
using System.Collections;

namespace MediaCenter
{
	public class MovieDetail:Canvas
	{
		private ImageView backdropView;
		private ImageView posterView;
		private Canvas overlay;
		private Canvas contentOverlay;
		private Movie movie;
		private Image originalBackdrop;
		private Label title;
		private Label year;
		private HBox titleBox;
		private Label labelDuration;
		private VBox contentBox;
		private Label labelGenre ;
		private VBox movieKeyVals;


		private TMDbClient client = new TMDbClient ("401e6b7de800347b1775291af806724d");

		public MovieDetail ()
		{
			client.DefaultCountry = "it";
			client.DefaultLanguage = "it";

			//		BackgroundColor =  new Xwt.Drawing.Color(0.06,0.06,0.08);
			ExpandHorizontal = true;

		
		 

		}

		private void createBackground()
		{
			overlay = new Canvas ();
			overlay.BackgroundColor = new Xwt.Drawing.Color(0.16,0.16,0.17).WithAlpha (0.4);
			contentOverlay = new Canvas ();
			contentOverlay.BackgroundColor = new Xwt.Drawing.Color(0.11,0.11,0.12).WithAlpha (0.4);
			Image imgBackdrop = null;
			if (movie.BackdropPath != null) {
				originalBackdrop = downloadImage (movie.BackdropPath);			
				imgBackdrop = originalBackdrop.WithSize (this.Bounds.Size);
			} else {				 
				if (movie.PosterPath != null) {
					originalBackdrop = downloadImage (movie.PosterPath);	
					imgBackdrop = originalBackdrop.WithSize (this.Bounds.Size);
				} 
			}


			backdropView = new ImageView (imgBackdrop);
			backdropView.VerticalPlacement = WidgetPlacement.Center;
			backdropView.HorizontalPlacement = WidgetPlacement.Center;
			AddChild (backdropView);
			SetChildBounds (backdropView, this.Bounds);
			AddChild (overlay);			 
			AddChild (contentOverlay);
			resizeBackdrop ();
			resizeOverlays ();
		}

		 
		private void resizeOverlays()
		{
			SetChildBounds (overlay, this.Bounds);
			SetChildBounds (contentOverlay, new Rectangle(0,140,this.Bounds.Width,this.Bounds.Height-140));
		}

		private void resizeBackdrop()
		{
			if (originalBackdrop == null) {
				return;
			}
			Size backdropSize = new Size ();

			double widthProportion = this.Bounds.Width / originalBackdrop.Width;
			double heightProportion = this.Bounds.Height / originalBackdrop.Height;
			if (widthProportion > heightProportion) {
				backdropSize.Height = originalBackdrop.Height * widthProportion;
				backdropSize.Width = originalBackdrop.Width * widthProportion;			 
			} else {
				backdropSize.Height = originalBackdrop.Height * heightProportion;
				backdropSize.Width = originalBackdrop.Width * heightProportion;
			}

			backdropView.Image = backdropView.Image.WithSize (backdropSize);

		}

		private void createPoster()
		{
			Image img = null;
			if (movie.PosterPath != null) {
				img = downloadImage (movie.PosterPath);					 					
			} 
			posterView = new ImageView (img.WithSize (new Size (250, 350)));
			posterView.WidthRequest = 250;
			posterView.HeightRequest = 3500;
			posterView.MinWidth = 250;
			posterView.MinHeight = 350;

			posterView.MarginTop = 100;
			posterView.ExpandVertical = false;
			posterView.ExpandHorizontal = false;
			posterView.HorizontalPlacement = WidgetPlacement.Start;
			posterView.VerticalPlacement = WidgetPlacement.Start;
			posterView.MarginLeft = 0;
			AddChild (posterView);
			Rectangle bounds = new Rectangle (20, 80, 250, 350);
			SetChildBounds (posterView, bounds);

		}

		private void createTitleView()
		{

			titleBox = new HBox ();
			titleBox.VerticalPlacement = WidgetPlacement.Start;
			titleBox.HorizontalPlacement = WidgetPlacement.Start;
			titleBox.ExpandVertical = false;
			titleBox.ExpandHorizontal = false;
		
			AddChild (titleBox);


			title = new MyLabel ();
			title.MarginLeft = 10;
			title.Text = movie.Title;
			title.TextColor = Colors.White;
			title.Font = Font.WithSize (28).WithWeight (FontWeight.Light);			
			title.VerticalPlacement = WidgetPlacement.Start;
			title.HorizontalPlacement = WidgetPlacement.Start;
			titleBox.PackStart (title);

			String date = "";
			if (movie.ReleaseDate != null) {
				date = movie.ReleaseDate.Value.Year + "";
			}
			year = new MyLabel ();
			year.Text = date;
			year.MarginRight = 20;
			year.VerticalPlacement = WidgetPlacement.Start;
			year.HorizontalPlacement = WidgetPlacement.Start;
			year.TextColor = Colors.LightGray;
			year.Font = Font.WithSize (28).WithWeight (FontWeight.Light);
			titleBox.PackEnd (year);

			resizeTitleView ();
		}

		private void createMovieInfo()
		{
			contentBox = new VBox ();

			HBox durationBox = new HBox ();
			durationBox.VerticalPlacement = WidgetPlacement.Start;
			durationBox.HorizontalPlacement = WidgetPlacement.Fill;


			contentBox.PackStart (durationBox);
			durationBox.ExpandHorizontal = false;
			durationBox.ExpandVertical = false;
			labelDuration = new Label ();


			labelDuration.Text = "1 ora 30 minuti";
			labelDuration.MarginTop = 20;
			labelDuration.MarginLeft = 20;
			labelDuration.VerticalPlacement = WidgetPlacement.Start;
			labelDuration.HorizontalPlacement = WidgetPlacement.Start;
			labelDuration.ExpandHorizontal = false;
			labelDuration.ExpandVertical = false;
			labelDuration.TextColor = Colors.WhiteSmoke;
			labelDuration.Font = Font.WithSize (18).WithWeight (FontWeight.Light);
			durationBox.PackStart (labelDuration);


		    labelGenre = new Label ();
			labelGenre.MarginTop = 20;
			labelGenre.MarginRight = 20;
			labelGenre.VerticalPlacement = WidgetPlacement.Start;
			labelGenre.HorizontalPlacement = WidgetPlacement.Start;
			labelGenre.ExpandHorizontal = false;
			labelGenre.ExpandVertical = false;
			String genres = "";
			foreach (Genre g in movie.Genres) {
				genres += g.Name + ", ";
			}
			genres = removeLastComma (genres);
			labelGenre.Text = genres;
			labelGenre.TextColor = Colors.WhiteSmoke;
			labelGenre.Font = Font.WithSize (18).WithWeight (FontWeight.Light);
			durationBox.PackEnd (labelGenre);

			createCrew ();


			AddChild (contentBox);
			resizeMovieInfo ();




		}

		private void createCrew()
		{
			//movieKeyVals = new VBox ();
			//movieKeyVals.WidthRequest = this.Bounds.Width - 270;


			String registaStr = "";
			String autoreStr = "";
			String produttoreStr = "";
			String musicaStr = "";
			String direttoreFoto = "";
			String editoreStr = "";
			String artDirectoStr = "";
			foreach (Crew c in movie.Credits.Crew) {
				if (c.Job.Equals ("Director")) {
					registaStr += c.Name + ", ";
				}else if (c.Job.Equals ("Producer")) {
					produttoreStr += c.Name + ", ";
				}else if (c.Job.Equals ("Music")) {
					musicaStr += c.Name + ", ";
				}else if (c.Job.Equals ("Director of Photography")) {
					direttoreFoto += c.Name + ", ";
				}else if (c.Job.Equals ("Screenplay")||c.Job.Equals ("Writer")) {
					autoreStr += c.Name + ", ";
				}else if (c.Job.Equals ("Editor")) {
					editoreStr += c.Name + ", ";
				}else if (c.Job.Equals ("Art Director")) {
					artDirectoStr += c.Name + ", ";
				} else {
					Console.WriteLine ("Attribute not managed: " + c.Job);
				}
			}

			if (!registaStr.Equals ("")) {
				registaStr = removeLastComma (registaStr);
				MovieKeyVal regista = new MovieKeyVal ("Regista", registaStr);
				regista.MarginTop = 30;
				contentBox.PackStart (regista);
			}
			if (!autoreStr.Equals ("")) {
				autoreStr = removeLastComma (autoreStr);
				MovieKeyVal autore = new MovieKeyVal ("Sceneggiatore", autoreStr);				 
				contentBox.PackStart (autore);
			}
			if (!produttoreStr.Equals ("")) {
				produttoreStr = removeLastComma (produttoreStr);
				MovieKeyVal produttore = new MovieKeyVal ("Produttore", produttoreStr);				 
				contentBox.PackStart (produttore);
			}
			if (!musicaStr.Equals ("")) {
				musicaStr = removeLastComma (musicaStr);
				MovieKeyVal musica = new MovieKeyVal ("Musica", musicaStr);				 
				contentBox.PackStart (musica);
			}
			if (!direttoreFoto.Equals ("")) {
				direttoreFoto = removeLastComma (direttoreFoto);
				MovieKeyVal foto = new MovieKeyVal ("Direttore fotografia", direttoreFoto);				 
				contentBox.PackStart (foto);
			}
			if (!editoreStr.Equals ("")) {
				editoreStr = removeLastComma (editoreStr);
				MovieKeyVal foto = new MovieKeyVal ("Editore", editoreStr);				 
				contentBox.PackStart (foto);
			}
			if (!artDirectoStr.Equals ("")) {
				artDirectoStr = removeLastComma (artDirectoStr);
				MovieKeyVal foto = new MovieKeyVal ("Editore", artDirectoStr);				 
				contentBox.PackStart (foto);
			}


			String castStr = "";

			foreach (Cast c in movie.Credits.Cast) {
				castStr += c.Name + ", ";			 
			}

			if (!castStr.Equals ("")) {
				castStr = removeLastComma (castStr);
				MovieKeyVal cast = new MovieKeyVal ("Cast", castStr);			
				contentBox.PackStart (cast);
			}

			 
		}

		private void createDescription()
		{
			Label overview = new Label ();
			overview.VerticalPlacement = WidgetPlacement.Start;
			overview.VerticalPlacement = WidgetPlacement.Start;
			overview.MarginLeft = 20;
			overview.MarginRight = 20;
			overview.MarginTop = 20;
			overview.Text = movie.Overview;
			overview.Font = Font.WithSize (14);
			overview.TextColor = Colors.White;
			overview.Wrap = WrapMode.WordAndCharacter;
			contentBox.PackStart (overview);
		}


		 

		private void updateTextChildren(Box box)
		{
			if (box == null || box.Children == null) {
				return;
			}
			IEnumerator en = box.Children.GetEnumerator ();
			while (en.MoveNext ()) {
				Widget current = (Widget) en.Current;
				if (current is MovieKeyVal) {
					updateTextChildren ((MovieKeyVal)current);
				}else if (current is Label) {
					((Label)current).Text = ((Label)current).Text;
				} else if (current is Box) {
					updateTextChildren ((Box)current);
				}else if (current is Canvas) {
					updateTextChildren ((Canvas)current);
				}
			}
		}

		private void updateTextChildren(Canvas canvas)
		{
			if (canvas == null || canvas.Children == null) {
				return;
			}
			IEnumerator en = canvas.Children.GetEnumerator ();
			while (en.MoveNext ()) {
				Widget current = (Widget) en.Current;
				if (current is MovieKeyVal) {
					updateTextChildren ((MovieKeyVal)current);
				}else if (current is Label) {
					((Label)current).Text = ((Label)current).Text;
				} else if (current is Box) {
					updateTextChildren ((Box)current);
				} else if (current is Canvas) {
					updateTextChildren ((Canvas)current);
				} 
			}
		}

		private void updateTextChildren(MovieKeyVal item)
		{
			if (item == null) {
				return;
			}
			//item.WidthRequest = this.Bounds.Width - 270;
			item.labelVal.WidthRequest = Bounds.Width / 2;
			item.labelVal.Text = item.labelVal.Text;
			item.labelKey.Text = item.labelKey.Text;
				 
		}


		private String removeLastComma(String str)
		{
			if(str.EndsWith(", ")){
				str = str.Substring(0,str.Length-2);
			}
			return str;
		}

		private void resizeMovieInfo()
		{
			SetChildBounds(contentBox,new Rectangle(20+250,140,this.Bounds.Width-270,this.Bounds.Height-140));
		}

		private void resizeTitleView()
		{		
			SetChildBounds(titleBox,new Rectangle(20+250,100,this.Bounds.Width-270,40));
		}


		public Movie Movie {
			get {
				return movie;
			}
			set {
				this.movie = value;
				this.Clear ();
			
				this.BoundsChanged += delegate {
					resizeBackdrop();

					resizeTitleView();
									 
					resizeOverlays();
					resizeMovieInfo();
					updateTextChildren (this);
				};

				createBackground ();
			 
				createPoster ();

				createTitleView ();		
				createMovieInfo ();
				createDescription ();
				updateTextChildren (this);
			}
		}

		private Image downloadImage (String url)
		{
			client.GetConfig ();
			String path = client.GetImageUrl (client.Config.Images.PosterSizes [5], url).ToString ();
			HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create (path); 							 
			HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse (); 							 
			Stream receiveStream = myHttpWebResponse.GetResponseStream ();
			return Image.FromStream (receiveStream);							 					
		}
	}

	class MovieKeyVal:HBox
	{
		public Label labelKey;
		public Label labelVal;
		private String key;
		private String val;

		public MovieKeyVal(String key,String val)
		{
			this.key = key;
			this.val = val;

			labelKey = new Label ();
			labelKey.VerticalPlacement = WidgetPlacement.Start;
			labelKey.HorizontalPlacement = WidgetPlacement.Start;
			labelKey.Text = key.ToUpper()+" ";
			labelKey.Font = Font.WithSize (14);
			labelKey.TextColor = Colors.LightGray;
			labelKey.Wrap = WrapMode.Word;
			PackStart (labelKey);

			labelVal = new Label ();
			labelVal.VerticalPlacement = WidgetPlacement.Start;
			labelVal.HorizontalPlacement = WidgetPlacement.Start;

			labelVal.Text = val+" ";
			labelVal.Font = Font.WithSize (14);
			labelVal.TextColor = Colors.White;
			labelVal.Wrap = WrapMode.WordAndCharacter;
			PackStart (labelVal);

			MarginLeft = 20;
		}
	}
}

