﻿using System;
using Xwt;
using Xwt.Drawing;

namespace MediaCenter
{
	public class HorizonalMenuItem:VBox
	{
		Label tentry;
		private Boolean selected = false;
		Canvas canvaSelected;
		String text;
		public HorizonalMenuItem (String text)
		{
			this.text = text;
			Initialize ();
		}


		protected virtual void Initialize(){
			Spacing = 0;
			Margin = new WidgetSpacing (0, 0, 0, 0);		
			tentry = new Label ();

			tentry.Margin = new WidgetSpacing (0, 0, 0, 0);
			tentry.ExpandVertical = false;
			tentry.ExpandHorizontal = false;
			tentry.Text = text.ToUpper();
			tentry.MinWidth = 50;
			tentry.TextColor = new Color (0.43, 0.43, 0.46);
			tentry.Font = tentry.Font.WithSize (15).WithWeight(FontWeight.Bold).WithStyle(FontStyle.Normal);

			tentry.ButtonPressed += OnButtonPressedOnChild;

			tentry.MouseEntered += delegate {

				Cursor = CursorType.Hand;
				tentry.TextColor = Colors.White;
			};
			tentry.MouseExited += delegate {
				if(!selected){
					tentry.TextColor = new Color (0.43, 0.43, 0.46);
				}
			};
			//tentry.BackgroundColor = Colors.Red;
			tentry.MarginTop = 15;
			tentry.MarginBottom = 8;
			tentry.MarginRight = 20;
			PackStart (tentry);

			canvaSelected = new Canvas ();
			canvaSelected.MinHeight = 1;
			canvaSelected.MarginRight = 20;
			canvaSelected.ButtonPressed += OnButtonPressedOnChild;

			PackStart (canvaSelected);
		}

		private void OnButtonPressedOnChild(object sender, ButtonEventArgs e){
			OnButtonPressed (e);
		}

		private void selectItem()
		{
			canvaSelected.BackgroundColor = Colors.Yellow;
			tentry.TextColor = Colors.White;
		}

		private void deselectItem()
		{		
			canvaSelected.BackgroundColor = Colors.Transparent;
			tentry.TextColor = new Color (0.43, 0.43, 0.46);
		}

		public Boolean Selected {
			get {
				return selected;
			}
			set{
				selected = value;
				if (selected) {
					selectItem ();
				} else {
					deselectItem ();
				}
			}
		}
	}
}

