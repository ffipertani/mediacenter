﻿using System;
using Xwt;
using TMDbLib.Client;
using TMDbLib.Objects.General;
using System.Collections.Generic;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.Search;
using Xwt.Drawing;
using System.Net;
using System.IO;
using SQLite;

namespace MediaCenter
{
	public class ItemSelectedEventArgs:EventArgs
	{
		public Object Movie{get;set;}
	}
	public class MovieList:Table
	{
		TMDbClient client = new TMDbClient("401e6b7de800347b1775291af806724d");
		public event EventHandler<ItemSelectedEventArgs> ItemSelected;

		private int itemsForRow=3;
		int added = 0;
		private int currentX;
		private int currentY;
	 
		public MovieList ()
		{
			client.DefaultCountry = "it";
			client.DefaultLanguage = "it";

		 
			MinWidth = 300;
			MinHeight = 400;
			MarginLeft = 20;
			//MarginRight = 20;
			MarginTop = 40;
			MarginBottom = 20;
			DefaultRowSpacing = 5;
			DefaultColumnSpacing = 5;		 
			 

			List<int> movies = getMovies ();
			while (added-- > 0) {
				MovieListItem item = CreateMovieListItem (movies [added]);
				if (item == null) {
					continue;
				}
				Add(item);
			}
			 
					 
			 
		}

		public void PrepareForAdd()
		{
			currentX = 0;
			currentY = 0;
			Clear ();
		}


		public void Add(MovieListItem item)
		{			 
			if (currentX >= itemsForRow) {
				currentX = 0;
				currentY++;
			}
			this.Add (item, currentX++, currentY,1,1,false,false,WidgetPlacement.Center,WidgetPlacement.Center);
		}

		private MovieListItem CreateMovieListItem(int movieId)
		{
			Movie movie = client.GetMovie(movieId,MovieMethods.Credits|MovieMethods.Changes|MovieMethods.Images|MovieMethods.Keywords|MovieMethods.Lists|MovieMethods.Releases|MovieMethods.SimilarMovies|MovieMethods.Translations);
			if (movie == null) {
				return null;
			}

			Image img=null;
			if (movie.PosterPath!=null) {

				client.GetConfig ();
				String path = client.GetImageUrl (client.Config.Images.PosterSizes[1], movie.PosterPath).ToString ();
				HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create (path); 							 
				HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse (); 							 
				Stream receiveStream = myHttpWebResponse.GetResponseStream ();
				img = Image.FromStream (receiveStream);							 					
			} 
			String date = "";
			if (movie.ReleaseDate != null) {
				date = movie.ReleaseDate.Value.Year + "";
			}
			MovieListItem item = new MovieListItem (movie);
			item.ItemClicked+= delegate(object sender, ButtonEventArgs e) {
				if(ItemSelected!=null){
					MovieListItem senderItem = (MovieListItem)sender;
					ItemSelected(this,new ItemSelectedEventArgs{Movie = senderItem.Movie});
				}
			};

			return item;
		}

		public void Search(String text)
		{
			SearchContainer<SearchMovie> mr = client.SearchMovie (text, 0, true);
			PrepareForAdd ();
			foreach (SearchMovie sm in mr.Results) {
				Console.WriteLine ("Found movie:" + sm.OriginalTitle);
				Add(CreateMovieListItem(sm.Id));
			//	addMovieListItem(createMovieListItem(sm.Id,	 					
			}							 
		}

		public void VisibleRectChanged(Size size)
		{
			Console.WriteLine("CHANGED"+size);
			if(size.Width>(DefaultColumnSpacing+150)*(itemsForRow+1)+ MarginLeft+DefaultColumnSpacing+10){
				itemsForRow++;
				reposition();

			}else if(size.Width<(DefaultColumnSpacing+150)*(itemsForRow)+ MarginLeft+DefaultColumnSpacing+10){
				if (itemsForRow > 3) {
					itemsForRow--;
					reposition ();
				}

			}
		}

		private List<int> getMovies()
		{
			List<int> movies = new List<int> ();
	
			List<Genre> genres = client.GetGenres ();	//.(47964);
			foreach (Genre g in genres) {
				Console.WriteLine("Genre name: {0}", g.Name);
				SearchContainerWithId<MovieResult> mr = client.GetGenreMovies (g.Id,0, true);

				foreach (MovieResult sm in mr.Results) {
					Console.WriteLine ("Found movie:" + sm.OriginalTitle);


					movies.Add (sm.Id);
					added++;
					if (added == 25) {
						break;
					}
				}
				if (added == 25) {
					break;
				}

			}
			return movies;
		}

		private List<int> getMoviesFromDb()
		{
			List<int> movies = new List<int> ();

			 
			return movies;
		}

		private void reposition(){
			IEnumerator<Widget> en = this.Children.GetEnumerator();
			List<Widget> children = new List<Widget> ();
			while (en.MoveNext ()) {
				children.Add (en.Current);
			}
			this.Clear ();
			en = children.GetEnumerator ();
			for (int y = 0; y < 100; y++) {
				for (int x = 0; x < itemsForRow; x++) {
					if(!en.MoveNext()){
						return;
					}
					this.Add (en.Current, x, y,1,1,false,false,WidgetPlacement.Center,WidgetPlacement.Center);
				}
			}
		}
	}
}

