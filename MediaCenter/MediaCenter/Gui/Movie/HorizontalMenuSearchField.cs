﻿using System;
using Xwt;

namespace MediaCenter
{
	public class SearchEventArgs
	{
		public String Text{get;set;}
	}
	public class HorizontalMenuSearchField:HorizonalMenuItem
	{
		private TextEntry textEntry;
		public event EventHandler<SearchEventArgs> Search;
		Boolean enter = false;

		public HorizontalMenuSearchField ():base("Search")
		{
		}

		protected override void Initialize()
		{
			textEntry = new TextEntry();
			textEntry.Changed += OnPress;
			textEntry.KeyPressed+= delegate(object sender, KeyEventArgs e) {
				if (Search != null && e.Key == Key.Return) {
					Search (this, new SearchEventArgs{ Text = textEntry.Text });
				}
			};
			//textEntry.SelectionChanged += OnPressEnter;
			textEntry.MarginTop = 15;
			textEntry.HorizontalPlacement = WidgetPlacement.End;
			PackStart (textEntry);
		}
		 

		public void OnPressEnter(object sender, EventArgs e)
		{
			if (enter) {
				if (Search != null) {
					Search (this, new SearchEventArgs{ Text = textEntry.Text });
				}
			}
			enter = true;

		}

		public void OnPress(object sender, EventArgs e)
		{
			enter = false;
			 
		}
	}
}

