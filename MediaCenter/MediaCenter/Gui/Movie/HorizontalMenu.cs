﻿using System;
using Xwt;
using System.Collections.Generic;
using Xwt.Drawing;

namespace MediaCenter
{
	public class HorizontalMenu:HBox
	{
		private HorizonalMenuItem selectedItem;
		private List<HorizonalMenuItem> items;
		public HorizontalMenu ()
		{
			 
			items = new List<HorizonalMenuItem> ();
			Spacing = 0;
		
			Margin = new WidgetSpacing (0, 0, 0, 0);
			MarginLeft = 20;
		}

		public void Add(HorizonalMenuItem item)
		{
			items.Add (item);
			item.ButtonPressed += onSelectItem;
			PackStart (item);
		}

		private void onSelectItem(object sender,EventArgs e)
		{
			if(selectedItem!=null){
				selectedItem.Selected =false;
			}
			selectedItem = (HorizonalMenuItem)sender;
			selectedItem.Selected = true;
			this.Show ();
		}
	}
}

