﻿using System;
using Xwt;
using Xwt.Drawing;
using TMDbLib.Objects.Movies;

namespace MediaCenter
{
	public class MovieView:MyScrollView
	{
		HorizontalMenu menu;
		MovieListFromDatabase movieList;
		VBox content = new VBox ();
		MediaPlayer mp = new MediaPlayer();
		public event EventHandler<ItemSelectedEventArgs> PlayMovie;

		public MovieView ()
		{
			BackgroundColor =  new Xwt.Drawing.Color(0.06,0.06,0.08);


			Content = content;

			content.HorizontalPlacement = WidgetPlacement.Fill;
			content.VerticalPlacement = WidgetPlacement.Fill;
			content.Spacing = 0;
			content.BackgroundColor =  new Xwt.Drawing.Color(0.06,0.06,0.08);
			menu = new HorizontalMenu ();
			menu.HorizontalPlacement = WidgetPlacement.Start;
			menu.VerticalPlacement = WidgetPlacement.Start;
			menu.Add(new HorizonalMenuItem("Menu1"));
			menu.Add(new HorizonalMenuItem("Menu2"));

			HorizontalMenuSearchField text = new HorizontalMenuSearchField ();
			text.Search+= delegate(object sender, SearchEventArgs e) {
				movieList.Search(e.Text);
			};

			menu.PackEnd(text);
			content.PackStart (menu);

			Canvas separator = new Canvas ();
			separator.BackgroundColor = new Color (0.43, 0.43, 0.46).WithAlpha (0.2);
			separator.MinHeight = 1;
			separator.Margin = new WidgetSpacing (20, 0, 8, 0);

			content.PackStart (separator);


			movieList = new MovieListFromDatabase ();
			movieList.MovieView = this;
			movieList.ItemSelected+= delegate(object sender, ItemSelectedEventArgs e) {
				MovieDetailFromDatabase detail = new MovieDetailFromDatabase();
				detail.StartPlayClicked+= delegate(object asender, StartPlayEventArgs ae) {
					//mp.Stop();
					mp.SetMedia(ae.Url);
					Content = mp;
					mp.Play();
				};
				Content = detail;
				detail.Movie = e.Movie as MovieProvider.Model.Movie;
			};
			content.PackStart(movieList);
		}


		public void Back()
		{	
			Content = content;
		}

		protected override void OnVisibleRectChanged(EventArgs e){
			movieList.VisibleRectChanged (Size);			 
		}
		 
	}
}

