﻿using System;
using Xwt;
using System.Collections.Generic;
using Xwt.Backends;
using Xwt.Drawing;


 

namespace MediaCenter
{
	[BackendType (typeof(IMyWindowBackend))]
	public class MyBaseWindow:Window
	{
		private Toolbar toolbar; 

		 
		IMyWindowBackend Backend {
			get { return (IMyWindowBackend) BackendHost.Backend; } 
		}

		public MyBaseWindow ()
		{
			Visible = false;			 
			Padding = new WidgetSpacing (0, 0, 0, 0);		
		}

		public Toolbar Toolbar { 
			get{
				return toolbar;
			}
			set {
				Backend.Toolbar = (IToolbarBackend)value.GetBackend ();
				toolbar = value;
				 
			}

		}
	 
		public void Invoke(InvokeAction del)
		{
			Backend.Invoke (del);
		}

		public Color BackgroundColor {
			get {
				return Backend.BackgroundColor;
			}
			set{
				Backend.BackgroundColor = value;
			}
		}

  
	}
}

