﻿using System;
using Xwt.Backends;
using Vlc.DotNet.Core.Medias;

namespace MediaCenter
{
	public interface IMediaPlayerBackend:ICanvasBackend
	{
		void Play();
		void Stop();
		void SetMedia(MediaBase media);
	}
}

