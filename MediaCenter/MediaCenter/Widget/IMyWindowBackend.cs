﻿using System;
using Xwt.Backends;
using Xwt;
using Xwt.Drawing;

namespace MediaCenter
{
	public delegate void InvokeAction ();

	public interface IMyWindowBackend:IWindowBackend
	{
		Color BackgroundColor { get; set; }
		IToolbarBackend Toolbar { get; set; }
		void Invoke(InvokeAction del);
	}
}

