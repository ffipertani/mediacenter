﻿using System;
using Xwt.Backends;
using Xwt;
using System.Collections.Generic;

namespace MediaCenter
{
	[BackendType (typeof(IToolbarBackend))]
	public class Toolbar:HBox
	{
		private Boolean isDragging = false;
		private Point initialLocation ;

		public Toolbar ()
		{
			//BackgroundColor = Xwt.Drawing.Colors.AliceBlue;
			//BackgroundColor = Xwt.Drawing.Colors.Transparent;
			//ButtonPressed += startDrag;
			//ButtonReleased += endDrag;
			//MouseMoved += doDrag;
		}


		void startDrag(object sender, ButtonEventArgs e)
		{
			initialLocation = new Point (e.X, e.Y);
			Console.WriteLine ("Before MOuse down at" + initialLocation);
			initialLocation = ConvertToScreenCoordinates (initialLocation);
			Console.WriteLine ("MOuse down at" + initialLocation);
			Point windowLocation = this.ParentWindow.Location;
			Console.WriteLine ("Screen bounds" + ParentWindow.ScreenBounds);
		//	windowLocation.Y = ScreenBounds.Y - windowLocation.Y;
		//	windowLocation.X = ScreenBounds.X - windowLocation.X;
			Console.WriteLine ("Window position " + windowLocation);
			//initialLocation.X -= windowLocation.X;
			//initialLocation.Y -= windowLocation.Y;
		 
			isDragging = true;
			Console.WriteLine ("DRAG");
		}

		void endDrag(object sender, EventArgs e)
		{

			isDragging = false;
			Console.WriteLine ("END DRAG");
		}

		void doDrag(object sender, MouseMovedEventArgs e)
		{
			if (isDragging) {
				Point eventPoint =ConvertToScreenCoordinates(new Point (e.X, e.Y));
				Console.WriteLine ("Event point " + eventPoint);
				Point newLocation = new Point (ParentWindow.Width,ParentWindow.Height);
				double height = ParentWindow.Height;
				//newLocation.X -= initialLocation.X - eventPoint.X  ;
				//newLocation.Y -= initialLocation.Y - eventPoint.Y ;
				newLocation.X += 1;
				newLocation.Y += 1;
				Console.WriteLine ("New position " + newLocation);
				ParentWindow.Location = newLocation;
				//ParentWindow.Height = height;
			}
		}

	}
}

