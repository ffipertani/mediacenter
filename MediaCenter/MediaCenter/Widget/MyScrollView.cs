﻿using System;
using Xwt;
using Xwt.Backends;
using MediaCenter;

namespace MediaCenter
{
	public class MyScrollView:ScrollView
	{
		public MyScrollView ()
		{
			 
		}

		IMyScrollViewBackend Backend {
			get { return (IMyScrollViewBackend)BackendHost.Backend; }
		}

		public new Size Size {
			get { return Backend.Size; }
		}

		public new Rectangle VisibleRect {
			get { return Backend.VisibleRect; }
		}
	}
}

