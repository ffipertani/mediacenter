﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Vlc.DotNet.Core;
using Vlc.DotNet.Core.Interops.Signatures.LibVlc.AsynchronousEvents;
using Vlc.DotNet.Core.Interops.Signatures.LibVlc.Media;
using Vlc.DotNet.Core.Medias;
using Vlc.DotNet.Core.Interops.Signatures.LibVlc.MediaListPlayer;

using Xwt;
using Xwt.Backends;
using Xwt.Drawing;

namespace MediaCenter
{

	[BackendType (typeof(IMediaPlayerBackend))]
	public class MediaPlayer:Canvas
	{
		private MediaBase media;



		public MediaBase Media
		{	 
			get{
				return media;
			}set{
				this.media = value;
				((IMediaPlayerBackend)this.GetBackend ()).SetMedia (media);
			}
		}

		public void SetMedia(String url)
		{			 
			if (url.StartsWith ("http://")) {
				media = new LocationMedia(url);
			}else{
				media = new PathMedia (url);
			}
			((IMediaPlayerBackend)this.GetBackend ()).SetMedia (media);
		}

		public void Play(){
			((IMediaPlayerBackend)this.GetBackend ()).Play(); 
		}


		public void Stop(){
			((IMediaPlayerBackend)this.GetBackend ()).Stop();
		}

		public MediaPlayer ()
		{
		
			base.BackgroundColor = Xwt.Drawing.Colors.Black;
 
		}

		 
	}
}