﻿using System;
using System.Net.Http;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading;
using System.Web;
using System.Net;
using System.Net.Http.Headers;

namespace MovieProvider
{
	public class StatefulHttpClient
	{
		private HttpClient client;
		private String lastUrl;
		private Exception lastException;
		private HttpContent lastContent;
		private int retryNum = 0;
		private int MAX_RETRY = 5;
	 

		public HttpResponseMessage LastResponse{ get; set; }


		public Boolean UseCompression{ get; set; }

		public Boolean FollowRedirect{ get; set; }

		public Boolean UseCookie{ get; set; }

		private void beforeRequest(String url,HttpContent content)
		{
			//client.DefaultRequestHeaders.Remove ("Accept-Encoding");

			if (UseCompression) {
				//	client.DefaultRequestHeaders.Add ("Accept-Encoding", "gzip, deflate");
			}  
			client.DefaultRequestHeaders.Remove ("Referer");
			if (lastUrl != null) {
				client.DefaultRequestHeaders.Add ("Referer", lastUrl);
				/*
				client.DefaultRequestHeaders.Add ("Accept", "text/html,application/xhtml+xml,application/xml");
				client.DefaultRequestHeaders.Add ("Accept-Encoding", "gzip, deflate");
				client.DefaultRequestHeaders.Add ("Accept-Language", "it-IT,it;q=0.8,en-US;q=0.5,en;q=0.3");
				client.DefaultRequestHeaders.Add ("Connection", "keep-alive");
				client.DefaultRequestHeaders.Add ("Cookie", "_ga=GA1.2.1712248116.1416852059; _gat=1; __unam=767b664-149e2f6b435-18d4f111-4; aff=21010");
				client.DefaultRequestHeaders.Add ("Host", "videopremium.tv");
				*/

			}
			lastException = null;
			lastUrl = url;
			lastContent = content;
			retryNum = 0;
		}

		public StatefulHttpClient ()
		{
			createClient ();
		 
			 
			FollowRedirect = true;
			UseCompression = true;
		}



		private void createClient()
		{
			HttpClient newClient = new HttpClient ();
			//newClient.Timeout = new TimeSpan(5000);

			if (client != null) {

				newClient.DefaultRequestHeaders.Clear ();
				IEnumerator<KeyValuePair<string,IEnumerable<string>>> en = client.DefaultRequestHeaders.GetEnumerator ();
				while (en.MoveNext ()) {
					newClient.DefaultRequestHeaders.Add (en.Current.Key, en.Current.Value);
				}
				client.Dispose ();
			} else {
				//First time initialization
				newClient.DefaultRequestHeaders.Add ("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:33.0) Gecko/20100101 Firefox/33.0");
			}
			client = newClient;
			//client.Timeout = new TimeSpan (1000);

		}

		public void AddHeader (String name, String value)
		{
			client.DefaultRequestHeaders.Add (name, value);	
		}

		public String Get (String url)
		{

			beforeRequest (url, null);
			StreamReader reader = new StreamReader (ReadGetResponse (url));
			return reader.ReadToEnd ();			 
		}

		public String Post (String url, HttpContent content)
		{
			beforeRequest (url, content);

			StreamReader reader = new StreamReader (ReadPostResponse (url, content));
			return reader.ReadToEnd ();			 

		}

		public MemoryStream  ReadGetResponse(String url)
		{
			try{
				var response = client.GetAsync (url).Result;
				return handleResponse (response);
			}catch(Exception e){
				lastException = e;
				return retry ();
			}

		}

		public MemoryStream  ReadPostResponse(String url, HttpContent content)
		{		 
			try{
				var response = client.PostAsync (url,content).Result;
				return handleResponse (response);
			}catch(Exception e){
				lastException = e;
				return retry ();
			}
		}

	 
		private MemoryStream retry()
		{
			Thread.Sleep (1000);
			retryNum++;
			if (retryNum < MAX_RETRY) {
				createClient ();
				if (lastContent != null) {

					return ReadPostResponse (lastUrl, lastContent);
				} else {
					return ReadGetResponse (lastUrl);
				}
			} else {

				throw new Exception ("Connection error "+LastResponse.StatusCode);
			}
		}

		private MemoryStream handleResponse(HttpResponseMessage response)
		{
			LastResponse = response;
			if (!response.IsSuccessStatusCode) {			 
				return retry ();
			}		
			 

			if (UseCookie) {
				client.DefaultRequestHeaders.Remove ("Cookie");
				String[] cookies = getAttributeValues (response, "Set-Cookie");
				if (cookies != null) {
					String cookie = "";

					foreach (String acookie in cookies) {
						cookie += acookie + ",";
					}
					if (cookie.EndsWith (",")) {
						cookie = cookie.Substring (0, cookie.Length - 1);
					}
					client.DefaultRequestHeaders.Remove ("Cookie");
					client.DefaultRequestHeaders.Add ("Cookie", cookie);
				}
			}
			 
			String val = getAttributeValue (response, "Location");
			if (val != null) {
				if (FollowRedirect) {
					return ReadGetResponse (val);
				}
			}

			byte[] responseBytes = response.Content.ReadAsByteArrayAsync ().Result;

			String encoding = getAttributeValue (response, "Content-Encoding");
			if ("gzip".Equals (encoding)) {
				return unzip (responseBytes);
			} else {
				return new MemoryStream (responseBytes);
			}
		}




		private String getAttributeValue (HttpResponseMessage msg, String attribute)
		{
			try{			 
				IEnumerator enumerator = msg.Headers.GetValues (attribute).GetEnumerator ();
				enumerator.Reset ();
				enumerator.MoveNext ();
				return(String)enumerator.Current;
			}catch(Exception e){
				return null;
			}
		}

		private String[] getAttributeValues (HttpResponseMessage msg, String attribute)
		{
			if (!msg.Headers.Contains (attribute)) {
				return null;
			}
			List<String> list = new List<String> ();
			IEnumerator enumerator = msg.Headers.GetValues (attribute).GetEnumerator ();
			enumerator.Reset ();
			while(enumerator.MoveNext ()){
				list.Add ((String)enumerator.Current);
			} 
			String[] values = new String[list.Count];
			list.CopyTo (values);
			return values;
		}


		private MemoryStream unzip (byte[] input)
		{		
			MemoryStream ms = new MemoryStream (input);
			MemoryStream msout = new MemoryStream ();
			GZipStream stream = new GZipStream (ms, CompressionMode.Decompress);

			stream.CopyTo (msout);
			msout.Position = 0;

			return msout;

		}

	}
}

