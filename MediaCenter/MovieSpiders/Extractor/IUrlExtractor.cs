﻿using System;

namespace MovieProvider.Extractor
{
	public interface IUrlExtractor
	{
		String ExtractUrl(String url);
		Boolean CanHandle(String url);
		Boolean CanHandleProvider (String name);
	}
}

