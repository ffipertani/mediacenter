﻿using System;
using System.Net.Http;
using HtmlAgilityPack;
using System.IO.Compression;
using System.IO;
using System.Collections;
using MovieProvider;

namespace MovieProvider.Extractor
{
	public class AkStreamUrlExtractor:IUrlExtractor
	{
		public AkStreamUrlExtractor ()
		{
		}
		 
		public String ExtractUrl(String url){
			StatefulHttpClient client = new StatefulHttpClient();

			client.Get (url);
			String res = client.Get ("http://akstream.net/viewprocess.php");
			HtmlDocument doc = new HtmlDocument ();
			doc.LoadHtml (res);
			HtmlNode source = doc.DocumentNode.SelectSingleNode ("//source");
			String val = source.GetAttributeValue ("src", null);
			return val;
		}

		public Boolean CanHandle(String url){
			if (url.StartsWith ("http://akstream.net/")) {
				return true;
			}
			return false;
		}

		public Boolean CanHandleProvider(String name){
			if (name.StartsWith ("Akstream")) {
				return true;
			}
			return false;
		}



	}


}

