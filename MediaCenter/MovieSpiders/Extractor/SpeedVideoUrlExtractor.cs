﻿using System;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Net.Http;

namespace MovieProvider.Extractor
{
	public class SpeedVideoUrlExtractor:IUrlExtractor
	{
		public SpeedVideoUrlExtractor ()
		{
		}

		 
		public String ExtractUrl(String url){
			String urlExtracted = null;
			String urlWithoutHttp = url.Replace ("http://", "");
			int firstSlashIndex = urlWithoutHttp.IndexOf ("/") + 1;
			String postUrl = url;//url.Substring (0, url.LastIndexOf ("/"));
			var postData = new List<KeyValuePair<string, string>>();
			 
			StatefulHttpClient client = new StatefulHttpClient();
			String res = client.Get (url);
		

			HtmlDocument doc = new HtmlDocument ();
			doc.LoadHtml (res);
			String inputPlaceholder="<input";
			String namePlaceholder="name=\"";
			String valuePlaceholder = "value=\"";
			String buttonPlaceholder = "<button";
			int index = res.IndexOf (inputPlaceholder);
			while (index>=0) {
				res = res.Substring (index + inputPlaceholder.Length);
				res = res.Substring (res.IndexOf (namePlaceholder)+namePlaceholder.Length);
				String name = res.Substring (0, res.IndexOf ("\""));
				res = res.Substring (res.IndexOf (valuePlaceholder)+valuePlaceholder.Length);
				String value = res.Substring (0, res.IndexOf ("\""));
				postData.Add (new KeyValuePair<string,string> (name, value));
				index = res.IndexOf (inputPlaceholder);
			}
		 
			res = res.Substring (res.IndexOf (buttonPlaceholder));
			res = res.Substring (res.IndexOf (namePlaceholder)+namePlaceholder.Length);
			String buttonname = res.Substring (0, res.IndexOf ("\""));
			res = res.Substring (res.IndexOf (valuePlaceholder)+valuePlaceholder.Length);
			String buttonvalue = res.Substring (0, res.IndexOf ("\""));
			postData.Add (new KeyValuePair<string,string> (buttonname, buttonvalue));
			 

			HttpContent content = new FormUrlEncodedContent(postData); 

			res = client.Post (postUrl, content );

			String stringToSearchForNext = "jwplayer.key=";
			String stringToSearchForBase64 = "var linkfile =\"";
			doc = new HtmlDocument ();
			doc.LoadHtml (res);

			Boolean isNext = false;

			HtmlNodeCollection scripts = doc.DocumentNode.SelectNodes ("//script");
			for (int i = 0; i < scripts.Count; i++) {						
				HtmlNode script = scripts [i];
				String text = script.InnerText;

				if (isNext) {
					int indexVal = text.IndexOf ("=") + 1;
					int count = text.IndexOf (";") - indexVal;
					String val = text.Substring(indexVal,count);
					text = text.Substring (text.IndexOf (stringToSearchForBase64)+stringToSearchForBase64.Length);					 
					text = text.Substring (0, text.IndexOf ("\""));
					//aHR0cDovLzE4OC4xMjIuOTEuMTAzOjg3NzcvNnBaidefeebgcsNnI3NWZzaHh5ZnR4eHltaDVhZml2Y2VidncyNmszbnl3ZGZmMmwzb3B0c3ZveGI3aGtkYmx3YWlxL3YuZmx2
					//39


						int resizeparam = int.Parse(val);
					String inizio = text.Substring (0, resizeparam);
					String fine = text.Substring (resizeparam + 10);
					String base64 = inizio + fine;					 
					base64 = System.Text.Encoding.UTF8.GetString (Convert.FromBase64String (base64));
					urlExtracted = base64;
					break;
				}else if (text.Contains (stringToSearchForNext)) {
					isNext = true;
				}
					 
			}

 			return urlExtracted;
		}


		public Boolean CanHandle(String url){
			if (url.StartsWith ("http://speedvideo.net")) {
				return true;
			}
			return false;
		}

		public Boolean CanHandleProvider(String name){
			if (name.StartsWith ("Speedvideo")) {
				return true;
			}
			return false;
		}
	}
}

