﻿using System;
using MovieProvider.Extractor;
using MovieProvider;
using HtmlAgilityPack;
using System.Web;

namespace MovieProvider.Extractor
{
	public class VkUrlExtractor:IUrlExtractor
	{
		public VkUrlExtractor ()
		{
		}

		 
		public String ExtractUrl(String url){
			String val = null;
			StatefulHttpClient client = new StatefulHttpClient();
			url = HttpUtility.HtmlDecode (url);
		 
			String res = client.Get (url);
			HtmlDocument doc = new HtmlDocument ();
			doc.LoadHtml (res);
			HtmlNode source = doc.DocumentNode.SelectSingleNode ("//embed");

			if (source != null) {
				val = source.GetAttributeValue ("flashvars", null);
				val = HttpUtility.HtmlDecode (val);
				String[] attribs = val.Split (new string[]{"&"},new StringSplitOptions());
				foreach (String attr in attribs) {
					String[] keyval = attr.Split (new String[]{"="},new StringSplitOptions());
					if (keyval[0].StartsWith ("url")) {
						val = "";
						for (int i = 1; i < keyval.Length; i++) {
							val += keyval [i];
						}

					}
				}
			}

			return val;
		}

		public Boolean CanHandle(String url){
			if (url.StartsWith ("http://vk.com/")) {
				return true;
			}
			return false;
		}

		public Boolean CanHandleProvider(String name){
			if (name.StartsWith ("VK")) {
				return true;
			}
			return false;
		}
	}
}

