﻿using System;
using MovieProvider;
using HtmlAgilityPack;
using System.Net.Http;
using System.Collections.Generic;

namespace MovieProvider.Extractor
{
	public class VideopremiumUrlExtractor:IUrlExtractor
	{
		 


		public VideopremiumUrlExtractor ()
		{
		}
		public String ExtractUrl(String url){
			String urlExtracted = null;
			StatefulHttpClient client = new StatefulHttpClient();
			String res = client.Get (url);
			String id = url.Substring (url.LastIndexOf ("/")+1);


			var postData = new List<KeyValuePair<string, string>>();
			postData.Add(new KeyValuePair<string, string>("id", id));
			postData.Add(new KeyValuePair<string, string>("op", "download1"));
			//postData.Add(new KeyValuePair<string, string>("usr_login", null));
			postData.Add(new KeyValuePair<string, string>("method_free", "Watch Free!"));
			//postData.Add(new KeyValuePair<string, string>("referer", null));
			//postData.Add(new KeyValuePair<string, string>("fname", "Una_Pallottola_SpuntataBYRelique.avi.mp4"));
						 
			HttpContent content = new FormUrlEncodedContent(postData); 
			res = client.Post (url, content );
			String stringToSearch = "this.vplayer = new Uppod({m:\"video\",uid:\"vplayer\",file:\"";
			HtmlDocument doc = new HtmlDocument ();
			doc.LoadHtml (res);


			HtmlNodeCollection scripts = doc.DocumentNode.SelectNodes ("//script");
			for (int i = 0; i < scripts.Count; i++) {						
				HtmlNode script = scripts [i];
				String text = script.InnerText;
				if (text.Contains (stringToSearch)) {

					text = text.Substring (text.LastIndexOf (stringToSearch));
					text = text.Replace (stringToSearch,"");
					text = text.Substring (0, text.IndexOf ("\""));
										 
					text = text.Trim ();
					 
					urlExtracted = text;
					break;
					 
				}
			}

			return urlExtracted;
		}

		public Boolean CanHandle(String url){
			if (url.StartsWith ("http://videopremium.tv/")) {
				return true;
			}
			return false;
		}

		public Boolean CanHandleProvider(String name){
			if (name.StartsWith ("Videopremium")) {
				return true;
			}
			return false;
		}

		 
	}
}

