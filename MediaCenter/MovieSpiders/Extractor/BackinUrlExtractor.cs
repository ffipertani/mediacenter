﻿using System;
using HtmlAgilityPack;

namespace MovieProvider.Extractor
{
	public class BackinUrlExtractor:IUrlExtractor
	{
		public BackinUrlExtractor ()
		{


		}

		public String ExtractUrl(String url)
		{
			StatefulHttpClient client = new StatefulHttpClient();

			client.Get (url);

			String filename = url.Substring (url.LastIndexOf ("/")+1);
			String urlToCall = "http://backin.net/s/streams.php?s=" + filename;
			//
			String res = client.Get (urlToCall);
			urlToCall = "http://backin.net/s/generating.php?code="+filename;
			res = client.Get (urlToCall);
			HtmlDocument doc = new HtmlDocument ();
			doc.LoadHtml (res);
			HtmlNode source = doc.DocumentNode.SelectSingleNode ("//source");
			String val = source.GetAttributeValue ("src", null);
			return val;			 
		}

		public Boolean CanHandle(String url)
		{
			if (url.StartsWith ("http://backin.net/")) {
				return true;
			}
			return false;
		}

		public Boolean CanHandleProvider(String name){
			if (name.StartsWith ("Backin")) {
				return true;
			}
			return false;
		}
	}
}

