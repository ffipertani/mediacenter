﻿using System;
using MovieProvider.Extractor;
using HtmlAgilityPack;

namespace MovieProvider.Extractor
{
	public class CloudZillaUrlExtractor:IUrlExtractor
	{
		public CloudZillaUrlExtractor ()
		{
		}

		public String ExtractUrl(String url)
		{
			StatefulHttpClient client = new StatefulHttpClient();
			String realUrl = null;

			String res = client.Get (url);


			HtmlDocument doc = new HtmlDocument ();
			doc.LoadHtml (res);
			HtmlNode source = doc.DocumentNode.SelectSingleNode ("//iframe");
			String val = source.GetAttributeValue ("src", null);


			res = client.Get (val);
			doc = new HtmlDocument ();
			doc.LoadHtml (res);
		 
			HtmlNodeCollection scripts = doc.DocumentNode.SelectNodes ("//script");
			for (int i = 0; i < scripts.Count; i++) {						
				HtmlNode script = scripts [i];
				String text = script.InnerText;
				if (text.Contains ("var vurl =")) {
					text = text.Substring (text.IndexOf ("var vurl ="));
					text = text.Substring (0, text.IndexOf (";"));
					text = text.Replace ("var vurl =","");
					text = text.Replace ("\"", "");
					text = text.Trim ();
					realUrl = text;
					break;
				}
			}
			return realUrl;
		}

		public Boolean CanHandle(String url){
			if (url.StartsWith ("http://www.cloudzilla.to")) {
				return true;
			}
			return false;
		}

		public Boolean CanHandleProvider(String name){
			if (name.StartsWith ("Cloudzilla")) {
				return true;
			}
			return false;
		}

	}
}

