﻿using System;
 
 
using System.Collections.Generic;
using TMDbLib.Client;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.Search;
using SQLite;
using TMDbLib.Objects.Find;
using MovieProvider;
using HtmlAgilityPack;
using System.Net;
using Xwt.Drawing;
using System.IO;

namespace CreateCatalog
{
	public class TMDbFinder
	{
		TMDbClient client = new TMDbClient("401e6b7de800347b1775291af806724d");
		public TMDbFinder ()
		{
			client.DefaultCountry = "it";
			client.DefaultLanguage = "it";
		}

		public String FindImdb(MovieProvider.Model.Movie movie)
		{
			String imdb = null;
			StatefulHttpClient client = new StatefulHttpClient();
			String title = movie.Title;
			title = title.Replace ("[B/N]", "");
			title = title.Replace ("[Sub-ITA]", "");//chilometro->km  title = title.Replace ("[Sub-iTA]", "");
			title.Trim ();

			String q = title.Replace(" ", "+");

			try{
				String content = client.Get ("http://www.imdb.com/find?q="+q);
				HtmlDocument doc = new HtmlDocument ();
				doc.LoadHtml (content);
				HtmlNode anchor = doc.DocumentNode.SelectNodes (".//td[@class='result_text']") [0].SelectSingleNode ("./a");
				if (anchor != null) {
					String attr = anchor.GetAttributeValue ("href", null);
					attr = attr.Replace ("/title/", "");
					attr = attr.Substring (0, attr.IndexOf ("/"));
					imdb = attr;
				}
			}catch(Exception e){
				Console.WriteLine ("non sono riuscito a trovare imdb per " + movie.Title);
			}
			//
			return imdb;
		}

		public Movie FindByImdb(String imdb)
		{


			FindContainer mr = client.Find (TMDbLib.Objects.Find.FindExternalSource.Imdb, imdb);
			foreach (MovieResult sm in mr.MovieResults) {
				Console.WriteLine ("Found movie:" + sm.OriginalTitle);

				Movie movie = client.GetMovie(sm.Id,MovieMethods.Credits|MovieMethods.Changes|MovieMethods.Images|MovieMethods.Keywords|MovieMethods.Lists|MovieMethods.Releases|MovieMethods.SimilarMovies|MovieMethods.Translations);
				//client.
				//	.getMovie (16271);
				//	client.Se
				return movie;
			}
			return null;
		}

		public Image DownloadImage (String url)
		{
			client.GetConfig ();
			String path = client.GetImageUrl (client.Config.Images.PosterSizes [5], url).ToString ();
			HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create (path); 							 
			HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse (); 							 
			Stream receiveStream = myHttpWebResponse.GetResponseStream ();
			return Image.FromStream (receiveStream);							 					
		}

		public Movie Find()
		{
			int count1 = 0;

			int count2 = 0;
			TMDbClient client = new TMDbClient("401e6b7de800347b1775291af806724d");
			client.DefaultCountry = "it";
			client.DefaultLanguage = "it";
			List<Genre> genres = client.GetGenres ();	//.(47964);
			foreach (Genre g in genres) {
				Console.WriteLine("Genre name: {0}", g.Name);
				SearchContainerWithId<MovieResult> mr = client.GetGenreMovies (g.Id,0, true);
				Console.WriteLine ("Page:" + mr.Page);
				Console.WriteLine ("Total pages:" + mr.TotalPages);
				Console.WriteLine ("Results:" + mr.Results);
				Console.WriteLine ("Total Results:" + mr.TotalResults);
				count1 += mr.TotalResults;
				 
				foreach (MovieResult sm in mr.Results) {
					Console.WriteLine ("Found movie:" + sm.OriginalTitle);
				
					Movie movie = client.GetMovie(sm.Id,MovieMethods.Credits|MovieMethods.Changes|MovieMethods.Images|MovieMethods.Keywords|MovieMethods.Lists|MovieMethods.Releases|MovieMethods.SimilarMovies|MovieMethods.Translations);
					//client.
					//	.getMovie (16271);
				//	client.Se
				}
 


				SearchContainer<SearchMovie> res = client.DiscoverMovies (0, "it", TMDbLib.Objects.Discover.DiscoverTvShowSortBy.Undefined, true, null, null, null, null, g.Id+"", null, null, null, null, null);
				Console.WriteLine ("Page:" + res.Page);
				Console.WriteLine ("Total pages:" + res.TotalPages);
				Console.WriteLine ("Results:" + res.Results);
				Console.WriteLine ("Total Results:" + res.TotalResults);
				count2 += res.TotalResults;
				/*
				foreach (SearchMovie sm in res.Results) {
					Console.WriteLine ("Found movie:" + sm.OriginalTitle);
					//client.
					//	.getMovie (16271);
				}
				*/

			}

			Console.WriteLine ("Total1:" + count1);
			Console.WriteLine ("Total2:" + count2);

			return null;
			//ServiceClient sclient;
		 
		}
	}
}

