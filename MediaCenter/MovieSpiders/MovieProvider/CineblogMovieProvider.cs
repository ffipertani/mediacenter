﻿using System.Collections.Generic;

using System.Net;
using System;
using System.IO;
using System.Diagnostics;
using HtmlAgilityPack;
using System.Net.Http;
using System.Web;
using MovieProvider.Model;
using MovieProvider;
using System.Threading;



namespace Provider.MovieProvider
{
	public class CineblogMovieProvider:BaseMovieProvider
	{

		internal readonly object syncLock = new object ();
		private Semaphore pool = new Semaphore (20, 20);
		List<Movie> movies = new List<Movie> ();
		bool restart = false;

		public CineblogMovieProvider ()
		{
	
		}

		protected override List<Movie> doFindMovies ()
		{

			Stopwatch sw = new Stopwatch ();
			sw.Start ();
			movies = new List<Movie> ();
			HtmlDocument doc = loadFilmList ();
			HtmlNodeCollection anchors = doc.DocumentNode.SelectNodes (".//div[@id='pagecontent']//table") [1].SelectNodes (".//tr") [2].SelectNodes (".//a");
			List<Thread> threads = new List<Thread> ();
			int size = anchors.Count;
			Console.WriteLine ("Total movies:" + size);
 		 
			for (int i = 0; i < anchors.Count - 1; i++) {						
				HtmlNode link = anchors [i];
				String att = link.GetAttributeValue ("href", "XXX");
				 			 
				pool.WaitOne ();
				if (restart) {
					foreach (Thread at in threads) {
						at.Abort ();
					}

					throw new Exception ("RESTART");
				}
				Thread t = new Thread (new ParameterizedThreadStart (createMovieThread));
				threads.Add (t);
				t.Start (att);					 			 
			}		 
 
			foreach (Thread t in threads) {
				t.Join ();
			}
 
			sw.Stop ();
			Console.WriteLine ("Totaltime: " + sw.ElapsedTicks);
			return movies;
		}



		private void createMovieThread (object att)
		{
			try {
				Console.WriteLine ("Thread " + att + " started");
				Movie m = createMovie ((String)att);
				lock (syncLock) { 
					movies.Add (m);			
				}
			} catch (Exception e) {
				Console.WriteLine ("Error in thread " + att);
				restart = true;
			} finally {
				pool.Release ();
				Console.WriteLine ("Thread " + att + " ended");
			}
		}


		public List<Movie> RestartFrom (String url)
		{
			List<Thread> threads = new List<Thread> ();
			List<Movie> movies = new List<Movie> ();
			Stopwatch sw = new Stopwatch ();
			sw.Start ();

			HtmlDocument doc = loadFilmList ();

			HtmlNodeCollection anchors = doc.DocumentNode.SelectNodes (".//div[@id='pagecontent']//table") [1].SelectNodes (".//tr") [2].SelectNodes (".//a");

			int size = anchors.Count;
			Console.WriteLine ("Total movies:" + size);

			Boolean canCreate = false;
			for (int i = 0; i < anchors.Count - 1; i++) {						
				HtmlNode link = anchors [i];
				String att = link.GetAttributeValue ("href", "XXX");
				if (canCreate) {				 
					pool.WaitOne ();
					if (restart) {
						foreach (Thread at in threads) {
							at.Abort ();
						}
						throw new Exception ("RESTART");
					}
					Thread t = new Thread (new ParameterizedThreadStart (createMovieThread));
					threads.Add (t);
					t.Start (att);					 		
				}
				if (url.Equals (att)) {
					canCreate = true;
				}
				 
			}

			foreach (Thread t in threads) {
				t.Join ();
			}


			sw.Stop ();
			Console.WriteLine ("Totaltime: " + sw.ElapsedTicks);
			return movies;
		}


		private HtmlDocument loadFilmList ()
		{
			String input = null;
			if (!File.Exists ("allfilm.html")) {
				StatefulHttpClient client = new StatefulHttpClient ();
				input = client.Get ("http://www.cb01.tv/lista-film-completa-streaming/");
				File.WriteAllText ("allfilm.html", input);
			} else {
				input = File.ReadAllText ("allfilm.html");
			}


			HtmlDocument doc = new HtmlDocument ();
			doc.LoadHtml (input);
			return doc;
		}


		public Movie createMovie (String url)
		{
			StatefulHttpClient client = new StatefulHttpClient ();
			String input = client.Get (url);

			HtmlDocument doc = new HtmlDocument ();
			doc.LoadHtml (input);


			Movie movie = new Movie ();

			movie.Title = grabTitle (doc);
			movie.PosterUrl = grabImage (doc);
			movie.Overview = grabOverview (doc);
			movie.Genre = grabGenre (doc);
			movie.Duration = grabDuration (doc);
			movie.Country = grabCountry (doc);
			movie.Year = grabYear (doc);
			movie.MovieFiles = grabFiles (url, doc);
			movie.Url = url;

			Save (movie);
						 
			return movie;
		}



		private List<MovieFile> grabFiles (String url, HtmlDocument doc)
		{
			Console.WriteLine ("Searching for movie " + url);

			List<MovieFile> files = new List<MovieFile> ();		
			HtmlNodeCollection tables = doc.DocumentNode.SelectSingleNode ("//div[@id='blogitem']").SelectNodes (".//table");
			for (int i = 0; i < tables.Count; i++) {						
				HtmlNode table = tables [i];
				if (table.Attributes.Count > 0) {
					continue;
				}
				if (!table.ParentNode.GetAttributeValue ("valign", "xxx").Equals ("top")) {
					continue;
				}
				HtmlNode textNode = table.SelectSingleNode (".//tr//strong");
				if (textNode != null) {
					String text = textNode.InnerText;
					if (text != null) {
						Console.WriteLine ("Text node found");
						if (text.StartsWith ("Streaming")) {
							continue;
						}
						if (text.StartsWith ("Download")) {
							break;
						}
					}
				}

			
				HtmlNode anchorNode = table.SelectSingleNode (".//tr//a");
				if (anchorNode != null) {
					String provider = anchorNode.InnerText;
					String movieUrl = anchorNode.GetAttributeValue ("href", "XXX");
					if (provider != null) {
						Console.WriteLine ("Processing provider " + provider + ", URL:" + movieUrl);
					} else {
						Console.WriteLine ("Provider not found");
						continue;
					}

					if (!CanHandle (provider)) {
						Console.WriteLine ("Cannot handle Provider " + provider);
						continue;
					}

					if (movieUrl != null) {
						try {
							files.AddRange (grabFiles (url, movieUrl));														
						} catch (Exception e) {
							Console.WriteLine ("Error grabbing movie url " + movieUrl + " from page " + url);
						}
					} else {
						Console.WriteLine ("Movie url not found");
					}
				}
			}
			 

			HtmlNodeCollection iframes = doc.DocumentNode.SelectNodes (".//div[@class='tabs-catch-all']/iframe");
			if (iframes != null) {
				for (int i = 0; i < iframes.Count; i++) {						
					HtmlNode link = iframes [i];
					String att = link.GetAttributeValue ("src", "XXX");

					List<MovieFile> extracted = extractFiles (att);
					if (extracted != null) {
						files.AddRange (extracted);						 
					} 	
				}		 

			}

			Console.WriteLine ("Found " + files.Count + " files");
			return files;
		}

	 

		private String clearText (String text)
		{
			text = WebUtility.HtmlDecode (text);

			//text = System.Text.Encoding.UTF8.GetString(System.Text.Encoding.Convert (System.Text.Encoding.GetEncoding ("ISO-8859-1"), System.Text.Encoding.UTF8,System.Text.Encoding.GetEncoding ("ISO-8859-1").GetBytes(text)));
			text = text.Replace (char.ConvertFromUtf32 (8211), char.ConvertFromUtf32 (45));
			//
			//45
			return text;
		}

		private String grabTitle (HtmlDocument doc)
		{
			try {
				HtmlNode node = doc.DocumentNode.SelectSingleNode ("//div[@id='blogitem']").SelectSingleNode (".//h3");
				if (node != null) {
					String text = node.InnerText;
					if (text.EndsWith (")")) {
						text = text.Substring (0, text.LastIndexOf ("("));					 
						return clearText (text.Trim ());
					}
				}
			} catch (Exception e) {
				Console.WriteLine ("Errore grabbing title " + e);
			}
			return null;
		}

		private String grabYear (HtmlDocument doc)
		{
			try {
				HtmlNode node = doc.DocumentNode.SelectSingleNode ("//div[@id='blogitem']").SelectSingleNode (".//h3");
				if (node != null) {
					String text = node.InnerText;
					if (text.EndsWith (")")) {
						text = text.Substring (text.LastIndexOf ("(") + 1);
						text = text.Substring (0, text.LastIndexOf (")"));
						return clearText (text.Trim ());
					}
				}
			} catch (Exception e) {
				Console.WriteLine ("Errore grabbing year " + e);
			}

			return null;
		}

		private String grabImage (HtmlDocument doc)
		{
			try {
				HtmlNode node = imageNode (doc);
				if (node != null) {
					return node.GetAttributeValue ("src", null);
				}
			} catch (Exception e) {
				Console.WriteLine ("Errore grabbing image " + e);
			}
			return null;
	
		}

		private String grabOverview (HtmlDocument doc)
		{
			try {
				HtmlNode node = genreNode (doc);
				if (node != null) {
					node = node.ParentNode.NextSibling;
					while (node != null && !"p".Equals (node.Name)) {
						node = node.NextSibling;
					}
					if (node != null) {
						return clearText (node.InnerText);
					}

				}
			} catch (Exception e) {
				Console.WriteLine ("Errore grabbing overview " + e);
			}
			return null;
		}

		private String grabGenre (HtmlDocument doc)
		{
			try {
				HtmlNode node = genreNode (doc);
				if (node != null) {
					String text = clearText (node.InnerText);
					text = text.Substring (0, text.IndexOf ("-"));
					return text.Trim ();
				}
			} catch (Exception e) {
				Console.WriteLine ("Errore grabbing genre " + e);
			}
			return null;
		}

		private String grabDuration (HtmlDocument doc)
		{
			try {
				HtmlNode node = genreNode (doc);
				if (node != null) {
					String text = clearText (node.InnerText);
					text = text.Substring (text.IndexOf ("-") + 1);
					text = text.Substring (0, text.IndexOf ("-"));
					text = text.Replace ("DURATA", "");
					return text.Trim ();
				}
			} catch (Exception e) {
				Console.WriteLine ("Errore grabbing duration " + e);
			}
			return null;
		}


		private String grabCountry (HtmlDocument doc)
		{
			HtmlNode node = genreNode (doc);
			if (node != null) {
				String text = clearText (node.InnerText);
				text = text.Substring (text.IndexOf ("-") + 1);
				text = text.Substring (text.IndexOf ("-") + 1);

				return text.Trim ();

			}
			return null;
		}

		private HtmlNode genreNode (HtmlDocument doc)
		{
			HtmlNode node = imageNode (doc);
			if (node != null) {
				node = node.ParentNode.NextSibling;
				while (node != null && !"p".Equals (node.Name)) {
					node = node.NextSibling;
				}
				if (node != null) {
					return node.SelectSingleNode ("./strong");
				}
			}
			return null;			 
		}

		private HtmlNode imageNode (HtmlDocument doc)
		{
			try {

				return doc.DocumentNode.SelectSingleNode ("//div[@id='blogitem']").SelectSingleNode (".//img");
			} catch {
				return null;
			}
		}

		private List<MovieFile> grabFiles (String caller, String url)
		{
			List<MovieFile> files = new List<MovieFile> ();
			try {
				HttpClient client = new HttpClient ();
				String realUrl = null;
				client.DefaultRequestHeaders.Add ("Referer", caller);


				//client.Timeout = new TimeSpan (1000);
				var response = client.GetAsync (url).Result;

				if (response.IsSuccessStatusCode) {			 
					var responseContent = response.Content; 								 
					string responseString = responseContent.ReadAsStringAsync ().Result;

					HtmlDocument doc = new HtmlDocument ();
					doc.LoadHtml (responseString);
					HtmlNodeCollection scripts = doc.DocumentNode.SelectNodes ("//script");
					for (int i = 0; i < scripts.Count; i++) {						
						HtmlNode script = scripts [i];
						String text = script.InnerText;
						if (text.Contains ("window.location.href =")) {
							text = text.Substring (text.IndexOf ("window.location.href ="));
							text = text.Substring (0, text.IndexOf (";"));
							text = text.Replace ("window.location.href =", "");
							text = text.Replace ("\"", "");
							text = text.Trim ();
							realUrl = text;
							Console.WriteLine ("Found real url " + text);
							break;
						}
					}

				}

				//realUrl = url;
				if (realUrl == null) {
					realUrl = url;
				}
				files = extractFiles (realUrl);
				if (files == null) {
					return new List<MovieFile> ();
				}
			} catch (Exception e) {
				Console.Error.WriteLine ("Errore durante grabFiles " + e);
			}
			return files;
		}
		 


	}
}
