﻿using System;
using MovieProvider;
using MovieProvider.Model;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Diagnostics;
using Provider.MovieProvider;
using System.Net.Http;

namespace Provider.MovieProvider

{
	public class ItaliaFilmMovieProvider:BaseMovieProvider
	{
		public ItaliaFilmMovieProvider ()
		{
		}


		protected override List<Movie> doFindMovies ()
		{

			List<Movie> movies = new List<Movie> ();
			Stopwatch sw = new Stopwatch ();
			sw.Start ();
			 
			movies.AddRange (parsePage ("http://www.italia-film.org/archivio-alfabetico-film-e-serie-tv/"));				 
			 

			sw.Stop ();
			Console.WriteLine ("Totaltime: " + sw.ElapsedTicks);
			return movies;
		}


		private List<Movie> parsePage(String url)
		{
			List<Movie> movies = new List<Movie> ();
			StatefulHttpClient client = new StatefulHttpClient();
			String input = client.Get (url);

			HtmlDocument doc = new HtmlDocument ();
			doc.LoadHtml (input);
			HtmlNodeCollection anchors = doc.DocumentNode.SelectNodes (".//main/ul/li/a");


			for (int i = 0; i < anchors.Count; i++) {						
				HtmlNode link = anchors [i];
				String att = link.GetAttributeValue ("href", "XXX");
				movies.Add (createMovie (att));				 
			}

			HtmlNode nextPage  = doc.DocumentNode.SelectSingleNode (".//a[@class='next page-numbers']");
			if(nextPage!=null){
				String nextUrl = nextPage.GetAttributeValue ("href", "XXX");
				movies.AddRange (parsePage (nextUrl));
			}

			return movies;
			
		}


		private Movie createMovie (String url)
		{
		//	StatefulHttpClient client = new StatefulHttpClient();
		//  String input = client.Get (url);
			/*
			HtmlDocument doc = new HtmlDocument ();
			doc.LoadHtml (input);
			HtmlNodeCollection tables = doc.DocumentNode.SelectSingleNode ("//div[@id='blogitem']").SelectNodes (".//table");
			for (int i = 0; i < tables.Count; i++) {						
				HtmlNode table = tables [i];
				HtmlNode textNode = table.SelectSingleNode (".//tr//strong");
				if (textNode != null) {
					String text = textNode.InnerText;
					if (text != null) {
						Console.WriteLine ("Text node found");
						if (text.StartsWith ("Streaming:")) {
							continue;
						}
						if (text.StartsWith ("Download:")) {
							break;
						}
					}
				}

				HtmlNode anchorNode = table.SelectSingleNode (".//tr//a");
				if (anchorNode != null) {
					String provider = anchorNode.InnerText;
					String movieUrl = anchorNode.GetAttributeValue ("href", "XXX");
					if (provider != null) {
						Console.WriteLine ("Processing provider " + provider);
					} else {
						Console.WriteLine ("Provider not found");
					}
					if (movieUrl != null) {
						Console.WriteLine ("Searching for movie " + url);
						grabUrl (url,movieUrl);
					} else {
						Console.WriteLine ("Movie url not found");
					}
				}


			}
*/
			Movie movie = new Movie ();
			return movie;
		}


	 


	}
}

