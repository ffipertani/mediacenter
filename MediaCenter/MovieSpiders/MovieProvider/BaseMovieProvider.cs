﻿using System;
using System.Collections;
using System.Collections.Generic;
using MovieProvider.Extractor;
using MovieProvider.Model;
using MovieProvider;
using TMDbLib.Client;
using System.Runtime.CompilerServices;


namespace Provider.MovieProvider
{
	public abstract class BaseMovieProvider
	{
		private List<IUrlExtractor> extractors = new List<IUrlExtractor> ();

		public List<Movie> FindMovies()
		{
			List<Movie> movies = doFindMovies ();
			foreach(Movie movie in movies)
			{
				//

			}
			return null;


		}

		protected abstract List<Movie> doFindMovies ();

		[MethodImpl(MethodImplOptions.Synchronized)]
		protected void Save(Movie movie)
		{
			try{
				Program.db.BeginTransaction ();
				Program.db.Insert (movie);
				foreach (MovieFile mf in movie.MovieFiles) {
					mf.MovieId =movie.Id;

					Program.db.Insert (mf);
				}
				Program.db.Commit();
			}catch(Exception e){
				Console.WriteLine ("Errore in scrittura:" + e);
			}
		}


		protected BaseMovieProvider(){
			//AddExtractor (new AkStreamUrlExtractor ());
			//AddExtractor (new BackinUrlExtractor ());
			//AddExtractor (new CloudZillaUrlExtractor ());
			//AddExtractor (new FireDriveUrlExtractor ());
			//AddExtractor (new VideopremiumUrlExtractor ());
			//AddExtractor (new SpeedVideoUrlExtractor ());
			AddExtractor (new VkUrlExtractor ());
		}

		protected void AddExtractor(IUrlExtractor extractor){
			extractors.Add (extractor);
		}


		protected bool CanHandle(String provider)
		{
			foreach (IUrlExtractor extractor in extractors) {
				if (extractor.CanHandleProvider (provider)) {
					return true;
				}
			}
			return false;
		}

		protected List<MovieFile> extractFiles(String url){
			List<MovieFile> files = new List<MovieFile> ();
			String extractedUrl = null;
			Boolean handled = false;
			foreach (IUrlExtractor extractor in extractors) {
				if (extractor.CanHandle (url)) {	
					handled = true;
					MovieFile mf = new MovieFile();
					mf.Provider = extractor.ToString();
					mf.ProviderUrl = url;
					try{
						mf.FileUrl  = extractor.ExtractUrl (url);
						mf.Message = "OK";
					}catch(Exception e){
						mf.Message = e.ToString();
						Console.Error.WriteLine ("Errore durante l'estrazione dell'url:"+url+" con l'extractor "+ extractor+".\n Eccezione:"+e);
					}
					files.Add (mf);
				}
			}
			if (!handled) {
				MovieFile mf = new MovieFile();								 
				mf.ProviderUrl = url;
				mf.Message = "SKIP";
				files.Add(mf);
			}
			return files;
		}
	}
}

