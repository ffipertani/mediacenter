﻿using System;
using System.Collections.Generic;

using System.Reflection;
using System.Diagnostics;
using Provider.MovieProvider;
using MovieProvider.Model;
using CreateCatalog;
using SQLite;
using System.Xml;
using System.Net;
using Platinum;
using System.Net.Sockets;
using System.Threading;
 

namespace MovieProvider
{

	class MediaControllerDelegate:PltMediaControllerDelegate
	{
		public override bool OnMRAdded (PltDeviceData device)
		{
			Console.WriteLine (device.FriendlyName);
			return base.OnMRAdded (device);
		}


	}
 


	class MyCtrlPointListener: PltCtrlPointListener{
		public Boolean OnDeviceAdded (PltDeviceData device)
		{
			Console.WriteLine (device);
			return true;
		}

		public Boolean OnDeviceRemoved (PltDeviceData device)
		{
			return true;
		}

		public Boolean OnActionResponse (Boolean res, PltAction action, byte[] userdata)
		{
			return true;
		}


		public Boolean OnEventNotify (PltService service, List<PltStateVariable> vars)
		{
			return true;
		}
	}
	public class Program
	{

		public static SQLiteConnection db;
		private static bool clear = false;
		static 	UdpClient client;

		public static void OnReceiveSink(IAsyncResult ar)
		{
			IPEndPoint ep = null;
			IPEndPoint local = (IPEndPoint)ar.AsyncState;
			byte[] buf = null;
			try { buf = client.EndReceive(ar, ref ep); } catch (Exception) { }
			 
			try { client.BeginReceive(new AsyncCallback(OnReceiveSink), local); }
			catch (Exception) { }
		}

		[STAThread]
		public static void Main()
		{

			/*
			IPEndPoint local = new IPEndPoint (IPAddress.Parse("192.168.1.85"), 1900);
			client = new UdpClient(local.AddressFamily);
			try { client.ExclusiveAddressUse = false; } catch (SocketException) { }
			try { client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true); } catch (SocketException) { }
			try { client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ExclusiveAddressUse, false); } catch (SocketException) { }
			try { client.EnableBroadcast = true; } catch (SocketException) { }
			 
				//client.Client.Bind(local);
				if (local.AddressFamily == AddressFamily.InterNetwork) client.Client.Bind(new IPEndPoint(IPAddress.Any, local.Port));
				if (local.AddressFamily == AddressFamily.InterNetworkV6) client.Client.Bind(new IPEndPoint(IPAddress.IPv6Any, local.Port));
			 
			if (local.AddressFamily == AddressFamily.InterNetwork) client.JoinMulticastGroup(IPAddress.Parse("239.255.255.250"), local.Address);
			if (local.AddressFamily == AddressFamily.InterNetworkV6)
			{
				if (local.Address.IsIPv6LinkLocal) client.JoinMulticastGroup((int)local.Address.ScopeId, IPAddress.Parse("FF02::C"));
				else client.JoinMulticastGroup((int)local.Address.ScopeId, IPAddress.Parse("FF05::C"));
			}
			client.BeginReceive(new AsyncCallback(OnReceiveSink), local);
		 
			while (true) {
				Thread.Sleep (300);
			}



*/






















			MediaRenderer rendered = new MediaRenderer ("PIPPUZZO");

			MediaServer server = new MediaServer ("GIACOMINO");

			PltCtrlPoint ctrlPoint = new PltCtrlPoint ();
			ctrlPoint.AddListener (new MyCtrlPointListener ());
			MediaController controller = new MediaController(ctrlPoint, new MediaControllerDelegate());



			PltUPnP pp = new PltUPnP();
			pp.AddDevice (rendered);
			//pp.AddDevice (server);
			//pp.AddDevice (controller);
			pp.AddCtrlPoint (ctrlPoint);

			pp.Start ();

			ctrlPoint.Search ();
		
			ctrlPoint.Discover ();

			if (true)
				while (true) {
					Thread.Sleep (10000);
			//		ctrlPoint.Search ();
					Console.WriteLine (ctrlPoint.m_RootDevices);
				}
				return;




			db = new SQLiteConnection ("./db.data");
			db.CreateTable<Movie> ();
			//db.CreateTable<MovieInfo> ();
			db.CreateTable<MovieFile> ();

			TMDbFinder finder = new TMDbFinder ();
			TableQuery<Movie> movies = db.Table<Movie> ();
			foreach (Movie movie in movies) {
				String imdb = finder.FindImdb (movie);
				if(imdb!=null){
					movie.Imdb = imdb;
					db.Update(movie);
				}
			}

			//finder.FindByImdb ("tt0065553");
		}


		public static void RestoreFrom(String url)
		{

			String urlRestart = url;// "http://www.cb01.tv/5-centimetri-al-secondo-2007/";
			while (true) {
				CineblogMovieProvider prov = new CineblogMovieProvider ();
				//prov.createMovie ("http://www.cb01.tv/una-pallottola-spuntata-1988/");
				//	prov.FindMovies ();


				try{
					prov.RestartFrom (urlRestart);
				}catch(Exception e){	
					var tmpmovies = db.Table<Movie>();
					urlRestart = tmpmovies.ElementAt(tmpmovies.Count() - 1).Url;
					continue;
				}


				break;
			}
		}

		public static void Restore()
		{

			String urlRestart;// = "http://www.cb01.tv/5-centimetri-al-secondo-2007/";
			while (true) {
				CineblogMovieProvider prov = new CineblogMovieProvider ();				 
				try{
					var tmpmovies = db.Table<Movie>();
					urlRestart = tmpmovies.ElementAt(tmpmovies.Count() - 1).Url;
					prov.RestartFrom (urlRestart);
				}catch(Exception e){					 					 
					continue;
				}


				break;
			}
		}

		public static void RedoAll()
		{
			if (clear) {
				db.DeleteAll<MovieFile> ();
				db.DeleteAll<Movie> ();
			}
			List<BaseMovieProvider> providers = new List<BaseMovieProvider> ();
			providers.Add (new CineblogMovieProvider ());
			//providers.Add (new ItaliaFilmMovieProvider ());


			List<Movie> movies = new List<Movie> ();

			foreach (BaseMovieProvider provider in providers) {
				movies.AddRange (provider.FindMovies ());
			}

			Console.WriteLine ("The End");
		}
	}

}

