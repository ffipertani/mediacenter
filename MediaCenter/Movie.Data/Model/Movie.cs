﻿using System;
using System.Collections.Generic;
using SQLite;
using CreateCatalog;

namespace MovieProvider.Model
{
	public class Movie
	{
		[PrimaryKey, AutoIncrement, Column("_id")]
		public int Id{ get; set; }	
		public String Title{ get; set; }
		public String Year{ get; set; }
		public String Overview{ get; set; }
		public String PosterUrl{ get; set; }
		public String Info{ get; set; }
		public String Genre{ get; set; }
		public String Duration{ get; set; }
		public String Country{ get; set; }
		public String Url{get;set;}
		public String Imdb{ get; set; }
	 
		[Ignore]
		public List<MovieFile> MovieFiles{ get; set; }

	}
}

