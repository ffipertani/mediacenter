﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviePlayer
{
    public partial class XamlBrowserPage
    {
        public XamlBrowserPage(string xaml)
        {
            InitializeComponent();
            label.Text = xaml;
        }
    }
}
