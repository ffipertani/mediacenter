﻿using System;
using Xamarin.Forms;

namespace MoviePlayer
{
	public class App
	{
		public static Page GetMainPage ()
		{	
			return new NavigationPage(new HomePage());
			/*

			return new ContentPage { 
				Content = new Label {
					Text = "Hello, Forms !",
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
				},
			};
			*/
		}
	}
}

