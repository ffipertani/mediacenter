﻿using System;

namespace  Movie.Data
{
	public class MovieFile
	{
		public MovieFile ()
		{
		}

		public int MovieId{get;set;}
		public String ProviderUrl{ get; set; }
		public String Provider{ get; set; }
		public String FileUrl{get;set;}
		public String Message{ get; set; }
	}
}

