﻿using System;
using SQLite.Net;
using System.Collections.Generic;

namespace Movie.Data
{
	public class MovieManager
	{
		private SQLiteConnection db;
		private int pageSize = 20;
		private String lastSearch ="";

		public MovieManager (SQLiteConnection db)
		{
			this.db = db;
		}


		public void Search(){
			Search("");
		}

		public void Search(String text){

			Search(text,0,pageSize);
		}

		public void Search (String text,int fromIndex, int count)
		{
			//IEnumerator<Movie> enumerator = db.Query<Movie> ("select * from Movie where upper(Title) LIKE upper(?) or upper(Overview)  LIKE upper(?)and (select count(MovieId) from MovieFile where Message = 'OK'  and MovieId = _id  group by MovieId)>0 order by Year desc LIMIT "+count+" OFFSET "+fromIndex, new object[] {
			IEnumerator<Movie> enumerator = db.Query<Movie> ("select * from Movie, MovieFile where Movie._id = MovieFile.MovieId and (upper(Title) LIKE upper(?) or upper(Overview)  LIKE upper(?) ) and Moviefile.message = 'OK' and MovieFile.fileUrl is not null order by Movie.Year desc LIMIT "+count+" OFFSET "+fromIndex, new object[] {
				"%"+text+"%",
				"%"+text+"%"
			}).GetEnumerator ();


			List<Movie> movies = new List<Movie> ();
			while (enumerator.MoveNext ()) {
				Movie movie = enumerator.Current;
				IEnumerator<MovieFile> enumeratorFile = db.Query<MovieFile> ("select * from MovieFile where MovieId = ? and Moviefile.message = 'OK' and MovieFile.fileUrl is not null", new object[] {
					movie.Id					 
				}).GetEnumerator ();
				movie.MovieFiles = new List<MovieFile> ();
				while (enumeratorFile.MoveNext ()) {
					movie.MovieFiles.Add (enumeratorFile.Current);
				}
				movies.Add (movie);

			}	

			lastSearch = text;
		}

	}
}

